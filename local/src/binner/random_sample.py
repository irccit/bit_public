#!/usr/bin/env python
# encoding: utf-8

from optparse import OptionParser
from random import randint
from sys import exit

def main():
	parser = OptionParser(usage='%prog SAMPLE_RANGE SAMPLE_SIZE')
	options, args = parser.parse_args()

	if len(args) != 2:
		exit('Unexpected argument number.')
	
	try:
		sample_range = int(args[0])
		if sample_range <= 1:
			raise ValueError
	except ValueError:
		exit('Invalid value for sample range.')
	
	try:
		sample_size = int(args[1])
		if sample_size <= 0:
			raise ValueError
	except ValueError:
		exit('Invalid value for sample size.')
	
	for i in xrange(sample_size):
		print randint(0, sample_range)

if __name__ == '__main__':
	main()
