#!/usr/bin/env python
#
# Copyright 2011 Paolo Martini <paolo.cavei@gmail.com>
# Copyright 2011 Gabriele Sales <gbrsales@gmail.com>

from ftplib import all_errors, error_reply, FTP
from optparse import OptionParser
from urlparse import urlparse, urlunparse
from vfork.util import exit, format_usage, ignore_broken_pipe
from re import match


def main():
    parser = OptionParser(usage=format_usage('''
            %prog FTP

            Retrive the list of file or directories from the ftp dir.
    '''))
    parser.add_option('-r', '--recursive', dest='recursive', action='store_true', default=False, help='Recursively retrieve the list of files from FTP.')
    parser.add_option('-f', '--only-files', dest='files', action='store_true', default=False, help='Retrive only the list of files.')
    parser.add_option('-u', '--full-url', dest='full', action='store_true', default=False, help='Print the full URL of each result.')
    options, args = parser.parse_args()

    if len(args) != 1:
        exit('Unexpected argument number.')

    url = urlparse(args[0])
    if url.scheme != 'ftp':
        exit('Invalid FTP url: ' + args[0])

    ftp = connect(url)
    scanner = FTPScanner(ftp, url.path, options.recursive, options.files)

    if options.full:
        scanner.scan(print_full(url))
    else:
        scanner.scan(print_path)

def connect(url):
    try:
        ftp = FTP(url.netloc)
    except all_errors:
        exit(url.netloc + ': not a valid host.')

    try:
        ftp.login()
    except all_errors:
        exit(url.netloc + ' requires a password.')

    return ftp

class FTPScanner(object):
    def __init__(self, ftp, root, recursive, files_only):
        self.ftp = ftp
        self.root = root if len(root) else '/'
        self.recursive = recursive
        self.files_only = files_only

        if not self._chdir(root):
            exit(root + ': not such directory.')

    def scan(self, visitor):
        for item in self.ftp.nlst():
            if item in ['.', '..']:
                continue

            is_dir = self._chdir(item)
            if not is_dir:
                visitor(self.path + '/' + item)

            else:
                if not self.files_only:
                    visitor(self.path)

                if self.recursive:
                    self.scan(visitor)

                self._dir_up()

    def _chdir(self, dirname):
        try:
            self.ftp.cwd(dirname)

            if dirname[0] == '/':
                self.path = dirname
            else:
                self.path += '/' + dirname

            return True

        except all_errors:
            return False

    def _dir_up(self):
        try:
            self.ftp.cwd('..')
            self.path = self.path[:self.path.rfind('/')]
        except all_errors:
            exit('Cannot chdir to the parent directory.')

def print_full(url):
    url = list(url)

    def worker(path):
        url[2] = path
        print urlunparse(url)

    return worker

def print_path(path):
    print path


if __name__== '__main__':
    ignore_broken_pipe(main)
