#!/usr/bin/perl

use warnings;
use strict;
$,="\t";
$\="\n";

my $usage="$0 file_1 file_2

format of input file:
chr	b	e	chr	b	e	id 	any 	other 	column
";

my $filename1=shift;
my $filename2=shift;

die($usage) if $filename1 eq '-h';

my $sh = "(intersection $filename1 $filename2 | cut -f 11,15; intersection <(sort -k 4,4 -k 5,5n $filename1):3:4:5 <(sort -k 4,4 -k 5,5n $filename2):3:4:5 | cut -f 11,15) | sort | uniq -c;";

open PIPE, "bash -c '$sh' |" or die("Can't open PIPE");

for(<PIPE>){
	my ($count, $id1, $id2) = split;
	print $id1,$id2 if $count > 1;
}
