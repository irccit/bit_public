#!/usr/bin/perl
use strict;
use warnings;

$,="\t";
$\="\n";

my %first=();
my %offset=();

open FIRST,shift(@ARGV) or die("Can't open file");

my @first;
while(<FIRST>){
	chomp;
	next if m/^>/;
	my @tmp=split;
	push @first, \@tmp;
}

while(<>){
	chomp;
	next if m/^>/;
	
	my ($id, $b, $e) = split;
	for(@first){
		my ($s_id,$s_b,$s_e) = @{$_};
		next if $id ne $s_id;
		next if $b >= $s_e;
		next if $e <= $s_b;
		print 	$id,
			$b > $s_b ? $b : $s_b,
			$e < $s_e ? $e : $s_e,
			$s_b,
			$s_e,
			$b,
			$e;
	}
}
