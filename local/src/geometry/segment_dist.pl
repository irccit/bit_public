#!/usr/bin/perl

use warnings;
use strict;

$\="\n";

my $usage="$0 CHR_COL BEGIN_COL END_COL FILE";

my $c=shift;
$c =~ /\d+/ or die($usage);
$c--;

my $b=shift;
$b =~ /\d+/ or die ($usage);
$b--;

my $e=shift;
$e =~ /\d+/ or die ($usage);
$e--;


my $prestop=undef;
my $prechr=undef;
while(<>){
	my @F=split;
	print $F[$b] - $prestop if defined($prestop) and $prechr eq $F[$c];
	$prechr=$F[$c];
	$prestop=$F[$e];
	$prechr=$F[$c];
}
