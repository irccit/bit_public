#!/usr/bin/env python

from os import system
from os.path import join
from tempfile import mkdtemp
from random import choice, seed, randint
from shutil import rmtree

def gen_chr(chr_name, i1, i2, o):
	start = 0
	while start < 0xffff:
		start += randint(0, 10)
		stop = start + randint(1, 10)
		
		tp = choice(('left', 'right', 'intersection'))
		if tp == 'left':
			print >>i1, '\t'.join((chr_name, str(start), str(stop)))
			start = stop + 10
		
		elif tp == 'right':
			print >>i2, '\t'.join((chr_name, str(start), str(stop)))
			start = stop + 10
		
		else:
			left_start = start - randint(0, 10)
			right_stop = stop + randint(0, 10)
			
			print >>i1, '\t'.join((chr_name, str(left_start), str(stop)))
			print >>i2, '\t'.join((chr_name, str(start), str(right_stop)))
			print >>o, '\t'.join((chr_name, str(start), str(stop), str(left_start), str(stop), str(start), str(right_stop)))
			
			start = right_stop + 10

def main():
	work_dir = mkdtemp()
	i1_path = join(work_dir, 'i1')
	i2_path = join(work_dir, 'i2')
	o1_path = join(work_dir, 'o1')
	i1 = i2 = o1 = None
	
	try:
		i1 = file(i1_path, 'w')
		i2 = file(i2_path, 'w')
		o1 = file(o1_path, 'w')
		
		seed()
		gen_chr('chr1', i1, i2, o1)
		
		i1.close()
		i2.close()
		o1.close()
		i1 = None
		i2 = None
		o1 = None
		
		o2_path = join(work_dir, 'o2')
		if system('./intersection.py %s %s >%s' % (i1_path, i2_path, o2_path)) != 0:
			raise RuntimeError, 'intersection.py exited with an error'
		else:
			if system('diff -u %s %s' % (o1_path, o2_path)) == 0:
				print 'Test OK'
			else:
				print 'Test FAILED'
	
	finally:
		if i1 is not None:
			i1.close()
		if i2 is not None:
			i2.close()
		if o1 is not None:
			o1.close()
		rmtree(work_dir)

if __name__ == '__main__':
	main()
