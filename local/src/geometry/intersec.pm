sub intersec{
	(my $s1, my $e1, my $s2, my $e2) = @_;
	my $max_s = $s1 > $s2 ? $s1 : $s2;
	my $min_e = $e1 < $e2 ? $e1 : $e2;
	if($max_s <= $min_e){
		return ($max_s,$min_e);
	}else{
		print STDERR "WARNING: empty intersection on ",join ("\t",@_);
		return (0,0);
	}
}

1
