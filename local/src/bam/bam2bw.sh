#!/bin/bash
# Argument = -t test -r server -p password -v
set -e

usage()
{
cat << EOF
usage: $0 [-n] input.bam chromSizes

This script convert a bam file in a bigWig file

OPTIONS:
   -h           Show this message

   -n           normalize per million reads, the scale factor 
                is just the totalNumReads/1,000,000 and do not 
                take into account sequence size in any way

   -m NORM      do not compute the normalization param from the input bam file
                but take NORM instead. NORM is usually the number of read in millions

   -f CUT       put to to 0 regions with less than CUT overlapping bam

   -g PATTERN	pass to grep the PATTERN ad apply the filter on each rows of the input
		e.g.:
			-g "-v chrM" remove the read mapped on chrM
			-g "chr"     keep only read that aling on chromesomes whose name start with "chr"
EOF
}

NORMALIZE=
NORMALIZE_PARAM=
FILTER=-1
GREP=""
IBAM="-ibam"
while getopts "hbnm:f:g:" OPTION
do
     case $OPTION in
         h)
             usage
             exit 1
             ;;
         b)
	     IBAM=""
             ;;
         n)
             NORMALIZE=1
             ;;
         m)
             NORMALIZE_PARAM=$OPTARG
             ;;
         g)
             GREP="grep $OPTARG"
             ;;
         f)
             FILTER=$OPTARG
             ;;
         ?)
             usage
             exit
             ;;
     esac
done

shift $(($OPTIND - 1))
if [ -z $2 ]
then
        echo "ERROR $0: Wrong argument number" >&2
        usage
        exit 1
fi

SCALE=
if [[ $NORMALIZE -eq 1 ]]
then
	if [[ $NORMALIZE_PARAM == "" ]]
	then
		if [[ $IBAM == "" ]]
		then
			echo "ERROR: -n requires bam input or -m NORM" >&2
			exit 1
		fi

		if [ ! -e $1.bai ]
		then
			echo "WARING $0: producing $1.bai" >&2
			samtools index $1
		fi
		SCALE="-scale `samtools idxstats $1 | awk 'BEGIN{tot=0}{tot+=$3} END{print tot/1000000}'`"
	else
		SCALE="-scale $NORMALIZE_PARAM"
	fi
fi;

filter=""
if [[ $FILTER -gt 0 ]]
then
	filter="| awk '\$4>=$FILTER'"
fi

if [ -n "$GREP" ]
then
	filter="$filter | $GREP"
fi


tmpfile=$(basename `mktemp`)
cmd="genomeCoverageBed $SCALE -bga -split $IBAM $1 -g $2 $filter > $tmpfile"
bash -c "$cmd"
bash -c "bedGraphToBigWig $tmpfile $2 /dev/stdout"
rm $tmpfile
