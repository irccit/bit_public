#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;

$,="\t";
$\="\n";

my $usage = "cat file | $0 -c 2 -p 5
	-c column_data
	-p percentage\n";


my $col = undef;
my $percentage = undef;

GetOptions(
	'column_data|c=i' => \$col,
	'percentage|p=i'  => \$percentage
);

die ($usage) if (!defined $col or !defined $percentage);
die "ERROR: -p percentage must be an integer between 1 and 99" if ( ($percentage > 99) or ($percentage < 1) );
$col--;

my @data = ();
while (<>) {
	chomp;
	my @F = split /\t/,$_;
	
	push @data,$F[$col];
}


my @sorted_data = sort {$a <=> $b} @data;
my $n = scalar @sorted_data;
my $idx = int ( ($n * (100 - $percentage)) / 100) - 1;

die "ERROR: not defined \$sorted_data[$idx]" if !defined $sorted_data[$idx];
print $sorted_data[$idx];
