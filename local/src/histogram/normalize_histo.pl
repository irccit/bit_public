#!/usr/bin/perl

use warnings;
use strict;

$,="\t";
$\="\n";

my $usage = "cat qualcosa.binner | $0";

die($usage) if @ARGV and $ARGV[0] eq '-h';

my @X=();
my @Y=();
my $sum = 0;

while(<>){
	chomp;
	my ($x, $y) = split;
	$sum += $y;
	push @X, $x;
	push @Y, $y;
}

for(my $i=0; $i < scalar(@X); $i++){
	print $X[$i], $Y[$i]/$sum;
}
