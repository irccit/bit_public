#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;

my $normalize=0;

GetOptions (
	'normalize|n' => \$normalize,
);

$,="\t";
$\="\n";

my $sum=0;

my @val=();

while(<>){
	my @F=split;
	$sum+=$F[1];
	$F[1]=$sum;
	if(!$normalize){
		print @F;
	}else{
		push @val,\@F;
	}
}
if($normalize){
	for(@val){
		print ${$_}[0],${$_}[1]/$sum;
	}
}
