#!/usr/bin/env python
#
# Copyright 2012 Michele Vidotto <michele.vidotto@gmail.com>
# Copyright 2012 Gabriele Sales <gbrsales@gmail.com>
#
# This file is part of BioinfoTree. It is licensed under the
# terms of the GNU Affero General Public License version 3.


from argparse import ArgumentParser, RawDescriptionHelpFormatter
from sys import stdin
from vfork.fasta.reader import MultipleBlockStreamingReader, FormatError
from vfork.util import exit, format_usage, ignore_broken_pipe


def main():
    parser = ArgumentParser(formatter_class=RawDescriptionHelpFormatter,
                            description=format_usage('''
      Converts a QUALITY file into a TSV file with two columns:
      1) label
      2) sequence
    '''))
    parser.add_argument("--allow-empty", "-e", dest="allow_empty", default=False, action="store_true", help="allow empty sequences")
    parser.add_argument("--multi-line", "-m", dest="multi", default=False, action="store_true", help="keep sequences split over multiple lines, each one prefixed by the sequence label")
    args = parser.parse_args()

    try:
        for label, seq in MultipleBlockStreamingReader(stdin, join_lines=False):
            seq = list(seq)

            if len(seq) == 0:
                if not args.allow_empty:
                    exit('Empty QUALITY sequence in "%s".' % label)
                else:
                    print '%s\t' % label

            elif args.multi:
                for s in seq:
                    check_qualities(s, label)
                    print '%s\t%s' % (label, s)
            else:
                seq = ' '.join(seq)
                check_qualities(seq, label)
                print '%s\t%s' % (label, seq)

    except FormatError, e:
        exit('Malformed QUALITY input: ' + e.args[0])

def check_qualities(seq, label):
    for word in seq.split():
        parse_qual(word, label)

def parse_qual(word, label):
    try:
        value = int(word)
    except ValueError:
        raise FormatError('invalid quality "%s" in "%s".' % (word, label))

    if value < -5:
        raise FormatError('invalid negative quality score "%s" in "%s".' % (value, label))

    return value


if __name__ == '__main__':
    ignore_broken_pipe(main)
