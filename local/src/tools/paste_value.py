#!/usr/bin/env python
#
# Copyright 2010 Gabriele Sales <gbrsales@gmail.com>

from optparse import OptionParser
from os.path import basename
from sys import argv, stdin
from vfork.io.util import parse_int, safe_rstrip
from vfork.util import exit, format_usage, ignore_broken_pipe


def parse_args():
    name = basename(argv[0])
    if name == 'append_each_row':
        parser = append_option_parser()
    else:
        parser = standard_option_parser()

    parser.add_option('-F', '--field-separator', dest='separator', default='\t', help='field separator (default: \\t)')
    options, args = parser.parse_args()

    try:
        if name == 'append_each_row':
            if len(args) != 1:
                raise ValueError
            elif not options.before:
                args.append('-1')
        else:
            if len(args) not in (1,2):
                raise ValueError
    except ValueError:
        exit('Unexpected argument number.')

    return options, args
        

def standard_option_parser():
    parser = OptionParser(usage=format_usage('''
	Usage: %prog [OPTIONS] VALUE [COL]

	Insert VALUE in each line of input in column COL.

	If COL is missing, the VALUE is inserted in the first column.
	Negative values represent indexes from the last column (for example,
	-1 is the last column itself).
    '''))
    parser.add_option('-p', '--pad', dest='pad', action='store_true', default=False, help='add empty columns in order to reach COL')
    return parser

def append_option_parser():
    parser = OptionParser(usage=format_usage('''
	Usage: %prog [OPTIONS] VALUE

	Insert VALUE in the last column of each line.
    '''))
    parser.add_option('-B', '--before', dest='before', action='store_true', default=False, help='insert VALUE in the first column')
    return parser


def iter_lines(fd, separator):
    colnum = None
    for lineidx, line in enumerate(fd):
        tokens = safe_rstrip(line).split(separator)

        if colnum is not None and colnum != len(tokens):
            exit('Column number changed betwenn line %d and the following.' % lineidx)
        colnum = len(tokens)

        yield lineidx+1, tokens


def main():
    options, args = parse_args()

    value = args[0]
    if len(args) == 1:
        col = 1
    else:
        col = parse_int(args[1], 'COL')
        if col == 0: exit('Invalid COL: 0')
    
    for lineno, cols in iter_lines(stdin, options.separator):
        try:
            if col < 0:
                idx = len(cols) + col + 1
                if idx < 0:
                    raise ValueError
            else:
                idx = col - 1
                if len(cols) < idx:
                    if options.pad:
                        cols += [''] * (idx - len(cols))
                    else:
                        raise ValueError
        except ValueError:
            exit('Insufficient columns at line: %d.' % lineno)

        cols.insert(idx, value)
        print options.separator.join(cols)


if __name__ == '__main__':
    ignore_broken_pipe(main)
