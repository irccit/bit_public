#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\t";
$\="\n";

my $usage = "sort_pl [-a|alpha] COL < FILE";
my $help=0;
my $alpha=0;
GetOptions (
	'h|help' => \$help,
	'a|alpha' => \$alpha,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

my $col = shift @ARGV;
die($usage) if $col!~/^\d+$/ ;
$col--;

my @all=();
while(<>){
	chomp;
	my @F=split /\t/;
	push @all,\@F;
}

if(!$alpha){
	for(sort {${$a}[$col]<=>${$b}[$col]} @all){
		print @{$_};
	}
}else{
	for(sort {${$a}[$col] gt ${$b}[$col]} @all){
		print @{$_};
	}
}
