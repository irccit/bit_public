#!/usr/bin/env python
#
# Copyright 2009 Gabriele Sales <gbrsales@gmail.com>

from collections import deque
from optparse import OptionParser
from sys import stdin, stdout
from vfork.util import exit, format_usage

def main():
	parser = OptionParser(usage=format_usage('''
		%prog [OPTIONS] <IN >OUT
		
		Removes the last N lines of input.
	'''))
	parser.add_option('-n', '--number', dest='num', type='int', default=1, help='number of rows to remove (default: 1)')
	options, args = parser.parse_args()

	if len(args) != 0:
		exit('Unexpected argument number.')
	elif options.num < 1:
		exit('Invalid row number: %d' % options.num)
	
	records = deque()
	n = int(options.num)
	for line in stdin:
		records.append(line)
		if len(records) > n:
			stdout.write(records.popleft())

if __name__ == '__main__':
	main()
