#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
use Scalar::Util 'looks_like_number'; 
$,="\t";
$\="\n";

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 [-h|help] [-p PRECISION]\n";

my $help=0;
my $precision=2;
GetOptions (
	'h|help' => \$help,
	'p|precision=i' => \$precision,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

while(<>){
	chomp;
	my @F = split /\t/,$_,-1;
	@F=map{looks_like_number($_) ? sprintf("%.$precision".'g',$_) : $_} @F;
	print @F
}

