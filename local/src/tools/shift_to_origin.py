#!/usr/bin/env python
from optparse import OptionParser
from sys import exit, stdin
from vfork.io.util import safe_rstrip

def parse_cols(cols):
	values = []
	for col, descr in zip(cols, ('chromosome', 'start', 'stop')):
		try:
			v = int(col) - 1
			if v < 0:
				raise ValueError
		except ValueError:
			exit('Invalid index for %s column.' % descr)
		else:
			values.append(v)
	
	return values

def iter_coords(fd, chr_col, start_col, stop_col):
	last_chr = None
	for idx, line in enumerate(fd):
		tokens = safe_rstrip(line).split('\t')
		if len(tokens) <= max(chr_col, start_col, stop_col):
			exit('Insufficient token number at line %d.' % (idx+1,))

		if last_chr is None:
			last_chr = tokens[chr_col]
		elif last_chr != tokens[chr_col]:
			exit('Chromosome changed at line %d.' % (idx+1,))

		try:
			start = int(tokens[start_col])
			if start < 0:
				raise ValueError
		except ValueError:
			exit('Invalid start coordinate at line %d.' % (idx+1,))

		try:
			stop = int(tokens[stop_col])
			if stop <= start:
				raise ValueError
		except ValueError:
			exit('Invalid stop coordinate at line %d.' % (idx+1,))

		yield tokens, start, stop

def main():
	parser = OptionParser(usage='%prog CHR_COL START_COL STOP_COL <COORDS')
	options, args = parser.parse_args()

	if len(args) != 3:
		exit('Unexpected argument number.')
	
	chr_col, start_col, stop_col = parse_cols(args)

	records = list(iter_coords(stdin, chr_col, start_col, stop_col))
	min_start = min(r[1] for r in records)
	for tokens, start, stop in records:
		tokens[start_col] = start - min_start
		tokens[stop_col] = stop - min_start
		print '\t'.join(str(s) for s in tokens)

if __name__ == '__main__':
	main()

