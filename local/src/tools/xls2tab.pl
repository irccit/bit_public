#!/usr/bin/perl

#~ Converter Script for XLS to TSV.  Handles Multiple Tabs.
#~ (c)2004 Anima Legato <l3gatosan@gmail.com>
#~
#~ This code is redistributable and modifiable under the same terms as Perl
#~ itself.

use strict;
use warnings;
use Getopt::Long;
use Spreadsheet::ParseExcel;
use File::Spec;
$,="\t";
$\="\n";

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 [-h|help] FILE.xls\n
	Convert the file given as parameter (not STDIN) in tab delimited.
	Each sheet is preceded by a fasta header like
	>sheet1

	DEPENDENCIES:
		libspreadsheet-parseexcel-perl
";

my $help=0;
GetOptions (
	'h|help' => \$help,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

binmode STDOUT, ":encoding(UTF-8)";

my ($vol, $path, $file) = File::Spec->splitpath(shift);
my $oBook = Spreadsheet::ParseExcel::Workbook->Parse(File::Spec->catpath($vol,$path,$file));
unless (defined $oBook) {
    warn "Can't open Spreadsheet in file $file (@".File::Spec->catpath($vol,$path,$file)."\n";
    return undef;
}
    
my @sheet = @{$oBook->{Worksheet}};
my $i=1;
for (0..@sheet-1) {
    print ">".$sheet[$_]->{Name};

    my ($rStart, $rStop) = $sheet[$_]->RowRange();
    my ($cStart, $cStop) = $sheet[$_]->ColRange();
    foreach my $y ($rStart..$rStop) {
	    my @line;
	    foreach my $x ($cStart..$cStop) {
		    my $c = $sheet[$_]->Cell($y, $x);
		    my $v = "";
		    if (defined($c)) {
			$v = $c->Value;
		    }
#		    $v =~ s/[^[:ascii:]]+//g;  # get rid of non-ASCII characters
		    push(@line, $v);
            }

	    print @line;
    }
    $i++;
}
