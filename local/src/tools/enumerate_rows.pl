#!/usr/bin/perl
use warnings;
use strict;
$,="\t";
$\="\n";
use Getopt::Long;
$SIG{__WARN__} = sub {die @_};

my $right=0;
my $shift=0;
my $normalize=0;
my $quantile=0;

my $help=0;
my $usage="$0 [-r|right] [-s|shift 10] [-n|normalize]";

GetOptions (
	'h|help' => \$help,
	'r|right' => \$right,
	's=i' => \$shift,
	'n|normalize' => \$normalize,
	'q|quantile=i' => \$quantile,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

die("-q require -n")  if $quantile and not $normalize;

if(!$normalize){
	while(<>){
		chomp;
		my $n = $.;
		#$n = $. + 1;
		$n += $shift;
		if(!$right){
			print $n,$_;
		}else{
			print $_,$n;
		}
	}
}else{
	my @lines = <>;
	my $N = scalar(@lines);
	if($quantile){
		$quantile=$N/$quantile
	}
	$N=$N-1; #-1 in order to have the topmost row numbered as 1 and not someting like 0.9999
	$N=1 if $N==0;
	$N+=$shift;
	my $i=$shift;
	for(@lines){
		chomp;
		my $n;
		if($quantile){
			$n = int($i/$quantile)
		}else{
			$n = $i/$N;
		}
		#$n = $. + 1;
		if(!$right){
			print $n,$_;
		}else{
			print $_,$n;
		}
		$i++;
	}
}
