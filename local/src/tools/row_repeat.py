#!/usr/bin/env python
#
# Copyright 2012 Paolo Martini <paolo.cavei@gmail.com>

from optparse import OptionParser
from sys import stdin
from vfork.io.util import parse_int, safe_rstrip
from vfork.util import exit, format_usage, ignore_broken_pipe


def main():
    parser = OptionParser(usage=format_usage('''
            %prog [OPTIONS] N <ROW(S)

            Repeats each row in input N times.
    '''))
    options, args = parser.parse_args()
    if len(args) != 1:
        exit("Unexpected argument number.")

    n = parse_int(args[0], "N", "strict_positive")

    for line in stdin:
        for i in xrange(n):
            print safe_rstrip(line)


if __name__=='__main__':
    ignore_broken_pipe(main)
