#!/usr/bin/perl
use warnings;
use strict;

my $seq='';

while(<>){
	chomp;
	$seq.=$_;
}
$seq = reverse $seq;
$seq =~ tr/ACGT/TGCA/;
$seq =~ tr/acgt/tgca/;
print STDERR "[revcompl] WARNING: input contain someting different than acgtACGTN" if $seq=~/[^acgtACGTN]/;
print $seq."\n";
