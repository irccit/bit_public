#!/usr/bin/perl

use warnings;
use strict;
$,="\t";
$\="\n";
use Getopt::Long;

$SIG{__WARN__} = sub {CORE::die @_};

my $usage="$0 [-a|-b -l] [-g GLUE] [-m MAP_FILE] COL1 COL2 ... COLn < file
	convert the values in the COLs in integers,
	the 1 to 1 map is saved in file MAP_FILE

	in -a or -b mode the program as a different beaviour:
	-a disambiguate repeated values in the cols adding a count 
	-b disambiguate repeated values in the cols adding a count but do not add a count to the first value encountered
	
	-l add the count to the left instead to right
";


my $help=0;
my $disambig=0;
my $append=0;
my $disambig_but_first=0;
my $glue = "_";
my $map_file = undef;
my $left=0;

&GetOptions (
	'h|help' => \$help,
	"disambig|a" => \$disambig,
	"disambig_but_first|b" => \$disambig_but_first,
	"append|c" => \$append,
	"glue|g=s" => \$glue,
	"map_file|m=s" => \$map_file,
	"l|left" => \$left
) or die($usage);

$disambig=1 if $disambig_but_first;

if($help){
	print $usage;
	exit(0);
}

my @cols = map{
	die("column number not valid ($_)") if !m/^\d+$/;
	$_ - 1;
} @ARGV;

@ARGV=();

my %a=();
while(<>){
	chomp;
	my @F=split /\t/;
	foreach my $col (@cols){
		if(!$disambig){
			$a{$F[$col]} = scalar(keys(%a)) if( ! defined($a{$F[$col]}));
			if($append){
				$F[$col] = $F[$col].="\t".$a{$F[$col]};
			}else{
				$F[$col] = $a{$F[$col]};
			}
		}else{
			$a{$F[$col]}++;
			if(!$disambig_but_first){
				if($left){
					$F[$col]  = $a{$F[$col]} . $glue . $F[$col]
				}else{
					$F[$col] .= $glue . $a{$F[$col]};
				}
			}else{
				if($a{$F[$col]}>=2){
					if($left){
						$F[$col]  = ($a{$F[$col]} - 1) . $glue . $F[$col];
					}else{
						$F[$col] .= $glue . ($a{$F[$col]} - 1);	
					}
				}
			}
		}
	}
	print @F;
}

if(defined $map_file){
	open FH, ">$map_file" or die("Can't write on file ($map_file)");
	for(keys %a){
		print FH $_,$a{$_};
	}
}
