# Copyright 2012,2014 Gabriele Sales <gbrsales@gmail.com>
#
# This file is part of BioinfoTree. It is licensed under the
# terms of the GNU Affero General Public License version 3.

from database import PackageDb
from link import resolve_link
from os import environ
from os.path import abspath, basename, isdir, join
from subprocess import CalledProcessError, call, check_output
from sys import exit


class BioinfoTree(object):
    def __init__(self, is_external, ghc_options):
        self._setup_env()
        self.pkg_db = PackageDb(join(self.root, 'binary', self.host))
        self.git_version = None
        self.is_external = is_external
        self.ghc_options = ghc_options

    def binary_dir(self):
        return join(self.root, 'binary', self.host)

    def is_outside(self, path):
        return not path == self.root and not path.startswith(self.root + '/')

    def install_path(self, path, use_stow):
        if use_stow:
            suffix = join('local', 'stow', basename(path))
        else:
            suffix = 'local'

        if self.is_external:
            path = ''
        else:
            low = len(self.root) + 1
            high = path.index('/local/src/')
            path = path[low:high]

        return join(self.binary_dir(), path, suffix)

    def list_pkgs(self):
        for name, _, version in self.pkg_db.list_all():
            yield name, version

    def register_pkg(self, path, use_stow):
        if not self.is_external:
            relative_path = path[len(self.root)+1:]

            if self.git_version is None:
                self._load_git_version()

            self.pkg_db.register(relative_path, use_stow, self.git_version)

    def outdated_pkgs(self):
        for path, use_stow, version in self.pkg_db.list_all():
            abs_path = resolve_link(join(self.root, path))
            if self.is_outside(abs_path) or not isdir(abs_path):
                continue

            if self._locate_commit(version) != 'this_branch' or self._dir_changed_since(path, version):
                yield abs_path, use_stow

    def clear_pkgs(self):
        self.pkg_db.clear()

    def forget_pkg(self, path):
        return self.pkg_db.forget(path)

    def _setup_env(self):
        try:
            root = environ['BIOINFO_ROOT']
            if len(root) == 0: raise KeyError
        except KeyError:
            exit('BIOINFO_ROOT is not defined or empty.')

        root = abspath(root)
        if not isdir(root):
            exit('BIOINFO_ROOT does not point to a valid directory.')

        try:
            host = environ['BIOINFO_HOST']
            if len(host) == 0: raise KeyError
        except KeyError:
            exit('BIOINFO_HOST is not defined.')

        self.root = root
        self.host = host

    def _load_git_version(self):
        try:
            self.git_version = check_output(['git', 'log', '-n1', '--format=%H'], cwd=self.root).strip()
        except CalledProcessError:
            exit('[binary_install] Error running git.')

    def _locate_commit(self, version):
        try:
            out = check_output(['git', 'rev-list', 'HEAD..' + version])
        except CalledProcessError:
            return None

        if len(out):
            return 'other_branch'
        else:
            return 'this_branch'

    def _dir_changed_since(self, path, version):
        try:
            out = check_output(['git', 'log', '--format=format:', '--name-only', version + '..'])
        except CalledProcessError:
            exit('[bit_install] Error running git.')

        expected = path + '/'
        for line in out.split('\n'):
            if line.startswith(expected):
                return True

        return False
