# Copyright 2012,2015 Gabriele Sales <gbrsales@gmail.com>
#
# This file is part of BioinfoTree. It is licensed under the
# terms of the GNU Affero General Public License version 3.

from commands import getstatusoutput
from fnmatch import fnmatch
from os import chdir, environ, listdir, makedirs
from os.path import basename, dirname, isfile, join
from shutil import copy, rmtree
from subprocess import call
from sys import stderr
from tempfile import mkdtemp
import errno
import re


def init_package(path, filenames, bit):
    for k in (CabalPackage, PythonPackage, RPackage, AutotoolsPackage, MakefilePackage):
        pkg = k.from_dir(path, filenames, bit)
        if pkg is not None:
            return pkg
    return None


class Package(object):
    def __init__(self, path, bit):
        self.path = path
        self.bit = bit

    @staticmethod
    def match(pattern, filenames):
        for fname in filenames:
            if fnmatch(fname, pattern):
                return fname
        return None

    def build(self, force, use_stow):
        try:
            chdir(self.path)
            self._build(force, use_stow)
            self.bit.register_pkg(self.path, use_stow)
            return True
        except BuildError:
            print >>stderr, '[binary_install] error in:', self.path
            return False

    def stow(self, install_dir):
        if call(['stow', '-R', '-d', dirname(install_dir), basename(install_dir)]) != 0:
            raise BuildError

class BuildError(Exception):
    pass


class AutotoolsPackage(Package):
    @classmethod
    def from_dir(klass, path, filenames, bit):
        if klass.match('configure', filenames) is not None or \
           klass.match('configure.ac', filenames) is not None:
            return klass(path, bit)
        else:
            return None

    def _build(self, force, use_stow):
        if not isfile('configure'):
            if call(['autoreconf', '-fvi']) != 0:
                raise BuildError

        if force and 'makefile' in [ f.lower() for f in listdir(self.path) ]:
            if call(['make', 'distclean']) != 0:
                raise BuildError

        install_dir = self.bit.install_path(self.path, use_stow)
        if call(['./configure', '--prefix', install_dir]) != 0:
            raise BuildError

        if call(['make']) != 0:
            raise BuildError

        if call(['make', 'install']) != 0:
            raise BuildError

        if use_stow:
            self.stow(install_dir)


class CabalPackage(Package):
    @classmethod
    def from_dir(klass, path, filenames, bit):
        m = klass.match('*.cabal', filenames)
        if m is not None and klass._is_executable(join(path, m)):
            pkg = klass(path, bit)
            pkg.cabal = m
        else:
            pkg = None
        return pkg

    @staticmethod
    def _is_executable(path):
        with file(path) as cabal:
            for line in cabal:
                if line.lower().startswith('executable '):
                    return True
        return False

    def _build(self, force, use_stow):
        if force:
            rmtree(join(self.path, 'dist'))

        if call(['cabal', 'configure']):
            raise BuildError

        cmd = ['cabal', 'build']
        if self.bit.ghc_options is not None:
            cmd += ['--ghc-options', self.bit.ghc_options]
        if call(cmd):
            raise BuildError

        install_dir = self.bit.install_path(self.path, use_stow)
        self._install(install_dir)

        if use_stow:
            self.stow(install_dir)

    def _install(self, install_dir):
        tmp = mkdtemp()
        try:
            if call(['cabal', 'copy', '--destdir=' + tmp]):
                raise BuildError

            st, cabal_dir = getstatusoutput("find '%s' -type d -name .cabal" % tmp)
            if st != 0:
                raise BuildError

            cabal_bin_dir = join(cabal_dir, 'bin')
            exe_paths = [ join(cabal_bin_dir, e) for e in listdir(cabal_bin_dir) ]
            if len(exe_paths) == 0:
                raise BuildError

            bin_dir = join(install_dir, 'bin')
            try:
                makedirs(bin_dir)
            except OSError, e:
                if e.errno != errno.EEXIST:
                    raise e

            for exe in exe_paths:
                copy(exe, bin_dir)

        finally:
            rmtree(tmp)


class PythonPackage(Package):
    @classmethod
    def from_dir(klass, path, filenames, bit):
        m = klass.match('setup.py', filenames)
        return klass(path, bit) if m is not None else None

    def _build(self, force, use_stow):
        if force:
            rmtree(join(self.path, 'build'))

        if call(['python', 'setup.py', 'build']) != 0:
            raise BuildError

        prefix = join(self.bit.binary_dir(), 'local')
        if call(['python', 'setup.py', 'install', '--prefix', prefix]) != 0:
            raise BuildError


class RPackage(Package):
    manifest   = 'DESCRIPTION'
    package_rx = re.compile('\s*Package:')

    @classmethod
    def from_dir(klass, path, filenames, bit):
        m = klass.match(klass.manifest, filenames)
        if m is None: return None
        return klass(path, bit) if klass._check_manifest(path) else None

    @classmethod
    def _check_manifest(klass, path):
        with file(join(path, klass.manifest)) as fd:
            for line in fd:
                if klass.package_rx.match(line) is not None:
                    return True
        return False

    def _build(self, force, use_stow):
        if call(['R', 'CMD', 'INSTALL', self.path]) != 0:
            raise BuildError

class MakefilePackage(Package):
    @classmethod
    def from_dir(klass, path, filenames, bit):
        m = klass.match('[mM]akefile', filenames)
        return klass(path, bit) if m is not None else None

    def _build(self, force, use_stow):
        if force:
            if call(['make', 'distclean']) != 0:
                raise BuildError

        install_dir = self.bit.install_path(self.path, use_stow)

        env = dict(environ)
        env['PREFIX'] = install_dir

        if call(['make', 'install'], env=env) != 0:
            raise BuildError

        if use_stow:
            self.stow(install_dir)
