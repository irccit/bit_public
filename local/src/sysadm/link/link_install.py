#!/usr/bin/env python2.7
#
# Copyright 2008,2012 Gabriele Sales <gbrsales@gmail.com>

from optparse import OptionParser
from vfork.path import link_relative
from vfork.util import exit, format_usage
import os
import errno


def main():
    parser = OptionParser(usage=format_usage('''
        %prog SRC [SRC2 SRC3 ...] DEST

        Create a relative symbolic link DEST pointing to SRC.
        If more than one SRC are indicated DEST must be a directory and then %prog works like ln: create a symlink for each source file in that directory.
    '''))
    parser.add_option('-f', '--force', dest='force', action='store_true', default=False, help='silently overwrite existing links')
    parser.add_option('-l', '--real_path', dest="real_path", action='store_true', default=False, help="if SRC is a symlink in turn, link the real path insthead")
    parser.add_option('-a', '--absolute', dest="absolute", action='store_true', default=False, help="create link using absolute path")
    options, args = parser.parse_args()
    if len(args) < 2:
        exit('Unexpected argument number.')


    if len(args) == 2:
        sources=[args[0],]
        dests=[args[1],]
        if os.path.exists(dests[0]) and os.path.isdir(dests[0]) and not os.path.isdir(sources[0]):
            dests[0]+="/"+os.path.basename(sources[0])
    else: #args>2
        dest_dir=args.pop()
        sources=args
        dests = ["%s/%s" % (dest_dir, os.path.basename(s) ) for s in sources]

    for s,d in zip(sources,dests):
        if options.real_path or options.absolute:
            s=os.path.realpath(s)
        try:
            if options.absolute:
                try:
                    os.symlink(s, d)
                except OSError, e:
                    if e.errno == errno.EEXIST and options.force:
                        os.remove(d)
                        os.symlink(s, d)
            else:
                link_relative(s, d, force=options.force)
        except IOError, e:
            if e.errno == errno.EEXIST:
                exit('Link already exists.')
            else:
                exit(str(e))


if __name__ == '__main__':
    main()
