#!/bin/bash
#
# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

function error {
	echo "[uninstall_ensembl_api] $1" >&2
	exit 1
}

function print_help {
	echo "Usage: $1 API_VERSION"
	echo
	echo "Uninstalls the required version of Ensembl APIs from the system."
	exit 1
}

if [ $# -ne 1 ]; then
	error "Unexpected argument number."
elif [ $1 == "-h" -o $1 == "--help" ]; then
	print_help "$0"
elif [ -z $BIOINFO_ROOT ]; then
	error "BIOINFO_ROOT is not defined."
elif [ -z $BIOINFO_HOST ]; then
	error "BIOINFO_HOST is not defined."
fi

install_dir_base="$BIOINFO_ROOT/binary/$BIOINFO_HOST/"
if [ ! -d "$install_dir_base" ]; then
	error "Missing directory: $install_dir_base"
fi

install_dir="$install_dir_base/local/lib/ensembl/$1"
if [ ! -d "$install_dir" ]; then
	error "Ensembl APIs version $1 are not installed on this system."
fi

work_dir="$(dirname "$install_dir")"
cd "$work_dir" || error "Cannot change directory to parent of: $work_dir"
rm -rf "$1" || error "Cannot remove directory $work_dir/$1"
