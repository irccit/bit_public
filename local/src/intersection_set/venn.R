#!/usr/bin/env Rscript
sink(stderr())
options(error = function() traceback(2))
suppressMessages(library("optparse"))
suppressMessages(library("limma"))

option_list <- list(
	#make_option(c("-c", "--coef"), action="store_true", default=FALSE, help="Print coefficients"),
        #make_option(c("-r", "--randomizations"), type="integer", default=10000, help="Numbewr of randomizations used to compute empirical pvalue [default \"%default\"]"),
        #make_option(c("-H", "--header"), action="store_false", default=TRUE, help="The input files do not have an header in the first row"),
        #make_option(c("-t", "--test"), action="store", default="pearson", dest="method", help="Compute correlation using this method [default \"%default\"]")
)
parser<-OptionParser(usage = "%prog [options]
.META: stdin
        a matrix with header
	genes on rows, sets in columns, 0 o 1 as values

.META: stdout
	pdf file of a venn diagram

", option_list=option_list)

arguments <- parse_args(parser, positional_arguments = 0)
opt<-arguments$options

stdin <- file("stdin")
open(stdin, blocking=TRUE) # http://stackoverflow.com/questions/9370609/piping-stdin-to-r
data<-read.table(stdin,h=T, row.names=1)
x<-data.matrix(data)
a<-vennCounts(x)
pdf("/dev/stdout") 
vennDiagram(a)
dev.off()
sink()
                
w=warnings()
sink(stderr())
if(!is.null(w)){
        print(w)
}
sink()

