#!/usr/bin/env python
from sys import exit, stderr, argv, maxint
from optparse import OptionParser
from subprocess import Popen, PIPE
from vfork.util import exit, format_usage
from random import shuffle
from operator import itemgetter
from collections import defaultdict
from math import log

class Hyper(object):
	def __init__(self, exename):
		self.exe = Popen(exename, stdin=PIPE, stdout=PIPE, close_fds=True, shell=False)
	
	def close(self):
		self.exe.stdin.close()
		self.exe.wait()
	
	def calc(self, N, M, n, x):
		print >>self.exe.stdin, '%d\t%d\t%d\t%d' % (N, M, n, x)
		self.exe.stdin.flush()
		
		tokens = self.exe.stdout.readline().split(None)
		if(len(tokens) == 5):
			return float(tokens[4])
		else:
			stderr.write("[WARNING] None p-value: %i\t%i\t%i\t%i\n" % (N, M, n, x))
			return None

def load_sets_fasta(path, min_set_size):
	sets = {}
	tmp_list = [] 
	set_name = None
	N = set()
	fd = file(path, 'r')
	for line in fd:
		if line[0] == '>':
			if set_name is not None:
				if min_set_size is None or len(tmp_list) >= min_set_size:
					sets[set_name]=set(tmp_list)
					N|=set(tmp_list)
				tmp_list=[]
			set_name = line.strip()
		else:
			if tmp_list is None:
				exit(1)
		
			tokens = line.rstrip().split("\t")
			assert len(tokens) == 1
			tmp_list.append(tokens[0])
	
	if len(tmp_list) > 0 and (min_set_size is None or len(tmp_list) >= min_set_size):
		sets[set_name]=set(tmp_list)
		N|=set(tmp_list)
	
	fd.close()
	return (N,sets)

def load_sets_single(path):
	sets = {}
	tmp_list = [] 
	set_name = None
	N = set()
	fd = file(path, 'r')
	for line in fd:
		tokens = line.rstrip().split("\t")
		assert len(tokens) == 1
		tmp_list.append(tokens[0])

	if len(tmp_list) > 0:
		sets[1]=set(tmp_list)
		N|=set(tmp_list)
	
	fd.close()
	return (N,sets)

def load_sets_family(path, min_set_size, max_set_size, universe, options):
	sets = {}
	tmp_list = [] 
	N = set()
	last_family_id = None
	fd = file(path, 'r')
	for line in fd:
		tokens = line.rstrip().split("\t")
		assert len(tokens) == 2, repr(tokens)
		if last_family_id is not None and last_family_id != tokens[0]:
			if tokens[0] < last_family_id:
				exit("Input not lexicograpically sorted on column 1")
			if len(tmp_list) > 0 and (min_set_size is None or len(tmp_list) >= min_set_size):
				if max_set_size is None or len(tmp_list) <= max_set_size:
					sets[last_family_id] =set(tmp_list)
					N|=set(tmp_list)
			tmp_list=[]
		last_family_id = tokens[0]	
		elem=tokens[1]
		if universe is not None:
			if elem not in universe:
				if options.ignore_not_in_universe:
					continue
				else:
					exit("Element in set not in universe")
		tmp_list.append(elem)
	
	if len(tmp_list) > 0 and (min_set_size is None or len(tmp_list) >= min_set_size):
		sets[last_family_id]=set(tmp_list)
		N|=set(tmp_list)
	
	fd.close()
	return (N, sets)

def load_sets_family_sorted(path, min_set_size, max_set_size):
	tmp_list = [] 
	last_family_id = None
	fd = file(path, 'r')
	for line in fd:
		tokens = line.rstrip().split("\t")
		assert len(tokens) == 2, repr(tokens)
		if last_family_id is not None and last_family_id != tokens[0]:
			if tokens[0] < last_family_id:
				exit("Input not lexicograpically sorted on column 1")
			if min_set_size is None or len(tmp_set) >= min_set_size:
				if max_set_size is None or len(tmp_set) <= max_set_size:
					yield tmp_set
			tmp_set=set()
		last_family_id = tokens[0]
		tmp_set.add(tokens[1])
	
	if len(tmp_set) > 0 and (min_set_size is None or len(tmp_set) >= min_set_size):
		sets[last_family_id]=set(tmp_set)
	
	fd.close()
	yield tmp_set

def process_sets(sets1, sets2, universe, N, options):
	hyper = Hyper('ipergeo')
	for (set_name1, set1) in sets1.iteritems():
		for (set_name2, set2) in sets2.iteritems():
			if(options.simmetric_input and set_name1 >= set_name2):
				continue

			M = len(set1)
			if M == 0:
				exit(set_name1)
			n = len(set2)

			intersection = set1.intersection(set2)
			x = len(intersection)

			if float(x)/M < options.fract_cutoff: 
				continue
			
			pvalue = None
			if x > 0:
				pvalue = hyper.calc(N, M, n, x)

			#print (set_name1, set_name2, N, M, n, x, pvalue)
			yield (set_name1, set_name2, N, M, n, x, pvalue)

def populate_universe(sets1, sets2):
	retval = set();
	for name, s in sets1.iteritems():	
		retval |= s
	for name, s in sets2.iteritems():
		retval |= s
	return retval

def randomizze(named_sets, rand_map, options):
	retvals = {}
	for name, s in named_sets.iteritems():
		s1 = set()
		for e in s:
			try:
				s1.add(rand_map[e])
			except KeyError:
				if not options.ignore_not_in_universe:
					exit("Element in set not in universe, elements: %s" % e)

		retvals[name]= s1
	return retvals
	
def compute_empirical_pvalues(sets1, sets2, universe, true_pvalues, options):
	#if universe is None:
	#	u = populate_universe(sets1, sets2)
	#else:
	#	u = universe
	#
	#u = list(u)
	u = list(reduce(lambda x,y: x.union(y), sets1.values())) #universo limitato al set1

	N = len(u)
	
	count = {} 
	r = 0

	while r < options.randomizations:
		r+=1

		u_random = u[:]
		shuffle(u_random)

		random_univese_map = {}
		for k, v in zip(u, u_random): 
			random_univese_map[k] = v

		sets1_rand = randomizze(sets1, random_univese_map, options)

		for set_name1, set_name2, N, M, n, x, random_pvalue in \
		   process_sets(sets1_rand, sets2, universe, N, options):
			for p in  true_pvalues:
				if random_pvalue < p:
					try:
						count[p]+=1
					except KeyError:
						count[p]=1
	
	for k in true_pvalues:
		try:
			count[k] = float(count[k])/options.randomizations
		except KeyError:
			count[k] = 1./options.randomizations

	
	return count
	
					
def compute_pvalue_fdr_cutoff(empirical_pvalues, options):
	keys = empirical_pvalues.keys()
	cutoff = 0
	count = 1
	for k in sorted(keys):
		if empirical_pvalues[k]/count < options.fdr_cutoff:
			cutoff = k
		count += 1
	return cutoff

def print_(sets1, sets2, set_name1, set_name2, N, M, n, x, pvalue, empirical_pvalue, fdr, evalue, options):
	if options.report_intersection:
		intersection = sets1[set_name1].intersection(sets2[set_name2])
		if not options.report_none_pvalue:
			if options.randomizations is None:
				print "%s\t%s\t%i\t%i\t%i\t%i\t%s\t%s\t%s" %\
				  (set_name1, set_name2, N, M, n, x, pvalue, evalue, ';'.join(( str(i) for i in intersection)))
			else:
				print "%s\t%s\t%i\t%i\t%i\t%i\t%s\t%s\t%s\t%s\t%s" %\
				  (set_name1, set_name2, N, M, n, x, pvalue, empirical_pvalue, fdr, evalue, ';'.join(( str(i) for i in intersection)))
				
		else:
			print "%s\t%s\t%i\t%i\t%i\t%i\t%s" % (set_name1, set_name2, N, M, n, x, ';'.join(( str(i) for i in intersection)))
	else:
		if not options.report_none_pvalue:
			if options.randomizations is None:
				print "%s\t%s\t%i\t%i\t%i\t%i\t%s\t%s" % (set_name1, set_name2, N, M, n, x, pvalue, evalue)
			else:
				print "%s\t%s\t%i\t%i\t%i\t%i\t%s\t%s\t%s\t%s" % (set_name1, set_name2, N, M, n, x, pvalue, empirical_pvalue, fdr, evalue)
	
def main():
	parser = OptionParser(usage='''
            %prog sets_file1 sets_file2
            
            .META: input files:
                1   set_id
                2   element_id

            .META: stdout
                1   set1_id
                2   set2_id
                3   N       total number of item seen
                4   M       number of items in set1
                5   n       number of items in set2
                6   x       number of items in common
                7   pvalue  from fisher exact test
                8   evalue  bonferroni
        ''')
	parser.add_option('-s', '--single', default=False, action="store_true", help='assume input files containing a single set (1 col). [default: %default]')
	parser.add_option('-f', '--fasta', default=False, action="store_true", help='assume input files in fasta format, one set per block. [default: %default]')
	parser.add_option('-r', '--report_intersection', default=False, action="store_true", help='report the elements of each intersection. [default: %default]')
	parser.add_option('-R', '--randomizations', type=int, default=None, help='the number of randomizzation to perform. [default: %default]')
	parser.add_option('-F', '--fdr_cutoff', type=float, default=None, help='print the set of best intersectionso that the FDR of the set is FDR. [default: %default]', metavar="FDR")
	parser.add_option('-n', '--report_none_pvalue', default=False, action="store_true", help='donot report statistics. [default: %default]')
	parser.add_option('-c', '--corrected_cutoff', type=float, dest='corrected_cutoff', default=None, help='do not report items with a *corrected* p-value (e-value) larger than CUTOFF. [default: %default]', metavar='CUTOFF')
	parser.add_option('-d', '--raw_cutoff', type=float, dest='raw_cutoff', default=None, help='do not report items with a *raw* p-value larger than CUTOFF. [default: %default]', metavar='CUTOFF')
	parser.add_option('-m', '--min_set_size', type=int, dest='min_set_size', default=None, help='do not consider set smaller than MIN_SET_SIZE. [default: %default]', metavar='MIN_SET_SIZE')
	parser.add_option('-M', '--max_set_size', type=int, dest='max_set_size', default=None, help='ONLY FOR THE FILE IN SECOND ARGUMENT, do not consider set bigger than MAX_SET_SIZE. [default: %default]', metavar='MAN_SET_SIZE')
	parser.add_option('-l', '--simmetric_input', action="store_true", dest='simmetric_input', default=False, help='assume that the two input file are the same file and then compute the intersections only for the couple A,B and not for the couple B,A. [default: %default]')
	parser.add_option('-g', '--fract_cutoff', type=float, default=None, help='do not report intersection with a x/M < of CUTOFF. [default: %default]', metavar="CUTOFF")
	parser.add_option('-u', '--universe', type=str, default=None, help='Take the elements in the FILE as universe.\nFILE must contain a single column. [default: %default]', metavar="FILE")
	parser.add_option('-i', '--ignore_not_in_universe', action="store_true", help='Ignore element in set not present in universe (meaningless without --universe FILE). [default: %default]')
	parser.add_option('-I', '--mutual_information',action='store_true', help='Compute the mutal information between two clustering (set of sets). In this mode do not print the single intersections. [default: %default]')
	options, args = parser.parse_args()

	if len(args) != 2:
		exit('Unexpected argument number.')
	elif options.corrected_cutoff is not None and options.corrected_cutoff <= 0:
		exit('Invalid corrected_cutoff value.')
	elif options.raw_cutoff is not None and options.raw_cutoff <= 0:
                exit('Invalid raw_cutoff value.')
	elif options.fract_cutoff is not None and (options.fract_cutoff <= 0 or options.fract_cutoff>=1):
                exit('Invalid fract_cutoff value.')

		
	if options.randomizations and options.mutual_information:
		exit("Cannot compute the mutual information with ranodomization")
	
	if options.mutual_information:
		if options.simmetric_input:
			exit('Can\'t compute the MI on a simmetric_input (it will not be the entropy)')
		options.report_none_pvalue = True;


	universe = None
	N = 0
	if options.universe:
		if options.fasta or options.single:
			exit("fasta or single input mode non allowed if universe is given.")

		universe = set()
		with file(options.universe, 'r') as fd:
			for n,line in enumerate(fd):
				tokens = line.rstrip().split('\t')
				if len(tokens)!=1:
					exit("Exactly 1 element expected in the universe file, %d found at line %d" % (len(tokens),n))
				universe.add(tokens[0])
		N = len(universe)
	

	sets1 = {}
	sets2 = {}
	if options.fasta:
		if options.max_set_size is not None:
			exit("-M and -f are incompatible.")
		(N1,sets1) = load_sets_fasta(args[0], options.min_set_size)
		(N2,sets2) = load_sets_fasta(args[1], options.min_set_size)
	elif options.single:
		(N1,sets1) = load_sets_single(args[0])
		(N2,sets2) = load_sets_single(args[1])
	else:
		(N1,sets1) = load_sets_family(args[0], options.min_set_size, options.max_set_size, universe, options)
		(N2,sets2) = load_sets_family(args[1], options.min_set_size, options.max_set_size, universe, options)

	if universe is None:
		N = len(N1|N2)
	


	test_number = len(sets1) * len(sets2)
	if options.simmetric_input:
		test_number = len(sets1) * (len(sets2) -1) / 2 
	
	if options.randomizations is not None:
		retvals = [ x for x in process_sets(sets1, sets2, universe, N, options) ]
		retvals.sort(key=itemgetter(6))
	else:
		retvals = ( x for x in process_sets(sets1, sets2, universe, N, options) )
		

	if options.randomizations is not None:
		true_pvalues = set([1]) # il pvalue 1 voglio che ci sia sempre
		for set_name1, set_name2, N, M, n, x, pvalue in retvals:
			true_pvalues.add(pvalue)
		empirical_pvalue_map = compute_empirical_pvalues(sets1, sets2, universe, true_pvalues, options)
		if options.fdr_cutoff is not None:
			pvalue_fdr_cutoff = compute_pvalue_fdr_cutoff(empirical_pvalue_map, options)

	count = 1

	MI = None
	if options.mutual_information:
		MI=0

	for set_name1, set_name2, N, M, n, x, pvalue in retvals:

		if x == 0:
			pvalue = 1

		evalue = None
		empirical_pvalue = None
		fdr = None
		if pvalue is not None:
			evalue = pvalue * test_number
			if options.randomizations is not None:
				empirical_pvalue = empirical_pvalue_map[pvalue]
				fdr = empirical_pvalue/count

		if options.fdr_cutoff and fdr > options.fdr_cutoff:
			break

		if options.mutual_information and x>0: # lim_{x->0} x log(x) -> 0 
			p_ij = float(x)/N
			p_i  = float(M)/N
			p_j  = float(n)/N
			MI  += p_ij * log((p_ij)/(p_i*p_j)) / log(2)

		else:
			if (\
				pvalue is not None \
					and \
				(options.corrected_cutoff is None or evalue < options.corrected_cutoff) \
					and
				(options.raw_cutoff is None or pvalue < options.raw_cutoff) \
					and
				(options.fdr_cutoff is None or pvalue < pvalue_fdr_cutoff) \
			    ) or (\
				options.report_none_pvalue and pvalue is None\
			    ):
				print_(sets1, sets2, set_name1, set_name2, N, M, n, x, pvalue, empirical_pvalue, fdr, evalue, options)
				count+=1

	if options.mutual_information:
		print MI

if __name__ == '__main__':
	main()
