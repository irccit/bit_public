#!/bin/bash
#
# Copyright 2013 Gabriele Sales <gbrsales@gmail.com>
set -e -o pipefail

PKGDIR=""
STARTDIR="$PWD"
WORKDIR=""

function main() {

  parseOpts "$@"

  if [ $# -ne 1 ]; then
    error "Unexpected argument number."
  fi

  if [ ! -d "$1" -o ! -e "$1/DESCRIPTION" ]; then
    error "\"$1\" must be a package directory."
  fi

  pkgname="$(sed -r 's|^Package:\s+||;t;d' "$1/DESCRIPTION")"
  make_workdir "$pkgname"

  cd "$WORKDIR"
  R CMD build "../$1"
  tar -xf *.tar.gz
  R CMD check "$pkgname"
  exit $?
}

function parseOpts() {
  while getopts "h" opt; do
    case $opt in
      h)
        echo "Usage: $0 PACKAGE_DIR" >&2
        exit 1
        ;;
      \?)
        exit 1
        ;;
    esac
  done
}

function error() {
  echo "[ERROR] $1" >&2
  exit 1
}

function make_workdir() {
  WORKDIR="$(mktemp -d "${1}.XXXXX")"
  trap remove_workdir EXIT
}

function remove_workdir() {
  cd "$STARTDIR"
  rm -r "$WORKDIR"
}

main "$@"
