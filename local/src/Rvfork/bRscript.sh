#!/bin/bash

exec Rscript --default-packages=utils,grDevices,graphics,stats,methods,bitutils "$@"
