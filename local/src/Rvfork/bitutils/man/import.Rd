\name{import}
\alias{import}
\title{Quiet version of library}
\description{Loads a package suppressing all startup messages.}
\usage{import(package, description=NULL, character.only=FALSE)}
\arguments{
  \item{package}{String indicating the name of the package to be loaded}
  \item{description}{Additional information regarding the imported
    library}
  \item{character.only}{Boolean indicating whether \code{package} can be
    assumed to be a character string.}
}
\references{BioinfoTree Core Utilities \url{http://bioinfotree.org}}
\author{Paolo Martini \email{paolo.cavei@gmail.com}}
\seealso{\code{\link[base]{library}}, \code{\link[base]{invisible}}}
