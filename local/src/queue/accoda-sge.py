#!/usr/bin/env python
from optparse import OptionParser
from os import environ, getcwd, popen

def bioinfo_root():
	try:
		return environ['BIOINFO_ROOT'] + '/'
	except KeyError:
		exit('BIOINFO_ROOT not defined.')

def work_dir():
	path = getcwd() + '/'
	if not path.startswith(bioinfo_root()):
		exit('Outside BIOINFO_ROOT.')
	return path

def job_name(args):
	if len(args) == 2 and args[0] == 'make':
		return args[1].replace('.', '_')
	else:
		return args[0]

def main():
	parser = OptionParser()
	parser.add_option('-n', '--name', dest='name', help='the job NAME', metavar='NAME')
	options, args = parser.parse_args()

	if len(args) == 0:
		exit('Insufficient argument number.')

	cwd = work_dir()
	if options.name is None:
		name = job_name(args)
	else:
		name = options.name
	
	rows = []
	rows.append('#$ -N %s' % name)
	rows.append('#$ -o %s/%s.out' % (cwd, name)) 
	rows.append('#$ -j n -e %s/%s.err' % (cwd, name))
	rows.append('source /etc/bioinfo.conf')
	rows.append('cd %s' % cwd)
	rows.append(' '.join(args))

	fd = popen('qsub', 'w')
	fd.write('\n'.join(rows))
	res = fd.close()
	if res is not None:
		exit('Error submitting job.')

if __name__ == '__main__':
	main()

