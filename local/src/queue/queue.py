# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

def make_queue(config):
	modname = '%s_queue' % config.type
	try:
		mod = __import__(modname)
		return mod.Queue(config)
	except (ImportError, AttributeError), e:
		raise ValueError, 'unsupported queue type'

class QueueError(Exception):
	''' Class used to signal queue errors. '''
