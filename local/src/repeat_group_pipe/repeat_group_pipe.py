#!/usr/bin/env python
#
# Copyright 2008-2009 Gabriele Sales <gbrsales@gmail.com>

from errno import EPIPE
from optparse import OptionParser
from os import environ
from subprocess import Popen, PIPE
from sys import stdin, argv
from vfork.util import exit, format_usage
import re

def close_proc(proc, command):
	proc.stdin.close()
	retcode = proc.wait()
	if retcode != 0:
		exit("'%s' exited with error code %d" % (command, retcode))

def build_command(template, serial, args):
	serial = str(serial)
	args = [ a.replace("'", "\\'") for a in args ]
	
	def arg_matcher(match):
		try:
			idx = int(match.group(1)[1:])
			if idx == 0:
				return serial
			else:
				return args[idx-1]
		except IndexError:
			exit('Invalid reference $%d' % idx)
	
	return re.sub(r'(?<!\$)(\$[0-9]+)', arg_matcher, template).replace('$$', '$')


def main():
	parser = OptionParser(usage=format_usage('''
		%prog COMMAND COLUMNS... <INPUT
	'''))
	parser.add_option('-s', '--sorted', dest='sorted', action='store_true', default=False, help='assume sorted input file (default: %default)')
	options, args = parser.parse_args()
	if len(args) < 2:
		exit('Unexpected argument number.')
	
	try:
		columns = tuple((int(arg) - 1) for arg in args[1:])
		for column in columns:
			if column < 0:
				raise ValueError
	except ValueError:
		exit('Invalid column number')
	
	proc = None
	last_values = None
	serial = 0
	for lineno, line in enumerate(stdin):
		try:
			tokens = line.rstrip().split('\t')
			values = [ tokens[c] for c in columns ]
		except IndexError:
			exit('Insufficient column number')
		
		if values != last_values:
			if not options.sorted and values < last_values:
				exit('Disorder found at line %d' % (lineno+1,) )

			if proc:
				close_proc(proc, args[0])
			last_values = values
			serial += 1
			proc = Popen([environ.get('SHELL', 'bash'), '-c', build_command(args[0], serial, values)], shell=False, stdin=PIPE)
		
		elif proc is None:
			continue
		
		try:
			proc.stdin.write(line)
		except IOError, e:
			if e.errno == EPIPE:
				close_proc(proc, args[0])
				proc = None
			else:
				raise
	
	if proc:
		close_proc(proc, args[0])

if __name__ == '__main__':
	main()
