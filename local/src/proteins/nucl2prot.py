#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

from Bio.Seq import Seq
from Bio.Alphabet import generic_dna


def main():

	usage = format_usage('''
		%prog PARAM1 < STDIN
		retrun convert a nucleotide sequence to protein

		STDIN
		must contain only sequences, one per line

		STDOUT
		the proteic converted sequence

	''')
	parser = OptionParser(usage=usage)
	
	#parser.add_option('-c', '--cutoff', type=float, dest='cutoff', default=0.05, help='some help CUTOFF [default: %default]', metavar='CUTOFF')
	#parser.add_option('-p', '--dump-params', dest='params_file', help='some help FILE [default: %default]', metavar='FILE')
	#parser.add_option('-v', '--verbose', dest='verbose', action='store_true', default=False, help='verbose debugging mode [default: %default]')
	#parser.add_option('-s', '--sequence', dest='sequence', action='store_true', default=False, help='return	sequence from the occurred kmer [default: %default]')

	#options, args = parser.parse_args()

	#if len(args) != 1:
	#	exit('Unexpected argument number.')

	for seq in stdin:
		fields = seq.split()
		dna = Seq(fields[0], generic_dna)
		protein = dna.translate()
		print "%s\t%s" % (dna,protein)

if __name__ == '__main__':
	main()
