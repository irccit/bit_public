#!/usr/bin/env python
#
# Copyright 2010 Paolo Martini <paolo.cavei@gmail.com>
# Copyright 2011-2012 Gabriele Sales <gbrsales@gmail.com>

from itertools import groupby
from operator import itemgetter
from optparse import OptionParser
from sys import stdin
from vfork.io.util import parse_int, safe_rstrip
from vfork.util import exit, format_usage, ignore_broken_pipe


def iter_rows(fd, glue, assume_sorted, check_duplicated):
    last_chr = None
    labels = set()

    for idx, line in enumerate(fd):
        tokens = safe_rstrip(line).split('\t')
        if len(tokens) < 4:
            exit("Insufficient column number.")

        chr = tokens[0]

        if not assume_sorted:
            if last_chr is not None and chr < last_chr:
                exit("Disorder found in input column 1 at line %d" % (idx+1,))
            last_chr = chr

        try:
            start = int(tokens[1])
            if (start < 0):
                exit("Invalid coordinates at row %d: non-sense coordinate \"%d\"" % (idx+1,start))
        except ValueError:
            exit("Invalid coordinates at row %d: \"%s\" is not an integer." % (idx+1,tokens[1]))

        try:
            end = int(tokens[2])
            if (end <=0):
                exit("Invalid coordinates at row %d: non-sense coordinates \"%d\"" % (idx+1,end))
        except ValueError:
            exit("Invalid coordinates at row %d: \"%s\" is not an integer." % (idx+1,tokens[2]))

        if start >= end:
            exit("Invalid coordinates at row %d: start greater or equal then stop" % (idx+1,))

        label = glue.join(tokens[3:])

        if check_duplicated:
            if label in labels:
                exit('Duplicated label. Maybe you should try "--allow-duplicates".')
            labels.add(label)

        yield chr, start, end, label

def check_group_order(group, assume_sorted):
    last_start = None
    for region in group:
        start = region[1]

        if not assume_sorted and last_start is not None and start < last_start:
            exit("Disorder found in input column 2.")
        last_start = start

        yield region

def merge_regions(regions, min_overlap):
    block = None
    for chr, start, end, label in regions:
        if block is not None and block[2] - start >= min_overlap:
            block[2] = max(block[2], end)
            block[3].add(label)
        else:
            if block is not None:
                yield block

            block = [ chr, start, end, set([label]) ]

    if block is not None:
        yield block


def main():
    parser = OptionParser(usage=format_usage('''
              Usage: %prog [OPTIONS] <REGIONS

              Computes the union of multiple regions.

              Input must be formatted as follows:
                1) chromosome
                2) region start
                3) region end
                4-) region ID (one or multiple columns)

              The check on the order of the first column can be disabled using the
              "--sorted" option. Note, however, that columns 2 and 3 must still be
              sorted inside each group of rows sharing the same value in column 1.
    '''))
    parser.add_option('-g', '--glue', dest='glue', default=';', help='string used to glue multiple IDs (deafult: "%default")')
    parser.add_option('-o', '--minimum-overlap', dest='min_overlap', default=0, type='int', help='minimal overlap required to merge regions (default: %default)', metavar='OVERLAP')
    parser.add_option('-s', '--assume-sorted', dest='assume_sorted', action='store_true', default=False, help='assume the input is sorted on the first column (chromosome)')
    parser.add_option('',   '--allow-duplicates', dest='allow_duplicated', action='store_false', default=True, help='allow the presece of duplicated labels')
    parser.add_option('',   '--region-id-glue', dest='id_glue', default=',', help='string used to glue multiple columns of a region ID (default: "%default")', metavar='GLUE')
    options,args = parser.parse_args()

    if len(args) != 0:
        exit('Unexpected argument number.')
    elif options.min_overlap < 0:
        exit('Invalid minimum overlap: ' + str(options.min_overlap))

    for _, grp in groupby(iter_rows(stdin, options.id_glue, options.assume_sorted, options.allow_duplicated), itemgetter(0)):
        for chr, start, end, labels in merge_regions(check_group_order(grp, options.assume_sorted), options.min_overlap):
            print '%s\t%d\t%d\t%s' % (chr, start, end, options.glue.join(sorted(labels)))

if __name__ == '__main__':
    ignore_broken_pipe(main)

