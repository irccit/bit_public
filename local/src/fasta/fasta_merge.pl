#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\t";
$\="\n";

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 [-h|help] [-r|reformat_output N]
	merge each fasta bloch having the same header, the order of the merged sequence is the order of the block in stdin
	block must be lexicografically sorted on header

	-r|reformat_output N
		remove /n form the block contents and write a merged block N char wide
\n";

my $help=0;
my $reformat_output = undef;
my $assume_sorted = 0;
GetOptions (
	'h|help' => \$help,
	'r|reformat_output=i' => \$reformat_output,
	's|assume_sorted' => \$assume_sorted
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

my $HEADER=undef;
my $HEADER_PRE=undef;
my $seq='';
while(<>){
	if(m/^>/){
		chomp;
		$HEADER = $_;
		$HEADER =~ s/^>//;
		die("Fasta block are not lexicografically sorted on header.") if defined($HEADER_PRE) and !$assume_sorted and $HEADER lt $HEADER_PRE;
		if(defined($HEADER_PRE) and $HEADER ne $HEADER_PRE){
			print ">$HEADER_PRE";
			print_seq($seq);
			$seq = '';
		}
		$HEADER_PRE = $HEADER;
	}else{
		die("HEADER not defined") if !defined($HEADER);
		$seq.=$_;
	}
}
if(defined($HEADER)){
	print ">$HEADER";
	print_seq($seq);
}

sub print_seq
{
	my $seq = shift;
	if(defined $reformat_output){
		$seq=~s/\n//g;
		while(length($seq)){
			print substr($seq,0,120,'');
		}
	}else{
		$seq=~s/\n$//;
		print $seq; 
	}
}
