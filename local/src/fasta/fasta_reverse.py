#!/usr/bin/env python
#
# Copyright 2010 Paolo Martini <paolo.cavei@gmail.com>

from optparse import OptionParser
from sys import stdin, stdout
from vfork.fasta.reader import MultipleBlockStreamingReader, FormatError
from vfork.fasta.writer import MultipleBlockWriter
from vfork.sequence import check_nucleotides, complement, reverse_complement
from vfork.util import exit, format_usage, ignore_broken_pipe

def reverse_seq(seq):
    return seq[::-1]

def main():
    parser = OptionParser(usage=format_usage('''
            %prog [OPTIONS] <FASTA >REVERSE_FASTA

            From a input FASTA formatted %prog returns the reverse complement of the sequences.
            By default all FASTA blocks are reversed.
    '''))
    parser.add_option('-c', '--strand-field', dest='strand', type='int', help='In the header, indicate the number of the field that corresponds to the strand. Sequence labeled with "-1" or "-" are marked for program action while the others remain unchanged')
    parser.add_option('-s', '--separator', dest='separator', help='Separator in the header. Default tab')
    parser.add_option('--only-complement', dest='complement', default=False, action='store_true', help='Perform only complement of the sequences')
    parser.add_option('--only-reverse', dest='rev', default=False, action='store_true', help='Perform only reverse of the sequences')
    parser.add_option('-i', '--ignore-alphabet', dest='check_alphabet', default=True, action='store_false', help='Allow non-nucleotide letters.')
    options, args = parser.parse_args()

    if len(args) != 0:
        exit('Unexpected argument number.')

    action = reverse_complement
    if options.complement and options.rev:
        exit('option "--only-complement" and option "--only-reverse" are mutually exclusive.')
    if options.complement:
        action=complement
    if options.rev:
        action=reverse_seq

    writer = MultipleBlockWriter(stdout)
    try:
        if options.separator is None:
            options.separator = '\t'
        for header, seq in MultipleBlockStreamingReader(stdin, join_lines=True):
            if options.check_alphabet and not check_nucleotides(seq):
                exit('Non-nucleotide letters found in sequence: "%s". Use "-i,--ignore-alphabet" to ignore this error.' % (header,))

	    if options.strand is not None:
                header_fields = header.split(options.separator)
                col = options.strand - 1
                if len(header_fields) < col:
                    exit('Field not present in header. Check your glue separation.')
                if header_fields[col] == '-1' or header_fields[col] == '-':
                    seq=action(seq)
            else:
                seq=action(seq)
            writer.write_header(header)
            writer.write_sequence(seq)
	writer.flush()
    except FormatError, e:
        exit('Malformed FASTA input: ' + e.args[0])

if __name__ == '__main__':
    ignore_broken_pipe(main)

