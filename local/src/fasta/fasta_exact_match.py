#!/usr/bin/env python
#
# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

from __future__ import with_statement
from optparse import OptionParser
from Queue import Empty, Full
from sys import stdin
from vfork.fasta.reader import MultipleBlockStreamingReader
from vfork.util import exit, format_usage
try:
	import multiprocessing as mp
except:
	pass


class Scanner(object):
	def __init__(self, cdnas, partial):
		self.cdnas = cdnas
		self.partial = partial
	
	def start(self):
		raise NotImplemented
	
	def stop(self):
		raise NotImplemented
	
	def queue(self, query_id, query_sequence):
		raise NotImplemented
	
	def _scan(self, query_id, query_sequence):
		for cdna_id, cdna_sequence in self.cdnas:
			query_len = len(query_sequence)
			cdna_len = len(cdna_sequence)
			if self.partial and cdna_len < query_len:
				for pos in xrange(query_len - cdna_len + 1):
					if query_sequence[pos:pos+cdna_len] == cdna_sequence:
						self._write_match(query_id, cdna_id, 0, pos)
			else:
				start = 0
				while True:
					pos = cdna_sequence.find(query_sequence, start)
					if pos == -1:
						break
					
					self._write_match(query_id, cdna_id, pos, 0 if self.partial else None)
					start = pos+1
	
	def _write_match(self, query_id, cdna_id, target_pos, query_pos):
		raise NotImplemented
	
	def _print_match(self, query_id, cdna_id, target_pos, query_pos):
		if query_pos is None:
			print '%s\t%s\t%d' % (query_id, cdna_id, target_pos)
		else:
			print '%s\t%s\t%d\t%d' % (query_id, cdna_id, target_pos, query_pos)

class SerialScanner(Scanner):
	def __init__(self, *args):
		super(SerialScanner, self).__init__(*args)
		self.queue = self._scan
		self._write_match = self._print_match
	
	def start(self):
		pass
	
	def stop(self):
		pass

class ParallelScanner(Scanner):
	def start(self):
		self.task_queue = mp.Queue(mp.cpu_count()*4)
		self.done_queue = mp.Queue(mp.cpu_count()*4)
		
		for i in range(mp.cpu_count()):
			mp.Process(target=self._worker).start()
	
	def stop(self):
		for i in range(mp.cpu_count()):
			self.task_queue.put('DONE')
		
		self.task_queue.close()
		self._flush_done()
	
	def _flush_done(self):
		expected = mp.cpu_count()
		while expected > 0:
			res = self.done_queue.get()
			if res == 'DONE':
				expected -= 1
			else:
				self._print_match(*res)
	
	def queue(self, query_id, query_sequence):
		while True:
			self._consume_done()
			
			try:
				self.task_queue.put((query_id, query_sequence), timeout=1)
			except Full:
				continue
			else:
				break
	
	def _consume_done(self):
		try:
			while True:
				self._print_match(*self.done_queue.get_nowait())
		except Empty:
			pass
	
	def _worker(self):
		while True:
			work = self.task_queue.get()
			if work == 'DONE':
				self.done_queue.put('DONE')
				break
			else:
				self._scan(*work)
	
	def _write_match(self, query_id, cdna_id, target_pos, query_pos):
		self.done_queue.put((query_id, cdna_id, target_pos, query_pos))

def main():
	parser = OptionParser(usage=format_usage('''
		%prog TARGETS <QUERIES >MATCHES
		
		Given a FASTA file of query sequences and one of targets, searches exact matches
		between any two.
		
		The output has the following format:
		 1. query ID
		 2. target ID
		 3. start position of the match on the target
		 4. start position of the match on the query (ONLY when using --partial)
	'''))
	parser.add_option('-p', '--parallel', dest='parallel', action='store_true', default=False, help='perform a parallel search')
	parser.add_option('-r', '--partial', dest='partial', action='store_true', default=False, help='when a target is shorter than the query accept a partial match')
	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')
	
	with file(args[0], 'r') as fd:
		cdnas = list(MultipleBlockStreamingReader(fd, force_lower=True))
	
	if options.parallel:
		scanner = ParallelScanner
	else:
		scanner = SerialScanner
	scanner = scanner(cdnas, options.partial)
	
	scanner.start()
	for query_id, query_sequence in MultipleBlockStreamingReader(stdin, force_lower=True):
		scanner.queue(query_id, query_sequence)
	scanner.stop()

if __name__ == '__main__':
	main()
