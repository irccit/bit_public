#!/usr/bin/env python
# encoding: utf-8

from cStringIO import StringIO
from optparse import OptionParser
from os.path import isdir, isfile, join
from sys import exit, stdin, stderr
from vfork.fasta.reader import SingleBlockReader, FormatError
from vfork.io.util import safe_rstrip

def parse_line(line, lineno, gen_id, insert_gaps):
	line = safe_rstrip(line)
	tokens = line.split('\t')

	if len(tokens) < 3:
		raise ValueError, 'insufficient token number (found %d, expected at least 3)' % len(tokens)

	if gen_id:
		tokens.insert(0, '_'.join(tokens[0:3]))
	elif len(tokens) < 4:
		exit('Insufficient token number at line %d.' % lineno)
	
	try:
		start = int(tokens[2])
	except ValueError:
		raise ValueError, 'invalid start coordinate'
	
	try:
		stop = int(tokens[3])
		if stop <= start:
			raise ValueError
	except ValueError:
		raise ValueError, 'invalid stop coordinate'

	if insert_gaps:
		gaps = [ parse_gap(g) for g in tokens[-1].split(';') ]
	else:
		gaps = None
	
	return tokens[0], tokens[1], start, stop, gaps

def parse_gap(s):
	tokens = s.split(',')
	if len(tokens) != 2:
		raise ValueError, 'invalid gap'
	else:
		return tuple(int(d) for d in tokens)

def insert_gaps(sequence, gaps):
	#b = pos
	#e = pos + len(line)
	#my_gaps = [ p for p in gaps if p[0] >= b and p[1] < e ]
	#
	#retval = ''
	#pre_pos = 0
	#for p in my_gaps:
	#	retval = retval + line[pre_pos:p[0]]
	#	pre_pos = p[0]
	#	for i in xrange(0,p[1]):
	#		retval += '-'
	#return retval
	res = StringIO()
	start = 0
	gap_cum = 0
	for gap in gaps:
		end = gap[0] - gap_cum
		res.write(sequence[start:end])
		res.write('-'*gap[1])
		start = end
		gap_cum += gap[1]
	
	res.write(sequence[start:])
	return res.getvalue()

def main():
	parser = OptionParser(usage='%prog SEQUENCE_DIR <SEQLIST >FASTA')
	parser.add_option('-c', '--clamp', dest='clamp', action='store_true', default=False, help='clamp coordinates to chromosome edges')
	parser.add_option('-i', '--gen-id', dest='gen_id', action='store_true', help='automatically generate IDs for sequences')
	parser.add_option('-r', '--report', dest='report_coordinates', action='store_true', help='report coordinates in headers')
	parser.add_option('-g', '--gaps', dest='insert_gaps', action='store_true', help='insert gaps in the sequence')
	parser.add_option('-o', '--append-offset', dest='append_offset', action='store_true', help='append the sequence start offset to the FASTA header')
	parser.add_option('-l', '--line', dest='report_lines', action='store_true', help='report input rows in headers')
	parser.add_option('-s', '--separator', dest='separator', default='\t', help='the character used to separate values in headers', metavar='CHAR')
	parser.add_option('-x', '--suffix', dest='suffix', default='fa', help='search for file of sequences in the form *.suffix insehead of *.fa', metavar='SUFFIX')
	options, args = parser.parse_args()

	if len(args) != 1:
		exit('Unexpected argument number.')
	elif options.append_offset and options.report_coordinates:
		exit('--append-offset and --report are mutually exclusive.')
	elif options.report_lines and options.report_coordinates:
		exit('--lines and --report are mutually exclusive.')
	elif options.insert_gaps:
		print >>stderr, '[WARNING] the --gaps option is not well tested'
	elif len(options.separator) != 1:
		exit('Invalid separator character.')
	
	seq_dir = args[0]
	if not isdir(seq_dir):
		exit('Invalid sequence dir ' + seq_dir)
	
	last_chromosome = None
	reader = None
	for lineno, line in enumerate(stdin):
		try:
			line = safe_rstrip(line)
			label, chromosome, start, stop, gaps = parse_line(line, lineno, options.gen_id, options.insert_gaps)
		except ValueError, e:
			exit('Malformed input at line %d: %s.' % (lineno+1, e.args[0]))
		
		if chromosome != last_chromosome:
			if reader is not None:
				reader.close()
			last_chromosome = chromosome
			
			seq_templates =  ('%s.' + options.suffix, 'chr%s.' + options.suffix)
			for seq_template in seq_templates:
				seq_filename = join(seq_dir, seq_template % chromosome)
				if isfile(seq_filename):
					try:
						reader = SingleBlockReader(seq_filename)
					except FormatError:
						exit('Malformed FASTA file %s.' % seq_filename)
					else:
						break
			else:
				exit('Cannot find chromosome %s.' % chromosome)
		
		if options.clamp:
			if start < 0: start = 0
			if stop > reader.size: stop = reader.size

		try:
			sequence = reader.get(start, stop - start)
		except ValueError:
			exit('Invalid coordinates at line %d (%s,%d,%d).' % (lineno+1,chromosome, start, stop))

		if options.insert_gaps and len(gaps) > 0:
			sequence = insert_gaps(sequence, gaps)
		
		if options.append_offset:
			label += '_%d' % start
		elif options.report_coordinates:
			label = options.separator.join((label, chromosome, str(start), str(stop)))
		elif options.report_lines:
			label = line

		print '>%s' % label
		
		for pos in xrange(0, len(sequence), 80):
			print sequence[pos:pos+80]
	
	if reader is not None:
		reader.close()

if __name__ == '__main__':
	main()
