#!/usr/bin/env python
#
# Copyright 2013 Michele Vidotto <michele.vidotto@gmail.com>
# Copyright 2013 Gabriele Sales <gbrsales@gmail.com>

from argparse import ArgumentParser, FileType, RawDescriptionHelpFormatter
from itertools import izip
from sys import stdin, stdout
from vfork.fasta.reader import FormatError, MultipleBlockStreamingReader
from vfork.util import exit, format_usage, ignore_broken_pipe
import numpy as np


def main():
	args = parseArgs()

	totalBlockNum = 0
	totalSeqLen = 0
	totalCounts = arrayOfCounts()

	for fd in args.files:

		blockNum = 0
		seqLen = 0
		counts = arrayOfCounts()

		try:
			for label, seq in MultipleBlockStreamingReader(fd, join_lines=True):

				lines = list(seq)
				if len(lines) == 0 and not args.allow_empty:
					exit('Empty FASTA sequence "%s" in: %s' % (label, fd.name))

				blockNum += 1
				seqLen += len(seq)
				counts += countChars(seq)

		except FormatError, e:
			exit('%s in: %s' % (e.args[0], fd.name))

		totalBlockNum += blockNum
		totalSeqLen += seqLen
		totalCounts += counts

		if args.seq:
			stdout.write('%u\t' % blockNum)
		if args.len:
			stdout.write('%u\t' % seqLen)
		if args.bases:
			printBaseCounts(counts)
		stdout.write('%s\n' % fd.name.replace(stdin.name,""))

	if len(args.files) > 1:
		if args.seq:
			stdout.write('%u\t' % totalBlockNum)
		if args.len:
			stdout.write('%u\t' % totalSeqLen)
		if args.bases:
			printBaseCounts(totalCounts)
		stdout.write('total\n')

	return 0

def parseArgs():
	parser = ArgumentParser(formatter_class=RawDescriptionHelpFormatter,
	                        description=format_usage('''
		Print FASTA block counts, total sequences length, base frequencies
		for each FILE.

		Non-standard bases (NSB) are shown as comma-separated values in the form:
		[NSB:COUNT,[NSB:COUNT,...]].

		Total values are shown if more then one FILE is specified.

		When FILE is -, read from standard input.
	'''))

	parser.add_argument('files', metavar='FILE', nargs='*', type=FileType('r'), default=[stdin])
	parser.add_argument('-b','--bases', action='store_true', dest='bases', default=False, help='print base counts in sequences: A T C G NSB:COUNT,[NSB:COUNT],...')
	parser.add_argument('-e','--allow-empty', action='store_true', dest='allow_empty', default=False, help='allow empty sequences')
	parser.add_argument('-l','--lengths', action='store_true', dest='len', default=False, help='print sequences length')
	parser.add_argument('-s','--sequences', action='store_true', dest='seq', default=False, help='print block counts')

	args = parser.parse_args()

	if not args.seq and not args.len and not args.bases:
		args.seq = args.len = args.bases = True

	return args

def arrayOfCounts():
	return np.zeros(256, dtype='int')

def countChars(string):
	intArray = np.frombuffer(string, np.uint8)
	return np.bincount(intArray, minlength=256)


canonical = [ ord(b) for b in 'ATCG' ]

def printBaseCounts(counts):

	stdout.write('%s\t%s\t%s\t%s\t' % tuple(counts[canonical]))
	counts[canonical] = 0

	extra = np.nonzero(counts)[0]
	extraCounts = counts[extra]

	if extra.any():
		stdout.write(','.join('%s:%s' % (chr(e),c) for e,c in izip(extra, extraCounts)) + '\t')


if __name__ == '__main__':
	ignore_broken_pipe(main)
