#!/usr/bin/env tool_test
#
# Copyright 2013 Michele Vidotto <michele.vidotto@gmail.com>

# standard usage
* standard usage 1 file
  In:   simple1.fa
  Out:  standard1.out
  Cmd:  fasta_count $1 >$@

* standard usage 2 files
  In:   simple1.fa simple2.fa
  Out:  standard2.out
  Cmd:  fasta_count $1 $2 >$@

* standard usage stdin
  In:   simple1.fa
  Out:  standard_stdin.out
  Cmd:  cat $1 | fasta_count >$@

* standard usage emtpy seq
  In:   empty-seq1.fa
  Fail: yes
  Cmd:  fasta_count $1

* standard usage emtpy label
  In:   empty-label.fa
  Fail: yes
  Cmd:  fasta_count $1


# standard canonical only
* standard canonical 1 file
  In:   simple_canonical1.fa
  Out:  standard_canonical1.out
  Cmd:  fasta_count $1 >$@

* standard canonical 2 files
  In:   simple_canonical1.fa simple_canonical2.fa
  Out:  standard_canonical2.out
  Cmd:  fasta_count $1 $2 >$@

* standard canonical stdin
  In:   simple_canonical1.fa
  Out:  standard_canonical_stdin.out
  Cmd:  cat $1 | fasta_count >$@


# base counts (-b) only
* base counts only 1 file
  In:   simple1.fa
  Out:  base_counts1.out
  Cmd:  fasta_count -b $1 >$@

* base counts only 2 files
  In:   simple1.fa simple2.fa
  Out:  base_counts2.out
  Cmd:  fasta_count -b $1 $2 >$@

* base counts only stdin
  In:   simple1.fa
  Out:  base_counts_stdin.out
  Cmd:  cat $1 | fasta_count -b >$@

* base counts only emtpy seq
  In:   empty-seq1.fa
  Fail: yes
  Cmd:  fasta_count -b $1

* base counts only emtpy label
  In:   empty-label.fa
  Fail: yes
  Cmd:  fasta_count -b $1

# sequences length (-l) only
* sequences length only 1 file
  In:   simple1.fa
  Out:  length1.out
  Cmd:  fasta_count -l $1 >$@

* sequences length only 2 files
  In:   simple1.fa simple2.fa
  Out:  length2.out
  Cmd:  fasta_count -l $1 $2 >$@

* sequences length only stdin
  In:   simple1.fa
  Out:  length_stdin.out
  Cmd:  cat  $1 | fasta_count -l >$@

* sequences length only emtpy seq
  In:   empty-seq1.fa
  Fail: yes
  Cmd:  fasta_count -l $1

* sequences length only emtpy label
  In:   empty-label.fa
  Fail: yes
  Cmd:  fasta_count -l $1


# block counts (-s) only
* block counts only 1 file
  In:   simple1.fa
  Out:  blocks1.out
  Cmd:  fasta_count -s $1 >$@

* block counts only 2 files
  In:   simple1.fa simple2.fa
  Out:  blocks2.out
  Cmd:  fasta_count -s $1 $2 >$@

* block counts only stdin
  In:   simple1.fa
  Out:  blocks_stdin.out
  Cmd:  cat $1 | fasta_count -s >$@

* block counts only emtpy seq
  In:   empty-seq1.fa
  Fail: yes
  Cmd:  fasta_count -s $1

* block counts only emtpy label
  In:   empty-label.fa
  Fail: yes
  Cmd:  fasta_count -s $1


 # empty seq
* empty seq 1 file
  In:   empty-seq1.fa
  Out:  empty1.out
  Cmd:  fasta_count -e $1 >$@

* empty seq 2 files
  In:   empty-seq1.fa empty-seq2.fa
  Out:  empty2.out
  Cmd:  fasta_count -e $1 $2 >$@

* empty seq stdin
  In:   empty-seq1.fa
  Out:  empty_stdin.out
  Cmd:  cat $1 | fasta_count -e >$@

* empty label
  In:   empty-label.fa
  Fail: yes
  Cmd:  fasta_count -e $1


# base counts and sequences length (-bl)
* base counts, seq len 1 file
  In:   simple1.fa
  Out:  base-len1.out
  Cmd:  fasta_count -bl $1 >$@

* base counts, seq len 2 files
  In:   simple1.fa simple2.fa
  Out:  base-len2.out
  Cmd:  fasta_count -bl $1 $2 >$@

* base counts, seq len stdin
  In:   simple1.fa
  Out:  base-len_stdin.out
  Cmd:  cat $1 | fasta_count -bl >$@

* base counts, seq len emtpy seq
  In:   empty-seq1.fa
  Fail: yes
  Cmd:  fasta_count -bl $1

* base counts, seq len emtpy label
  In:   empty-label.fa
  Fail: yes
  Cmd:  fasta_count -bl $1

# block counts and sequences length (-sl)
* block counts, seq len 1 file
  In:   simple1.fa
  Out:  blocks-len1.out
  Cmd:  fasta_count -sl $1 >$@

* block counts, seq len 2 files
  In:   simple1.fa simple2.fa
  Out:  blocks-len2.out
  Cmd:  fasta_count -sl $1 $2 >$@

* block counts, seq len stdin
  In:   simple1.fa
  Out:  blocks-len_stdin.out
  Cmd:  cat $1 | fasta_count -sl >$@

* block counts, seq len emtpy seq
  In:   empty-seq1.fa
  Fail: yes
  Cmd:  fasta_count -sl $1

* block counts, seq len emtpy label
  In:   empty-label.fa
  Fail: yes
  Cmd:  fasta_count -sl $1

# block counts and base counts (-sb)
* block counts, base counts 1 file
  In:   simple1.fa
  Out:  blocks-bases1.out
  Cmd:  fasta_count -sb $1 >$@

* block counts, base counts 2 files
  In:   simple1.fa simple2.fa
  Out:  blocks-bases2.out
  Cmd:  fasta_count -sb $1 $2 >$@

* block counts, base counts stdin
  In:   simple1.fa
  Out:  blocks-bases_stdin.out
  Cmd:  cat $1 | fasta_count -sb >$@

* block counts, base counts emtpy seq
  In:   empty-seq1.fa
  Fail: yes
  Cmd:  fasta_count -sb $1

* block counts, base counts empty label
  In:   empty-label.fa
  Fail: yes
  Cmd:  fasta_count -sb $1
