#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;

my $usage="$0 [-r|-i] [-f] [-n] 'id strand' file.fa";

my $regex=0;
my $id_only=0;
my $id_from_file=0;
my $no_header=0;
my @ids=();

GetOptions (
	'r' => \$regex,
	'i' => \$id_only,
	'f' => \$id_from_file,
	'n' => \$no_header
)or die($usage);

die($usage) if $id_only and $regex;

my $id_str = shift @ARGV;
my ($id,$strand) = split /\s+/,$id_str;
die $usage if (!defined $id or !defined $strand);

if($id_from_file){
	die "lo script non fiunziona con questa opzione";
	open (FH, $id) or die("Can't open file $id\n");
	while(<FH>){
		chomp;
		push @ids,$_;
	}
}else{
	@ids=($id);
}



my $print=0;
while(<>){
	if($print){
		if (m/^>/){
			if(!&match($_)){
				$print=0;
				next;
			}else{
				next if $no_header;
			}
		}
		if ( (!m/^>/) and ($strand eq '-') ) {
			chomp;
			$_ = reverse $_;
			$_ =~ tr/ACGT/TGCA/;
			$_ .= "\n";
		}
		print;
		$print=1;
	}else{
		if(m/^>/ and &match($_)){
			$print=1;
			if($no_header){
				next;
			}else{
				print;
			}
		}
	}
}


sub match
{
	
	my $a=shift;
	if($regex){
		for(@ids){
			return 1 if $a=~/$_/;
		}
	}elsif($id_only){
		for(@ids){
			return 1 if $a=~/^>$_\W/;
		}
	}else{
		for(@ids){
			return 1 if $a eq ">$_\n";
		}
	}
}
