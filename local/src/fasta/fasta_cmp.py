#!/usr/bin/env python
#
# Copyright 2009 Gabriele Sales <gbrsales@gmail.com>

from hashlib import sha1
from optparse import OptionParser
from vfork.fasta.reader import MultipleBlockStreamingReader
from vfork.util import exit, format_usage

def compute_fprint(content):
	h = sha1()
	for line in content:
		h.update(line)
	return h.digest()

def main():
	parser = OptionParser(usage=format_usage('''
		%prog [OPTIONS] SOURCE_FASTA DEST_FASTA

		Compares the contents of two FASTA files, printing
		the labels of records that have been added,
		removed or updated.

		FASTA block in the two files may appear in different
		order.
	
		The output has the following format:
		  + LABEL1
		  - LABEL2
		  M LABEL3
	'''))
	options, args = parser.parse_args()

	if len(args) != 2:
		exit('Unexpected argument number.')

	source_reader = MultipleBlockStreamingReader(args[0], join_lines=False)
	source_fprints = {}
	for header, content in source_reader:
		source_fprints[header] = compute_fprint(content)
	
	res = []
	dest_reader = MultipleBlockStreamingReader(args[1], join_lines=False)
	for header, content in dest_reader:
		if header not in source_fprints:
			res.append((header, '+'))
		else:
			source_fprint = source_fprints.pop(header)
			dest_fprint = compute_fprint(content)
			if source_fprint != dest_fprint:
				res.append((header, 'M'))
	
	for header in source_fprints.iterkeys():
		res.append((header, '-'))
	
	res.sort()
	for header, op in res:
		print '%s\t%s' % (op, header)
				

if __name__ == '__main__':
	main()
