#!/usr/bin/env python
#
# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

from optparse import OptionParser
from vfork.fasta.reader import SingleBlockReader
from vfork.util import exit, format_usage

class Buffer(object):
	def __init__(self, input_iterator):
		self.content = ''
		self.input_iterator = input_iterator
		self.offset = 0
		self.eof = False
		
	def fill(self, limit):
		while not self.eof and len(self.content) < limit:
			try:
				self.content += self.input_iterator.next()
			except StopIteration:
				self.eof = True
				break
	
	def consume(self, size):
		if size > len(self.content):
			size = len(self.content)
		
		self.offset += size
		self.content = self.content[size:]

def compare_buffers(c1, c2, ignore_case, ignore_masked):
	for i in xrange(size):
		if c1[i] != c2[i]:
			if ignore_masked and (c1[i] in 'acgtN' or c2[i] in 'acgtN'):
				continue
			else:
				assert buf1.offset == buf2.offset
				return -(buf1.offset + i)
	
	return size

def main():
	parser = OptionParser(usage=format_usage('''
		%prog FASTA1 FASTA2
		
		Reads two files holding a *single* FASTA block
		and compares their content ignoring masked regions.
	'''))
	parser.add_option('-c', '--ignore-case', dest='ignore_case', action='store_true', default=False, help='ignore the case of the sequences')
	options, args = parser.parse_args()
	
	if len(args) != 2:
		exit('Unexpected argument number.')
	
	reader1 = SingleBlockReader(args[0], force_lower=options.ignore_case)
	reader2 = SingleBlockReader(args[1], force_lower=options.ignore_case)
	
	if reader1.size != reader2.size:
		exit('Content size differs: %d vs %d.' % (reader1.size, reader2.size))
	
	ignore_chrs = 'acgtnN'
	block_size = 4096
	for i in xrange(0, reader1.size, block_size):
		chunk1 = reader1[i:i+block_size]
		chunk2 = reader2[i:i+block_size]
		
		for j in xrange(len(chunk1)):
			if chunk1[j] != chunk2[j]:
				if not (chunk1[j] in ignore_chrs or chunk2[j] in ignore_chrs):
					exit('Mistmatch at offset %d.' % (i+j,))

if __name__ == '__main__':
	main()
