#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;

$SIG{__WARN__} = sub {die @_};

my $usage="$0 'PERL_CODE' file.fa
run the PERL_CODE indipendently for each fasta block.
The following variables are in the scope of PERL_CODE:
\$header      the header string of the block
\$id	      the ID portion of the header
\$block       the entire block
\@rows        each element of \@rows contains the a row string
\@table       each element of \@table contains the reference of the array
              produced by split /\\t/ a single row of te block (without header).
";

my $help=0;
GetOptions (
	'h|help' => \$help,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

my $perl_code = shift;
die($usage) if !$perl_code;

my @table=();
my @rows=();
my $block="";
my $header=undef;
my $id=undef;
while(<>){
	chomp;
	next if length==0 ;
	if(/^>/){
		my $post_header=$_;
		eval $perl_code;
		$header=$post_header;
		$header=~m/^>(\S+)/;
		$id=$1;
		@table=();
		@rows=();
		$block = "";
		next;
	}
	my @F=split /\t/,$_,-1;
	push @table, \@F;
	push @rows, $_;
	$block.=$_."\n";
}

eval($perl_code);
warn $@ if $@;
