#ifndef INCLUDE_HISTOGRAM_H
#define INCLUDE_HISTOGRAM_H


#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <tnt.h>

using namespace std;
using namespace TNT;

class Histogram2d
{
public:

	Histogram2d(float xmin, float xmax, int xbin_n, bool xl, float ymin, float ymax, int ybin_n, bool yl);
	~Histogram2d(){};
	void add(float fx, float fy);
	void print();

   private:
   	Array2D<int> count;
	float xmin;
	float xmax;
	int xbin_n;
	bool xl;
	float xtot_span;
	float xspan;
	float ymin;
	float ymax;
	int ybin_n;
	bool yl;
	float ytot_span;
	float yspan;
};

#endif
