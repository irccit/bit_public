#include "Histogram.h"
#include <math.h>

Histogram::Histogram(float min,float max, int bin_n, bool l){
	this->count = new int[bin_n];
	this->l = l;
	if (min >= max) {
		cerr<<"error: Histogram::Histogram: min >= max"<<endl;
		exit(-1);
	}
	if (this->l){
		if (min <= 0) {
			cerr<<"error: Histogram::Histogram: min <= 0"<<endl;
			exit(-1);
		}
		this->min = log10(min);
		this->max = log10(max);
	} else {
		this->min = min;
		this->max = max;
	}

	this->tot_span = this->max-this->min;
	this->span = this->tot_span/bin_n;
	this->bin_n = bin_n;
		
}

void Histogram::add(float f){
	int bin;
	if (this->l){
		if (f <= 0) {
			cerr<<"error: Histogram::add: f <= 0"<<endl;
			exit(-1);
		}
		f = log10(f);
	}
	if(f!=this->max){
		bin = (int)((f - this->min) / this->span);
	}else{
		bin = bin_n - 1;
	}
	this->count[bin]++;
}

void Histogram::print(){
	for(int i=0; i<this->bin_n; i++){
		float left=i * this->span + this->min;
		if (this->l){
			left=exp(left * log(10));
		}
		cout << left << "\t" << this->count[i] << endl; 
	}
}

