#!/usr/bin/env python
#
# Copyright 2016 Paolo Martini <paolo.cavei@gmail.com>

from argparse import FileType
from csv import reader
from sys import stdin
from vfork.util import ArgumentParser, ignore_broken_pipe


def main():
  args = parse_args()

  csv = reader(args.csv, quotechar=args.qchar)
  for line in csv:
    print '\t'.join(line)

def parse_args():
  parser = ArgumentParser(description='''
    Transforms a comma separated values file in tab separated values file.
  ''')

  parser.add_argument('csv', metavar='CSV', nargs='?',
                      type=FileType('r'), default=stdin,
                      help='input CSV file')

  parser.add_argument('-q' ,'--quote-character', dest='qchar', default='"',
                      help='quote charater to use (default: \'%(default)s)\'')

  return parser.parse_args()


if __name__ == '__main__':
  ignore_broken_pipe(main)
