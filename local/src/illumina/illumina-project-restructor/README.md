This program restructures your illumina output.

For a more pleasant reading, please open the file from here: https://bitbucket.org/alexandre-nadin/illumina-project-restructor/src

---

## Table of Contents

* [INTRODUCTION](https://bitbucket.org/alexandre-nadin/illumina-project-restructor#markdown-header-introduction)
* [OVERVIEW](https://bitbucket.org/alexandre-nadin/illumina-project-restructor#markdown-header-overview)
* [COMMANDS](https://bitbucket.org/alexandre-nadin/illumina-project-restructor#markdown-header-commands)
* [TESTING TOOL](https://bitbucket.org/alexandre-nadin/illumina-project-restructor#markdown-header-testing-tool) 

## INTRODUCTION

This program takes the file name of the working folder, searches for its matching sample sheet in
 a specified folder (default /lustre1/SampleSheets) and performs thorough checks on the validity of your data based on it.
 
 It then splits fastq files in relevant sample folders, and rename folders.
 
 Settings can be changed in a configuration file.
 
 
## OVERVIEW

```
#!bash
$ qsub -I
$ cd /lustre1/tools/libexec/illumina-project-restructor/scripts

# Help for launch-py
$ ./launch-py -h  

# help for program restruct-illumina-projects.py
$ ./launch-py -p restruct-illumina-projects.py -h  

# Dry run / test project's conformity. Uses the sample sheet "run_IEM.csv"
$ ./launch-py -p restruct-illumina-projects.py /your/run/folder

# Uses the old version of sample sheet "run.csv"
$ ./launch-py -p restruct-illumina-projects.py /your/run/folder -o

# Execute the program.
$ ./launch-py -p restruct-illumina-projects.py /your/run/folder [-o] -x

```

#### RULES

First of all, projects folders are located in a a folder corresponding to the run. This folder
contains others corresponding to projects.

The sample sheet is a csv file named after the run folder. It contains informations regarding 
each sample for each project.

The input folder of the script is the run folder. It is not a project folder, which is expected
to be inside of this run folder.

The sample sheet can have any metadata, like [Data], or [Any other metadata].
 
The sample sheet will be parsed and the metadata [Data] will be searched for.

Data are comprised of one header followed by samples' informations. 

The header cannot hold empty fields between comas.

Each sample must have the same number of fields than the header.

The order of each field does not matter. But keep in mind it might of your next tools 
(bpipe-config) 

Each sample must have a project folder and a fastq file named after the Sample_Id.


#### CONFIGURATION

The user can generate a configuration file, allowing to change parameters such as the
number of chunks a fastq file should be splitted into, the available fields in the sample 
sheet, the path to the sample sheets folder, and much more.


## COMMANDS


#### THE LAUNCHER


launch-py is a bash script that launches python scripts, as the name suggests. It will take care of variable environments such as the python version to use. It also creates a directory in your home folder that is named after the 
name of the project being launched. Inside of this folder will be saved each one of your commands called with this launcher. For instance, the command above will create the folder "illumina-project-restructor", and save a file named "YY-MM-DD_hh:mm:ss_restruct-illumina-projects.output". 

Note: you can simply launch your python script without the launcher. But you will have to take care of your environment yourself, such as the python version to use.

Access the launcher's help:

```
#!bash
$ ./launch-py -h
```


#### RESTRUCTURING

The main program for restructuring is located in scripts/restruct-illumina-project.py
The program is launched with the script . 

You will have to enter an interactive session first:

```
#!bash
$ qsub -I
```


Then call the program with the launcher "launch-py":


```
#!bash
$ cd /lustre1/tools/libexec/illumina-project-restructor/scripts
$ ./launch-py -h
```

Access the scripts' help: 
```
#!bash
$ ./launch-py restruct-illumina-projects.py -h
```


Next step you are interested in is the processing of a folder:

```
#!bash
$ ./launch-py restruct-illumina-projects.py /your/run/folder
```

Note again that the folder to process is not a project folder but its parent, the run folder, which may contain several projects. Please refere to
the "Rules" section from the [INTRODUCTION](https://bitbucket.org/alexandre-nadin/illumina-project-restructor#markdown-header-introduction)

Once the command has been launched, you will see that checks are being performed. If your 
run folder along with the project folders specified in the sample sheet are valid, only then the restructuring can take place. For that you have to 
specify the --execute argument (or -x for short):

```
#!bash
$ ./launch-py restruct-illumina-projects.py /your/run/folder -x
```
			
It is also possible to specify the --oldSample argument (or -o), to tell the program to look for an 
old version of the SampleSheet. If your RUN is named "RUN_Y", the new sample sheet in the
sample sheets folder should be named "RUN_Y_IEM.csv". However if the argument -o is specified, 
the old sample sheet model named "RUN_Y.csv" will be fetched.


Note: it is advisable not to use the --execute (or -x) argument at first, to ensure your folder structure meet the requirements. Even though the execution won't happen if something goes wrong, you may have second thoughts after looking at the checks begin performed.

It is possible that some files fail at being split. It is most likely a result of PBS losing track of jobs. The script will try to submit new jobs to process the remaining files. Indeed the script includes a tracking system allowing to know the state of a sample file. If at the end of the script some sample files could not be split, you can relaunch the command another time, the script will recognize the unprocessed files and try to process only these ones.


#### CONFIGURATION FILE
As mentioned in the introduction, it is possible to generate a configuration file:
```
#!bash
$ ./launch-py restruct-illumina-projects.py your/folder -c
```

The configuration file will be generated in you/folder/rip.cfg by default. There you can access all the default parameterizable variables. Each time the argument -c is specified, a new configuration file will be generated, OVERRIDING the previous one. Be sure to save your own configuration file then.


#### CANCELING THE RESTRUCTURING
By default, split sample files will NOT be removed. Canceling the restructuring will do the following:
 - recursive removal of each sample directory "Sample_*". Make sure you really don't need the content.
 - Rename the split files to their original extensions (.fastq.gz)
 - Rename each project folder, removing the "Projet_" tag.
 The process is fast, and irreversible. Before using it, make sure you don't need the new sample folders' content. Make sure that your original split files have not been removed.

The canceling is done by launching the following bash script:

```
#!bash
$ ./reset-illumina-projects.sh your/run/folder
```

Once again, this command might be useful yet dangerous as well. You know what you are doing.


## TESTING TOOL

In the scripts folder, there are other useful scripts, one of which is named lcp and stands for "Light CoPy". With it you can make empty copies of any file and folder, mimicking its whole architecture. Hence it saves time while copying the folder. Just run the following command:

```
#!bash
$ cd /lustre1/tools/libexec/illumina-project-restructor/scripts/
$ lcp src dest
```

The 'lcp' command will copy 'src' in 'dest', without any of the files' content. In other 
words, each one of your copied file will be about 0 bytes. You can however specify which type of file you want to be copied in full:

```
#!bash
$ lcp src dest --full-copy csv pdf
```

This command will copy empty files, except for those with 'csv' or 'pdf' extensionm which will be fully copied.

This tools is pretty convenient if one wants to control the end result of the folder
restructuring since only the folder structure matters here.

Then after using 'lcp' to the destination folder 'dest':

```
#!bash
$ ./launch-py restruct-illumina-projects.py dest
$ ./launch-py restruct-illumina-projects.py dest -x
```