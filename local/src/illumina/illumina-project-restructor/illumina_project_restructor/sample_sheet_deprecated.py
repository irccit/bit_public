__author__ = 'alexandre-nadin'

"""
This module was a first sketch before the implementation of the metadata_reader module.
It is presently deprecated. Please use the new module sample_sheet.py instead.
"""
import sys
import csv
import re


class SampleSheet:

    SAMPLE_SHEET_NAME = "SampleSheet"
    SAMPLE_SHEET_EXTENSION = ".csv"
    SAMPLE_SHEET_SEPARATOR = ","
    DATA_HEADER = "Data"
    METADATA_PATTERN = re.compile(r'\s*\[\s*((\w*\s*\w*)+)\s*\]\s*$')
    DATA_HEADER_PATTERN = re.compile(r'\s*\[\s*' + DATA_HEADER + r'\s*\]\s*', re.IGNORECASE)

    def __init__(self, csvFile, separator=',', oldModel=False):
        self._oldModel = oldModel
        self.sampleSheetPath = csvFile
        self.validSampleSheet = True
        self.separator = separator
        self._data_header = "" # Contains the name of all the available fields
        self._data_rows = []  # Contains the data of each sample

        # self.illuminaSamples = []
        self._rowErrors = []
        self._headerErrors = []

        print "\n[READING SAMPLE SHEET] ", csvFile
        try:
            with open(self.sampleSheetPath, 'rb') as file:
                csvReader = csv.reader(file, delimiter=self.separator)

                # Check if there is metadata to be stripped before parsing.
                if self.hasMetadata(csvReader):
                    file.seek(0)
                    self.parseReaderWithMetadata(csvReader)
                else:
                    file.seek(0)
                    self.parseData(csvReader)

            self.printSummary()
        except IOError as e:
            print "\n[ERROR READING FILE] Sample Sheet ", self.sampleSheetPath, ": \n\t-> ", e.strerror


    def goToMetadata(self, csvReader, metadataPattern=METADATA_PATTERN):
        """
        Move the reader's cursor to the next metadata pattern in the reader.
        Assumes the metadata is located at the beginning of a line.

        :param metadataPattern: pattern to be matched
        :param csvReader: the csv reader
        :return: an array containing the data found before the pattern.
        """
        data = []
        for row in csvReader:
            # Check if the line contains something
            if len(row) > 0:
                match = metadataPattern.match(row[0])
                if match:
                    print "Metadata matched: .", match.group(), "."
                    return data
                data.append(row)
        return data


    def getDataFromMetadata(self, csvReader, metadataPattern=METADATA_PATTERN):
        """
        Gets the data corresponding to the [metadata_pattern] specified in a csvReader.
        :param metadataPattern:
        :param csvReader:
        :return: an array of rows containing the data. None if no data was found.
        """
        if not self.goToMetadata(csvReader, metadataPattern):
            return None
        return self.goToMetadata(csvReader)


    def parseReaderWithMetadata(self, csvReader):
        """
        Parses the reader.
        :param csvReader:
        :return: 1 if some data was found, 0 otherwise.
        """
        print "\n[PARSING WITH METADATA] This file has metadata."
        data = self.getDataFromMetadata(csvReader, self.DATA_HEADER_PATTERN)
        if data is None or len(data) == 0:
            self.validSampleSheet = False
            return 0
        self.parseData(data)
        return 1


    def parseData(self, csvReader):
        """
        Parse a csv reader. Assumes there is NO metadata.

        :param csvReader: The csv reader containing only the data fields and its raw data.
        :return:
        """
        for row in csvReader:
            # Initiate the data header.
            if self._data_header.__len__() == 0:
                self._data_header = row
                # Check there are no empty field
                empty_fields = 0
                for r in row:
                    if str(r).strip(" ").__len__() == 0:
                        self.validSampleSheet = False
                        empty_fields += 1
                if empty_fields > 0:
                    self._headerErrors.append("The header has %d empty field(s)." %(empty_fields))
            else:
                # Check if number of fields correspond
                if self._data_header.__len__() == len(row):
                    self._data_rows.append(row)
                else:
                    self.validSampleSheet = False
                    self._rowErrors.append(row)


    def hasMetadata(self, csvReader):
        """
        Looks for existing metadata which looks like "[Meta_data]".

        :param csvReader: the csv reader
        :returns: True as soon as a metadata header has been found.
        """
        for row in csvReader:
            matcher = SampleSheet.METADATA_PATTERN.match(row[0])
            if matcher:
                return True
        return False


    def isValidSampleSheet(self):
        return self.validSampleSheet


    def printSummary(self):
        if self._data_header.__len__() == 0:
            print "\n[WARNING] No data found"
        if self.isValidSampleSheet():
            print "\n[SAMPLE SHEET VALIDATED] Number of lines found: ", self._data_rows.__len__()
        else:
            print "\n[INVALID SAMPLE SHEET]"
            if self._headerErrors.__len__() > 0:
                print "There are errors with the header: "
                for e in self._headerErrors:
                    print "\t* ", e
            if self._rowErrors.__len__() > 0:
                print "Those lines don't have the same number of field as the header: "
                for e in self._rowErrors:
                    print "\t* ", ','.join(e), "(", len(e), "/", self._data_header.__len__(), ")"


    def getDataHeader(self):
        return self._data_header

    def getDataRows(self):
        return self._data_rows

def runTests(argv=None):
    ## Test pattern matching
    # testPatternMatching()

    # raw_input("Press enter to continue")
    ## Test different Sample sheets
    path = "/Users/alexandre-nadin/dev/illumina-project-restructor/tests/lustre1/SampleSheets/"
    fileNames = [
        "test-no-metadata.csv"
        ,"test-with-metadata-end.csv"
        ,"test-with-metadata-between.csv"
        ,"test-with-metadata-no-data.csv"
        ,"test-with-metadata-between-emtpy.csv"
        , "151211_SN859_0250_AH7T57BCXX.csv"
        , "151211_SN859_0250_AH7T57BCXX_IEM.csv"

    ]

    # sheet = SampleSheet(path + fileNames[5])
    from tools.parameterization.metadata_reader import MetadataReader
    mr = MetadataReader(path + fileNames[6])
    print "has metadata? ", mr.hasMetadata()
    for line in mr.getRawDataFromMd("Data"):
        print line
    # print "\nHeader:\n", sheet.getDataHeader()
    # print "\nRows:\n"
    # for row in sheet.getDataRows():
    #     print row

    # for fileName in fileNames:
    #     print "\n" + 20 * "*"
    #     sheet = SampleSheet(path + fileName)


def testPatternMatching():
    """
    Test matching of regular expression
    """
    print "\n[TEST PATTERN MATCHING]"
    testMatchPatter(SampleSheet.METADATA_PATTERN, r' [  word_in_bracket ]   ')
    testMatchPatter(SampleSheet.METADATA_PATTERN, r' before_bracket  [  word1 word2 ]  ')
    testMatchPatter(SampleSheet.METADATA_PATTERN, r'   [  word1 word2 ]  after_bracket')
    testMatchPatter(SampleSheet.METADATA_PATTERN, r'  word1 word2 word3 ]   ')
    testMatchPatter(SampleSheet.METADATA_PATTERN, r'  word1 word2 word3')
    testMatchPatter(SampleSheet.METADATA_PATTERN, r' [  word1 word2 word3 ]   ')
    testMatchPatter(SampleSheet.DATA_HEADER_PATTERN, r' [  dAtA ]   ')
    testMatchPatter(SampleSheet.DATA_HEADER_PATTERN, r' [  dAtA1 data2 ]   ')
    testMatchPatter(SampleSheet.DATA_HEADER_PATTERN, r' [  da ta ]   ')


def testMatchPatter(pattern, content):
    match = pattern.match(content)
    print "Content: .", content, ". : %s" %("matched" if match is not None and match.group() is not None\
                                            else "not matched.")


def main(argv=None):
    runTests(argv)
    # test(path + "151104_SN859_0242_AHTKFYADXX_IEM.csv")


if __name__ == "__main__":
    print "[SampleSheet] main function"
    sys.exit(main(sys.argv))