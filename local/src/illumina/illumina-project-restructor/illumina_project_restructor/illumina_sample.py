__author__ = 'alexandre-nadin'

import sys
from illumina_config import IlluminaConfig
# from sample_file import get_sample_file_name, SampleFile
import sample_file
from tracking import file_tracking
from log import LogTracker


class IlluminaSample:
    """
    An Illumina Sample is created with the data header and one data row from a Sample Sheet.
    """

    def __init__(self, header_fields, row_fields):
        self._header_fields = header_fields[:]
        self._row_fields = row_fields[:]
        self._sample_dict = {}
        self._valid_sample = True
        self._sample_errors = []  # Contains all encountered errors
        self._file_names = []  # Contains all the file names bearing the same sample id
        self._sample_files = []  # Contains instances of SampleFile.
        self._log_tracker = LogTracker()

        if IlluminaConfig.TAKE_OLD_SAMPLE_SHEET_MODEL:
            from illumina_config import SampleSheetFieldsOld as ssFields
        else:
            from illumina_config import SampleSheetFieldsNew as ssFields

        self._ssFields = ssFields

        ## Check validity of the sample
        if not self._header_fields.__len__() == self._row_fields.__len__():
            self._sample_errors.append("Number of fields don't match:\n\tsample header has "
                                         + str(self._header_fields.__len__()) + " fields: " + ','.join(self._header_fields)
                                         + "\n\tsample row has " + str(self._row_fields.__len__()) + " fields: "
                                         + ','.join(self._row_fields)+ "")
            self._valid_sample = False
            return

        ## Create the dictionnary
        # self._sample_dict = dict(zip(map(str.strip, self._header_fields), map(str.strip, self._row_fields)))
        self._sample_dict = dict(zip(self._header_fields, self._row_fields))

        # self.checkMandatoryFields()


    def add_file_name(self, fileName):
            self._file_names.append(fileName)


    def track_sample_file(self, file_name, sample_id):
        """
        Records a sample file fragment to track.
        :param file_name:
        :param destination:
        :return:
        """

        sample_file_name = sample_file.SampleFields.assess_sample_file_name(file_name)
        is_registered = False
        new_sf = sample_file.SampleFile(file_name, sample_id=sample_id)

        for sf in self._sample_files:
            if sf.get_sample_file_name() == sample_file_name:
                # print "  sample file name matches"
                if sf.has_fragment_file(file_name):
                    # print "  but file has already been registered"
                    is_registered = True
                    break
                else:
                    sf.add_fragment_file(file_name)
                    is_registered = True
                    break
            else:
                # print "  sample file name does not exist"
                continue

        if not is_registered:
            # new_sf = sample_file.SampleFile(file_name, sample_file_name, sample_id=sample_id)
            # new_sf.add_fragment_file(file_name)
            self._sample_files.append(new_sf)
            # print "  added file to fragments"


    def add_field(self, value_header, value_row, position=None):
        """
        Adds a  field to the data header, as well as its corresponding value in to the data row.
        :param value_header: the value to add in the header
        :param value_row: the value to add in the row
        :return:
        """
        if self._header_fields:
            self.add_to_list(self._header_fields, value_header, position)
        if self._row_fields:
            self.add_to_list(self._row_fields, value_row, position)
        self._sample_dict = dict(zip(self._header_fields, self._row_fields))

    def add_to_list(self, alist, value, position=None):
        if position is not None:
            alist.insert(position, value)
        else:
            alist.append(value)


    def check_mandatory_fields(self):
        """
        Checks whether the sample has all the mandatory fields. Invalidate the sample if not.
        """
        for field in self._ssFields.MANDATORY_FIELDS:
            eval_field = self.evaluate(str("self._ssFields") + "." + str(field))
            print "  field: ", field, " vs evaluated: ", eval_field
            if eval_field is None or eval_field not in self._sample_dict.keys():
                self._sample_errors.append("Mandatory field does not exist: " + field)
                self._valid_sample = False
            elif str(self._sample_dict[eval_field]).strip(" ").__len__() == 0:
                self._sample_errors.append("Mandatory Field is empty: " + field)
                self._valid_sample = False


    def get_sample_files(self):
        return self._sample_files


    def evaluate(self, content):
        """
        Evaluates expressions. Used to make sure a class or a class attribute exists.
        :param content:
        :return:
        """
        try:
            return eval(str(content))
        except NameError as ne:
            return None
        except AttributeError as ae:
            return None

    def get_sample_row(self):
        return self._row_fields

    def get_sample_header(self):
        return self._header_fields

    def get_file_names(self):
        return self._file_names

    def get_sample_id(self):
        return self._sample_dict[self._ssFields.F_SAMPLE_ID]

    def is_valid_sample(self):
        return self._valid_sample

    def get_sample_errors(self):
        return self._sample_errors

    def get_sample_dict(self):
        return self._sample_dict

    def get_log_tracker(self):
        ## TODO Inherit the Logging module and implement the log tracker -> simpler and multiprocess safe.
        common_log = LogTracker()
        common_log.merge_logs(self._log_tracker)
        ## Merge all sub logs
        for sf in self._sample_files:
            common_log.merge_logs(sf.get_log_tracker())
        return common_log



def main(argv=None):
    ar= ["r ", "  t ", " r \n \n"]
    print map(str.strip, ar)


if __name__ == "__main__":
    print "[IlluminaSample] main function"
    sys.exit(main(sys.argv))
