__author__ = 'Alexandre Nadin'

from tools.ky_output import kmessage, coloured_output as colout

class LogTracker:
    def __init__(self):
        self._errors = []
        self._notifs = []


    def merge_logs(self, log_tracker):
        self._errors += log_tracker.get_errors()
        self._notifs += log_tracker.get_notifs()


    def append_errors(self, error_list):
        self._errors += error_list

    def append_error(self, msg):
        self._errors.append(str(msg))

    def append_notifs(self, notif_list):
        self._notifs += notif_list

    def append_notif(self, msg):
        self._notifs.append(str(msg))

    def reset_logs(self):
        self._errors = []
        self._notifs = []

    def get_errors(self):
        return self._errors

    def get_notifs(self):
        return self._notifs

    def has_errors(self):
        return self._errors.__len__() > 0

    def has_notifications(self):
        return self._notifs.__len__() > 0

    def get_summaries(self):
        summaries = []
        summaries.append(self.get_summary(kmessage.MessageType.NOTICE, self._notifs))
        summaries.append(self.get_summary(kmessage.MessageType.ERROR, self._errors))
        return summaries

    def get_summary(self, msg_type, array2Summarize):
        msg = ""
        if array2Summarize and array2Summarize.__len__() > 0:
            title = ""
            if msg_type == kmessage.MessageType.ERROR:
                title = "errors"
            elif msg_type == kmessage.MessageType.NOTICE:
                title = "notifications"
            elif msg_type == kmessage.MessageType.WARNING:
                title = "warnings"
            else:
                title = "summary"

            msg = colout.message("\n[" + str(title).upper() + "]", msg_type)
            for elem in array2Summarize:
                msg += colout.message("\n * ", msg_type) + str(elem)

        return msg


    def get_summaries(self):
        return self.get_summary(kmessage.MessageType.NOTICE, self._notifs) + "\n" \
                + self.get_summary(kmessage.MessageType.ERROR, self._errors)


if __name__ == "__main__":
    sum = LogTracker()
    sum2 = LogTracker()
    sum.merge_logs(sum2)
