__author__ = 'Alexandre Nadin'

import os

from illumina_config import IlluminaConfig
from log import LogTracker
from tools import string_utils
from tools.ky_output import coloured_output as colout
from tools.pipeline.pipeline import PipelineState


class SampleFields:
    """
    Samples' Field Names, based on Illumina specifications.

    """
    ILLUMINA_FILE_CONVENTION = "<Sample_ID>_<BarCode>_<LaneNumber>_<ReadNumber>_<SerialNumber>.fastq.gz"

    SAMPLE_ID, BAR_CODE, LANE_NB, READ_NB, FRAG_NB, EXT = 0, 1, 2, 3, 4, 5
    SEPARATOR = '_'

    @classmethod
    def assess_sample_file_fragment_name(cls, file_name):
        """

        :param file_name:
        :return: the name of the sample file's fragment. Last field should be the fragment number.
        """
        return cls.assess_sample_file_fields(file_name, cls.SAMPLE_ID, cls.FRAG_NB)

    @classmethod
    def assess_sample_file_name(cls, file_name):
        """

        :param file_name:
        :return: the sample name common to all the sample file's fragments. Last field should be the read number.
        """
        return cls.assess_sample_file_fields(file_name, cls.SAMPLE_ID, cls.READ_NB)

    @classmethod
    def assess_sample_file_fields(cls, file_name, from_f_field, to_f_field):
        """
        Tries to guess the file fields within the specified SampleFile ranges. The last field is included.
        :param file_name:
        :param from_f_field:
        :param to_f_field:
        :return:
        """
        file_fields = cls.split_file_fields(os.path.basename(file_name))
        last_included_position = to_f_field+1 if to_f_field < file_fields.__len__() else file_fields.__len__()
        return None if None in file_fields \
            else cls.SEPARATOR.join(file_fields[from_f_field:last_included_position])

    @classmethod
    def split_file_fields(cls, file_name):
        """
        Splits a sample file basename into a list, according to illumina sample fields.
        :param file_name:
        :return:
        """
        basename = os.path.basename(file_name)
        ## First get the extension.
        rest, ext = basename.split(os.extsep, 1)
        ext = os.extsep + ext  # Restore the dot before extension.

        ## Get the other fields.
        fields = rest.split(SampleFields.SEPARATOR)
        frag_nb = None if fields.__len__() == 0 else fields.pop()

        read_nb = None if fields.__len__() == 0 else fields.pop()
        lane_nb = None if fields.__len__() == 0 else fields.pop()
        bar_code = None if fields.__len__() == 0 else fields.pop()

        sample_id = '' if fields.__len__() == 0 else cls.SEPARATOR.join(fields)
        return [sample_id, bar_code, lane_nb, read_nb, frag_nb, ext]

class Step:
    INIT = ".fastq.gz"
    SPLITTING = INIT + ".splitting"
    SPLIT = INIT + ".split"


class SampleFile:

    def __init__(self, file_path, sample_id):
        self._project_name = os.path.basename(os.path.dirname(os.path.abspath(file_path)))  ## The original project path.
        self._project_name_old = IlluminaConfig.PROJECT_FOLDER_TAG + self._project_name
        self._run_path = os.path.dirname(os.path.dirname(os.path.abspath(file_path)))  ## The Run folder
        self._sample_id = sample_id

        self._fragment_files = []  # Contains basenames of a sample file's fragments.
        self._fragment_names = []  # Contains only fragment names, without extension.
        self._split_files = []  # Contains basenames of sample file's splits.
        self._sample_file_name = SampleFields.assess_sample_file_name(file_path)
        self._current_step = Step.SPLIT  # By default.
        self._log_tracker = LogTracker()
        self.add_fragment_file(file_path)
        self.check_validity()

    def check_validity(self):
        if self._sample_file_name is None:
            msg = string_utils.stringify("File names for sample", self._sample_id, "in project", self._project_name
                                         , "seem to violate the illumina file name conventions"
                                         , "(" + SampleFields.ILLUMINA_FILE_CONVENTION + ")")
            self._log_tracker.append_error(msg)

    def reset_step(self):
        """
        Resets files'extensions.
        Removes the files eventually produced by a previous split.
        :return:
        """
        self.remove_processed_fragments()
        return self.update_step(Step.INIT)

    def remove_processed_fragments(self):
        """
        Removes the files with in the sample destination file that match the sample file name and the initial file
        extension.
        :return:
        """
        # print "try to remove file matching", self._sample_file_name, "*", Step.INIT
        sample_dir = self.get_sample_destination()
        # print "\nremoving processed frags..."
        if os.path.isdir(sample_dir):
            for f in os.listdir(sample_dir):
                # print "  test ", sample_dir, f
                if os.path.isfile(os.path.join(sample_dir, f)) and f.startswith(self._sample_file_name)\
                        and f.endswith(Step.INIT):
                    # print "  recognized ", f
                    os.remove(os.path.join(sample_dir, f))
                    print "removed file", os.path.join(sample_dir, f)

    def update_step(self, step):
        """
        Updates the fragment files' extension, before updating the general step.
        :param step:
        :return:
        """
        # print "Update sample", self._sample_file_name, "file to step ." + step + "."
        if self.replace_fragments_extensions(step) == PipelineState.SUCCESS:
            self._current_step = step
            return PipelineState.SUCCESS
        return PipelineState.FAIL

    def harmonize_step(self, file_name):
        """
        Initializes the current step based on file extension.
        Mainly used to harmonize the current step between fragment files, in case they differ.
        :param file_name:
        :return:
        """
        if not os.path.basename(file_name).endswith(Step.SPLIT):
            self._current_step = Step.INIT


    def add_fragment_file(self, file_name):
        self._fragment_files.append(os.path.basename(file_name))
        self._fragment_names.append(SampleFields.assess_sample_file_fragment_name(file_name))
        self.harmonize_step(file_name)  # Initializes the current step.


    def replace_fragments_extensions(self, new_ext):
        # self.replace_files_extension(self._fragment_names, self._current_step, new_ext)
        return self.replace_files_extension(self._fragment_files, new_ext)

    def replace_files_extension(self, frag_list, new_ext):

        # print "replace frags extensions -> ." + new_ext + ".", "in frags", self._fragment_files

        for index, frag_name in enumerate(frag_list):
            # os.rename(os.path.join(self._run_path, self._project_name_old, f + current_step)
            #           , os.path.join(self._run_path, self._project_name_old, f + new_ext))


            from_path = os.path.join(self._run_path, self._project_name_old, os.path.basename(frag_name))
            if from_path.endswith(str(new_ext)):
                continue

            sample_fragment_name = SampleFields.assess_sample_file_fragment_name(from_path)

            to_path = os.path.join(self._run_path, self._project_name_old
                                   , sample_fragment_name + new_ext)
            # print "   ", from_path, "\n ->", to_path
            try:
                os.rename(from_path, to_path)
                frag_list[index] = os.path.basename(to_path)
                # os.rename(os.path.join(self._orig_project, f + current_step)
                #           , os.path.join(self._orig_project, f + new_ext))
            except Exception, err:
                print colout.error(string_utils.stringify("ERROR renaming:", err.message, from_path, "\n\t->", to_path))
                return PipelineState.FAIL
        return PipelineState.SUCCESS


    def has_fragment_file(self, file_name):
        if os.path.basename(file_name) in self._fragment_files:
            return True
        return False

    def get_orig_project(self):
        return os.path.join(self._run_path, self._project_name)

    def get_orig_run_path(self):
        return self._run_path

    def get_sample_id(self):
        return self._sample_id

    def get_fragment_files(self):
        return self._fragment_files

    def get_fragment_names(self):
        return self._fragment_names

    def get_fragment_files_paths(self):
        paths = []
        for frag in self._fragment_names:
            paths.append(os.path.join(self._run_path, self._project_name_old, frag + self._current_step))
        return paths

    def get_sample_destination(self):
        return os.path.join(self._run_path, self._project_name_old, IlluminaConfig.SAMPLE_FOLDER_TAG + self._sample_id)

    def get_sample_file_name(self):
        return self._sample_file_name

    def get_current_step(self):
        return self._current_step

    def get_log_tracker(self):
        return self._log_tracker


def check_fields():
    pass
    # check no empty field
    # Check sampleid = original sample id


def tests():
    sample_files = []
    dir = "/Users/alexandre-nadin/dev/tests/runs/151113_SN859_0245_AHCWNWBCXX/NousCom_266_MouseExome_RNA"
    files = ["2R_bis_S1_L001_R1_001.fastq.gz", "2R_bis_S1_L001_R1_001.fastq.gz"
        , "2R_bis_S1_L001_R1_002.fastq.gz", "2r_S3_L001_R1_001.fastq.gz"]

    # dir_files = os.listdir(os.path.join(dir))
    # sample_file_name = get_sample_file_name(files[0])
    # print sample_file_name
    # for f in dir_files:
    #     if f.startswith(sample_file_name):
    #         print "  ", f,

    # if True: return
    for f in files:

        print "Sorting file", f
        new_sf = SampleFile(os.path.join(dir, f), sample_id="Some ID")
        sample_file_name = SampleFields.assess_sample_file_name(f)
        is_registered = False
        for sf in sample_files:
            if sf.get_sample_file_name() == new_sf.get_sample_file_name():
                print "  sample file name matches"
                if f in sf.get_fragment_files():
                    print "  but file has already been registered"
                    is_registered = True
                    break
                else:
                    sf.add_fragment_file(f)
                    is_registered = True
                    break
            else:
                # print "  sample file name does nto exist"
                continue

        if not is_registered:
            sample_files.append(new_sf)
            print "  added file to gragments"

    # print "\n[check frag paths]"
    # for sf in sample_files:
    #     print "sample file: ", sf.get_sample_file_name()
    #     for fragp in sf.get_fragment_files_paths():
    #         print "  ", fragp
    # if True:
    #     return None

    print "\n[SUmmary fragments:]"
    for sf in sample_files:
        sf.update_step(Step.INIT)

        print "Sample File:", sf.get_sample_file_name()
        for frag in sf.get_fragment_files():
            print "  ", frag

    ## Rename sample fragments
    print "splitting..."
    for sf in sample_files:
        sf.update_step(Step.SPLITTING)

    import time
    time.sleep(5)

    ## Rename sample fragments
    print "split..."
    for sf in sample_files:
        sf.update_step(Step.SPLIT)
    time.sleep(5)

    ## Rename sample fragments
    for sf in sample_files:
        sf.update_step(Step.INIT)

    print "back to normal"

if __name__ == "__main__":
    tests()
