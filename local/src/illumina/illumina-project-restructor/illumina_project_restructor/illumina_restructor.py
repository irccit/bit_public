#!/usr/bin/env python

import sys
import os.path
import shutil
import time
import multiprocessing
from multiprocessing import Process, Queue, Manager

import argparse

from tools.file_utils import file_compression as fc, file_manipulation as fm
from tools import coercer
from tools.ky_output import coloured_output as colout
from tools.ky_output.progress_bar import ProgressBar
from tools.ky_output import klogger, kmessage
from tools import string_utils
from illumina_sample import IlluminaSample
from sample_sheet import SampleSheet
from illumina_config import IlluminaConfig, CompressedFiles, Configuration

import subprocess
from tools.ky_multiprocessing import function_processor as fp

from sample_file import *
from log import LogTracker

__author__ = 'Alexandre Nadin'



class IlluminaRunRestructor:

    def __init__(self, run_path, old_sample_sheet=False, sample_sheet=None):
        print colout.notice("\n[STARTING VALIDATION]")
        self._run_abs_path = fm.build_dir_path(os.path.abspath(run_path))
        self._run_base_name = os.path.basename(os.path.dirname(self._run_abs_path))
        self._sample_sheets_dir = None
        self._sample_sheet_path = sample_sheet
        self._sample_sheet_old_model = False if old_sample_sheet is False else True
        self._valid_run = True
        self._sample_sheet = None
        self._projects_dict = {}  # A dict of all project containing samples
        self.__projects_files = {}  # A dict which will store lists of files unprocessed yet.

        self._total_bytes_to_process = 0
        self._nb_files_to_process = 0
        self._nb_sample_files = 0

        self._log_tracker = LogTracker()

        if self._sample_sheet_old_model:
            from illumina_config import SampleSheetFieldsOld as ssFields
            IlluminaConfig.TAKE_OLD_SAMPLE_SHEET_MODEL = True
        else:
            from illumina_config import SampleSheetFieldsNew as ssFields
            IlluminaConfig.TAKE_OLD_SAMPLE_SHEET_MODEL = False
        self._ssFields = ssFields

        ## Check the sample sheet
        self.check_sample_sheet()
        if not self.is_valid_run():
            print "invalid"
            # self.printSummaries()
            return
        print "sample sheet ok"

        ## Check each sample and project
        print colout.notice("\n\n[CHECK PROJECTS AND SAMPLES]")
        self.check_folders_and_files()
        if not self.is_valid_run():
            # self.printSummaries()
            return

        # print "\n\n[SUMMARY]"
        tracked_files = []
        count_sample_file = 0
        count_sample_fragments = 0
        for proj in self._projects_dict:
            # print "\nProject: ", proj,
            for sample in self._projects_dict[proj]:
                # print "\n  Sample ID:", sample.get_sample_id(),
                for sf in sample.get_sample_files():
                    count_sample_file += 1
                    # print "\n    Sample File:", sf.get_sample_file_name(), "\tcurrent step:", sf.get_current_step()
                    for frag in sf.get_fragment_files():
                        count_sample_fragments += 1
                        # print "      ", frag, "->", sf.get_sample_destination() #os.path.basename(sf.get_sample_destination())
                    tracked_files += sample.get_sample_files()
        print colout.notice("=> " + str(count_sample_file) + " tracked sample files (" \
                            + str(count_sample_fragments) + " parts).")

        print colout.valid("\nProjects validated.")
        self._log_tracker.append_notif("Run folder seems to be valid.")
        self._log_tracker.append_notif(string_utils.stringify(count_sample_file, "sample files will be processed"
                                                              ,  "(" + str(self._nb_files_to_process), "parts)"
                                                              , "totalizing", fm.human_size(self._total_bytes_to_process))
                                       )


        ## Check disk space
        self.check_disk_space()
        if not self.is_valid_run():
            # self.printSummaries()
            return

        self._log_tracker.append_notif("To restructure your projects, please relaunch the command adding the "\
                            + " --execute/-x argument.")
        # self._log_tracker.append_notif("To split your fastq files afterwards, please combine the --execute/-x argument with "
        #                     + "--split_files/-s.")


    def check_disk_space(self):
        du = fm.disk_usage(self._run_abs_path)
        self._log_tracker.append_notif(string_utils.stringify("Disk usage information:\n"
                                       , fm.disk_usage(self._run_abs_path, formatted=True)))
        try:
            if du.free < fm.computer_size(IlluminaConfig.BLOCK_SPACE_MIN):
                self._log_tracker.append_error(string_utils.stringify("You have exceeded the maximal disk use of"
                                                                      , IlluminaConfig.BLOCK_SPACE_MIN
                                                                      , ". Make some space or change your threshold in the"
                                                                      , "configuration file, then and relaunch the program."
                                                                      ))
                self._valid_run = False
        except ValueError as ve:
            self._log_tracker.append_error(string_utils.stringify(
                 "The field IlluminaConfig.BLOCK_SPACE_MIN defined in your configuration file is not valid:"\
                 , "\"" + str(IlluminaConfig.BLOCK_SPACE_MIN) + "\""
                 , "Please specify a valid number of bytes and multiples (ex: 10, 10B, 14 GB, 14 Mb, etc)."
            ))
            self._valid_run = False

    def check_disk_space_old(self):
        """
        Checks whether the disk space has enough left to start the program.
        Compares the capacity value with the maximum given in the configuration file.
        :return:
        """
        import subprocess
        df = subprocess.Popen(["df", "-h", self._run_abs_path], stdout=subprocess.PIPE)
        df.wait()
        # df.stdout.close()
        df_output = df.communicate()[0]
        df_output_split = df_output.split("\n")
        print "df_output_split: ", df_output_split
        disk_space = self.get_disk_space_dict(df_output_split)
        self._log_tracker.append_notif("Disk usage information:\n" + str(df_output_split[0]) + "\n" + str(df_output_split[1]))
            # dict(zip(df_output_split[0].split(), df_output_split[1].split()))
        key_available = 'capacity'
        print "disk space keys: ", disk_space
        for k in disk_space.keys():
            if str(k).lower() in ["capacity", 'capac', 'cap', 'use', 'use%']:
                key_available = k
                print "found key:", k
                break
        print "key available: ", key_available
        if coercer.percentage2int(disk_space[key_available]) > coercer.percentage2int(IlluminaConfig.BLOCK_IF_DISK_USE_ABOVE):
            print "Not enough space"
            self._log_tracker.append_error("You have exceeded the maximal disk use of " + str(disk_space[key_available])
                                    + ". Make some space and relaunch the command.")
            self._valid_run = False

    def get_disk_space_dict(self, split_output):
        keys = split_output.pop(0)  ## Assumes the header fits on one line...
        print "keys: ", keys
        values = []
        for array_values in split_output:
            values += array_values
        print "values:", values
        return dict(zip(keys.split(), values.split()))

    def start_restruct_tracking(self):
        from illumina_project_restructor.restructor_tracking import Splitter, Restructer
        restructer = Restructer(self)
        restructer.restructurate()
        splitter = Splitter(self._projects_dict)
        return splitter.split()

    def start_splitting_files(self):
        """
        Splits the tracked fastq files.
        Checks the line counts of each file before splitting. Splits the file only if it is big enough.
        :return:
        """
        print "splitting files."
        ## Resets notifications
        self._log_tracker.reset_logs()

        ## Get all the tracked fastq files
        tracked_files = []
        fastq_files = []
        sample_files = None
        for proj in self._projects_dict:
            print "Proj:", proj
            for sample in self._projects_dict[proj]:
                print "  Sample: ", sample
                for sample_file in sample.get_sample_files():
                    print "      ", sample_file.get_restruct_path()
                sample_files = sample.get_sample_files()
                tracked_files += sample_files
                if sample_files is not None:
                    for f in sample_files:
                        fastq_files.append(f.get_restruct_path())

        print "splitting", fastq_files.__len__(), "files"

        ## Process al the files
        split_fastq_files(fastq_files, force_split=False)

    def start_restructure_projects(self):
        """
        Starts the restructuring of the RUN folder.
        There is a lock based on the validity of the RUN folder, which is set during the class instanciation when
        abnormalities occur. This lock prevents the restructuring to take place in case of remaining errors.
        I do not advise to remove it as it may prove difficult to cancel any modification taken in case of unexpected
        behaviour.
        """
        ttime = time.time()

        ## The lock: prevents to run the process as long as the project remains invalid.
        if not self.is_valid_run():
            self.get_summaries()
            print colout.warn("\nThis RUN folder is not valid. There might still be errors remaining. Please \
                    correct them and launch this program again.\n")
            return None

        self._log_tracker.reset_logs()
        print colout.notice("\n[STARTING RESTRUCTURING]")

        ## Copy raw sample sheet in the RUN folder
        print colout.notice(string_utils.stringify("\n[COPY SAMPLE SHEET] ", self._sample_sheet_path)
                            , "\n\t-> ", self._run_abs_path + SampleSheet.SAMPLE_SHEET_NAME + SampleSheet.SAMPLE_SHEET_EXTENSION)
        try:
            new_sample_sheet_name = self._run_abs_path + SampleSheet.SAMPLE_SHEET_NAME + SampleSheet.SAMPLE_SHEET_EXTENSION

            shutil.copy2(self._sample_sheet_path, new_sample_sheet_name)
            fm.change_file_permission(new_sample_sheet_name, IlluminaConfig.CREATED_FILES_PERMISSION, update_owner=True)

        except IOError as e:
            print colout.error(string_utils.stringify("\n[ERROR COPYING FILE] Sample Sheet ", self._sample_sheet_path
                                                      , ": \n\t-> ", str(e.strerror)))
            return None


        ## Process each project in separate process.
        queueProjects = Queue(maxsize=multiprocessing.cpu_count())
        queue_bytes_processed = Queue()
        projectProcessList = []

        ## Initiate the progress bar and the logger progress.
        pbar = ProgressBar(self._total_bytes_to_process)
        klogger.progress_queue = pbar.getProgress()

        ## Initialize the log for sub processed.
        manager = Manager()
        process_logger = manager.dict()
        process_logger[klogger.PROCESSED_BYTES] = 0
        process_logger[klogger.PROGRESS_BAR] = ""
        process_logger[klogger.TOTAL_BYTES] = self._total_bytes_to_process


        for projectName in self._projects_dict:
            p = Process(target=self.restructure_project, args=(projectName, process_logger))
            p.start()
            projectProcessList.append(p)
            queueProjects.put(projectName)
            # self.processProject(projectName, process_logger, queueProjects)

        ## Wait for all processes to end.
        for p in projectProcessList:
            p.join()

        runTime = time.time() - ttime
        strTime = str(runTime) + " sec" if runTime < 60 else str(runTime/60) + " min"
        print colout.valid("\nProjects processed in %s." %(strTime))
        manager.shutdown()


    def restructure_project(self, projectName, process_logger):
        """
        Processes a project.
        :param projectName:
        :param queue:
        :return:
        """
        # if queue:
        #     queue.get()
        illuProject = _IlluminaProject(self._projects_dict[projectName], self._run_abs_path, process_logger)
        illuProject.restructProject()
        if illuProject.hasErrors():
            illuProject.printErrors()


    def check_sample_sheet(self):
        """
        Checks the sample sheets folder. Checks the sample sheet. If old model specified in command line, withdraws the new model extension (usually _IEM)
        Invalidates the RUN if a problem occurs.
        """

	## Check if a custom sample sheet has been given first
	if self._sample_sheet_path is None:
		## Check the first available path
		for ss_path in IlluminaConfig.PATH_SAMPLE_SHEETS:
		    if os.path.isdir(ss_path):
			self._sample_sheets_dir = fm.build_dir_path(ss_path) + self._run_base_name
			break

		## Check a sample sheet directory has been found
		if self._sample_sheets_dir is None:
			self._log_tracker.append_error("Neither of those paths exist: " + str(IlluminaConfig.PATH_SAMPLE_SHEETS)
				    + ".\nPlease specify a valid path to the sample sheets in the configuration file."
				    + "\nUse this command's help for more informations.")
			self._valid_run = False
			return 0

		sample_sheet_tag = "" if self._sample_sheet_old_model else IlluminaConfig.SAMPLE_SHEET_NEW_EXT

		self._sample_sheet_path = self._sample_sheets_dir + sample_sheet_tag + SampleSheet.SAMPLE_SHEET_EXTENSION

        print "Checking Sample Sheet: ", self._sample_sheet_path

        if os.path.isfile(self._sample_sheet_path):
            self._sample_sheet = SampleSheet(self._sample_sheet_path, oldModel=self._sample_sheet_old_model)
        else:
            self._log_tracker.append_error("Sample Sheet does not exist: " + self._sample_sheet_path)
            self._valid_run = False
            return 0

        if not self._sample_sheet.is_valid_sample_sheet():
            self._log_tracker.append_error("Sample Sheet is invalid.")
            self._sample_sheet.print_data()
            self._sample_sheet.print_summary()
            self._valid_run = False
            return 0
        return 1

    def check_smaple_sheet_path(self):
        pass

    def check_folders_and_files(self):
        """
        Checks each sample and project based on a list of illumina samples. Invalidates the RUN if a problem occurs.

        A dictionary containing the list of each file for each project is first built. Then each sample name will match
        the corresponding file, starting by the longest file name in the illuminag sample list. The matched file will
        then be removed from the available files to be processed in the dict. In doing so, bugs from sample names
        starting with identical string than longer sample names are prevented. Example: there is one sample named
        'sample_2x_4y.fastq.gz', and another named 'sample_2x_4y_longer.fastq.gz'. It is then vital the longer file be
        matched before the shorter. Else, the shorter could match both the right file and the longer.
        """
        ## Get the FCID
        fcid = None
        if not self._sample_sheet_old_model:
            fcid = self.get_run_fcid(self._run_base_name)
            if fcid is None:
                self._log_tracker.append_notif("No FCID was recognized from the run name: " + self._run_base_name)
            else:
                self._log_tracker.append_notif("The FCID " + fcid + " will be added to each sample.")

        ## Get all the samples first.
        illumina_samples = []
        for sample_fields in self._sample_sheet.get_data_rows_fields():
            illuSample = IlluminaSample(self._sample_sheet.get_data_header_fields(), sample_fields)

            if not illuSample.is_valid_sample():
                error = "[sample] " + ",".join(sample_fields)
                for e in illuSample.get_sample_errors():
                    error += "\n\t* " + e
                self._log_tracker.append_error(error)
                self._valid_run = False
            else:
                if not self._sample_sheet_old_model and fcid:
                    illuSample.add_field(self._ssFields.F_FCID, fcid, 0)
                # Make sure illumina samples have unique sample name and id
                if not self.has_sample_id(illuSample, illumina_samples):
                    illumina_samples.append(illuSample)

        ## IMPORTANT STEP : reverse-sort each sample by it's sample name.
        illumina_samples.sort(key=lambda isample: len(isample.get_sample_dict()[self._ssFields.F_SAMPLE_ID]), reverse=True)


        ##
        for isample in illumina_samples:
            self.check_sample_files(isample)

        # self.printProjectsSummary()


    def has_sample_id(self, illuSample, illuSamples):
        """
        Checks if an IlluminaSample name or Id already exists in a list of illumina samples.
        :param illuSample:
        :param illuSamples:
        :return:
        """
        for isample in illuSamples:
            if isample.get_sample_id() == illuSample.get_sample_id():
                return True
        return False


    def find_project_path(self, project_name):
        """
        Checks the existence of the project folder, and if it already has the project tag.
        :param project_name: returns the project path if found. Else returns None.
        :return:
        """
        path = os.path.join(self._run_abs_path, os.path.basename(project_name))
        if os.path.isdir(path):
            return path
        path = os.path.join(self._run_abs_path, IlluminaConfig.PROJECT_FOLDER_TAG + os.path.basename(project_name))
        if os.path.isdir(path):
            return path
        return None


    def check_sample_files(self, illuSample):
        """
        Ensures the sample has corresponding files and a corresponding project folder it belongs to.
        Builds the dictionary of project and samples associated accordingly.

        :param illuSample:
        :return:
        """
        """ Check project folder """
        project_name = illuSample.get_sample_dict()[self._ssFields.F_SAMPLE_PROJECT]
        sample_id = illuSample.get_sample_dict()[self._ssFields.F_SAMPLE_ID]
        print "\nChecking sample ID:", sample_id

        """ Check the project folder already exists. Initialize the dictionaries. """
        project_path = self.find_project_path(project_name)
        if not project_name in self._projects_dict.keys():
            if project_path is None:
                self._log_tracker.append_error("Project folder \"" + project_name + "\" does not exist.")
                self._projects_dict[project_name] = None  # This project is invalid, so it is set to None.
                self._valid_run = False
                return 0

            self._projects_dict[project_name] = []  # Instanciate the dict for this project.
            self.__projects_files[project_name] = os.listdir(project_path)
            # print "\n  List Files in Project:", project_name, "(" + project_path + ")"  ## REMOVE ME
            # for f in self.__projectsFiles[project_name]:
            #     print "   ", f,


        """ Exclude non-existing folders from the process. """
        if project_name in self._projects_dict.keys() and self._projects_dict[project_name] is None:
            return 0

        """ Look for matching sample files existing in the project folder for each sample. """
        sampleFilesFound = False

        availableFiles = self.__projects_files[project_name][:]  # Need a copy since elements will be removed in the loop.
        for aFileName in self.__projects_files[project_name]:

            if aFileName in availableFiles and os.path.isfile(os.path.join(project_path, aFileName)) \
                    and fm.includes_extension(aFileName, IlluminaConfig.SAMPLE_EXTENSIONS)\
                    and fm.starts_with_field(aFileName, sample_id):

                # print "Sample \"" + sampleId + "\" has corresponding file in project \""\
                #                        + projectName + "\" : \"", aFileName, "\""
                self._total_bytes_to_process += fm.get_file_size(os.path.join(project_path, str(aFileName)))
                self._nb_files_to_process += 1
                print "  +", aFileName, "(" + project_name + ")"  ## REMOVE ME


                illuSample.add_file_name(aFileName)
                illuSample.track_sample_file(os.path.join(self._run_abs_path, project_name, aFileName), sample_id)
                availableFiles.remove(aFileName)  # File is removed as it has just been matched .
                sampleFilesFound = True

        if sampleFilesFound:
            self._projects_dict[project_name] += [illuSample]
        else:
            self._log_tracker.append_error("Sample \"" + sample_id + "\" has no file equivalent in project \""\
                                       + project_name + "\". Please check missing files and file extensions.")
            self._valid_run = False

        ## Check children errors
        ## TODO change try inheriting the Logging module
        illu_log = illuSample.get_log_tracker()
        if illu_log.has_errors():
            self._log_tracker.merge_logs(illu_log)
            self._valid_run = False

    def get_run_fcid(self, runName):
        """
        Tries to guess the FCID from the name of the RUN folder.
        Assumes the RUN file name is comprised of fields, separated form each other by underscores.
        Assumes the FCID information position is in config file : ProjectConf.FCID_POSITION_IN_RUN_NAME.
        """
        fileName = os.path.basename(os.path.dirname(fm.build_dir_path(runName)))
        filefields = fileName.split("_")
        if filefields.__len__() < IlluminaConfig.FCID_POSITION_IN_RUN_NAME:
            return None
        fcid = filefields[IlluminaConfig.FCID_POSITION_IN_RUN_NAME - 1]
        if fcid.strip(" ").__len__() == 0:
            return None
        return fcid


    def is_valid_run(self):
        return self._valid_run


    def print_projects_summary(self):
        """
        Prints information on how many existing projects and samples were found.

        """
        print colout.notice("\n[PROJECTS SUMMARY]")
        projectCount = 0
        sampleCount = 0
        for project in self._projects_dict.keys():
            if self._projects_dict[project] is None:
                print " ", project, "xxx INEXISTANT"
                continue
            projectCount += 1
            for illuSample in self._projects_dict[project]:
                sampleCount += 1
                print " ", project, "/", illuSample.get_sample_id(), "(" + str(illuSample.get_file_names().__len__()) + " files)"
                for f in illuSample.get_file_names():
                    print "   ", f

        print projectCount, "existing projects for", sampleCount, "samples"

    def getSummary(self, msg_type, array2Summarize):
        msg = ""
        if array2Summarize and array2Summarize.__len__() > 0:
            title = ""
            if msg_type == kmessage.MessageType.ERROR:
                title = "errors"
            elif msg_type == kmessage.MessageType.NOTICE:
                title = "notifications"
            elif msg_type == kmessage.MessageType.WARNING:
                title = "warnings"
            else:
                title = "summary"

            msg = colout.message("\n[" + str(title).upper() + "]", msg_type)
            for elem in array2Summarize:
                msg += colout.message("\n * ", msg_type) + str(elem)

        return msg

    def get_summary(self):
        summary = ''
        for summ in self.get_summaries():
            summary += summ
        return summary

    def get_summaries(self):
        return self._log_tracker.get_summaries()

    def get_run_abs_path(self):
        return self._run_abs_path

    def get_projects_dict(self):
        return self._projects_dict

    def get_sample_sheet_path(self):
        return self._sample_sheet_path

def split_fastq_files(file_list, force_split=True):
    """
    Splits a list of file in parallel processes.
    Checks if the files' number of lines is enough to be splitted.
    :param file_list: the files to split.
    :param force_split: if True, splits avoid the counting of lines and splits the files anyway.
    :return: a map of results for the split of each file.
    """

    import functools
    proc_list = []
    for f in file_list:
        proc_list.append(
            functools.partial(
                split_fastq_file
                , file_name=f
                , force_split=force_split
            )
        )
    return fp.map_partial_functions(proc_list)


def split_fastq_file(file_name, force_split=True):
    """
    Splits a gzip fastq file.
    If the split is not forced, first checks whether the file needs to be splitted.
    :param file_name:
    :param force_split:
    :return:
    """
    if not force_split:
        if not fastq_has_enough_sequences(file_name):
            print file_name, "\n  file has less than", CompressedFiles.CHUNKS_PER_SPLIT, "sequences. No splitting."
            return

    print file_name, "\n  splitting file", file_name
    fc.split_gz_file(file_names=file_name
                     , chunks_per_split=CompressedFiles.CHUNKS_PER_SPLIT
                     , lines_per_chunk=CompressedFiles.LINES_PER_CHUNK
                     , verbose=False)


def fastq_has_enough_sequences(file_name):
    """
    Tells whether the fastq file has enough sequences compared to the number specified in the comfiguration file.
    :param file_name:
    :return: True if so, else False.
    """
    line_count = get_fastq_line_count(file_name)
    if line_count is None:
        return False
    print "  nb seqs: ", line_count / CompressedFiles.LINES_PER_CHUNK, " / ", CompressedFiles.CHUNKS_PER_SPLIT
    if line_count / CompressedFiles.LINES_PER_CHUNK <= CompressedFiles.CHUNKS_PER_SPLIT:
        return False
    return True

def get_fastq_line_count(file_name):
    """
    Counts the number of lines in a compressed fastq file.
    :param file_name:
    :return:
    """
    cmd_1 = "gunzip -c " + file_name
    cmd_2 = "wc -l"
    df = subprocess.Popen(cmd_1.split(), stdout=subprocess.PIPE)
    pipe = subprocess.Popen(cmd_2.split(), stdin=df.stdout, stdout=subprocess.PIPE)
    df.stdout.close()
    line_count = pipe.communicate()[0]
    df.wait()
    return coercer.coerce2int(line_count, rounded=True) if line_count is not None else line_count

#################################
##########################
##################
class _IlluminaProject:
    """
    This class is private. No tests on the validity of data are performed there as they should have been done in the
    calling class, i.e. IlluminaProjectRestructor.
    Thus, this class expects clean data obtained from the instanciation of the IlluminaProjectRestructor class.
    """

    VERBOSE = False
    def __init__(self, illuminaSamples, runPath, process_logger):
        self._illuSamples = illuminaSamples
        self._runPath = runPath
        self._process_logger = process_logger
        self._errors = []

        if IlluminaConfig.TAKE_OLD_SAMPLE_SHEET_MODEL:
            from illumina_config import SampleSheetFieldsOld as ssFields
        else:
            from illumina_config import SampleSheetFieldsNew as ssFields

        self._ssFields = ssFields
        self._projectName = self._illuSamples[0].get_sample_dict()[self._ssFields.F_SAMPLE_PROJECT]
        self._processList = []
        self._total_blocks = self.setTotalFileSize()
        self._blocks_processed = 0


    def setTotalFileSize(self):
        total_blocks = 0
        for illuSample in self._illuSamples:
            for file_name in illuSample.get_file_names():
                total_blocks += fm.get_file_size(fm.build_dir_path(self._runPath, self._projectName) + file_name)
        return total_blocks

    def restructProject(self):
        print colout.notice("\n[PROCESSING PROJECT] " + str(self._projectName))

        counter = 0
        ttime = time.time()

        queueSamples = Queue()
        for aSample in self._illuSamples:
            counter += 1
            print "Processing sample", self._projectName, "/", aSample.get_sample_id()\
                            , "(sample", counter, "/", len(self._illuSamples), ")"
            self.processSample(aSample)

        for p in self._processList:
            p.join()

        # if True:    ## REMOVE ME
        #     return
        ## Rename the project folder
        print "Renaming Project Folder: ", self._projectName
        if not str(self._projectName).startswith(IlluminaConfig.SAMPLE_FOLDER_TAG):
            projectPath = os.path.join(self._runPath, self._projectName)
            projectPathTagged = os.path.join(self._runPath, IlluminaConfig.PROJECT_FOLDER_TAG + self._projectName)
            fm.rename_file(projectPath, projectPathTagged, verbose=self.VERBOSE)

        projectTime = time.time() - ttime
        strTime = str(projectTime) + " sec" if projectTime < 60 else str(projectTime/60) + " min"
        # print "Project %s processed in %s." %(self._projectName, strTime)


    def processSample(self, aSample, queue=None):
            if queue:
                queue.get()

            ## Check the sample folder exists
            sampleDir = os.path.join(self._runPath, self._projectName, IlluminaConfig.SAMPLE_FOLDER_TAG + aSample.get_sample_id())
            if not os.path.isdir(sampleDir):
                print "Created Sample Directory: ", sampleDir
                os.mkdir(sampleDir)
                fm.change_file_permission(sampleDir, IlluminaConfig.CREATED_FILES_PERMISSION, update_owner=True)

            splitQueue = Queue()
            ## Move sample files in corresponding sample directory
            for aFileName in aSample.get_file_names():
                # print "processing sample file: ", aFileName
                filePath = fm.build_dir_path(self._runPath, self._projectName) + aFileName
                splitQueue.put(aFileName)
                p = Process(target=self.processSampleFile, args=(filePath, sampleDir, splitQueue, True))
                p.start()
                self._processList.append(p)
                time.sleep(0.5)

            ## Update the sample sheets
            projectSampleSheetPath = fm.build_dir_path(self._runPath, self._projectName)\
                                     + SampleSheet.SAMPLE_SHEET_NAME + SampleSheet.SAMPLE_SHEET_EXTENSION

            sampleSampleSheetPath = fm.build_dir_path(sampleDir) + SampleSheet.SAMPLE_SHEET_NAME\
                                   + SampleSheet.SAMPLE_SHEET_EXTENSION  # The sample sheet in the sample directory

            dataHeader = SampleSheet.SAMPLE_SHEET_SEPARATOR.join(aSample.get_sample_header())
            dataSample = SampleSheet.SAMPLE_SHEET_SEPARATOR.join(aSample.get_sample_row())

            ## Update the Project Sample Sheet
            if not os.path.isfile(projectSampleSheetPath):
                self.appendSampleSheet(projectSampleSheetPath, dataSample, dataHeader)
            else:
                self.appendSampleSheet(projectSampleSheetPath, dataSample)

            fm.change_file_permission(projectSampleSheetPath, IlluminaConfig.CREATED_FILES_PERMISSION, update_owner=True)

            ## Update to the Sample Sample Sheet
            if not os.path.isfile(sampleSampleSheetPath):
                self.appendSampleSheet(sampleSampleSheetPath, dataSample, dataHeader)
            fm.change_file_permission(sampleSampleSheetPath, IlluminaConfig.CREATED_FILES_PERMISSION, update_owner=True)


    def processSampleFile(self, filePath, destFolder, queue, verbose=VERBOSE):
        fileName = queue.get()
        if verbose:
            print "Splitting File: " + str(fileName) + "..."

        bytes = fm.get_file_size(filePath)

        fc.split_gz_file(filePath, destination=destFolder, chunks_per_split=CompressedFiles.CHUNKS_PER_SPLIT \
                         , lines_per_chunk=CompressedFiles.LINES_PER_CHUNK
                         , verbose=self.VERBOSE
                         , file_permission=IlluminaConfig.CREATED_FILES_PERMISSION
                         , zero_padding=CompressedFiles.PADDING_ZEROS_FOR_SPLITTED_FILES
                         )

        ## Delete or move the splitted file.
        if CompressedFiles.DELETE_ORIGINAL_FILES:
            os.remove(filePath)
        ## Else let in current directory.
        else:
            # Move the file
            # fm.renameFile(filePath, os.path.join(destFolder, os.path.basename(filePath)), verbose=self.VERBOSE)
            pass

        self._process_logger[klogger.PROCESSED_BYTES] += bytes

        try:
            percentage = (100 * self._process_logger[klogger.PROCESSED_BYTES])/self._process_logger[klogger.TOTAL_BYTES]
        except ZeroDivisionError:
            percentage = 100
        if bytes >= 0:
            print colout.notice("  -> file " + str(fileName) + " has been successfully split." )\
                              , colout.valid(5 * "\t" + "(" + str(percentage) + "%)")

    def appendSampleSheet(self, sampleSheetPath, row, header=None):
        try:
            with open(sampleSheetPath, 'a') as sSheet:
                if header:
                   sSheet.write(header)
                sSheet.write("\n" + row)
            return 0
        except IOError as e:
            msg = "\n[ERROR WRITING FILE] Sample Sheet " + sampleSheetPath + ": \n\t-> " + str(e.strerror)
            self._errors.append(msg)
            return None


    def hasErrors(self):
        return self._errors.__len__() > 0


    def printErrors(self):
        if self._errors.__len__() > 0:
            print "\n[ERRORS]"
            for e in self._errors:
                print "*", e



#################################
##########################
##################
def printMan():

    man = "\n" + "-" * 40 + "\nMANUAL - illumina_project_restructor\n" + "-" * 40
    man += "\n\nDescription: \n\n\tThis program takes as input an illumina RUN directory and converts it to an old illumina output"
    man += "\n\nUsage: \n\n\t$ restruct-illumina-projects.py [-h] [-x] run_folder"
    man += "\n\n"
    print man


def getParser():
    """
    Parses the input arguments.
    :param argv:
    :return: The parsed arguments.
    """
    parser = argparse.ArgumentParser(description="Restructure an Illumina output folder to the old way.")
    parser.add_argument("run_folder", help="The RUN folder to be restructured.", type=str)
    parser.add_argument("-x", "--execute", help="Execute the program once validity is checked.", action="store_true")
    parser.add_argument("-o", "--oldSample", help="Fetches the old sample sheets, without corresponding extension."\
                        , action="store_true")
    parser.add_argument("-c", "--config", help="Generates a user configuration file on the specified folder."\
                        , action="store_true")
    return parser


def main(argv=None):
    parser = getParser()
    if argv is None:
        argv = sys.argv

    # argv += ["fileX"]
    args = parser.parse_args(argv[1:])
    # print "args: ", args
    # exit()
    if args.run_folder and not os.path.exists(args.run_folder) and not os.path.isdir(args.run_folder):
        print colout.error(string_utils.stringify("\n Folder ", args.run_folder, " does not exist.\n")
                           , with_header=True)
        parser.print_help()
        return None

    cfg = Configuration()
    if args.config:
        file_path = fm.build_dir_path(args.run_folder) + IlluminaConfig.CONFIG_FILE_NAME
        cfg.writeIniConfigFile(file_path)
        print "Config file successfully written: " + file_path + "."\
                + "\nPlease check it then relaunch the command.\n"\
                + colout.warn("", with_header=True)\
                + " Note that it will overwrite any existing configuration file present in your working directory."

        return None

    ## Updating Configurations
    if cfg.updateConfiguration(args.run_folder) is None:
        return None

    ## Starts restructuring
    restructer = IlluminaRunRestructor(args.run_folder, args.oldSample)
    if restructer.is_valid_run():
        if args.execute:
            restructer.start_restructure_projects()
        restructer.get_summaries()


if __name__ == "__main__":
    sys.exit(main(sys.argv))
