__author__ = 'alexandre-nadin'

from multiprocessing import Lock
import fcntl

dirs = [
        "/Users/alexandre-nadin/dev/tests/lustre2/raw_data/Runtest/VanAnken_289_RNASeqLite/"
    ]
files = [
    "5_L001_R1.fastq.gz"
    , "5_L001_R2.fastq.gz"
    , "testlock.txt"

]

filename = dirs[0] + files[2]

def testOpenWith(fn=filename, mode='r+'):
    with open(fn, mode=mode) as fd:
        fcntl.lockf(fd, fcntl.LOCK_EX)
        fd.fileno()
        try:
            fcntl.lockf(fd, fcntl.LOCK_EX)
        except IOError:
            print("can't immediately write-lock the file ($!), blocking ...")
        else:
            print("No error")
        raw_input("Enter to proceed")
        print "read line: ", fd.readline()


def testOpen(fn=filename, mode='r+'):
    print "opening file", filename
    fd = open(fn, mode=mode)
    fcntl.lockf(fd, fcntl.LOCK_EX)
    try:
        fcntl.lockf(fd, fcntl.LOCK_EX)
    except IOError:
        print("can't immediately write-lock the file ($!), blocking ...")
    else:
        print("No error")
    raw_input("Enter to proceed")
    print "read line: ", fd.readline()
    fd.close()

def testFsync(fn=filename, mode='r+'):
    print "opening file", filename
    import os
    fd = open(fn, mode=mode)
    fcntl.lockf(fd, fcntl.LOCK_EX)
    try:
        fcntl.lockf(fd, fcntl.LOCK_EX)
    except IOError:
        print("can't immediately write-lock the file ($!), blocking ...")
    else:
        print("No error")
    raw_input("Enter to proceed")
    print "read line: ", fd.readline()
    fd.close()

if __name__ == "__main__":
    # testOpen(mode='r+b')
    testOpenWith(mode='r+b')

    lineCounter = []

    lck = Lock()






