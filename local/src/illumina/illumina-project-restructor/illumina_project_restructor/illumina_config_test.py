__author__ = 'alexandre-nadin'

import unittest
from illumina_config import Configuration

class TestConfiguration(unittest.TestCase):


    def __init__(self, *args, **kwargs): # The TestCase constructor takes two arguments beside self.
        super(TestConfiguration, self).__init__(*args, **kwargs)
        self.ipc = Configuration()



    def testGetNewClassAttributeValue(self):
        ## When class name or attribute do not exist.
        self.assertEquals(self.ipc._getNewClassAttributeValue("ProjectConf", "SAMPLE_EXTENSIONS", ''), None)
        self.assertEquals(self.ipc._getNewClassAttributeValue("IlluminaConfig", "SAMPLE_EXTedEedNSIONS", ''), None)

        ## When original value is a list
        # self.assertEquals(self.ipc._getNewClassAttributeValue("IlluminaConfig", "SAMPLE_EXTENSIONS", ''), [''])
        self.assertEquals(self.ipc._getNewClassAttributeValue("IlluminaConfig", "SAMPLE_EXTENSIONS", 'single'), ['single'])
        self.assertEquals(self.ipc._getNewClassAttributeValue("IlluminaConfig", "SAMPLE_EXTENSIONS", []), [])
        self.assertEquals(self.ipc._getNewClassAttributeValue("IlluminaConfig", "SAMPLE_EXTENSIONS", ['']), [''])
        self.assertEquals(self.ipc._getNewClassAttributeValue("IlluminaConfig", "SAMPLE_EXTENSIONS", ['first', '', 'third'])
                          , ['first', '', 'third'])
        self.assertEquals(self.ipc._getNewClassAttributeValue("IlluminaConfig", "SAMPLE_EXTENSIONS", ['', 'second', 'third'])
                          , ['', 'second', 'third'])


        ## When original value is not a list
        self.assertEquals(self.ipc._getNewClassAttributeValue("IlluminaConfig", "FCID_POSITION_IN_RUN_NAME", ''), '')
        self.assertEquals(self.ipc._getNewClassAttributeValue("IlluminaConfig", "FCID_POSITION_IN_RUN_NAME", 'single'), 'single')
        self.assertEquals(self.ipc._getNewClassAttributeValue("IlluminaConfig", "FCID_POSITION_IN_RUN_NAME", 4), 4)

        self.assertEquals(self.ipc._getNewClassAttributeValue("IlluminaConfig", "FCID_POSITION_IN_RUN_NAME", []), '')
        self.assertEquals(self.ipc._getNewClassAttributeValue("IlluminaConfig", "FCID_POSITION_IN_RUN_NAME", ['']), '')
        self.assertEquals(self.ipc._getNewClassAttributeValue("IlluminaConfig", "FCID_POSITION_IN_RUN_NAME", ['first', '', 'third'])
                          , None)
        self.assertEquals(self.ipc._getNewClassAttributeValue("IlluminaConfig", "FCID_POSITION_IN_RUN_NAME", ['', 'second', 'third'])
                          , None)


        self.assertEquals(self.ipc._getNewClassAttributeValue("IlluminaConfig", "PROJECT_FOLDER_TAG", 'Project_23_'), 'Project_23_')
        self.assertEquals(self.ipc._getNewClassAttributeValue("IlluminaConfig", "PROJECT_FOLDER_TAG", ['Project_23_']), 'Project_23_')


    def testCopyVariableType(self):
        self.assertEquals(self.ipc.copyVariableType([1, 2, 3], ["e", "w"]), ["e", "w"])
        self.assertEquals(self.ipc.copyVariableType(5, "9"), 9)
        self.assertEquals(self.ipc.copyVariableType(5, "9sd"), None)
        self.assertEquals(self.ipc.copyVariableType("ok", 12), "12")
        self.assertEquals(self.ipc.copyVariableType(True, "string"), True)
        self.assertEquals(self.ipc.copyVariableType(True, ""), False)
        self.assertEquals(self.ipc.copyVariableType(False, "string"), True)
        self.assertEquals(self.ipc.copyVariableType(False, ""), False)
        self.assertEquals(self.ipc.copyVariableType(False, "True"), True)
        self.assertEquals(self.ipc.copyVariableType(False, "False"), False)
        self.assertEquals(self.ipc.copyVariableType(True, "False"), False)
        self.assertEquals(self.ipc.copyVariableType(True, "True"), True)
        self.assertEquals(self.ipc.copyVariableType(True, "True"), True)

        self.assertEquals(self.ipc.copyVariableType(True, "yes"), True)
        self.assertEquals(self.ipc.copyVariableType(True, "y"), True)
        self.assertEquals(self.ipc.copyVariableType(True, 1), True)
        self.assertEquals(self.ipc.copyVariableType(True, "no"), False)
        self.assertEquals(self.ipc.copyVariableType(True, "n"), False)
        self.assertEquals(self.ipc.copyVariableType(True, 0), False)
        self.assertEquals(self.ipc.copyVariableType(True, "t"), True)
        self.assertEquals(self.ipc.copyVariableType(True, "f"), False)

        self.assertEquals(self.ipc.copyVariableType(False, "yes"), True)
        self.assertEquals(self.ipc.copyVariableType(False, "y"), True)
        self.assertEquals(self.ipc.copyVariableType(False, 1), True)
        self.assertEquals(self.ipc.copyVariableType(False, "no"), False)
        self.assertEquals(self.ipc.copyVariableType(False, "n"), False)
        self.assertEquals(self.ipc.copyVariableType(False, 0), False)
        self.assertEquals(self.ipc.copyVariableType(False, "t"), True)
        self.assertEquals(self.ipc.copyVariableType(False, "f"), False)