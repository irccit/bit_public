__author__ = 'alexandre-nadin'
# -*- coding: utf-8 -*-
import unittest
from metadata_reader import MetadataReader

class TestMetadataReader(unittest.TestCase):
    # sampleSheetPath = "/Users/alexandre-nadin/dev/illumina-project-restructor/tests/lustre1/SampleSheets/"
    sampleSheetsPath = "/Users/alexandre-nadin/dev/illumina-project-restructor/tests/sample-sheets/"
    configPath = "/Users/alexandre-nadin/dev/illumina-project-restructor/tests/config-files/"
    fileNames = [
        "test-no-metadata.csv"
        ,"test-with-metadata-end.csv"
        ,"test-with-metadata-between.csv"
        ,"test-with-metadata-no-data.csv"
        ,"test-with-metadata-between-emtpy.csv"
        ,"test-normal.cfg"
    ]

    def __init__(self, *args, **kwargs): # The TestCase constructor takes two arguments beside self.
        super(TestMetadataReader, self).__init__(*args, **kwargs)
        self.mdr = MetadataReader(self.sampleSheetsPath + self.fileNames[2])
        print "\nmdr pattern: ", self.mdr.getMdPattern("DaTa")
        print "MDR_PATTERN: ", self.mdr.METADATA_PATTERN_DEFAULT


    def testSetMdDelimiter(self):
        print "\n[testCheckDelimiter]"
        self.assertEquals(self.mdr.checkMdDelimiters(None), MetadataReader.MD_DELIMITERS_DEFAULT)
        self.assertEquals(self.mdr.checkMdDelimiters([" ", "\\"]), MetadataReader.MD_DELIMITERS_DEFAULT)
        self.assertEquals(self.mdr.checkMdDelimiters(["patt1 ", " "]), ["patt1 ", "patt1 "])
        self.assertEquals(self.mdr.checkMdDelimiters(["patt1 "]), ["patt1 ", "patt1 "])
        self.assertEquals(self.mdr.checkMdDelimiters([None, "patt2"]), MetadataReader.MD_DELIMITERS_DEFAULT)
        self.assertEquals(self.mdr.checkMdDelimiters([" patt1", None]),[" patt1", " patt1"])
        self.assertEquals(self.mdr.checkMdDelimiters([" pae\n f\tt1 ", None]), [" pae\n f\tt1 ", " pae\n f\tt1 "])
        self.assertEquals(self.mdr.checkMdDelimiters(["%"]), ["%", "%"])


    def testGetMdName(self):
        print "\n[testGetMdName]"
        self.mdr.setMdDelimiters(["[", "]"])
        self.assertEquals(self.mdr._getMdName(" b efore  [  data1 data2  ] af t er"), "data1 data2")
        self.assertEquals(self.mdr._getMdName(" b efore  %  data1 data2  % af t er"), None)

        self.mdr.setMdDelimiters(["%"])
        self.assertEquals(self.mdr._getMdName(" b efore  [  data1 data2  ] af t er"), None)
        self.assertEquals(self.mdr._getMdName(" b efore  %  data1 data2  % af t er"), "data1 data2")

        self.mdr.setMdDelimiters(["$", "%"])
        self.assertEquals(self.mdr._getMdName(" b efore  %  data1 data2  $ af t er"), None)
        self.assertEquals(self.mdr._getMdName(" b efore  $  data1 data2  $ af t er"), None)
        self.assertEquals(self.mdr._getMdName(" b efore  $  data1  data2  % af t er"), "data1  data2")
        self.assertEquals(self.mdr._getMdName(" b efore  $  data1 data2  % "), "data1 data2")
        self.assertEquals(self.mdr._getMdName(" b efore  $  data1 data2  %"), "data1 data2")
        self.assertEquals(self.mdr._getMdName(" $  data1 data2  % af t er"), "data1 data2")
        self.assertEquals(self.mdr._getMdName("$  data1 data2  % af t er"), "data1 data2")


    def testMatchPattern(self):
        """
        Test matching of regular expression
        """
        print "\n[testDetectMetadata]"
        # expression = self.mdr.METADATA_RE
        self.mdr.setMdDelimiters(["[", "]"])
        expression = self.mdr.getMdPatternRe()
        self.assertEquals(self.mdr.matchPattern(expression, " [  word_in_bracket ]   "), " [  word_in_bracket ]   ")
        self.assertEquals(self.mdr.matchPattern(expression, " before_bracket  [  word1 word2 ]  "), None)
        self.assertEquals(self.mdr.matchPattern(expression, "   [  word1 word2 ]  after_bracket"), "   [  word1 word2 ]  ")
        self.assertEquals(self.mdr.matchPattern(expression, "  word1 word2 word3 ]   "), None)
        self.assertEquals(self.mdr.matchPattern(expression, "  word1 word2 word3"), None)
        self.assertEquals(self.mdr.matchPattern(expression, " [  word1 word2 word3 ]   "), " [  word1 word2 word3 ]   ")

        self.mdr.setMdDelimiters(["[", "]"])
        print "delim: ", self.mdr._md_delimiters
        print "\n[testFindMetadata] ", self.mdr.getMdPattern("Data")
        expression = self.mdr.getMdPatternRe("Data")
        self.assertEquals(self.mdr.matchPattern(expression, " [  dAtA ]   "), " [  dAtA ]   ")
        self.assertEquals(self.mdr.matchPattern(expression, " [  dAtA1 data2 ]   "), None)
        self.assertEquals(self.mdr.matchPattern(expression, " [  da ta ]   "), None)

        expression = self.mdr.getMdPatternRe("Data2")
        self.assertEquals(self.mdr.matchPattern(expression, " [  dAtA ]   "), None)
        self.assertEquals(self.mdr.matchPattern(expression, " [  dAtA2 ]   "), " [  dAtA2 ]   ")
        self.assertEquals(self.mdr.matchPattern(expression, " ,  dAtA ]   "), None)
        self.assertEquals(self.mdr.matchPattern(expression, " ,  dAtA2 ]   "), None)


    def testHasMetadata(self):
        self.assertEquals(self.mdr.hasMetadata(), True)

        self.mdr = MetadataReader(self.sampleSheetsPath + self.fileNames[0])
        self.assertEquals(self.mdr.hasMetadata(), False)


    def testGetData(self):
        self.mdr = MetadataReader(self.sampleSheetsPath + self.fileNames[2])
        self.assertGreater(self.mdr.getRawDataFromMd("alternative data ").__len__, 0)
        self.assertGreater(self.mdr.getRawDataFromMd(" alteRnatIve dAta ").__len__, 0)
        self.assertEquals(self.mdr.getRawDataFromMd(" alterRnatIve dAta "), None)
        self.assertEquals(self.mdr.getRawDataFromMd(" alteRnatIve  dAta "), None)


    def testIsMetadata(self):
        self.mdr = MetadataReader(self.configPath + self.fileNames[5])
        self.assertEquals(self.mdr.isMetadata("Hello"), False)
        self.assertEquals(self.mdr.isMetadata("ProjectConf"), True)
        self.assertEquals(self.mdr.isMetadata("Projectconf"), False)


    def testGetKeyValFromData(self):
        self.mdr = MetadataReader(self.sampleSheetsPath + self.fileNames[2])

        import re
        regex = re.compile("^\w+$")
        self.assertEquals(self.mdr._getKeyValFromData("my-key  = value  \n\n", key_regex=regex), [None, None])
        self.assertEquals(self.mdr._getKeyValFromData("my_key  = value  \n\n", key_regex=regex), ["my_key", ["value  \n\n"]])

        self.assertEquals(self.mdr._getKeyValFromData("my-key  = value  \n\n"), ["my-key", ["value  \n\n"]])
        self.assertEquals(self.mdr._getKeyValFromData("my key  = value,"), ["my key", ["value", ""]])
        self.assertEquals(self.mdr._getKeyValFromData("my key  = value,", key_val_delim="$"), [None, None])
        self.assertEquals(self.mdr._getKeyValFromData("my key  $ value, value2", key_val_delim="$", val_delim="/")
                          , ["my key", ["value, value2"]])
        self.assertEquals(self.mdr._getKeyValFromData("my key  $ value/ value2 / val3", key_val_delim="$", val_delim="/")
                          , ["my key", ["value", "value2", "val3"]])
        self.assertEquals(self.mdr._getKeyValFromData("my key , or something, else"), [None, None])

        self.assertEquals(self.mdr._getKeyValFromData("my key , or something, else\n"), [None, None])


    def testGetKeyValuesFromMd(self):
        self.mdr = MetadataReader(self.configPath + self.fileNames[5])
        key_values = self.mdr.getAllKeyValuesFromMd("RandomMetadata")
        self.assertEquals(key_values, None)
        key_values = self.mdr.getAllKeyValuesFromMd("ProjectConf")
        self.assertNotEquals(key_values, None)
        self.assertEquals(key_values.keys().__len__(), 9)
        self.mdr.reportErrors()
        self.assertEquals(self.mdr._errors.__len__(), 2)
        self.mdr.reportErrors()

        # self.assertEquals(key_values.keys().__len__(), 8)


        # print mdr.getRawDataFromMd("ProjectConf")
        # print "[GET STRUCTURED]"

if __name__ == "__main__":
    unittest.main()