__author__ = 'alexandre-nadin'


def stringify_list(l, with_space=True):
    """
    Converts a tuple into a string. Adds a space between each element by default.
    :param l:
    :return:
    """
    res = ""
    for arg in l:
        res += str(arg)
        res += " " if with_space else ""
    return str(res)


def stringify(*args):
    return stringify_list(args)


if __name__ == "__main__":
    print stringify("wefrsf", 23, [23, 3, 5])
