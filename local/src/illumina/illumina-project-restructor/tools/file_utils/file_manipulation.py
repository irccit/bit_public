__author__ = 'alexandre-nadin'

import os, sys


def change_file_permission(file_name, file_permission, update_owner=False):
    """
    Changes the file permissions of a given file.
    If specified, ownership is updated to the current user's
    :param file_name:
    :param file_permission:
    :param update_owner:
    :return:
    """
    if os.path.isfile(file_name):
        if file_permission is not None and type(file_permission) == int:
            os.chmod(file_name, file_permission)
        if update_owner is True:
            from getpass import getuser
            usr = getuser()

            from pwd import getpwnam
            pwnam = getpwnam(usr)
            os.chown(file_name, pwnam.pw_uid, pwnam.pw_gid)
        return 1
    else:
        return None


def starts_with_field(file_name, pattern, field_separators="_", verbose=False):
    """
    Checks whether a file name starts with a specified pattern as a field.
    Each field is separated with an underscore by default.
    Tries to match first match the whole file name, then if unsuccesful, tries to match the pattern with the underscore.
    :param file_name:
    :param field_separators:
    :return:
    """

    baseName = os.path.basename(file_name)
    ## Compute file fields
    split_name = baseName.split(os.extsep, 1)
    rawBaseName = split_name[0]
    extensions = split_name[1] if split_name.__len__() > 1 else ""

    debug_msg = ""
    debug_msg += "  File: " + str(file_name)
    debug_msg += "    rawBaseName: " + str(rawBaseName)
    debug_msg += "    extensions: " + str(extensions)

    result = None
    if str(rawBaseName) == str(pattern) or str(rawBaseName).startswith(pattern + field_separators):
        debug_msg += "    -> file starts with " + str(pattern)
        result = True
    else:
        debug_msg += "    -> file does NOT start with " + str(pattern)
        result = False

    if verbose:
        print(debug_msg)
    return result


def has_last_extension(name, extensions=[]):
    """
    Checks if a file name ends with one of the specified extensions.
    :param name:
    :param extensions:
    :return:
    """
    if extensions.__len__() == 0:
        return True
    for ext in extensions:
        if name.endswith("." + ext):
            return True
    return False

def includes_extension(name, exts_list=[]):
    """
    Checks if a file name's extension contains at least on from a specified list of extensions.
    :param name:
    :param exts_list:
    :return:
    """
    if exts_list.__len__() == 0:
        return True
    try:
        split_name = str(os.path.basename(name)).split(os.extsep, 1)
    except ValueError as ve:
        print "Can't split file name with", name, ".", ve.message

    if not split_name.__len__() == 2:
        return False

    raw_name, extension = split_name[0], split_name[1]

    extensions = extension.split(os.extsep)
    for ext in extensions:
        if ext in exts_list:
            return True
    return False

def rename_file(src, dst, verbose=False):
    if verbose:
        print "\tRenaming file: \n\t     ", src, "\n\t  -> ", dst
    os.rename(src, dst)


def move_file(origFolder, origFileName, destFolder, destFileName=None, verbose=True):

    origFilePath = addFolderSeparator(origFolder) + origFileName
    if destFileName is None:
        newFileName = origFileName
    else:
        newFileName = destFileName

    destFilePath = addFolderSeparator(destFolder) + newFileName
    if verbose:
        print "\tMoving file: \n\t     ", origFilePath, "\n\t  -> ", destFilePath
    try:
        os.rename(origFilePath, destFilePath)
    except:
        print "\n[ERROR RENAMING FILE] ", origFilePath, "\n -> ", destFilePath
        return None


def build_dir_path(*args):
    """
    Takes a list or arguments and join them to build a file path.
    NOTE: os.path.join() does the job better. I did not know about it by the time.
    :param args:
    :return:
    """
    path = ""
    for arg in args:
        path += addFolderSeparator(arg)
    return path


def addFolderSeparator(path):
        """
        Makes sure the provided path ends with the os path separator, ex. '/' in Unix systems
        """
        return path if path.endswith(os.sep) else path + os.sep


def get_file_size(file_name):
    return int(os.path.getsize(file_name))


def computer_size(hsize):
    suffixes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB']
    hsize = str(hsize).upper()
    counter = suffixes.__len__()
    while counter > 1:
        counter -= 1
        if hsize.endswith(suffixes[counter]):
            return int(float(hsize.rstrip(suffixes[counter]).strip()) * pow(1024, counter))

    return int(hsize)

def human_size(nbytes):
    suffixes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB']

    if nbytes == 0:
        return '0 B'

    i = 0
    while nbytes >= 1024 and i < len(suffixes)-1:
        nbytes /= 1024.
        i += 1
    f = ('%.2f' % nbytes).rstrip('0').rstrip('.')
    return '%s %s' % (f, suffixes[i])


def disk_usage(path, formatted=False, round_digits=2):
    """
    Computes the disk usage: 'total', 'used', 'free', 'perc_used' and 'perc_left', the two latters standing for the
    percentage of the capacity used and left.

    :param path:
    :param formatted: Specify if the results are to be formatted. If so, results are formatte to the corresponding power
    of bytes, and capacities are set as percentages.
    :return: disk usage statistics about the given path, in bytes or in a formatted results.
    """
    st = os.statvfs(path)

    from collections import namedtuple
    _ntuple_diskusage = namedtuple('usage', 'total used free perc_used perc_left')

    free = st.f_bavail * st.f_frsize
    total = st.f_blocks * st.f_frsize
    used = (st.f_blocks - st.f_bfree) * st.f_frsize
    perc_left = float(free)/total
    perc_used = float(used)/total
    if formatted:
        return _ntuple_diskusage(human_size(int(total)), human_size(int(used)), human_size(int(free))
                                 , str(round(perc_used * 100, round_digits)) + '%'
                                 , str(round(perc_left*100, round_digits)) + '%'
                                 )
    return _ntuple_diskusage(total, used, free, perc_used, perc_left)

def tests(argv=None):

    print "\n[TEST LISTING FILES]"
    for file in os.listdir(os.curdir):
        print "  ", file

    print "\n[TEST DIRECTORIES]"
    directory = os.curdir
    abspath = os.path.abspath(directory)
    basename = os.path.basename(abspath)
    print "Curdir: ", os.path.curdir
    print "Abspath: ", abspath
    print os.path.isdir(abspath + "/")
    print "Dirpath: ", os.path.dirname(abspath + "/")
    print "path: ", basename

    path = "/blab/file/3e/"
    print "\npath: ", path
    print "BaseDirName: ", os.path.basename(os.path.dirname(path))
    print "BaseName: ", os.path.basename(path)

    path = "/blab/file/3e"
    print "\npath: ", path
    print "BaseDirName: ", os.path.basename(os.path.dirname(path))
    print "BaseName: ", os.path.basename(path)

    print "\n[TEST DICT]"
    ar1 = [1, 2, 3, 4, "e"]
    ar2 = [5, 6, 7]
    ar3 = ["Nwe path", "another new path"]
    ar4 = os.listdir(os.curdir)
    print ar4
    myDict = {}
    myDict["first"] = ar1
    print myDict["first"]

    myDict["first"] += ar2
    print myDict["first"]


    myDict["first"] += ar4
    print myDict["first"]

    myDict["first"].remove("file_manipulation.py")
    print myDict["first"]
    print myDict


    print "\n[TEST ARRAY]"
    ar1 = [17, 24, 7, 4]
    ar2 = ar1
    print "ar1: ", ar1, "\nar2: ", ar2
    ar1.remove(7)
    print "Remove ar1[7]\nar1: ", ar1, "\nar2: ", ar2

    ar3 = [57, 234, 32, 23]
    ar4 = ar3[:]
    print "\nar3: ", ar3, "\nar4: ", ar4
    ar3.remove(234)
    print "Remove ar3[234]\nar3: ", ar3, "\nar4: ", ar4

    ar3.append("insert last")
    print "\ninsert last ar3: ", ar3

    ar3.insert(0, "insert first")
    print "\ninsert first ar3", ar3


    print "\n[TEST BUILD PATH]"
    print build_dir_path("folder/1", "second path/", "troisieme/quarto", "lastFolder")
    print build_dir_path("folder/1", "second path/", "troisieme/quarto", "/lastFolder")

    print "\n[TEST USR]"
    import pwd
    try:
        pwd.getpwnam('alexandre-nadin')
        print "alexandre-nadin exists"
    except KeyError as ke:
        print ke.message
    try:
        pwd.getpwnam('anadin')
        print "anadin exists"
    except KeyError as ke:
        print ke.message

    with open("test", 'wb'):
        pass
    change_file_permission("test", 0774)

    print "\n[TEST FILE STARTING FIELD]"
    starts_with_field("here/we/are/f1_f2_f3_f4.ext1.ext2.ext3", "sdf", verbose=True)
    starts_with_field("here/we/are/f1_f2_f3_f4.ext1.ext2.ext3", "f1", verbose=True)
    starts_with_field("here/we/are/f1_f2_f3_f4.ext1.ext2.ext3", "f1_f2", verbose=True)
    starts_with_field("here/we/are/f1_f2_f3_f4.ext1.ext2.ext3", "f1_", verbose=True)

    starts_with_field("here/we/are/5_f2_f3_f4.ext1.ext2.ext3", "5", verbose=True)
    starts_with_field("here/we/are/5_f2_f3_f4.ext1.ext2.ext3", "50", verbose=True)

    starts_with_field("here/we/are/500_f2_f3_f4.ext1.ext2.ext3", "5", verbose=True)
    starts_with_field("here/we/are/500_f2_f3_f4.ext1.ext2.ext3", "50", verbose=True)
    starts_with_field("here/we/are/500_f2_f3_f4.ext1.ext2.ext3", "500", verbose=True)

    print "\n[TEST HUMAN SIZE FILES]"
    print human_size(1341403545)

    print "\n[TEST COMPUTER SIZE]"

    print "Disk usage: ", disk_usage("/")
    print "Disk usage: ", disk_usage("/", formatted=True)
    print "12 MB -> ." + str(computer_size("12 MB")) + ". B"
    print "12.02 GB -> ." + str(computer_size("12.02 GB")) + ". (B)"
    print "12.02GB -> ." + str(computer_size("12.02 GB")) + ". (B)"
    # print "free space:", human_size(disk_usage('/').free)\
    #     , "-> ." + str(computer_size("human_size(disk_usage('/').free)")) + ". B"
    print str(int(computer_size(human_size(disk_usage('/').free))))
    print str(computer_size(human_size(disk_usage('/').free))) + ". B"
    print computer_size(human_size(disk_usage('/').free))
    print "134 2computer size: ", computer_size("134")


if __name__ == "__main__":
    print "\n[File Manipulations] main function"
    sys.exit(tests(sys.argv))
