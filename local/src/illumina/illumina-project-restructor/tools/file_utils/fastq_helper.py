__author__ = 'alexandre-nadin'

import sys, os
import random
import file_compression as fc
from tools.ky_multiprocessing import function_processor as fp



SEQ_BASES = ['A', 'T', 'G', 'C']
SEQ_SCORES = ['A', 'E', '#', '<', '/', '6']
FASTQ_EXTS = ['.fastq']

def generate_fastq_files(folder, nb_files=10, nb_seq_per_file=200, nb_char_per_seq=100, zfill=4, to_zip=True):
    """
    Automatially generates fastq files in the specified folder
    :param folder: where new files will be generated.
    :param nb_files: number of files to generate.
    :param nb_seq_per_file: number of fastq sequence per file.
    :param nb_char_per_seq: number of characters per sequence.
    :param zfill: number of left padding zeros for the file number. zfill=3 -> 001, zfill=4 -> 0001, etc.
    :param to_zip: if fastq files should be zipped.
    """
    import functools
    file_list = []
    for i in range(1, nb_files+1):
        file_list.append(
            functools.partial(
                generate_fastq_file
                , os.path.join(folder, "sample-" + str(i).zfill(zfill) + FASTQ_EXTS[0])
                , nb_seq_per_file
                , nb_char_per_seq
                , to_zip

            )
        )
    fp.map_partial_functions(file_list)


def generate_fastq_file(file_name, nb_seq_per_file, nb_char_per_seq, to_zip):
    """
    Generates a fastq file.
    :param file_name:
    :param nb_seq_per_file:
    :param nb_char_per_seq:
    :param to_zip:
    :return:
    """
    content = '\n'.join(get_random_fastq_seqs(nb_seq_per_file, nb_char_per_seq, special_id=file_name))
    if to_zip:
        fc.write_gzip_file(file_name + fc.Extensions.GUNZIP, content)
    else:
        with open(file_name, 'w') as f:
            f.write(content)


def get_random_fastq_seqs(nb_seq, nb_char, special_id=""):
    """
    Generate a fastq content with the specified amount of random sequence.
    :param file_path:
    :param nb_seq: number of sequences in file.
    :return: a list of fastq sequences
    """
    seqs = []
    for seq in range(1, nb_seq+1):
        lines = []
        lines.append('@' + special_id + ": Seq-nb " + str(seq))
        lines.append('')
        lines.append('+')
        lines.append('')
        for c in range(nb_char):
            lines[1] += SEQ_BASES[random.randint(0, SEQ_BASES.__len__()-1)]
            lines[3] += SEQ_SCORES[random.randint(0, SEQ_SCORES.__len__()-1)]
        seqs.append('\n'.join(lines))
    return seqs


def parse_args(argv):
    ## Check inputs
    # 1: script name
    # 2: destination
    # 3: number of files
    # 4: number of sequence per file
    if argv.__len__() < 2:
        print "wrong number of arguments."
        return None
    return argv


def getArg(id1, id2):
    from os import getpid
    msg = 'helper-%s-%s (pid: %d)' %(str(id1), str(id2), getpid())
    # msg = "processed-" + str(arg) + "(pid: "
    # print msg
    return msg


def printMan():
    print "\n\tWho: \tfastq generator"
    print "\n\tWhat: \tGenerate fake fastq files"
    print "\n\tHow: \t$ python script_name destination [nb_file nb_seq_per_file]"
    print "\n"


def tests():
    #
    # parsed = parse_args(sys.argv)
    # if parsed is None:
    #     printMan()
    #     sys.exit(2)

    # path = "/Users/alexandre-nadin/dev/tests/generate_fastq2"
    # generate_fastq_files(path, nb_files=10,  nb_seq_per_file=50, nb_char_per_seq=100, to_zip=True)
    # generate_fastq_files(path, nb_files=10,  nb_seq_per_file=50, nb_char_per_seq=100, to_zip=False)

    path = "/Users/alexandre-nadin/dev/git-test"
    generate_fastq_files(path, nb_files=20,  nb_seq_per_file=50, nb_char_per_seq=100, to_zip=True)

if __name__ == "__main__":
    tests()