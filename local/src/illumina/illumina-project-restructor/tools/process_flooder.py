__author__ = 'alexandre-nadin'

import sys, os
from multiprocessing import Process
import time

def process_file(file_name, output):
    import random
    time.sleep(random.randint(0, 10))

    with open(output + "/" + file_name, 'w') as f:
        if file_name != "file_5.flood":
            f.writelines("Writing..." + file_name + "\n")
            f.writelines("Waiting few seconds...\n")
            time.sleep(5)
            for i in range(1, 4000000):
                f.write("\tData line" + str(i) + 100*"=" + "\n")
            f.writelines("Written!\n")


def check_output(file_list, output):
    for f in file_list:
        full_path = output + "/" + f
        if not os.path.isfile(full_path):
            print "\t[LOST JOB] ", full_path
            continue

        file_size = os.path.getsize(full_path)
        if file_size  == 0:
            print "\t[EMPTY FILE] ", full_path, file_size
        elif os.path.getsize(full_path) < 470000000:
            print "\t[INCOMPLETE FILE] ", full_path, file_size
        else:
            print "\t[UNKNOWN ERROR] ", full_path, file_size
        # print os.path.getsize(full_path)



def get_file_list(nb_proc):
    file_list = []
    for i in range(1, nb_proc):
        file_list.append("file_" + str(i) + ".flood")
    return file_list


def flood_process(argv):
    ## Check args
    parsed = parse_args(argv)
    if parsed is None:
        printMan()
        exit(2)

    (script_name, nb_proc, output, delay) = parsed

    ## Get list of files
    print "\ngetting list of files...",
    file_list = get_file_list(nb_proc)
    print " ok"

    ## Process each file
    print "\nsubmitting processes."
    proc_list = []
    for file_name in file_list:
        if file_name == "file_4.flood":
            print "   skipping ", file_name
            continue

        p = Process(target=process_file, args=(file_name, output))
        p.start()
        proc_list.append(p)
        time.sleep(delay)

    print "all processes submitted: ", proc_list.__len__()

    ## Joining pocesses
    print "\njoining all processes...",
    for proc in proc_list:
        proc.join()
    print " ok"

    ## Checking file losses
    print "\nchecking file loss..."
    check_output(file_list, output)
    print "file loss checked."


def parse_args(argv):
    ## Check inputs
    # 1: this file
    # 2: number of files
    # 3: output folder
    # 4: delay between job submission.
    if argv.__len__() < 4:
        print "wrong number of arguments."
        return None

    (script_name, nb_proc, output, delay) = argv

    nb_proc = coerse_integer(nb_proc)
    delay = coerse_integer(delay)
    if nb_proc is None:
        print "number of processes must be an integer."
        return None
    if delay is None:
        print "delay must be an integer."
        return None

    if not os.path.isdir(output):
        print "output directory does not exist."
        return None

    output = str(output).rstrip("/")

    return (script_name, nb_proc, output, delay)


def coerse_integer(nb):
    try:
        return int(nb)
    except ValueError:
        return None

def printMan():
    print "\n\tWho: \tProcess Flooder."
    print "\n\tWhat: \tFloods the system with the specified number of processes."
    print "\n\tHow: \t$ python script_name number_of_files output_folder delay_submitting_jobs"
    print "\n"


if __name__ == "__main__":
    flood_process(sys.argv)
