import kprint
from tools import string_utils
from multiprocessing import Queue, Manager
from multiprocessing.managers import DictProxy
import time
__author__ = 'alexandre-nadin'


logger_dict = None
progress_queue = None
message_queue = []
MAX_BUFFER = 5

PROCESSED_BYTES = 0
PROGRESS_BAR = 1
PROCESSED_FILES = 2
QUEUE_PROCESSED_FILES = 3
TOTAL_BYTES = 4



def addQueueMessage(*msg):
    str_msg = string_utils.stringify_list(msg)
    message_queue.append(str_msg)
    if message_queue.__len__() > MAX_BUFFER:
        flushMessages()

def flushMessages():
    # time.sleep(1)
    global message_queue
    messages = "\n".join(message_queue)
    message_queue = []
    kprint.replaceLine(messages, new_line=True)
    # if progress_queue is not None:
    #     # kprint.appendLine("[KLOGGER]"+ progress_queue)
    #     kprint.replaceLine("[KLOGGER]" + str(logger_dict[PROGRESS]), new_line=True)



if __name__ == "__main__":
    progress_queue = "[]"

    for i in range(1, 51):
        progress_queue = "[" + i*'=' + ">]" + " " + str(i) + "%"

        addQueueMessage("message No ", str(i))
        progress_queue = "[" + i*'=' + ">]" + " " + str(i) + "%"
    for i in range(51, 101):
        progress_queue = "[" + i*'=' + ">]" + " " + str(i) + "%"
        addQueueMessage("message No ", str(i))

    flushMessages()