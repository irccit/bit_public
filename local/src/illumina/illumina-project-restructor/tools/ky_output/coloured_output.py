from kmessage import MessageType

__author__ = 'alexandre-nadin'


class bcolors:

    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class AnsiCode:

    """   """

    """Sequence Codes """
    SEQ_ESCAPE = '\033['
    SEQ_END = 'm'
    SEQ_SEPARATOR = ';'

    """ Special """
    ATTRIBUTES_OFF = '0'
    BOLD_ON = '1'
    UNDERSCORE = '4'
    BLINK_ON = '5'
    REVERSE_VIDEO_ON = '7'
    CONCEALED_ON = '8'

    """ Foreground """
    BLACK_FG = '30'
    RED_FG = '31'
    GREEN_FG = '32'
    YELLOW_FG = '33'
    BLUE_FG = '34'
    MAGENTA_FG = '35'
    CYAN_FG = '36'
    WHITE_FG = '37'

    """ Background """
    BLACK_BG = '40'
    RED_BG = '41'
    GREEN_BG = '42'
    YELLOW_BG = '43'
    BLUE_BG = '44'
    MAGENTA_BG = '45'
    CYAN_BG = '46'
    WHITE_BG = '47'


class ColouredMsg:

    MSG_HEADERS = {
        MessageType.WARNING : "[!]"
        , MessageType.ERROR : "[ERR]"
        , MessageType.VALID : "[OK]"
        , MessageType.NOTICE : "[NOTICE]"
    }

    MSG_HEADERS_CODES = {
        MessageType.WARNING : (AnsiCode.YELLOW_FG, AnsiCode.BOLD_ON, AnsiCode.BLINK_ON)
        , MessageType.ERROR : (AnsiCode.RED_BG, AnsiCode.WHITE_FG, AnsiCode.BOLD_ON)
        , MessageType.VALID : (AnsiCode.GREEN_FG, AnsiCode.BOLD_ON)
        , MessageType.NOTICE : (AnsiCode.YELLOW_FG, AnsiCode.BOLD_ON)
    }

    MSG_CODES = {
        MessageType.WARNING : (AnsiCode.YELLOW_FG, AnsiCode.BOLD_ON)
        , MessageType.ERROR : (AnsiCode.RED_FG, AnsiCode.BOLD_ON)
        , MessageType.VALID : (AnsiCode.GREEN_FG, AnsiCode.BOLD_ON)
        , MessageType.NOTICE : (AnsiCode.YELLOW_FG, AnsiCode.BOLD_ON)
    }


    def __init__(self):
        pass


    def buildSequence(self, *codes):
        seq = AnsiCode.SEQ_ESCAPE
        for code in codes:
            seq += str(code) + AnsiCode.SEQ_SEPARATOR

        # Removes the last separator. For some reason it may not work on some terminal.
        seq = str(seq).rstrip(AnsiCode.SEQ_SEPARATOR)
        seq += AnsiCode.SEQ_END
        return seq



    def buildMessage(self, msg, msg_type, header_only, with_header, extra_codes):

        extra_codes = tuple(extra_codes) if extra_codes is not None else ()

        header_msg = ""
        if with_header or header_only:
            header_msg = str(self.buildSequence(*(self.MSG_HEADERS_CODES[msg_type])))\
                + str(self.MSG_HEADERS[msg_type])\
                + str(self.buildSequence(AnsiCode.ATTRIBUTES_OFF))\

        if header_only:
            return header_msg

        core_msg = str(self.buildSequence(*(self.MSG_CODES[msg_type] + extra_codes)))\
            + str(msg)\
            + str(self.buildSequence(AnsiCode.ATTRIBUTES_OFF))

        return header_msg + core_msg


    def warn(self, msg, header_only=False, with_header=False, ansi_codes=None):
        return self.buildMessage(msg, MessageType.WARNING, header_only, with_header, ansi_codes)


    def notice(self, msg, header_only=False, with_header=False, ansi_codes=None):
        return self.buildMessage(msg, MessageType.NOTICE, header_only, with_header, ansi_codes)


    def valid(self, msg, header_only=False, with_header=False, ansi_codes=None):
        return self.buildMessage(msg, MessageType.VALID, header_only, with_header, ansi_codes)


    def error(self, msg, header_only=False, with_header=False, ansi_codes=None):
        return self.buildMessage(msg, MessageType.ERROR, header_only, with_header, ansi_codes)


################
## This chunk is made to make ColouredMsg's functions more accessible form outside.
####################
cm = ColouredMsg()

def message(msg, msg_type, header_only=False, with_header=False, ansi_codes=None):
    return cm.buildMessage(msg, msg_type, header_only, with_header, ansi_codes)

def warn(msg="", header_only=False, with_header=False, ansi_codes=None):
    return cm.warn(msg, header_only, with_header, ansi_codes)


def notice(msg="", header_only=False, with_header=False, ansi_codes=None):
    return cm.notice(msg, header_only, with_header, ansi_codes)


def valid(msg="", header_only=False, with_header=False, ansi_codes=None):
    return cm.valid(msg, header_only, with_header, ansi_codes)


def error(msg="", header_only=False, with_header=False, ansi_codes=None):
    return cm.error(msg, header_only, with_header, ansi_codes)


def test():
    cm = ColouredMsg()
    print cm.warn("This is a warning!", header_only=True)
    print cm.valid("Step validated", with_header=True, ansi_codes=[AnsiCode.RED_BG])
    print cm.error("THat, is, wrong", with_header=True)
    print
    print warn("(hidden) This is a warning!", with_header=True)
    print valid("(hidden) Step validated")
    print error("(hidden) THat, is, wrong")
    print message("(hidden) error message", MessageType.WARNING, header_only=False, with_header=True)

if __name__ == "__main__":
    test()