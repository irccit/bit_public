__author__ = 'alexandre-nadin'


class MessageType:
    """
    Here are defined every type of message.
    """

    WARNING = 0
    ERROR = 1
    VALID = 2
    NOTICE = 3

    def __init__(self):
        pass


class Message:

    VALIDATED = "OK"
    INVALIDATED = "KO"

    def __init__(self):
        pass
