__author__ = 'alexandre-nadin'


def isProcessListTerminated(process_list):
    """
    Checks if all processes in a list are terminated.
    :param process_list:
    :return:
    """
    for p in process_list:
        if p.is_alive():
            return False
    return True


if __name__ == "__main__":
    pass