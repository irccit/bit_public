__author__ = 'alexandre-nadin'

import multiprocessing
from multiprocessing import Process
from os import getpid
import time

def execute_lambdas(lambda_list):
    """
    Executes a list of lambda functions in parallel.
    Not tested for unordered list!
    :param lambda_list: a list of lambda functions.
    :return:
    """
    proc_list = []
    for func in lambda_list:
        p = Process(target=func)
        p.start()
        proc_list.append(p)
        # time.sleep(delay)

    ## Joining pocesses
    for proc in proc_list:
        proc.join()
        print "joined proc", proc.results


def map_partial_functions(funcs, nb_procs=None):
    """
    Executes functions defined with functools.partial()
    Those partial functions seem to be pickable by pool.map.
    The use of pool.map seems easier to get the results from parallel processes, than using
    Process.start().
    :return: A list of each function' result.
    """
    pool = multiprocessing.Pool(processes=nb_procs)

    result = pool.map(execute_lambda, funcs)
    pool.close()
    pool.join()
    return result

def execute_lambda(func):
    return func()


def test_args(index, name, time_sleep, result_dic):
    # msg = "processed-" + str(arg) + "(pid: "
    # print msg
    time.sleep(time_sleep)
    msg = 'processed-%s-%s (pid: %d)' %(str(index), str(name), getpid())
    result_dic[index] = name
    # print msg

if __name__ == "__main__":
    pass