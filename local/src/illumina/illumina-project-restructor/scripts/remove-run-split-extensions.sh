echo "I take a run folder as input, go to each one of its subfolders and remove the .split* extensions from every fastq.gz file"

[[ ! -d ${1} ]] && echo "I need a Run folder. Feed me with one, now!" && exit
SPLIT_EXT_PATTERN=".split"
for fold in ${1}/*
do
        #rm $fold/*.fastq.gz
        for spli in ${fold}/*${SPLIT_EXT_PATTERN}*
        do  
                mv ${spli} ${spli%${SPLIT_EXT_PATTERN}*}
        done
done
