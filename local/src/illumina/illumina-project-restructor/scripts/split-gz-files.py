__author__ = 'Alexandre Nadin'

import os, sys
from tools.ky_multiprocessing import function_processor as fp
from tools.file_utils import file_compression as fc
from illumina_project_restructor.illumina_config import CompressedFiles

__file_name__ = os.path.basename(__file__)
ARGUMENTS = [__file_name__, "file.gz ..."]

def get_manual():
    return "\n\tWho: \t" + __file_name__ + "\n"\
         + "\n\tWhat: \tSplits files in parallel.\n"\
         + "\n\tHow: \t$ python " + " ".join(ARGUMENTS) + "\n"


def get_parsed_args(args):
    if args.__len__() < ARGUMENTS.__len__():
        print "Too few arguments provided.\n", get_manual()
        sys.exit(1)
    script, files = args.pop(0), args
    for f in files:
        if not os.path.isfile(f):
            print "This is not a file:", f, "\nPlease feed me with valid gz files.\n", get_manual()
            sys.exit(2)
        elif not str(f).endswith(fc.Extensions.GUNZIP):
            print "This is not a gzip file:", f, "\nPlease feed me with valid gzip files.\n", get_manual()
            sys.exit(2)
    return script, files

if __name__ == "__main__":
    (script, files) = get_parsed_args(sys.argv)

    print "Files to split: "
    for f in files:
        print "\t", f


    # from illumina_project_restructor.illumina_restructor import split_fastq_files
    # split_fastq_files(files)
    from illumina_project_restructor.illumina_config import IlluminaConfig, CompressedFiles
    import functools
    # proc_list = []
    # for f in files:
    #     proc_list.append(
    #         functools.partial(
    #             fc.splitGzipFile
    #             , fileName=f
    #             , chunkPerSplit=CompressedFiles.CHUNKS_PER_SPLIT
    #             , linePerChunk=CompressedFiles.LINES_PER_CHUNK
    #             , verbose=False
    #         )
    #     )
    # fp.map_partial_functions(proc_list)

    proc_list = []
    for f in files:
        proc_list.append(
            functools.partial(
                fc.split_gz_file
                , file_name=f
                , chunks_per_split=CompressedFiles.CHUNKS_PER_SPLIT
                , lines_per_chunk=CompressedFiles.LINES_PER_CHUNK
                , file_permission=IlluminaConfig.CREATED_FILES_PERMISSION
                , verbose=True
                , dest="test-splitting"
            )
        )
    fp.map_partial_functions(proc_list)
    print "Splitting is over."
