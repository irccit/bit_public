#!/bin/bash

# ----------------------
#   General Overview
# ----------------------
: << 'OVERVIEW'
Hi! I am a template script. I am aimed at launching various programs belonging to a project.

I provide useful general features and particularly I strive for setting environment variables in a clean and efficient
way. Here is a general description of what I am supposed to do.

First I set my own environment variables. In doing so, I create my project's home path in the user's home directory.

Then I am supposed to parse the arguments I am provided with, retrieving the program I am to launch along with it's own
corresponding arguments. This part is more likely the one you want to tweak the most. Do your stuff right, and let me
take care of the launching later.

Finally, I will launch your specified program, using one of the program translater you would have provided. I will
put the command output in a dated and time-stamped file located in your (newly created) project's home folder.

Now, let's get our naming convention clear. Whatever you decide to launch, whether it is a mere script, a program, a big
software, or whatever the crappy thing you want to launch, I am The SCRIPT. To me, what you want to launch is a PROGRAM.
Period. I am located in a folder which itself is located in your project folder (like project/scripts/, or project/bin/).
So basically I am part of your project.

The interpreter, the compiler, all those things are what I call a translater. The translater's path will be set
in my environment path.

Last words: make sure to export your project path. For instance if your project is written in python, you will certainly
need to export the variable PYTHONPATH, and append your project path into it.

Those conventions don't please you? By all means, tweak me! ;)

Any suggestion, you are welcome to send an email to the guy who programmed me.
OVERVIEW

# ----------------------
#   Launcher basics
# ----------------------

__WHO__=$(basename ${0})
__WHAT__="Launches a python program, taking care of environment variables."
__HOW__="$ ${__WHO__} python-script.py [arguments ...]"
__EXAMPLE__="$ $__WHO__ restruct-illumina-project.py -h"

## The translator's possible paths.
__TRANSLATOR__=("/usr/bin/python2.7/bin/python2.7" \
            "/some/other/path" \
            "default-python"
            )

function repeat_char() {
    res=""
    for ((i = 1; i <= ${2}; i++)); do
        res+=${1}
    done
    echo ${res}
}

function get_manual() {
    manual=""
    [[ -n ${__EXAMPLE__} ]] && manual="\n\tWho: ${__WHO__}\n"
    [[ -n ${__WHAT__} ]] && manual+="\n\tWhat: ${__WHAT__}\n"
    [[ -n ${__HOW__} ]] && manual+="\n\tHow: ${__HOW__}\n"
    [[ -n ${__EXAMPLE__} ]] && manual+="\n\tEx: ${__EXAMPLE__}\n"
    echo "$manual"
}

function get_full_path() {
    # Retrieves a full path. This assumes the path given is accessible from the working directory.
    #

    cur_dir=$(pwd)
    cd ${1}
    full_path=$(pwd)
    cd ${cur_dir}
    echo ${full_path}
}


# ------------------------------------------
#   Set script's and project's variables
# ------------------------------------------

__SCRIPT_CMD__=${0}
__SCRIPT_BASENAME__=$(basename ${__SCRIPT_CMD__})
__SCRIPT_DIR__=$(get_full_path $(dirname ${__SCRIPT_CMD__}))
__PROJECT_PATH__=$(dirname ${__SCRIPT_DIR__})
__PROJECT_BASENAME__=$(basename ${__PROJECT_PATH__})
echo script path: ${__SCRIPT_DIR__}
echo script basename: ${__SCRIPT_BASENAME__}
echo project path: ${__PROJECT_PATH__}
echo project basename: ${__PROJECT_BASENAME__}

## Set the project's home folder
[ ${HOME} ] && __USR_HOME__=${HOME} || [ ~ ] && __USR_HOME__=~ || __USR_HOME__="."
__PROJECT_HOME__=${__USR_HOME__}/.${__PROJECT_BASENAME__}
[ ! -d ${__PROJECT_HOME__} ] && mkdir ${__PROJECT_HOME__} && echo "Created project's home folder in ${__PROJECT_HOME__}."



## Get the expected arguments
P_CMD=${0}
P_SCRIPT=${1}
P_ARGS=${@:2}

## Test arguments
[[ -z ${P_SCRIPT} ]] && echo "Error: I run scripts. Please feed me with one." && echo -e $(get_manual) && exit


## Set environment variables
CURRENT_DIR=$(pwd)
CMD_DIR=$(get_full_path $(dirname ${P_CMD}))

[ ! -f ${P_SCRIPT} ] && echo -e "Error: script ${SCRIPT} does not exist." && echo -e $(get_manual) && exit

PROJECT_PATH=$(dirname ${CMD_DIR})
export PYTHONPATH=$PYTHONPATH:"$PROJECT_PATH"

## Set the interpreter's command.
CMD=python
for cmd in ${CMD_PATHS[@]}
do
    [[ -f ${cmd} ]] && CMD=${cmd}
done

## Launch the script.
${CMD} ${P_SCRIPT} ${P_ARGS[@]}
