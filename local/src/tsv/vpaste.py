#!/usr/bin/env python
#
# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage

def main():
	parser = OptionParser(usage=format_usage('''
		%prog [OPTIONS] INPUT... >PASTED

		Pastes multiple files together vertically.
	'''))
	options, args = parser.parse_args()

	if len(args) < 1:
		exit('Unexpected argument number.')

	fds = [ file(p, 'r') for p in args ]
	eofs = [ False for p in args ]
	while False in eofs:
		for idx, fd in enumerate(fds):
			if eofs[idx]: continue
			
			line = fd.readline()
			if len(line) == 0:
				eofs[idx] = True
				continue

			line = safe_rstrip(line)
			print line

if __name__ == '__main__':
	main()
