#!/usr/bin/env python
#
# Copyright 2008,2010 Gabriele Sales <gbrsales@gmail.com>

from array import array
from collections import defaultdict
from labels import load_labels
from optparse import OptionParser
from sys import stdin
from vfork.io.colreader import Reader
from vfork.util import exit, format_usage


def swap(tuples):
	for (a,b) in tuples:
		yield (b,a)

def label_serials(labels):
	return dict(swap(enumerate(labels)))

def label_set():
	return LabelSet([], False)

def label_frozenset(labels):
	return LabelSet(labels, True)


class LabelSet(object):
	def __init__(self, labels, frozen):
		self.labels = set(labels)
		self.sorted_labels = labels
		self.frozen = frozen

	def __len__(self):
		return len(self.labels)

	def __getitem__(self, lbl):
		if self.frozen:
			if lbl not in self.labels:
				raise KeyError
		else:
			if not lbl in self.labels:
				self.labels.add(lbl)
				self.sorted_labels.append(lbl)
		return lbl

	def __iter__(self):
		return self.sorted_labels.__iter__()


class BinaryMatrix(object):
	def __init__(self, rlbls, clbls):
		self.rlbls = rlbls
		self.xnum = len(self.rlbls)
		self.clbls = clbls
		self.ynum = len(self.clbls)
		self.data = array('B', [0]*(self.xnum*self.ynum))
	
	def set(self, rlbl, clbl, v):
		assert v == 1
		x = self.rlbls[rlbl]
		y = self.clbls[clbl]
		self.data[x*self.ynum + y] = 1

	def rows(self):
		for idx in xrange(self.xnum):
			start = idx * self.ynum
			yield self.data[start:start+self.ynum]


class WeightMatrix(object):
	def __init__(self, rlbls, clbls, missing):
		self.rlbls = rlbls
		self.clbls = clbls
		self.missing = missing
		self.values = defaultdict(lambda: defaultdict(dict))

	def set(self, rlbl, clbl, v):
		# register labels or check if they are present
		self.rlbls[rlbl]
		self.clbls[clbl]

		# TODO check for duplicates
		self.values[rlbl][clbl] = v

	def row(self, rlbl):
		row = self.values[rlbl]
		return [ row.get(c, self.missing) for c in self.clbls ]


def iter_binary_map(fd):
	for rlbl, clbl in Reader(fd, '0s,1s', False):
		yield rlbl, clbl, 1

def iter_weighted_map(fd):
	for rlbl, clbl, value in Reader(fd, '0s,1s,2s', False):
		yield rlbl, clbl, value


def main():
	parser = OptionParser(usage=format_usage('''
		%prog [OPTIONS] <XY_MAP >INCIDENCE_MATRIX

		Builds an incidence matrix out of a map file.

		When -b, -c and -r are used together the program
		adopts and optimized memory representation of the matrix.
	'''))
	parser.add_option('-b', '--binary', dest='binary', action='store_true', default=False, help='the map represents binary relations (if a pair is present, it has weight 1; otherwise 0)')
	parser.add_option('-c', '--column-labels', dest='column_labels', help='a FILE holding column labels', metavar='FILE')
	parser.add_option('-r', '--row-labels', dest='row_labels', help='a FILE holding row labels', metavar='FILE')
	parser.add_option('-m', '--missing', dest='missing', default='0', help='value for missing entries (default: %default)')
	parser.add_option('-C', '--column-header', dest='column_header', action='store_true', default=False, help='print the column header')
	parser.add_option('-R', '--row-header', dest='row_header', action='store_true', default=False, help='print the row header')
	parser.add_option('-k', '--kill', dest='kill', action='store_true', default=False, help='ignore relations between unknown labels')
	options, args = parser.parse_args()

	if len(args) != 0:
		exit('Unexpected argument number.')
	elif options.column_labels is None and not options.column_header:
		exit('You should use one of --column-labels and --column-header.')
	elif options.row_labels is None and not options.row_header:
		exit('You should use one of --row-labels and --row-header.')
	elif options.binary and (not options.column_labels or not options.row_labels):
		exit('--binary requires --column-labels and --row-labels.')
	
	if options.row_labels:
		labels = load_labels(options.row_labels)
		if options.binary:
			row_labels = label_serials(labels)
		else:
			row_labels = label_frozenset(labels)
	else:
		row_labels = label_set()

	if options.column_labels:
		labels = load_labels(options.column_labels)
		if options.binary:
			column_labels = label_serials(labels)
		else:
			column_labels = label_frozenset(labels)
	else:
		column_labels = label_set()

	if options.binary and options.row_labels and options.column_labels:
		matrix = BinaryMatrix(row_labels, column_labels)
		iter_map = iter_binary_map
	else:
		matrix = WeightMatrix(row_labels, column_labels, options.missing)
		iter_map = iter_weighted_map

	for rlbl, clbl, value in iter_map(stdin):
		try:
			matrix.set(rlbl, clbl, value)
		except KeyError:
			if not options.kill:
				exit('Unexpected relation between %s and %s.' % (rlbl, clbl))

	if options.column_header:
		o = []
		if options.row_header: o.append('')
		o += list(column_labels)
		print '\t'.join(o)
	
	if isinstance(matrix, BinaryMatrix):
		if options.row_header:
			rlbs = list(row_labels)

		def format_values(value):
			return '1' if value == 1 else options.missing

		for idx, row in enumerate(matrix.rows()):
			o = []
			if options.row_header: o.append(rlbs[idx])
			o += [ format_values(v) for v in row ]
			print '\t'.join(o)
	else:
		def format_values(value):
			return options.missing if value is None else value

		for rlbl in row_labels:
			o = []
			if options.row_header: o.append(rlbl)
			o += matrix.row(rlbl)
			print '\t'.join(o)


if __name__ == '__main__':
	main()
