#!/usr/bin/env python
#
# Copyright 2009,2012,2014 Gabriele Sales <gbrsales@gmail.com>

from optparse import OptionParser
from os import chdir, environ, getcwd, mkdir
from os.path import abspath, basename, exists, isdir, islink, join, normpath, split
from subprocess import check_call, CalledProcessError, Popen, PIPE
from vfork.io.util import NamedTemporaryDirectory
from vfork.path import bit_root, link_relative, readlink, subdir_of
from vfork.util import ArgumentParser, exit
import re


def main():
    args = parseArgs()

    root = bit_root()
    editor = load_env('EDITOR')

    dataset = abspath(args.template)
    if not isdir(dataset):
        exit('Not a directory: ' + dataset)
    elif not subdir_of(dataset, root):
        exit('The dataset must be inside BIOINFO_ROOT.')

    links = {}
    for l in ('makefile', 'rules.mk'):
        path = join(dataset, l)
        if not islink(path):
            exit('Missing "%s" link in: %s' % (l, dataset))

        lpath = readlink(path)
        if not subdir_of(lpath, root):
            exit('Link "%s" points outside of BIOINFO_ROOT: %s' % (l, abspath(lpath)))

        links[l] = normpath(join(dataset, lpath))

    dname = basename(dataset)
    mkpath, mkname = split(links['makefile'])

    mkname = transform_name(mkname, args.sed_exprs)
    if args.name is None:
        dname = transform_name(dname, args.sed_exprs)
    else:
        dname = args.name

    with file(links['makefile'], 'r') as fd:
        body = transform(fd.read(), args.sed_exprs)

    if not args.batch:
        with NamedTemporaryDirectory() as wdir:
            spec_path = join(wdir.path, 'spec')

            with file(spec_path, 'w') as fd:
                print >>fd, '# -*- dataset_clone -*-'
                print >>fd, '# DATASET_NAME  %s' % dname
                print >>fd, '# MAKEFILE_NAME %s' % mkname
                print >>fd, '#'
                print >>fd, '# Delete the following line to abort cloning.'
                print >>fd, '# CLONE OK'
                print >>fd, '# -*- dataset_clone -*-'
                fd.write(body)

            run_editor(spec_path, editor)

            with file(spec_path, 'r') as fd:
                (dname, mkname), body = split_manifest(fd.read())

    dataset = join(getcwd(), dname)
    if exists(dataset):
        exit('Dataset already exists: %s' % dataset)

    makefile = join(mkpath, mkname)
    if exists(makefile):
        exit('Makefile already exists: %s' % makefile)

    mkdir(dataset)
    chdir(dataset)

    with file(makefile, 'w') as fd:
        fd.write(body)
    link_relative(makefile, 'makefile')
    link_relative(links['rules.mk'], 'rules.mk')

    if args.git_add:
        gitAdd([makefile, 'makefile', 'rules.mk'])


def parseArgs():
    parser = ArgumentParser(description='''
        Clones a dataset transforming its name and makefile according to the
        provided list of sed-like replacement expressions.

        Such expressions are evaluated as extended regular expressions.
    ''')
    parser.add_argument('template', metavar='TEMPLATE',
                        help='the template dataset')

    parser.add_argument('sed_exprs', metavar='SED_EXPR', nargs='*',
                        help='regular expressions to transform the template')

    parser.add_argument('-b', '--batch', action='store_true', default=False,
                      help='run in batch mode: do not require any user interaction')

    parser.add_argument('-g', '--git-add', action='store_true', default=False,
                      help='run "git add" on newly created files')

    parser.add_argument('-n', '--dataset-name', dest='name', metavar='NAME',
                      help='set the name of the new dataset')

    return parser.parse_args()

def load_env(varname):
    try:
        v = environ[varname]
        if len(v) == 0: raise ValueError
        return v
    except (KeyError, ValueError):
        exit('Missing or empty environment variable: %s' % varname)

def transform(value, exprs):
    if len(exprs) == 0:
        return value

    cmd = ['sed', '-r']
    for expr in exprs:
        cmd += ['-e', expr]

    proc = Popen(cmd, shell=False, close_fds=True, stdin=PIPE, stdout=PIPE)
    pout, _ = proc.communicate(value)
    if proc.wait() != 0:
        exit('Error in sed expressions.')

    return pout.rstrip('\n')

def transform_name(name, exprs):
    return transform(name + '\n', exprs).rstrip('\n')

def run_editor(path, editor):
    proc = Popen([editor, path], shell=False, close_fds=True)
    if proc.wait() != 0:
        exit('Error running EDITOR.')

def split_manifest(content):
    header = []
    body = []
    inside_header = False

    for line in content.split('\n'):
        if '-*- dataset_clone -*-' in line:
            inside_header = not inside_header
        elif inside_header:
            header.append(line)
        else:
            body.append(line)

    return parse_header(header), '\n'.join(body)

def parse_header(lines):
    name = None
    clone = False

    name_rx = re.compile('DATASET_NAME\s+(.*)')
    makefile_rx = re.compile('MAKEFILE_NAME\s+(.*)')
    clone_rx = re.compile('CLONE\s+OK')

    for line in lines:
        m = name_rx.search(line)
        if m is not None:
            name = m.group(1)
            continue

        m = makefile_rx.search(line)
        if m is not None:
            mkname = m.group(1)
            continue

        m = clone_rx.search(line)
        if m is not None:
            clone = True

    if name is None:
        exit('Dataset name is missing from header; did you cancel it?')
    elif not clone:
        exit('No "CLONE OK" statement. Aborting.')
    else:
        return name, mkname

def gitAdd(paths):
    try:
        check_call(['git', 'add', '--force'] + paths, shell=False, close_fds=True)
    except CalledProcessError:
        exit('Git error.')


if __name__ == '__main__':
    main()
