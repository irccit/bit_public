#!/usr/bin/env python
#
# Copyright 2012 Gabriele Sales <gbrsales@gmail.com>

from optparse import OptionParser
from os import environ, walk
from os.path import relpath
from subprocess import check_output, CalledProcessError
from vfork.util import format_usage, exit
from vfork.path import bit_root, subdir_of
from sys import exit


def call_du(path, root):
    try:
        output = check_output(['du', '-hs', relpath(path, root)], cwd=root)
    except CalledProcessError:
        exit('Error running du on: ' + path)

    return output.strip()

def parse_size(entry):
    size = entry.split(None, 2)[0].lower()

    scale = 1
    if size[-1] in 'kmg':
        scale = size[-1]
        size = size[:-1]

        if scale == 'k':
            scale = 1024
        elif scale == 'm':
            scale = 1024*1024
        else:
            scale = 1024*1024*1024

    try:
        size = float(size.replace(',', '.'))
    except ValueError:
        exit('Unexpected size format in du output: %s' % size)

    return size * scale

def main():
    parser = OptionParser(usage=format_usage('''
        %prog [OPTIONS] [ROOT]

        Shows the disk usage of each dataset under ROOT (by default BIOINFO_ROOT).
    '''))
    parser.add_option('-s', '--sort', dest='sort', action='store_true', default=False, help='sort entries by size')
    options, args = parser.parse_args()
    if len(args) > 1:
        exit('Unexpected argument number.')

    root = bit_root()
    if len(args):
        if not subdir_of(args[0], root):
            exit('The supplied ROOT is outside of BIOINFO_ROOT.')
        root = args[0]

    log = []
    for dirpath, dirnames, filenames in walk(root):
        if '/dataset/' in dirpath and 'makefile' in [ f.lower() for f in filenames ]:
            dirnames[:] = []

            entry = call_du(dirpath, root)
            if options.sort:
                log.append((parse_size(entry), entry))
            else:
                print entry

    if options.sort: log.sort()
    for _, entry in log:
        print entry


if __name__ == '__main__':
    main()
