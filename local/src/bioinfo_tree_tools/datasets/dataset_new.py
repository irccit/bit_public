#!/usr/bin/env python
#
# Copyright 2012,2014 Gabriele Sales <gbrsales@gmail.com>

from os import chdir, environ, getcwd, mkdir, sep
from os.path import abspath, exists, isdir, join, split
from subprocess import check_call
from vfork.path import bit_root, link_relative, subdir_of
from vfork.util import ArgumentParser, exit


def main():
    args = parse_args()
    
    root = bit_root()
    work_dir = getcwd()
    if not subdir_of(work_dir, root):
        exit('Cannot work outside of BIOINFO_ROOT: ' + root)

    dataset_dir = join(work_dir, args.name)
    if exists(dataset_dir):
        exit('The required dataset already exists.')

    share_dir = find_share(work_dir, root)

    rules_dir = join(share_dir, 'rules')
    if not isdir(rules_dir):
        exit('The rules directory does not exist: ' + rules_dir)

    rules_path = add_mk_suffix_if_missing(join(rules_dir, args.rules))
    if exists(rules_path):
        if args.create_rules:
            exit('Rules file already exist: ' + rules_path)
    elif not args.create_rules:
        exit('Cannot find rules: ' + rules_path)
    else:
        touch(rules_path)

    makefiles_dir = join(share_dir, 'makefiles')
    if not isdir(makefiles_dir):
        exit('The makefiles directory does not exist: ' + makefiles_dir)

    makefile_path = join(makefiles_dir, args.name if args.makefile is None else args.makefile)
    if exists(makefile_path):
        exit('The required makefile already exists: ' + makefile_path)

    mkdir(dataset_dir)
    link_relative(rules_path, join(dataset_dir, 'rules.mk'))
    touch(makefile_path)
    link_relative(makefile_path, join(dataset_dir, 'makefile'))

    if args.git_add:
        paths = [ makefile_path, join(dataset_dir, 'rules.mk'),
                  join(dataset_dir, 'makefile') ]

        if args.create_rules:
            paths.append(rules_path)

        git_add(paths)

def parse_args():
    parser = ArgumentParser(description='''
      Creates a new dataset as a child of the current directory.
      Links the rules with the supplied filename (with or without
      the ".mk" extension) and creates (and links) a new empy makefile.

      If explicitly requested (--create-rules option) also creates a new
      empty file for the rules.
    ''')
    parser.add_argument('name', metavar='NAME', help='dataset name')
    parser.add_argument('rules', metavar='RULES', help='rule name')
    parser.add_argument('-m', '--makefile', dest='makefile', metavar='MAKEFILE',
                        help='makefile name (default: the same of the dataset)')
    parser.add_argument('-r', '--create-rules', dest='create_rules', action='store_true',
                        help='creates an empty file for the rules')
    parser.add_argument('-g', '--git-add', action='store_true',
                        help='run "git add" on newly created files')
    args = parser.parse_args()

    if sep in args.name:
        exit('Invalid dataset name: ' + args.name)

    return args

def load_env(varname):
    try:
        v = environ[varname]
        if len(v) == 0: raise ValueError
        return v
    except (KeyError, ValueError):
        exit('Missing or empty environment variable: %s' % varname)

def find_share(wdir, root):
    while subdir_of(wdir, root):
        path, name = split(wdir)
        candidate = join(wdir, 'local', 'share')
        if exists(candidate):
            return candidate
        
        wdir = path

    exit('Cannot find a valid "local/share" directory.')

def touch(path):
    with file(path, 'w'):
        pass

def add_mk_suffix_if_missing(path):
    if path.endswith('.mk'):
        return path
    else:
        return path + '.mk'

def git_add(paths):
    try:
        check_call(['git', 'add', '--force'] + paths, shell=False, close_fds=True)
    except CalledProcessError:
        exit('git error.')


if __name__ == '__main__':
    main()
