#!/usr/bin/env python2.7
#
# Copyright 2008-2011,2015 Gabriele Sales <gbrsales@gmail.com>

from glob import glob
from optparse import OptionParser
from os import environ, readlink, sep, stat, getcwd
from os.path import abspath, dirname, exists, isdir, islink, isfile, join, normpath
from sys import exit, stdin, stderr
from time import time
from vfork.path import bit_root, get_workdir, subdir_of
import errno


def load_env():
	root = bit_root()

	try:
		host = environ['BIOINFO_HOST']
	except KeyError:
		exit('BIOINFO_HOST is not defined.')

	start_time = environ.get('BMAKE_START_TIME', time())

	return root, host, start_time

class MalformedInfo(Exception): pass

def read_info(filename=None):
	res = dict((p,[]) for p in 'MIC')
	if filename is None:
		return res

	try:
		fd = file(filename, 'r')
	except IOError, e:
		if e.errno == errno.ENOENT:
			return res
		else:
			raise

	try:
		for line in fd:
			line = line.rstrip('\n')
			if len(line) < 3 or line[1] != '|': raise MalformedInfo
			try:
				res[line[0]].append(line[2:])
			except KeyError:
				raise MalformedInfo
	finally:
		fd.close()

	return res

def qualify_module(module):
	parts = module.split('/')
	varname = '__BMAKE_IMPORT_%s' % '_'.join(parts)

	parts = parts[:-1] + ['local', 'share', 'rules', parts[-1]+'.mk']
	path = join(*parts)

	return varname, path

class PathScanner(object):
	def __init__(self, base, root):
		self.start_points = [ base ]
		self.pos = base
		self.root = root

def is_subdir(path, root):
	root += '/'
	return path[:len(root)] == root

def select_children(root, paths):
	c = []
	o = []
	for p in paths:
		if is_subdir(p, root):
			c.append(p)
		else:
			o.append(p)
	return c, o

def list_paths(base, other_paths, root):

	def helper(base, other_paths, root, res):
		pos = base
		while pos == root or is_subdir(pos, root):
			children, other_paths = select_children(pos, other_paths)
			if len(children):
				children.sort(key=lambda x: len(x), reverse=True)
				helper(children[0], children[1:], pos, res)

			if pos != root:
				res.append(pos)
			pos = dirname(pos)

	res = []
	helper(base, other_paths, root, res)
	res.append(root)
	return res

def find_prj_root(path=getcwd()):
    if isfile(join(path,".PRJ_ROOT")):
        return path
    else:
        if len(path)>1:
            return find_prj_root(dirname(path))
        else:
            raise Exception("Can not find the PRJ_ROOT directory")

def collect_paths(base, module_dirs, root, host):
	dirnames = ('bin', 'lib')
	varnames = ('path', 'library_path')
	prefix_len = len(root) + 1
	paths = [ list() for i in dirnames ]
	task_root = None
	instance_root = None

	for path in list_paths(base, module_dirs, root):
		bin_base = join(root, 'binary', host, path[prefix_len:], 'local')
		for i, d in enumerate(dirnames):
			if isdir(join(bin_base, d)):
				paths[i].append(join('$(BIOINFO_ROOT)', 'binary', '$(BIOINFO_HOST)', path[prefix_len:], 'local', d))

		if instance_root is None and exists(join(path, 'makefile')) and not exists(join(path, '..', 'makefile')):
			instance_root = join('$(BIOINFO_ROOT)', path[prefix_len:])

		base = join(path, 'dataset')
		if task_root is None and isdir(base):
			task_root = join('$(BIOINFO_ROOT)', path[prefix_len:])

		base = join(path, 'local')
		for i, d in enumerate(dirnames):
			if isdir(join(base, d)):
				paths[i].append(join('$(BIOINFO_ROOT)', base[prefix_len:], d))

	if task_root is None:
		task_root = find_prj_root()

	return task_root, instance_root, dict(zip(varnames, paths))

def include2var(include):
	name = normpath(include).replace('..', '^').replace('/', '.')
	return '__BMAKE_INCLUDE_' + name.replace('.', '_')

output_level = None
def emit(v, level):
	if level >= output_level:
		print v

def main():
	global output_level

	parser = OptionParser(usage='%prog [OPTIONS] [INFO] >PATH_DEFS')
	parser.add_option('-f', '--fingerprints', dest='fprints', help='store module and include fingerprints into FILE', metavar='FILE')
	parser.add_option('-o', '--outside_root', dest='outside_root', action='store_true', default=False, help='allow execution outside of $BIOINFO_ROOT')
	parser.add_option('',   '--only-make-paths', dest='only_make_paths', action='store_true', default=False, help='print only make paths')
	options, args = parser.parse_args()
	if len(args) > 1:
		exit('Unexpected argument number.')

	output_level = 2 if options.only_make_paths else 1

	root, host, start_time = load_env()
	cwd = abspath(normpath(get_workdir()))

	if not options.outside_root and not subdir_of(cwd, root):
		exit("Can't work outside BIOINFO_ROOT.")

	try:
		info = read_info(args[0] if len(args) else None)
	except MalformedInfo:
		exit('malformed dynpaths information file.\nRemove the .bmake directory and rerun bmake.')

	fprints_fd = file(options.fprints, 'w') if options.fprints is not None else None

	emit('__BMAKE_DYNPATHS:=1', 1)

	module_dirs = set()
	for module in info['M']:
		varname, path = qualify_module(module)
		module_dirs.add(dirname(join(root, path)))

		emit('%s:=$(BIOINFO_ROOT)/%s' % (varname, path), 1)
		if fprints_fd:
			finfo = stat(join(root, path))
			print >>fprints_fd, 'M|%s|%s|%d|%d' % (module.replace('/', '.'), path, finfo.st_ino, min(finfo.st_mtime, start_time))

	if fprints_fd:
		finfo = stat('rules.mk')
		print >>fprints_fd, 'M|%s|%s|%d|%d' % ('rules', join(cwd[len(root)+1:], 'rules.mk'), finfo.st_ino, min(finfo.st_mtime, start_time))

	for include in info['I']:
		emit('%s:=%s' % (include2var(include), include), 1)
		if fprints_fd:
			finfo = stat(include)
			print >>fprints_fd, 'I|%s|%d|%d' % (include, finfo.st_ino, min(finfo.st_mtime, start_time))

	if fprints_fd:
		fprints_fd.close()

	for context in info['C']:
		module_dirs.add(join(root, context))
		emit('__BMAKE_CONTEXT_%s:=yes' % context.replace('/', '_'), 1)

	task_root, instance_root, make_paths = collect_paths(cwd, module_dirs, root, host)
	if not options.only_make_paths:
		emit('export TASK_ROOT:=%s' % task_root, 1)
		emit('export PRJ_ROOT:=%s' % task_root, 1)
		if instance_root:
			emit('export INSTANCE_ROOT:=%s' % instance_root, 1)

	for n, ps in make_paths.iteritems():
		if len(ps) > 0:
			fmt = 'MAKE_%s=%s' if options.only_make_paths else 'export MAKE_%s:=%s'
			emit(fmt % (n.upper(), ':'.join(ps)), 2)

if __name__ == '__main__':
	main()
