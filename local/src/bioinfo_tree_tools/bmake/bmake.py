#!/usr/bin/env python2.7
#
# Copyright 2009,2012,2014,2016 Gabriele Sales <gbrsales@gmail.com>

from __future__ import with_statement
from errno import EEXIST, ENOENT
from fcntl import lockf, LOCK_EX, LOCK_UN
from glob import glob
from optparse import OptionParser
from os import chdir, environ, execvp, makedirs, readlink, stat, symlink, unlink, O_NDELAY
from os.path import abspath, dirname, exists, isdir, isfile, islink, join, normpath, split
from shutil import rmtree
from subprocess import Popen, PIPE
from sys import stdout, stderr, version_info
from sys import exit as system_exit
from tempfile import NamedTemporaryFile
from time import time
from vfork.path import bit_root, get_workdir, subdir_of
from vfork.util import exit, format_usage, min_python_version
import re

work_dir = '.bmake'
host_dir = None


def lookup_env(var):
	try:
		return environ[var]
	except KeyError:
		exit(var + ' is not defined.')

def extend_path(elt):
	environ['PATH'] = '%s:%s' % (lookup_env('PATH'), elt)

def host_path(path):
	return join(host_dir, path)

def process_external(path, root):
	path = normpath(abspath(path))
	wdir, target = split(path)
	if len(wdir) == 0:
		exit('You defined an external dependency, but it resides in the current working directory: ' + target)
	elif not subdir_of(wdir, root):
		exit('Target is outside of BIOINFO_ROOT.')

	while not isfile(join(wdir,"makefile")) and len(wdir)>1:#search for a suitable makefile, if makefile do not exist in the first directory look if there is a makefile in a parent folder
		wdir, garbage = split(wdir)

	do_layout(wdir, root)
	remove_dangling_symlink(wdir, path)

	storage_path = external_storage_path(path, root)
	if storage_path is None:
		chdir(wdir)
		environ['PWD'] = wdir
		execvp('bmake', ['bmake', target])
		exit('Error running child bmake.')
	else:
		symlink(storage_path, path)

def remove_dangling_symlink(wdir, path):
	if not islink(path):
		return

	dest = readlink(path)
	if not exists(join(wdir, dest)):
		unlink(path)

def external_storage_path(path, root):
	try:
		storage_root = normpath(abspath(environ['BIOINFO_STORE']))
	except KeyError:
		return None

	storage_path = join(storage_root, path[len(root)+1:])
	if isfile(storage_path):
		return storage_path
	else:
		return None

def do_layout(path, root):
	if isdir(path): return

	cursor = path
	while True:
		cursor = dirname(cursor)
		if not subdir_of(cursor, root):
			exit('Cannot find a makefile to build target directory: ' + path)

		if exists(join(cursor, 'makefile')):
			break

	levels = path[len(cursor)+1:].split('/')
	for i in xrange(len(levels)):
		next_path = join(cursor, *levels[:i+1])
		if isdir(next_path):
			continue

		chdir(join(cursor, *levels[:i]))
		cmd = Popen(['bmake', 'layout'])
		if cmd.wait() != 0:
			exit('Error making layout at target site.')
		elif isdir(next_path):
			continue

		cmd = Popen(['bmake', levels[i]])
		if cmd.wait() != 0:
			exit('Error making directory: ' + next_path)

def open_makefile():
	try:
		fd = file('Makefile', 'r+')
	except IOError:
		try:
			fd = file('makefile', 'r+')
		except IOError:
			exit('Error opening makefile.')
	return fd

def delete_workdir():
	def check_error(func, path, exc):
		if path != work_dir:
			raise
	rmtree(work_dir, onerror=check_error)

def make_hostdir():
	try:
		makedirs(host_dir)
	except OSError, e:
		if e.errno != EEXIST:
			raise e

def clean_cache(root):
	for module in list_cached('module'):
		unlink(module)
	for include in list_cached('include'):
		unlink(include)
	for context in list_cached('context'):
		unlink(context)

	fprints = load_fprints(host_path('fprints'))
	reset = False

	mks = list_cached('mk')
	for mk in mks - set(host_path(f[1] + '.mk') for f in fprints if f[0] == 'M'):
		unlink(mk)
		reset = True

	incs = list_cached('inc')
	for inc in incs - set(host_path(f[1] + '.inc') for f in fprints if f[0] == 'I'):
		unlink(inc)
		reset = True

	for info in fprints:
		if info[0] == 'M':
			module, path, inode, mtime = info[1:]
			cached = host_path(module + '.mk')
			if cached not in mks:
				continue
			else:
				path = join(root, path)
		else:
			path, inode, mtime = info[1:]
			cached = host_path(path + '.inc')
			if cached not in incs:
				continue

		try:
			info = stat(path)
		except OSError:
			info = None

		if info is None or info.st_ino != inode or int(info.st_mtime) > mtime:
			unlink(cached)
			reset = True

	if reset:
		for name in ('dynpaths.info', 'paths'):
			try:
				unlink(host_path(name))
			except OSError:
				pass

def load_fprints(path):
	try:
		res = []
		with file(path, 'r') as fd:
			for line in fd:
				parts = line.rstrip().split('|')
				if len(parts ) < 1:
					raise ValueError

				elif parts[0] == 'M':
					if len(parts) != 5: raise ValueError
					parts[3] = int(parts[3])
					parts[4] = int(parts[4])
					res.append(parts)

				elif parts[0] == 'I':
					if len(parts) != 4: raise ValueError
					parts[2] = int(parts[2])
					parts[3] = int(parts[3])
					res.append(parts)

				else:
					raise ValueError

		return res
	except IOError, e:
		if e.errno ==  ENOENT:
			return []
		else:
			raise

	except ValueError:
		print >>stderr, '[WARNING] Malformed file %s; ignoring.' % path
		return []

def list_cached(suffix):
	return set(glob(host_path('*.' + suffix)))

def write_lines(fd, lines):
	fd.write('\n'.join(lines) + '\n')
	fd.flush()

def preprocess_rules(in_fd, out_fd, outside_root):
    if outside_root:
        outside_root="--outside_root"
    else:
        outside_root=""
    # --ignore-local-rules is for backward compatibility
    proc = Popen(['bmake-filter', '--no-layout-exclusion', '--ignore-local-rules', outside_root,
                  get_workdir()],
                  stdin=in_fd, stdout=out_fd, close_fds=True)
    if proc.wait() != 0:
        exit('Error preprocessing rules.')

def eval_concurrent(path, args):
	execvp('make_red_stderr', ['make_red_stderr', '-r', '-R', '-f', path] + args)
	exit('Error running make.')

class FileLock(object):
	def __init__(self, fd):
		self.fd = fd

	def __enter__(self):
		lockf(self.fd, LOCK_EX)

	def __exit__(self, type, value, traceback):
		lockf(self.fd, LOCK_UN)

def eval_exclusive(lock_fd):
	def helper(path, args):
		with FileLock(lock_fd):
			proc = Popen(['make_red_stderr', '-r', '-f', path] + args)
			try:
				system_exit(proc.wait())
			except KeyboardInterrupt:
				proc.terminate()
				system_exit(proc.wait())
	return helper

## BLOCK ADDED
def reset_dirs(root, to_reset=False):
	if to_reset:
        	delete_workdir()
        make_hostdir()
        if not to_reset:
        	clean_cache(root)

def set_shell_choice(qsub):
	if not qsub:
		return """	
ifdef BIOINFO_PARALLEL
ifeq ($(BIOINFO_PARALLEL),no)
	SHELL := $(LOCAL_SHELL)
else ifeq ($(BIOINFO_PARALLEL),0)
	SHELL := $(LOCAL_SHELL)
else ifeq ($(BIOINFO_PARALLEL),false)
	SHELL := $(LOCAL_SHELL)
else
	SHELL := $(PARALLEL_SHELL)
endif
else
	SHELL := $(LOCAL_SHELL)
endif
"""	
	else:
		return "SHELL := $(PARALLEL_SHELL)"
		
	
def main():
	global work_dir, host_dir

	parser = OptionParser(usage=format_usage('''
		%prog [OPTIONS] TARGET...

Produce the TARGET accorging to the rules written in makefile, rules.mk and (optionally) in prj_rules.mk

The rules in rules.mk override the rules in makefile.
The rules in prj_rules.mk override the rules in rules.mk.
For the variables assignment the usual ?= and := assignemtn operator propertines remain valid.

NON STANDARD INSTRUCTION

executable in the local/bin of the prj or task in which bmake is called are available as if they where in the PATH

extern path/to/file as FILENAME
	this instruction require require the presence of path/to/file, if it does not exist bmake try to create it doing:
		cd path/to; bmake file
	the path/to/file can be referred in the following part of the makefile using the variabile FILENAME

import additional_rules
	search for the file additional_rules.mk in the $(PRJ_ROOT)/local/share/rules and include the file

	import additional_rules is more concise with respect to include $(PRJ_ROOT)/local/share/rules/additional_rules.mk
	moreover bmake do not support non-standard make syntax in included file whose path is written using variables while
	the bmake-specific syntax is supoorted in imported files

	the import construct cannot be inserted in an else branch and only the import in the first if branch is supported,
	if you need to impor a file coditionally you can use someting like:

	ifeq ($(VAR), yes)
	IMPORT_RULES=rules-yes
	else
	IMPORT_RULES=rules-no
	endif

	import $(IMPORT_RULES)

context task/sequences
	will make all scripts of the "sequences" task available as if they where of the the current prj or task.

serial $(TARGET_LIST) [DEPRECATER]
        Such targets are built on the local machine, one at a time and in the order they are defined, even when BIOINFO_PARALLEL is enabled.

LOCAL+=targhet [DEPRECATED]
	Targets added to the LOCAL list are run on the local machine, even when BIOINFO_PARALLEL is enabled.

ALL+=targhet
	Targets added to the ALL are the default goal (built when bmake is called whithout a target argument)

	'''))
	parser.add_option('-c', '--preprocess', dest='preprocess', action='store_true', default=False, help='print the preprocessed makefile and exit')
	parser.add_option('-d', '--debug', dest='debug', action='store_true', default=False, help='debug mode')
	parser.add_option('-e', '--external', dest='external', action='store_true', default=False, help="build an external dependency (you shouldn't use this directly)")
	parser.add_option('-j', '--jobs', dest='jobs', type='int', help='the number of jobs (commands) to run simultaneously', metavar='NUMBER')
	parser.add_option('-k', '--keep-going', dest='keep_going', default=False, action='store_true', help='keep going when some targets can\'t be made')
	parser.add_option('-K', '--lock', dest='lock', action='store_true', default=False, help='prevent multiple instances of bmake from running concurrently')
	parser.add_option('-l', '--load-average', dest='load_average', type='float', help="don't start multiple jobs unless load is below N", metavar='N')
	parser.add_option('-n', '--dry-run', dest='dry_run', action='store_true', default=False, help='don\'t run commands; just print them')
	parser.add_option('-r', '--reset', dest='reset', action='store_true', default=False, help='ignore cached rulesets')
	parser.add_option('-t', '--touch', dest='touch', action='store_true', default=False, help='touch targets instead of remaking them')
	parser.add_option('-q', '--qsub', dest='qsub', action='store_true', default=False, help='run each command form a recipies using the qbash shell')
        parser.add_option('-o', '--outside_root', dest='outside_root', action='store_true', default=True, help='allow execution outside of $BIOINFO_ROOT')
	parser.add_option('',   '--no-print-directory', dest='no_print_directory', action='store_true', default=False, help='turn off -w, even if it was turned on implicitly.')
	parser.add_option('',   '--silent', dest='silent', action='store_true', default=False, help='don\'t echo commands')
	options, args = parser.parse_args()

	min_python_version(2, 5, 2)
	root = bit_root()
	host = lookup_env('BIOINFO_HOST')
	host_dir = join(work_dir, host)
	extend_path(join(root, 'local', 'libexec'))

	gmsl = join(root, 'binary', host, 'local', 'libexec', 'bmake', 'gmsl')
	if not isfile(gmsl):
		exit('Cannot find GMSL. Please install it from: $BIOINFO_ROOT/local/src/redist/gmsl.')

	if options.external:
		if len(args) != 1:
			exit('Unexpected argument number.')
		process_external(args[0], root)
	else:
		in_fd = open_makefile()
		
		## BLOCK CHANGED
		if options.lock:	
			with FileLock(in_fd):
				reset_dirs(root, options.reset)
		else:
			reset_dirs(root, options.reset)

		## BLOCK ORIGINAL
		#with FileLock(in_fd):
		#	if options.reset:
		#		delete_workdir()
		#	make_hostdir()
		#	if not options.reset:
		#		clean_cache(root)

		with NamedTemporaryFile() as fd:
			write_lines(fd,
			   [ 'include $(BIOINFO_ROOT)/local/share/bmake/pre-rules.mk',
			     '-include ' + host_path('paths'),
                             'include ' + gmsl,
			     'export BMAKE_START_TIME:=%d' % time(),
			     'ifndef __BMAKE_DYNPATHS',
			     '%s: SHELL=$(LOCAL_SHELL)' % host_path('paths'),
			     '%s: ; @dynpaths -o -f %s %s >$@' % (host_path('paths'), host_path('fprints'), host_path('dynpaths.info')),
			     'else',
			     'ifneq (,$(and $(filter 1,$(words $(MAKECMDGOALS))),$(or $(filter clean,$(MAKECMDGOALS)),$(filter clean.full,$(MAKECMDGOALS)))))',
			     'BMAKE_CLEANING:=yes',
			     'endif' ])
			preprocess_rules(in_fd, fd, options.outside_root)
			# --ignore-global-rules is for backward compatibility
			write_lines(fd,
			  [  '%s: SHELL=$(LOCAL_SHELL)' % host_path('rules.mk'),
			     '%s: rules.mk ; @bmake-filter -o --ignore-global-rules %s <$< >$@' % (host_path('rules.mk'), get_workdir()),
			     'include ' + host_path('rules.mk')])

			write_lines(fd,
			  [  '%s: prj_rules.mk ; @bmake-filter -o --ignore-global-rules %s <$< >$@' % (host_path('prj_rules.mk'), get_workdir()),
			     '-include ' + host_path('prj_rules.mk')])
				
			write_lines(fd,
			  [  set_shell_choice(options.qsub),	
			     'include $(BIOINFO_ROOT)/local/share/bmake/post-rules.mk',
			     'endif' ])

			if options.preprocess:
				with file(fd.name, 'r') as fd:
					stdout.write(fd.read())
			else:
				if options.jobs is not None and not options.dry_run:
					args = [ '-j', str(options.jobs) ] + args
					environ['BMAKE_SLOTS'] = str(options.jobs)

				if options.load_average is not None:
					if options.jobs is None:
						exit('--load-average requires --jobs.')
					else:
						args = [ '-l', str(options.load_average) ] + args
				if options.debug:
					args.insert(0, '-d')
				if options.dry_run:
					args.insert(0, '-n')
				if options.no_print_directory:
					args.insert(0, '--no-print-directory')
				if options.silent:
					args.insert(0, '--silent')
				if options.touch:
					args.insert(0, '-t')
				if options.keep_going:
					args.insert(0, '-k')

				if options.lock:
					evaluator = eval_exclusive(in_fd)
				else:
					in_fd.close()
					evaluator = eval_concurrent
				evaluator(fd.name, args)

if __name__ == '__main__':
	main()
