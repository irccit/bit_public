#!/bin/bash
#
# Copyright 2011 Gabriele Sales <gbrsales@gmail.com>


function help {
    echo "Usage: ${0##*/}"
    echo ""
    echo "Retrieves the value of a parameter in the dataset"
    echo "in the current working directory."
    exit 1
}

function error {
    echo "[${0##*/}] $1" >&2
    exit 1
}


while getopts "h" flag; do
    if [[ $flag == h ]]; then
	help
    fi
done

[[ $# -eq $OPTIND ]] || error "Unexpected argument number."

export BIOINFO_PARALLEL=no
exec bmake --no-print-directory --silent echo.${!OPTIND}
