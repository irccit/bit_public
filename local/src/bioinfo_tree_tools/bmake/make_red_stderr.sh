#!/bin/bash
color()(set -o pipefail;"$@" 2>&1>&3|sed $'s,.*,\e[31m&\e[m,'>&2)3>&1
#https://serverfault.com/questions/59262/bash-print-stderr-in-red-color
color make $@
