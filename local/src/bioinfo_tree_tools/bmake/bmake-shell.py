#!/usr/bin/env python
#
# Copyright 2009,2012,2016 Gabriele Sales <gbrsales@gmail.com>

from collections import namedtuple
from errno import EACCES, EAGAIN, ENOENT
from fcntl import fcntl, F_SETFL, F_GETFL, lockf, LOCK_EX, LOCK_NB, LOCK_UN
from os import environ, fstat, read, stat, unlink, write, O_NONBLOCK
from os.path import join
from select import select
from signal import signal, SIGTERM, SIGQUIT, SIGINT
from subprocess import Popen
from sys import argv, maxint, stderr
from sys import exit as system_exit
from time import sleep
from vfork.path import bit_root
from vfork.util import exit
import re


def main():
    if len(argv) != 2:
    	exit('Unexpected arguments.')

    try:
    	host = environ['BIOINFO_HOST']
    except KeyError:
    	exit('BIOINFO_HOST is not defined.')

    config = parse_annotations(argv[1])

    def abort_lock(signum, _):
    	system_exit(-signum)
    signal(SIGINT, abort_lock)

    job_server = JobServer.connect()
    if config.serial is None:
        with job_server.acquire(config.threads):
            run(config.command, job_server.acquired)

    else:
        lock = SerialLock(host, config.serial)

        try:
            if not lock.take(False):
                job_server.releaseAll()
                lock.take(True)

            with job_server.acquire(config.threads):
                run(config.command, job_server.acquired)

        finally:
            lock.release()


Config = namedtuple('Config', ('serial', 'threads', 'command'))

def parse_annotations(command):
    rx = re.compile(r'!(\w+)(?:\s+([^;]+))?;\s*')

    serial=None
    threads=1

    while True:
        m = rx.match(command)
        if m is None:
            break

        type_, args = m.groups()
        command = command[m.end():]

        if type_ == 'serial':
            serial = args
        elif type_ == 'threads':
            if args:
                threads = int(args)
            else:
                threads = maxint
        else:
            exit('Unexpected annotation: ' + type_)

    return Config(serial, threads, command)

def run(command, threads):
    env = dict(environ)
    env['THREADNUM'] = str(threads)

    proc = Popen([ 'bash-make', '-c', command ], shell=False, env=env)

    def killer(signum, _):
        proc.terminate()
        proc.wait()
        system_exit(-signum)

    signal(SIGTERM, killer)
    signal(SIGQUIT, killer)
    signal(SIGINT,  killer)
    system_exit(proc.wait())


class NoJobServer(object):
    acquired = 1
    def acquire(self, slots): return self
    def releaseAll(self): pass
    def __enter__(self): pass
    def __exit__(self, type, value, traceback): pass

class JobServer(object):
    def __init__(self, fd_in, fd_out, max_slots):
        self.fd_in = fd_in
        self.fd_out = fd_out
        self.max_slots = max_slots
        self.needed = None
        self.acquired = 1

    @classmethod
    def connect(klass):
        try:
            max_slots = int(environ.get('BMAKE_SLOTS', 'missing'))
        except ValueError:
            return NoJobServer()

        m = re.search(r'--jobserver-fds=(\d+),(\d+)', environ.get('MFLAGS', ''))
        if m is None:
            return NoJobServer()

        fd_in = int(m.group(1))
        fd_out = int(m.group(2))

        fl = fcntl(fd_in, F_GETFL)
        fcntl(fd_in, F_SETFL, O_NONBLOCK)

        return klass(fd_in, fd_out, max_slots)

    def acquire(self, slots):
        assert self.needed is None
        self.needed = min(slots, self.max_slots)
        return self

    def releaseAll(self):
        self._write_tokens(self.acquired)
        self.acquired = 0

    def __enter__(self):
        while True:
            timeout = 0.1 if self.acquired > 0 else None
            self._read_tokens(self.needed-self.acquired, timeout)
            if self._has_tokens():
                break
            else:
                self.releaseAll()
                sleep(1)

    def __exit__(self, type, value, traceback):
        if self.acquired == 0:
            while self.acquired == 0:
                self._read_tokens(1, None)
        elif self.acquired > 1:
            self._write_tokens(self.acquired-1)

    def _has_tokens(self):
        return self.acquired >= self.needed

    def _read_tokens(self, num, timeout):
        read_ready = select([self.fd_in], [], [], timeout)[0]
        if len(read_ready) == 0:
            return

        try:
            tokens = len(read(self.fd_in, self.needed-self.acquired))
            self.acquired += tokens
        except OSError, e:
            if e.errno == EAGAIN:
                return
            else:
                raise

    def _write_tokens(self, num):
        tokens = ' ' * num
        while len(tokens):
            released = write(self.fd_out, tokens)
            tokens = tokens[released:]


class SerialLock(object):
    def __init__(self, host, idx):
    	self.filename = join('.bmake', host, '%s.lock' % idx)
    	self.fd = None

    def take(self, block):
    	while True:
            fd = file(self.filename, 'w')
            try:
                try:
                    flags = LOCK_EX
                    if not block: flags |= LOCK_NB
                    lockf(fd, flags)
                except IOError, e:
                    if e.errno in (EACCES, EAGAIN):
                        return False
                    else:
                        raise

                try:
                    fs_ino = stat(self.filename).st_ino
                except OSError, e:
                    if e.errno == ENOENT:
                        fs_ino = -1
                    else:
                        raise

                if fs_ino != fstat(fd.fileno()).st_ino:
                    lockf(fd, LOCK_UN)
                else:
                    self.fd = fd
                    return True

            finally:
                if self.fd is None:
                    fd.close()

    def release(self):
        if self.fd is not None:
            unlink(self.filename)
            lockf(self.fd, LOCK_UN)
            self.fd.close()
            self.fd = None


if __name__ == '__main__':
    main()
