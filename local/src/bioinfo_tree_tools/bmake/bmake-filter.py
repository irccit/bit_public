#!/usr/bin/env python2.7
#
# Copyright 2009-2010,2012,2014,2016 Gabriele Sales <gbrsales@gmail.com>

from hashlib import md5
from optparse import OptionParser
from os import environ
from os.path import abspath, dirname, isdir, join, normpath
from sys import stdin, stdout
from vfork.io.util import safe_rstrip
from vfork.path import bit_root, subdir_of
from vfork.util import exit, format_usage
import re

root = None
rule_dir = None
work_dir = '.bmake'
host_dir = None

def host_path(path):
    return join(host_dir, path)

def local_shell(name):
    return '%s: SHELL=$(LOCAL_SHELL)' % host_path(name)

def safe_append_endif(lst):
    # We put a blank line before the endif so that
    # a continuation mark in the preceding line
    # doesn't end up capturing the endif.
    lst += ['', 'endif']

def include_remover(label):
    include_rx = re.compile(r' *include\s+(.+)$')

    def transformer(lines):
        for line in lines:
            m = include_rx.match(line)
            if m is None or m.group(1) != label:
                yield line

    return transformer

def section_remover(label):
    quoted_label = label.replace('.', '\\.')
    section_rx = re.compile(r' *%s\s*:' % quoted_label)

    def transformer(lines):
        ignore_block = False
        for line in lines:
            if ignore_block:
                if line.startswith('\t'):
                    continue
                else:
                    ignore_block = False

            m = section_rx.match(line)
            if m is None:
                yield line
            else:
                ignore_block = True

    return transformer

comment_rx = re.compile(r'\s{3,}\*')
def comment_remover(lines):
    for line in lines:
        m = comment_rx.search(line)
        if m is not None:
            line = line[:m.start()]
        yield line

numbered_prereq_rx = re.compile(r'(\$+)\^(\d+)')
def numbered_prereq(lines):
    for line in lines:
        yield numbered_prereq_rx.sub(r'\1(word \2,\1^)', line)

def search_rule_dir(path, root):
    while len(path) > len(root):
        r = join(path, 'local', 'share', 'rules')
        if isdir(r):
            return path[len(root):]
        else:
            path = dirname(path)

    return None

def meta_transformer(lines):
    meta_rx = re.compile(r'\.META\s*:\s*(.+)$')

    it = lines.__iter__()
    line = it.next()

    while True:
        m = meta_rx.match(line)
        if m is None:
            yield line
            line = it.next()
        else:
            target = m.group(1)
            tid = md5(target).hexdigest()

            yield 'define __bmake_meta_output_' + tid
            commands, line = gather_commands(it)
            for command in commands:
                yield command
            yield 'endef'

            yield '$(foreach t,%s,$(call __bmake_meta_emit,$t,$(__bmake_meta_output_%s)))' % \
              (target, tid)

def gather_commands(lines):
    commands = []

    for line in lines:
        if line.startswith('\t'):
            commands.append(line[1:])
        else:
            return commands, line

    return commands, ""


def extern_transformer(lines):
    extern_rx = re.compile(r' *extern\s+(.+?)(?:\s+as\s+(.+?))?\s*$')
    for line in lines:
        m = extern_rx.match(line)
        if m is None:
            yield line
        else:
            path = m.group(1)
            varname = m.group(2)
            if varname is None: varname = ''
            yield '$(call __bmake_extern,%s,%s)' % (path, varname)

def iter_directives(rx, action, lines):
    prefix = []
    suffix = list(lines)
    while len(suffix):
        for idx, line in enumerate(suffix):
            m = rx.match(line)
            if m is None:
                prefix.append(line)
            else:
                suffix = action(prefix, m.groups(), suffix[idx+1:])
                break
        else:
            break
    return prefix

def param_transformer(lines):
    param_rx = re.compile(r' *param\s+(.+?)\s+as\s+(.+?)\s+from\s+(.+?)\s*$')

    def action(prefix, (query, name, path), suffix):
        prefix.append('-include ' + host_path(name + '.param'))

        prefix.append('ifndef ' + name)
        prefix.append(local_shell(name + '.param'))
        prefix.append('$(call __bmake_extern,%s/makefile,)' % path)
        prefix.append('%s: %s/makefile ; (echo -n \'%s:=\' && cd $(<D) && bmake_query %s) >$@' % (host_path(name + '.param'), path, name, query))
        prefix.append('endif')

        prefix.append('ifdef ' + name)
        safe_append_endif(suffix)

        return suffix

    return iter_directives(param_rx, action, lines)

def include2vars(include):
    name = normpath(include).replace('..', '^').replace('/', '.')
    return name, '__BMAKE_INCLUDE_' + name.replace('.', '_')

def include_transformer(lines):
    include_rx = re.compile(r' *include ([^\s\$]+)\s*$')

    def action(prefix, (include,), suffix):
        name, guard = include2vars(include)

        prefix.append('ifndef ' + guard)
        prefix.append(local_shell(name + '.include'))
        prefix.append('%s: %s ; @echo \'I|%s\' >>%s && dynpaths -o -f %s %s >%s && touch $@' % (host_path(name + '.include'), include, include, host_path('dynpaths.info'), host_path('fprints'), host_path('dynpaths.info'), host_path('paths')))
        prefix.append('include ' + host_path(name + '.include'))
        prefix.append('endif')

        prefix.append('ifdef ' + guard)
        prefix.append(local_shell(name + '.inc'))
        prefix.append('%s: $(%s) ; @rm -f %s && bmake-filter -o $(<D) %s_PROCESSED <$< >$@' % (host_path(name + '.inc'), guard, host_path(name + '.include'), guard))
        prefix.append('include ' + host_path(name + '.inc'))
        prefix.append('ifdef %s_PROCESSED' % guard)

        safe_append_endif(suffix)
        safe_append_endif(suffix)
        return suffix

    return iter_directives(include_rx, action, lines)

def extract_directives(rx, lines):
    directives = []
    content = []
    for line in lines:
        m = rx.match(line)
        if m is None:
            content.append(line)
        else:
            directives.append(m.groups())
    return directives, content

def module2vars(module, kind):
    if '/' not in module:
        if rule_dir is None:
            exit('Cannot find rule directory.')
        module = '%s/%s' % (rule_dir, module)

    return module, '__BMAKE_%s_%s' % (kind, module.replace('/', '_')), module.replace('/', '.'),

def phase_transformer(layout_exclusion):
    def transformer(lines):
        phase_rx = re.compile(r' *phase\s+(\S+)(?:\s+using\s+(\S+))?(?:\s+when\s+(\S+))?\s*$')
        phases, content = extract_directives(phase_rx, lines)
        lines = []

        phase_num = len(phases)
        if phase_num:
            vars = [module2vars(p[1] if p[1] is not None else p[0], 'IMPORT') for p in phases]
            for idx, (module, guard, name) in enumerate(vars):
                lines.append('%sifndef %s' % ('else ' if idx!=0 else '', guard))
                lines.append(local_shell(name + '.module'))
                lines.append('%s: ; @echo \'M|%s\' >>%s && dynpaths -o -f %s %s >%s && touch $@' % (host_path(name + '.module'), module, host_path('dynpaths.info'), host_path('fprints'), host_path('dynpaths.info'), host_path('paths')))
                lines.append('include ' + host_path(name + '.module'))

            lines.append('else')
            last_phase = None
            for (phase, _, when_var), (_, guard, name) in zip(phases, vars):
                lines.append('')
                if when_var:
                    lines.append('ifdef %s' % when_var)

                lines.append('__BMAKE_PHASES+=%s' % phase)
                lines.append('%s/rules.mk: SHELL=$(LOCAL_SHELL)' % phase)
                lines.append('%s/rules.mk: $(%s) | %s ; @rm -f %s && link_install -f $< $@' % (phase, guard, phase, host_path(name + '.module')))

                if when_var:
                    lines.append('endif')

            lines.append('')

        for i in xrange(phase_num):
            name = phases[i][0]

            if i != 0:
                conditional = False
                j = i
                while j > 0:
                    j -= 1
                    (phase, _, when_var) = phases[j]
                    if when_var:
                        lines.append('%sifdef %s' % ('else ' if conditional else '', when_var))
                        lines.append('__BMAKE_PHASE_%s_PREV:=%s' % (name, phase))
                        conditional = True
                    else:
                        if conditional:
                            lines.append('else')
                        lines.append('__BMAKE_PHASE_%s_PREV:=%s' % (name, phase))
                        break

                if conditional:
                    lines.append('endif')

            if i != phase_num-1:
                conditional = False
                j = i
                while j < phase_num-1:
                    j += 1
                    (phase, _, when_var) = phases[j]
                    if when_var:
                        lines.append('%sifdef %s' % ('else ' if conditional else '', when_var))
                        lines.append('__BMAKE_PHASE_%s_NEXT:=%s' % (name, phase))
                        conditional = True
                    else:
                        if conditional:
                            lines.append('else')
                        lines.append('__BMAKE_PHASE_%s_NEXT:=%s' % (name, phase))
                        break

                if conditional:
                    lines.append('endif')

            lines.append('')

        if layout_exclusion:
            lines.append('ifndef __BMAKE_DO_LAYOUT')
        lines += content
        if layout_exclusion:
            safe_append_endif(lines)

        if len(phases):
            safe_append_endif(lines)

        return lines

    return transformer

def import_transformer(lines):
    import_rx = re.compile(r' *import (\S+)\s*$')

    def action(prefix, (import_,), suffix):
        module, guard, name = module2vars(import_, 'IMPORT')

        prefix.append('ifndef ' + guard)
        prefix.append(local_shell(name + '.module'))
        prefix.append('%s: ; @echo \'M|%s\' >>%s && dynpaths -o -f %s %s >%s && touch $@' % (host_path(name + '.module'), module, host_path('dynpaths.info'), host_path('fprints'), host_path('dynpaths.info'), host_path('paths')))
        prefix.append('include ' + host_path(name + '.module'))
        prefix.append('endif')

        prefix.append('ifdef ' + guard)
        prefix.append(local_shell(name + '.mk'))
        prefix.append('%s: $(%s) ; @rm -f %s && bmake-filter $(<D) %s_PROCESSED <$< >$@' % (host_path(name + '.mk'), guard, host_path(name + '.module'), guard))
        prefix.append('include ' + host_path(name + '.mk'))
        prefix.append('ifdef %s_PROCESSED' % guard)

        safe_append_endif(suffix)
        safe_append_endif(suffix)
        return suffix

    return iter_directives(import_rx, action, lines)

def trailing_spaces_remover(lines):
    for line in lines:
        yield line.rstrip()


def context_transformer(lines):
    context_rx = re.compile(r' *context (\S+)\s*$')

    def action(prefix, (context,), suffix):
        module, guard, name = module2vars(context, 'CONTEXT')

        prefix.append('ifndef ' + guard)
        prefix.append(local_shell(name + '.context'))
        prefix.append('%s: ; @echo \'C|%s\' >>%s  && dynpaths -f %s %s >%s && touch $@' % (host_path(name + '.context'), module, host_path('dynpaths.info'), host_path('fprints'), host_path('dynpaths.info'), host_path('paths')))
        prefix.append('include ' + host_path(name + '.context'))
        prefix.append('endif')

        prefix.append('ifdef ' + guard)
        safe_append_endif(suffix)

        return suffix

    return iter_directives(context_rx, action, lines)


class AnnotationTransformer(object):

    def __init__(self):
        self._serial = 1
        self._lines = None
        self._processed = []

    def __call__(self, lines):
        self._lines = lines

        while True:
            block = self.find_commands()
            if len(block) == 0:
                break

            level, annotations, commands = self.parse_annotations(block)
            if len(annotations) == 0:
                self._processed += block

            elif len(commands) == 0:
                exit('Unterminated command at the end of file.')

            else:
                self.output_commands(level, annotations, commands)

        return self._processed

    def find_commands(self):
        start = self.find_command_start(self._lines)
        end = self.find_command_end(self._lines, start)

        self._processed += self._lines[:start]
        command = self._lines[start:end]
        self._lines = self._lines[end:]

        return command

    def find_command_start(self, lines):
        idx = 0
        while idx < len(lines) and not self.is_command_line(lines[idx]):
            idx += 1
        return idx

    def is_command_line(self, line):
        return line.startswith('\t')

    def find_command_end(self, lines, start):
        idx = start
        while idx < len(lines) and self.is_command_line(lines[idx]):
            idx += 1
        return idx

    def parse_annotations(self, lines):
        level = 0
        annotations = []

        rx = re.compile(r'\t(!+)(\w+)(?:\s+(.*))?$')

        while len(lines):
            line = lines[0]
            m = rx.match(line)
            if m is None:
                break

            marks, type_, rest = m.groups()
            level = max(level, len(marks))
            args = rest.split() if rest else []

            if type_ == 'serial':
                parsed = self.parse_serial(args, line)
            elif type_ == 'threads':
                parsed = self.parse_threads(args, line)
            else:
                exit('Invalid command annotation: ' + line)

            annotations.append(parsed)
            del lines[0]

        return level, annotations, lines

    def parse_serial(self, args, raw):
        if len(args) == 0:
            group_id = 'group-%d' % self._serial
            self._serial += 1

        elif len(args) == 1:
            group_id = words[0]

        else:
            exit('Malformed annotation: ' + raw)

        return '!serial ' + group_id

    def parse_threads(self, args, raw):
        try:
            if len(args) == 0:
                return '!threads'

            elif len(args) == 1:
                thread_num = int(args[0])
                return '!threads ' + args[0]

            else:
                raise ValueError

        except ValueError:
            exit('Malformed annotation: ' + raw)

    def output_commands(self, level, annotations, commands):

        for command in self.iter_commands(commands):

            dollars = '$' * (2**(level-1))
            out = ['\t%s(__BMAKE_ANNOTATION_PREFIX)' % (dollars,)]

            if self.is_silent(command):
                self.drop_silent_mark(command)
                out.append('@')

            for idx, annotation in enumerate(annotations):
                out.append(annotation)
                out.append(';')

            out += [ c + '\n' for c in command ]
            self._processed.append(''.join(out))

    def iter_commands(self, block):
        while True:
            l = len(block)
            if l == 0:
                break

            idx = 0
            while idx < l and self.line_continues(block[idx]):
                idx += 1

            yield block[:idx+1]
            block = block[idx+1:]

    def line_continues(self, line):
        return line.endswith('\\')

    def is_silent(self, command):
        return command[0].lstrip().startswith('@')

    def drop_silent_mark(self, command):
        first = command[0]
        pos = first.index('@')
        command[0] = first[:pos] + first[pos+1:]


def chain_transformers(ts):
    def transform(lines):
        for t in ts:
            lines = t(lines)
        return lines
    return transform

def main():
    global root, rule_dir, work_dir, host_dir

    parser = OptionParser(usage=format_usage('''
        %prog [OPTIONS] BASE_PATH [VARS...]
    '''))
    parser.add_option('', '--ignore-local-rules', dest='ignore_local_rules', action='store_true', default=False, help='ignore includes of \'rules.mk\'')
    parser.add_option('', '--ignore-global-rules', dest='ignore_global_rules', action='store_true', default=False, help='ignore includes of \'global-rules.mk\'')
    parser.add_option('', '--no-layout-exclusion', dest='no_layout_exclusion', action='store_true', default=False, help='do not hide rules when building layout')
    parser.add_option('-o', '--outside_root', dest='outside_root', action='store_true', default=False, help='allow execution outside of $BIOINFO_ROOT')
    options, args = parser.parse_args()
    if len(args) < 1:
        exit('Unexpected argument number.')

    root = bit_root()
    base_path = abspath(normpath(args[0]))
    if not options.outside_root and not subdir_of(base_path, root):
        exit('BASE_PATH is outside of BIOINFO_ROOT.')

    try:
        host = environ['BIOINFO_HOST']
    except KeyError:
        exit('BIOINFO_HOST is not defined.')

    host_dir = join(work_dir, host)
    rule_dir = search_rule_dir(base_path, root)

    transformers = [ section_remover('.DOC')
                   , comment_remover
                   , numbered_prereq
                   , meta_transformer
                   , extern_transformer
                   , param_transformer
                   , include_transformer
                   , phase_transformer(not options.no_layout_exclusion)
                   , import_transformer
                   , context_transformer
                   , AnnotationTransformer()
                   , trailing_spaces_remover ]

    if options.ignore_local_rules:
        transformers.insert(0, include_remover('rules.mk'))
    if options.ignore_global_rules:
        transformers.insert(0, include_remover('$(BIOINFO_ROOT)/local/share/makefile/global-rules.mk'))

    for var in args[1:]:
        print '%s:=yes' % var

    undo_newline = re.compile(r'\\\\\s*$')

    transform = chain_transformers(transformers)
    for line in transform(safe_rstrip(l) for l in stdin):
        m = undo_newline.search(line)
        if m is None:
        	stdout.write(line)
	        stdout.write('\n')
	else:
		line = undo_newline.sub("",line)
        	stdout.write(line)
		
		

if __name__ == '__main__':
    main()
