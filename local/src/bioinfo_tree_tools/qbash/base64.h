// Copyright 2015 Gabriele Sales <gbrsales@gmail.com>

extern int base64_enc_len(int plain);
extern int base64_enc(char* output, const char* input, const int len);
