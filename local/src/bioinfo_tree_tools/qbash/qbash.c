// Copyright 2009,2013,2015 Gabriele Sales <gbrsales@gmail.com>

#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "base64.h"
#include <drmaa.h>

extern char** environ;

static char* payload;
static char cwd[4096];
static char* root;
static pid_t pid;
static char hostname[4096];
static const char* runner_tmpl = "%s/local/share/bmake/run_parallel";
static int interrupted = 0;
static char* job_id = NULL;
static const char* output_log_spec = NULL;
static const char* error_log_spec = NULL;
static char diagnosis[DRMAA_ERROR_STRING_BUFFER];


static void print_help(const char* name) {
  fprintf(stderr, "Usage: qbash -c QUOTED_COMMAND\n");
  exit(1);
}

static void parse_args(const int argc, char** argv) {
  int i;
  for (i = 1; i < argc; ++i) {
    if (strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0)
      print_help(argv[0]);
  }

  if (argc != 3) {
    fprintf(stderr, "[qbash] Unexpected argument number.\n");
    exit(1);
  } else if (strcmp(argv[1], "-c")) {
    fprintf(stderr, "[qbash] Expected '-c' as first argument.\n");
    exit(1);
  }

  payload = argv[2];
}

static void check_cwd(const char* cwd) {

  root = getenv("BIOINFO_ROOT");
  if (root == NULL) {
    fprintf(stderr, "[qbash] BIOINFO_ROOT is not defined.\n");
    exit(1);
  }

  const int root_len = strlen(root);
  if (root_len == 0) {
    fprintf(stderr, "[qbash] BIOINFO_ROOT is empty.\n");
    exit(1);
  }

  if (strncmp(root, cwd, root_len) != 0 ||
      (strlen(cwd) > root_len && cwd[root_len] != '/')) {
    fprintf(stderr, "[qbash] Outside BIOINFO_ROOT.\n");
    exit(1);
  }
}

static void cleanup() {
  if (job_id)
    drmaa_control(job_id, DRMAA_CONTROL_TERMINATE, NULL, 0);

  if (output_log_spec)
    unlink(output_log_spec+1);
  if (error_log_spec)
    unlink(error_log_spec+1);

  if (drmaa_exit(diagnosis, sizeof(diagnosis)) != DRMAA_ERRNO_SUCCESS) {
    fprintf(stderr, "[qbash] drmaa_exit() failed: %s\n", diagnosis);
    exit(1);
  }
}

static void signal_handler(int signal) {
  fprintf(stderr, "[qbash] signal received, exiting...\n");
  interrupted = 1;
}

static char* create_log_spec(const char* suffix) {

  char* spec = malloc(sizeof(char)*4096);
  if (!spec) {
    fprintf(stderr, "[qbash] malloc() failed.\n");
    exit(1);

  } else if (snprintf(spec, 4096, ":%s/%s-%ld%s", cwd, hostname, (long int)pid, suffix) >= 4096) {
    fprintf(stderr, "[qbash] sprintf() failed.\n");
    exit(1);
  }

  return spec;
}

static char* instance_runner() {

  const int l = strlen(runner_tmpl) + strlen(root) + 1;
  char* runner = malloc(sizeof(char)*l);
  if (!runner) {
    fprintf(stderr, "[qbash] malloc() failed.\n");
    exit(1);
  }

  sprintf(runner, runner_tmpl, root);
  return runner;
}

static char* encode_payload() {

  const int pl = strlen(payload);
  const int el = base64_enc_len(pl) + 1;

  char* enc = malloc(sizeof(char)*el);
  if (!enc) {
    fprintf(stderr, "[qbash] malloc() failed.\n");
    exit(1);
  }

  base64_enc(enc, payload, pl);
  return enc;
}

static drmaa_job_template_t* create_job_template() {

  drmaa_job_template_t* jt;
  if (drmaa_allocate_job_template(&jt, diagnosis, sizeof(diagnosis)) != DRMAA_ERRNO_SUCCESS) {
    fprintf(stderr, "[qbash] drmaa_allocate_job_template() failed: %s\n", diagnosis);
    exit(1);
  }

  drmaa_set_attribute(jt, DRMAA_WD, cwd, NULL, 0);
  drmaa_set_attribute(jt, DRMAA_REMOTE_COMMAND, "bash", NULL, 0);

  const char* argv[3];
  argv[0] = instance_runner();
  argv[1] = encode_payload();
  argv[2] = NULL;
  drmaa_set_vector_attribute(jt, DRMAA_V_ARGV, argv, NULL, 0);

  drmaa_set_attribute(jt, DRMAA_JOIN_FILES, "n", NULL, 0);
  output_log_spec = create_log_spec(".out");
  drmaa_set_attribute(jt, DRMAA_OUTPUT_PATH, output_log_spec, NULL, 0);
  error_log_spec = create_log_spec(".err");
  drmaa_set_attribute(jt, DRMAA_ERROR_PATH, error_log_spec, NULL, 0);

  const char* opts = getenv("QBASH_NATIVE_OPTS");
  if (opts) {
    char errbuf[1024];
    if (drmaa_set_attribute(jt, DRMAA_NATIVE_SPECIFICATION, opts, errbuf, sizeof(errbuf)-1) != DRMAA_ERRNO_SUCCESS) {
      fprintf(stderr, "[qbash] Invalid native options: %s\n", errbuf);
      exit(1);
    }
  }

  const char* env[3];
  env[0] = "BASH_ENV=$HOME/.bashrc";

  const char* broot = getenv("BIOINFO_ROOT");
  const int sz = strlen("BIOINFO_NEWROOT=") + strlen(broot) + 1;
  char* buf = malloc(sz);
  sprintf(buf, "BIOINFO_NEWROOT=%s", broot);
  env[1] = buf;

  env[2] = NULL;
  drmaa_set_vector_attribute(jt, DRMAA_V_ENV, env, NULL, 0);

  return jt;
}

static int open_log(const char* filename) {

  int log = open(filename, O_RDONLY | O_NONBLOCK);
  if (log == -1 && errno != ENOENT) {
    fprintf(stderr, "[qbash] open() failed.\n");
    return 1;

  } else
    return log;
}

static void relay(int in, int out) {
  char buf[4096];

  while (1) {
    int count = read(in, buf, 4096);
    if (count == 0)
      break;
    else if (count == -1) {
      if (errno == EAGAIN)
        break;
      else {
        fprintf(stderr, "[qbash] read() failed.\n");
        exit(1);
      }
    }

    do {
      int written = write(out, buf, count);
      if (written == -1) {
        fprintf(stderr, "[qbash] write() failed.\n");
        exit(1);
      }
      else
        count -= written;

    } while (count > 0);
  }
}

int main(int argc, char** argv) {

  parse_args(argc, argv);

  if (getcwd(cwd, 4096) == NULL) {
    fprintf(stderr, "[qbash] getcwd() failed.\n");
    return 1;
  }
  check_cwd(cwd);

  pid = getpid();
  if (gethostname(hostname, sizeof(hostname))) {
    fprintf(stderr, "[qbash] gethostname() failed.\n");
    return 1;
  }

  signal(SIGINT, signal_handler);
  signal(SIGTERM, signal_handler);
  atexit(cleanup);

  if (drmaa_init(NULL, diagnosis, sizeof(diagnosis)) != DRMAA_ERRNO_SUCCESS) {
    fprintf(stderr, "[qbash] drmaa_init() failed: %s\n", diagnosis);
    return 1;
  }

  drmaa_job_template_t* jt = create_job_template();

  job_id = malloc(sizeof(char)*4096);
  if (!job_id) {
    fprintf(stderr, "[qbash] malloc() failed.\n");
    return 1;

  } else if (drmaa_run_job(job_id, 4096, jt, diagnosis, sizeof(diagnosis)) != DRMAA_ERRNO_SUCCESS) {
    fprintf(stderr, "[qbash] drmaa_run_job() failed: %s\n", diagnosis);
    return 1;
  }
  drmaa_delete_job_template(jt, NULL, 0);

  int output_log = -1;
  int error_log = -1;
  int stat;

  while (1) {

    int wait_res = drmaa_wait(job_id, NULL, 0, &stat, 1, NULL, diagnosis, sizeof(diagnosis));
    if (interrupted)
      return 1;

    if (output_log == -1)
      output_log = open_log(output_log_spec+1);
    if (output_log != -1)
      relay(output_log, STDOUT_FILENO);

    if (error_log == -1)
      error_log = open_log(error_log_spec+1);
    if (error_log != -1)
      relay(error_log, STDERR_FILENO);

    if (wait_res == DRMAA_ERRNO_SUCCESS)
      break;
    else if (wait_res != DRMAA_ERRNO_EXIT_TIMEOUT) {
      fprintf(stderr, "[qbash] drmaa_wait() failed: %s\n", diagnosis);
      return 1;
    }
  }

  int exited;
  if (drmaa_wifexited(&exited, stat, diagnosis, sizeof(diagnosis)) != DRMAA_ERRNO_SUCCESS) {
    fprintf(stderr, "[qbash] drmaa_wifexited() failed: %s\n", diagnosis);
    return 1;
  }

  if (exited) {
    int exit_code;
    if (drmaa_wexitstatus(&exit_code, stat, diagnosis, sizeof(diagnosis)) != DRMAA_ERRNO_SUCCESS) {
      fprintf(stderr, "[qbash] drmaa_wexitstatus() failed: %s\n", diagnosis);
      return 1;
    }

    return exit_code;

  } else
    return 1;
}

