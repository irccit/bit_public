# Copyright 2012 Gabriele Sales <gbrsales@gmail.com>
#
# This file is part of BioinfoTree. It is licensed under the
# terms of the GNU Affero General Public License version 3.

from os.path import basename, exists, join
from shutil import copy
from subprocess import call, Popen, PIPE
from vfork.util import exit


class TestFile(object):
    decompressors = {'gz':'zcat', 'bz2':'bzcat', 'xz':'xzcat'}
    
    def __init__(self, path):
        self.filename = basename(path)
        self.real_path, self.format = self._find(path)

    def copy_to(self, destdir):
        if self.format == 'uncompressed':
            copy(self.real_path, destdir)
	else:
	    with file(join(destdir, self.filename), 'w') as fd:
	        call([self.decompressors[self.format], self.real_path], stdout=fd)

    def compare_to(self, reference):
        if self.format == 'uncompressed':
            return call(['cmp', '-s', self.real_path, reference]) == 0

        else:
            decompr = Popen([self.decompressors[self.format], self.real_path], shell=False, stdout=PIPE)
            compar  = Popen(['cmp', '-s', reference], shell=False, stdin=decompr.stdout)

            if decompr.wait() != 0:
                exit('Error decompressing: ' + self.real_path)

            return compar.wait() == 0

    def _find(self, path):
        if exists(path):
            return path, 'uncompressed'

        for fmt in ('gz', 'bz2', 'xz'):
            alt_path = '%s.%s' % (path, fmt)
            if exists(alt_path):
                return alt_path, fmt

        raise MissingFile('file does not exist: ' + path)


class MissingFile(Exception):
    pass
