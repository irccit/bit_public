#!/usr/bin/env python
#
# Copyright 2010,2012,2013 Gabriele Sales <gbrsales@gmail.com>
#
# This file is part of BioinfoTree. It is licensed under the
# terms of the GNU Affero General Public License version 3.

from file import TestFile, MissingFile
from optparse import OptionParser
from os import devnull, environ, listdir, unlink
from os.path import abspath, dirname, isdir, isfile, join, split
from parser import iter_tests
from shutil import rmtree
from subprocess import call, Popen, PIPE
from sys import stdin
from time import time
from vfork.io.util import NamedTemporaryDirectory
from vfork.util import exit, format_usage
import re


def main():
    parser = OptionParser(usage=format_usage('''
	Usage: %prog [OPTIONS] SPEC

	Test some tools using the provided SPEC.
    '''))
    options, args = parser.parse_args()
    if len(args) != 1:
        exit('Unexpected argument number.')

    start_time = time()

    source_dir = abspath(dirname(args[0]))
    total  = 0
    passed = 0
    failed = []
    errors = []

    extend_env(source_dir)

    with NamedTemporaryDirectory('.') as test_dir:
        for test in iter_tests(args[0]):
            total += 1

            try:
                copy_files((join(source_dir, f) for f in test.in_files), test_dir.path)

                success = run_test(test, test_dir.path)
                if success:
                    if test.fail:
                        failed.append(test)
                        continue
                else:
                    if test.fail:
                        passed += 1
                    else:
                        errors.append(test)
                    continue

                if check_results((join(source_dir, f) for f in test.out_files), test_dir.path):
                    passed += 1
                else:
                    failed.append(test)

            finally:
                clean_dir(test_dir.path)

    stop_time = time()
    print 'Performed %d tests in %.2f seconds.' % (total, stop_time-start_time)

    if len(failed):
        print '\nFAILED TESTS\n'
        for test in failed:
            print test.title

    if len(errors):
        print '\nERRORS IN TESTS\n'
        for test in errors:
            print '*', test.title
            print test.errlog

    if len(failed) or len(errors):
        print

    print 'Passed: %d  Failed: %d  Errors: %d' % (passed, len(failed), len(errors))

def extend_env(source_dir):
    try:
        root = environ['BIOINFO_ROOT']
    except KeyError:
        exit('BIOINFO_ROOT is not defined.')

    try:
        host = environ['BIOINFO_HOST']
    except KeyError:
        exit('BIOINFO_HOST is not defined.')

    def replace_ref(m):
        var = m.group(1)
        if var == 'BIOINFO_ROOT':
            return root
        elif var == 'BIOINFO_HOST':
            return host
        else:
            exit('Invalid reference in dynpaths output: ' + var)

    dynpaths = Popen([join(root, 'local', 'libexec', 'dynpaths'), '--only-make-paths'], shell=False, stdout=PIPE)
    out = dynpaths.communicate()[0]
    if dynpaths.returncode != 0:
        exit('Error running dynpaths.')

    for line in out.split('\n'):
        line = line.rstrip()
        if len(line) == 0:
            continue

        tokens = line.split('=')
        if len(tokens) != 2:
            exit('Malformed dynpaths output.')

        environ[tokens[0]] = re.sub(r'\$\(([^)]*)\)', replace_ref, tokens[1])

def copy_files(paths, test_dir):
    for path in paths:
        try:
            f = TestFile(path)
            f.copy_to(test_dir)
        except MissingFile:
            exit('Missing input file: ' + path)

def run_test(test, test_dir):
    def replace_ref(m):
        v = m.group(1)
        if v == '$':
            return '$'

        elif v[0] == '@':
            n = int(v[1:]) if len(v) > 1 else 1
            try:
                if n < 0 or test.out_files is None or n > len(test.out_files): raise IndexError
                return 'out%d' % (n-1,)
            except IndexError:
                exit('Invalid reference $%s in command for test: %s' % (v, test.title))

        else:
            n = int(v)
            try:
                if n < 0: raise IndexError
                return test.in_files[n-1]
            except IndexError:
                exit('Invalid reference $%s in command for test: %s' % (v, test.title))

    cmd = re.sub(r'\$(\d+|@(?:\d+)?|\$)', replace_ref, test.cmd)

    with file(devnull, 'r') as sinkR:
        with file(devnull, 'w') as sinkW:
            proc = Popen(['bash', '-c', 'source $BIOINFO_ROOT/local/share/bash/bashrc;'+cmd],
                         stdin=sinkR, stdout=sinkW, stderr=PIPE, close_fds=True, cwd=test_dir)

    test.errlog = proc.communicate()[1]
    return proc.returncode == 0

def check_results(paths, test_dir):
    for idx, path in enumerate(paths):
        try:
            f = TestFile(path)
            if not f.compare_to(join(test_dir, 'out%d' % idx)):
                return False

        except MissingFile:
            exit('Missing output file: ' + path)

    return True

def clean_dir(test_dir):
    for entry in listdir(test_dir):
        path = join(test_dir, entry)
        if isdir(path):
            rmtree(path)
        else:
            unlink(path)


if __name__ == '__main__':
    main()
