#!/usr/bin/env tool_test
#
# Copyright 2013 Michele Vidotto <michele.vidotto@gmail.com>
# Copyright 2013 Gabriele Sales <gbrsales@gmail.com>

* constant target: remake does not update mtime
  In:   static.txt makefile rules.mk
  Out:
  Cmd:  touch -m -t 01010000 $1 && \
        PRE=$(stat -c %Y $1) && \
        remake $1 && \
        POST=$(stat -c %Y $1) && \
        [ "$PRE" = "$POST" ]

* constant target: content remains the same
  In:   static.txt
  Out:  static.txt
  Cmd:  remake $1 && cp $1 $@

* constant target: remake prints a warning if target does not change
  In:   static.txt makefile rules.mk
  Out:  static.warn
  Cmd:  remake $1 2>&1 >/dev/null | grep '^\[remake\]' >$@

* constant target: dry-run mode
  In:   static.txt makefile rules.mk
  Out:  static-dry.txt
  Cmd:  remake -n $1 | sed -r 's|^\s*||' >$@

* random target: remake updates mtime
  In:   random.txt makefile rules.mk
  Out:
  Cmd:  touch -m -t 01010000 $1 && \
        PRE=$(stat -c %Y $1) && \
        remake $1 && \
        POST=$(stat -c %Y $1) && \
        [ "$PRE" != "$POST" ]

* recipe error: remake restores the target to its original state
  In:   with_errors.txt makefile rules.mk
  Out:  with_errors.txt
  Cmd:  touch -m -t 01010000 $1 && \
        cp $1 bak && \
        PRE=$(stat -c %Y $1) && \
        remake $1 && \
        POST=$(stat -c %Y $1) && \
	[ "$PRE" = "$POST" ] && \
	cp $1 $@

* missing dependency: remake restores the target to its original state
  In:   with_errors2.txt makefile rules.mk
  Out:  with_errors2.txt
  Cmd:  touch -m -t 01010000 $1 && \
        cp $1 bak && \
        PRE=$(stat -c %Y $1) && \
        remake $1 && \
        POST=$(stat -c %Y $1) && \
	[ "$PRE" = "$POST" ] && \
	cp $1 $@
