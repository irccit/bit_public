printf """\
Nel mezzo del cammin di nostra vita\n\
mi ritrovai per una selva oscura,\n\
che la diritta via era smarrita.\n\
\n\
""" \
>static_part1.txt
printf """\
Ahi quanto a dir qual era e' cosa dura\n\
esta selva selvaggia e aspra e forte\n\
che nel pensier rinova la paura!\n\
\n\
Tant'e' amara che poco e' piu' morte;\n\
ma per trattar del ben ch'i' vi trovai,\n\
diro' de l'altre cose ch'i' v' ho scorte.\
""" \
>static_part2.txt
cat static_part1.txt static_part2.txt >static.txt
rm static_part1.txt static_part2.txt
