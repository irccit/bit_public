#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\t";
$\="\n";

my $phase_rules = "";

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 [-h|help] [-n|dryrun] [-p|phases phase1 -p|phases phase2 ...] [-f|force] [-s|skip] prjname version\n
create the local/ directory structure for a prj and a skeleton based on prjname and version
-f 	force to overwrite the rules.mk if exixting (if at least one pahase is indicated)
-s 	skip to overwrite the rules.mk if exixting (if at least one pahase is indicated)";

my $help=0;
my $force=0;
my $skip=0;
my $dryrun=0;
my @phases=();
GetOptions (
	'h|help' => \$help,
	'n|dryrun' => \$dryrun,
	'f|force' => \$force,
	's|skip' => \$skip,
	'p|phases=s@' => \@phases
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

die("Wrong argument number") if scalar(@ARGV) != 2;

my $prjname = shift;
my $version = shift;
die("Do not include dataset/ in the version name") if $version =~ /^dataset/;
my $version_underscores = $version;
$version_underscores =~ s/\//_/g;
my $prj_root = $ENV{'BIOINFO_ROOT'}."/prj/$prjname";

if( (!$dryrun and !$force) and !$skip and (scalar(@phases) and -e "$prj_root/local/share/rules/rules.mk") ){
	warn("The file $prj_root/local/share/rules/rules.mk exist, it would be overwritten if you do mkprj with phases. Aborted.\n Use -f to force / -s to skip");
}

## Set folders
my $sh = "mkdir -p $prj_root;\n";
$sh .= "touch $prj_root/.PRJ_ROOT;\n";
$sh .= "mkdir -p $prj_root/local;\n";
$sh .= "mkdir -p $prj_root/local/bin;\n";
$sh .= "mkdir -p $prj_root/local/src;\n";
$sh .= "mkdir -p $prj_root/local/share;\n";
$sh .= "mkdir -p $prj_root/local/share/rules;\n";
$sh .= "mkdir -p $prj_root/local/share/makefiles;\n";
$sh .= "mkdir -p $prj_root/local/share/data;\n";
$sh .= "mkdir -p $prj_root/local/share/doc;\n";
$sh .= "mkdir -p $prj_root/dataset/;\n";
$sh .= "mkdir -p $prj_root/dataset/$version;\n";

## Set version-related files: makefile for bmake; config.sk and cluster.yaml for Snakemake
$sh .= "touch $prj_root/local/share/makefiles/makefile_$version_underscores;\n";
$sh .= "touch $prj_root/local/share/makefiles/config_${version_underscores}.sk;\n";
$sh .= "touch $prj_root/local/share/makefiles/cluster_${version_underscores}.yaml;\n";
$sh .= "link_install -f $prj_root/local/share/makefiles/makefile_$version_underscores $prj_root/dataset/$version/makefile;\n";
$sh .= "link_install -f $prj_root/local/share/makefiles/config_${version_underscores}.sk $prj_root/dataset/$version/config.sk;\n";
$sh .= "link_install -f $prj_root/local/share/makefiles/cluster_${version_underscores}.yaml $prj_root/dataset/$version/cluster.yaml;\n";

## Set project-related files for each phase: rules for bmake; Snakefile for Snakemake.
if(!scalar(@phases)){
	$sh .= "touch $prj_root/local/share/rules/rules.mk\n";
	$sh .= "touch $prj_root/local/share/rules/Snakefile\n";
	$sh .= "link_install -f $prj_root/local/share/rules/rules.mk $prj_root/dataset/$version/rules.mk;\n";
	$sh .= "link_install -f $prj_root/local/share/rules/Snakefile $prj_root/dataset/$version/Snakefile;\n";
## Set project/version-related files for each phase.
}else{
	for(@phases){
		$sh .= "mkdir -p $prj_root/dataset/$version/$_;\n";
		$sh .= "touch $prj_root/local/share/makefiles/makefile_$version_underscores;\n";
		$sh .= "touch $prj_root/local/share/makefiles/config_${version_underscores}.sk;\n";
		$sh .= "touch $prj_root/local/share/makefiles/cluster_${version_underscores}.yaml;\n";
		$sh .= "touch $prj_root/local/share/rules/rules-$_.mk;\n";
		$sh .= "touch $prj_root/local/share/rules/Snakefile-$_.sk;\n";
		$sh .= "link_install -f $prj_root/local/share/makefiles/makefile_$version_underscores $prj_root/dataset/$version/$_/makefile;\n";
		$sh .= "link_install -f $prj_root/local/share/makefiles/config_${version_underscores}.sk $prj_root/dataset/$version/$_/config.sk;\n";
		$sh .= "link_install -f $prj_root/local/share/makefiles/cluster_${version_underscores}.yaml $prj_root/dataset/$version/$_/cluster.yaml;\n";
		$sh .= "link_install -f $prj_root/local/share/rules/rules-$_.mk $prj_root/dataset/$version/$_/rules.mk;\n";
		$sh .= "link_install -f $prj_root/local/share/rules/Snakefile-$_.sk $prj_root/dataset/$version/$_/Snakefile;\n";
	}
}

$sh .= <<"END_MESSAGE";
cd $prj_root;
## Make sure project is initialized.
if ! [ -e .git ]; then
  git init;
  echo 'dataset/*' > .gitignore;
  echo -e '.DS_Store\n._.DS_Store' >> .gitignore;
  git add .gitignore;
fi
git add -f dataset/${version}/{makefile,config.sk,cluster.yaml}
git add ./local
if [ \$(git log 2> /dev/null | wc -l) -eq 0 ] ; then
  ## No commits yet, create one.
  git commit -m '[mkprj][version:${version}] Initial commit.';
elif [ \$(git diff --cached | wc -l) -gt 0 ]; then
  ## Commit changes
  git commit -m '[mkprj][version:${version}] Updated commit.';
else :
fi;
END_MESSAGE

my $existing_phases = `ls $prj_root/local/share/rules/rules-*.mk 2> /dev/null | sed 's/.mk\$//' | sed 's/^.*rules-//g' | xargs;`;
chop($existing_phases);
my @existing_phases = split / /, $existing_phases;
my %seen;
my @allphases = grep { ! $seen{$_}++ } (@existing_phases, @phases);

my $rules_content= "PHASES := " . join(" ",@allphases) . "\n";
$rules_content .= $phase_rules;

if($dryrun){
	print $sh;
	if(scalar(@phases)){
		print "cat $prj_root/local/share/rules/rules.mk << EOF";
		print $rules_content;
		print "EOF";
	}
}else{
	`$sh`;
	exit $? if $?;
	if(scalar(@phases) and not $skip){
		open FH,">$prj_root/local/share/rules/rules.mk" or die("Can't open file ($prj_root/local/share/rules/rules.mk)");
		print FH $rules_content;
	}
}
