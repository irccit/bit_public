#!/bin/bash

if [ $1 == "" -o $1 == "-h" -o $1 == "--help" ]; then
	echo -e "usage: $0 script_file\nopen the script_file with the editor $EDITOR" >&2;
	exit 1
elif [ "$EDITOR" == "" ]; then
	echo "Error: EDITOR variable not defined."
	exit 1
fi

if [ -e $BIOINFO_HOST.mkpaths ]; then
	DYN_PATH=`grep MAKE_PATH $BIOINFO_HOST.mkpaths | sed 's/.*=//'`;
	PATH="$DYN_PATH:$PATH";
fi;

if [ -e .bmake/paths ]; then
	DYN_PATH=`grep 'MAKE_PATH:=' .bmake/paths | sed 's/.*=//'`;
	PATH="$DYN_PATH:$PATH";
fi;

file=`which $1 2>/dev/null`

if [ "$file" != "" ]; then
	$EDITOR $file;
else
	echo "file not found in PATH ($1)" >&2;
	exit -1;
fi
