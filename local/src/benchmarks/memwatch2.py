#!/usr/bin/env python
# coding: utf-8

from __future__ import with_statement

from optparse import OptionParser
from os import _exit, execvp, fork, kill, listdir, setpgrp, wait
from os.path import join
from signal import SIGTERM
from sys import argv, exit
from time import sleep
import re

class MemoryUsageSampler(object):
	pid_rx = re.compile('[0-9]+')
	
	def __init__(self, interval):
		self.interval = interval
		self.exit_code = None
	
	def sample(self, command):
		child_pid = fork()
		if child_pid == 0:
			setpgrp()
			execvp(command[0], command)
			_exit(1)
		
		else:
			try:
				while True:
					stats = self._proc_stats(child_pid)
					if stats is None:
						break
					else:
						yield stats
					sleep(self.interval)
			
			except KeyboardInterrupt:
				kill(-child_pid, SIGTERM)
		
		pid, self.exit_code = wait()
		assert pid == child_pid
	
	def _proc_stats(self, pgrp):
		resident, shared = 0, 0
	
		for dirname in listdir('/proc'):
			if self.pid_rx.match(dirname) is None:
				continue
			
			try:
				stat_file = join('/proc', dirname, 'stat')
				with file(stat_file, 'r') as fd:
					info = fd.read().split(' ')
			except IOError:
				continue
		
			pgrp_found = int(info[4])
			if pgrp_found != pgrp:
				continue
		
			resident += int(info[23]) * 4096
			shared += int(info[22])
	
		if resident == 0:
			return None
		else:
			return resident, shared

def main():
	parser = OptionParser(usage='%prog [OPTIONS] -- COMMAND...')
	parser.add_option('-l', '--log', dest='log', default='mem.log', help='the name of the memory log (default: mem.log)', metavar='FILENAME')
	parser.add_option('-i', '--interval', dest='interval', type='int', default=5, help='the number of seconds between memory measurements', metavar='SECONDS')
	options, args = parser.parse_args()
	
	if len(args) < 1:
		exit('Unexpected argument number.')
	
	with file(options.log, 'w') as fd:
		memory_usage = MemoryUsageSampler(options.interval)
		for resident, shared in memory_usage.sample(args):
			print >>fd, '%d\t%d' % (resident, shared)
			fd.flush()

if __name__ == '__main__':
	main()
