#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\t"; 
$\="\n";

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 [-h|help] [-g glue]]\n";

my $help=0;
my $header="";
GetOptions (
	'h|help' => \$help,
	't|header=s' => \$header,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}


print '<html><head><style>td {background: #CCCCCC;} </style><title>table</title></head><body><table cellspacing="2">';
if($header){
	print '<thead><tr><th>';
	$,="</th><th>"; 
	print split(/\t/,$header);
	print "</th></tr></thead>"
}

$,="</td><td>";

print '<tbody>';
while(<>){
	chomp;
	my @F = split /\t/,$_,-1;
	print "<tr>";
	for(@F){
		my $class = m/^\d+(\.\d+)?([eE][+-]\d+)?$/  ? ' class="number"' : "";
		print "\t<td$class>$_</td>";
	}
	print "</tr>";
}
print "</tbody>\n<table></body></html>";
