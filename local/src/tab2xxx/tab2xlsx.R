#!/usr/bin/env Rscript --default-packages=methods,utils,stats
sink(stderr())
options(error = function() traceback(2))
suppressMessages(library("optparse"))
suppressMessages(library("xlsx"))

option_list <- list(
	#make_option(c("-c", "--coef"), action="store_true", default=FALSE, help="Print coefficients"),
        #make_option(c("-r", "--randomizations"), type="integer", default=10000, help="Numbewr of randomizations used to compute empirical pvalue [default \"%default\"]"),
        make_option(c("-H", "--header"), action="store_false", default=TRUE, help="The input files do not have an header in the first row")
        #make_option(c("-t", "--test"), action="store", default="pearson", dest="method", help="Compute correlation using this method [default \"%default\"]")
)
parser<-OptionParser(usage = "%prog [options] out_file.xlsx < in_file.tabi
the out filename must terinate with .xlsx
", option_list=option_list)

arguments <- parse_args(parser, positional_arguments = 1)
outfile <- arguments$args[1]
opt<-arguments$options

stdin <- file("stdin")
open(stdin, blocking=TRUE) # http://stackoverflow.com/questions/9370609/piping-stdin-to-r
.data <- read.table(stdin, head=opt$header, sep="\t")

sink()
write.xlsx2(.data, outfile, col.names=opt$header, row.names=FALSE) # col.names=NA => frist cell empty
                
w=warnings()
sink(stderr())
if(!is.null(w)){
        print(w)
}
sink()
