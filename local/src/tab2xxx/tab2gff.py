#!/usr/bin/env python
#
# Copyright 2012,2015 Gabriele Sales <gbrsales@gmail.com>
# Copyright 2012 Davide Risso <risso.davide@gmail.com>

# This file is part of BioinfoTree. It is licensed under the
# terms of the GNU Affero General Public License version 3.

from collections import defaultdict
from itertools import chain
from optparse import OptionParser
from sys import stdin
from types import StringTypes
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage, ignore_broken_pipe


def main():
    options, args = parse_args()

    mapping = column_mapping((parse_colspec(spec) for spec in args), options.reuse_columns)
    builders = make_builders(*mapping)
    n = len(builders)
    need_cols = max(b.rightmost_column() for b in builders)
    output = [None] * n

    for lineno, line in enumerate(stdin, 1):
        tokens = safe_rstrip(line).split('\t')
        if len(tokens) < need_cols:
            exit('Insufficient columns at line %d.' % lineno)

        for i in xrange(n):
            output[i] = builders[i].build_from(tokens)

        print '\t'.join(output)

def parse_args():
    parser = OptionParser(usage=format_usage('''
      %prog [OPTIONS] [COLUMN_SPEC...] <TAB >GFF

      Converts a tab-delimited file into a GFF.

      The format of the input file is specified by
      any number of COLUMN_SPEC, which have the following
      form:

        TAG:COLUMN_NUMBER

      By default, the script assumes the following specs (which can
      be freely overridden):
      * seqname:1
      * start:2
      * end:3
      * feature:4

      Other standard tags are:
      * source
      * score
      * strand
      * frame

      All other tags are inserted as attributes.

      Alternatively, a COLUMN_SPEC may be:

        TAG:STRING

      where STRING is taken as a constant value to be associated
      with TAG.
    '''))

    parser.add_option('-r', '--reuse-columns', dest='reuse_columns',
                      action='store_true', default=False,
                      help='allow associating a single column to multiple tags')

    return parser.parse_args()


class OutputBuilder(object):
    def __init__(self, source_col):
        self.source_col = source_col

    def rightmost_column(self):
        return self.source_col

    def build_from(self, source):
        return source[self.source_col]

class DefaultBuilder(object):
    def __init__(self, default_value):
        self.default_value = default_value

    def rightmost_column(self):
        return 0

    def build_from(self, source):
        return self.default_value

class AttributeBuilder(object):
    def __init__(self):
        self.attributes = []
        self.const_attributes = []

    def add_attribute(self, tag, column):
        self.attributes.append((tag, column))

    def add_const_attribute(self, tag, value):
        self.const_attributes.append('%s "%s"' % (tag, value))

    def rightmost_column(self):
        return max(a[1] for a in self.attributes) if len(self.attributes) else 0

    def build_from(self, source):
        attributes = ['%s "%s"' % (tag, source[column]) for tag, column in self.attributes] + \
                     self.const_attributes
        return '; '.join(attributes)


def parse_colspec(repr):
    tokens = repr.split(':', 1)
    if len(tokens) < 2:
        exit("Invalid column spec (missing ':'): " + repr)

    tag, column = tokens
    if len(tag) == 0:
        exit('Missing tag in column spec: ' + repr)

    try:
        column = int(column)-1
        if column < 0:
            exit('Invalid column number in: ' + repr)
    except ValueError:
        pass

    return tag, column

def column_mapping(colspecs, allow_column_reuse):
    column_to_tag = defaultdict(list)
    constant_tags = []
    used_tags = set()

    user_mapping(colspecs, column_to_tag, constant_tags, used_tags, allow_column_reuse)
    default_mapping(column_to_tag, used_tags, allow_column_reuse)

    return column_to_tag, constant_tags

def user_mapping(colspecs, column_to_tag, constant_tags, used_tags, allow_column_reuse):
    for tag, resource in colspecs:
        if tag in used_tags:
            exit('Tag already used: ' + tag)

        used_tags.add(tag)

        if type(resource) in StringTypes:
            constant_tags.append((tag, resource))
        else:
            column = resource
            if not allow_column_reuse and column in column_to_tag:
                exit('Column %d already in use for: %s' % (column+1, column_to_tag[column]))

            column_to_tag[column].append(tag)

def default_mapping(column_to_tag, used_tags, allow_column_reuse):
    default_tags = [ ('seqname', 0),
                     ('start',   1),
                     ('end',     2),
                     ('feature', 3) ]

    for tag, column in default_tags:
        if tag not in used_tags:
            if not allow_column_reuse and column in column_to_tag:
                exit("Trying to assign tag '%s' to column %d, which is already bound to: %s" % (column_to_tag[column][0], column+1, tag))
            else:
                column_to_tag[column].append(tag)


def make_builders(column_to_tag, constant_tags):
    standard_tags = { 'seqname': 0,
                      'source' : 1,
                      'feature': 2,
                      'start'  : 3,
                      'end'    : 4,
                      'score'  : 5,
                      'strand' : 6,
                      'frame'  : 7 }
    attrib_column = 8

    builders = [ DefaultBuilder('.') ] * len(standard_tags)
    builders.append(AttributeBuilder())

    for source_column, tags in column_to_tag.iteritems():
        for tag in tags:
            output_column = standard_tags.get(tag, None)
            if output_column is None:
                builders[attrib_column].add_attribute(tag, source_column)
            else:
                builders[output_column] = OutputBuilder(source_column)

    for tag, value in constant_tags:
        output_column = standard_tags.get(tag, None)
        if output_column is None:
            builders[attrib_column].add_const_attribute(tag, value)
        else:
            builders[output_column] = DefaultBuilder(value)

    return builders


if __name__ == '__main__':
    ignore_broken_pipe(main)
