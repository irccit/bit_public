#!/usr/bin/env python
#
# Copyright 2010,2012 Gabriele Sales <gbrsales@gmail.com>

from itertools import izip
from optparse import OptionParser
from sys import getfilesystemencoding, stdin
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage, ignore_broken_pipe

def load_data(fd, encoding):
	data = []
	colwidths = []
	
	for lineno, line in enumerate(fd, 1):
		try:
			tokens = tuple(safe_rstrip(line.decode(encoding)).split('\t'))
		except UnicodeDecodeError, e:
			exit('Decode error at line %d: %s:\n%s' % (lineno, str(e), line.rstrip()))

		if len(data) == 0:
			data = [tokens]
			colwidths = [ len(t) for t in tokens ]
		elif len(tokens) != len(colwidths):
			exit('Invalid column number at line %d:\n%s' % (lineno, line.rstrip()))
		else:
			data.append(tokens)
			colwidths = [ max(a,b) for a,b in izip((len(t) for t in tokens), colwidths) ]
	
	return data, colwidths

def build_format(colwidths, sep):
	parts = []
	if len(colwidths):
		for w in colwidths[:-1]:
			parts.append('%%- %ds' % (w+sep,))
		parts.append('%s')
	return ''.join(parts)

def main():
	usage = format_usage('''
		%prog OPTIONS [TSV]
		
		Reformats the input aligning table cells with spaces.

		If no filename is provided, input is taken from stdin.
	''')
	parser = OptionParser(usage=usage)
	parser.add_option('-s', '--separation', dest='separation', type='int', default=2, help='separation between cells (by default: 2)', metavar='SPACES')
	options, args = parser.parse_args()
	
	if len(args) > 1:
		exit('Unexpected argument number.')
	elif options.separation < 0:
		exit('Invalid separation value: %s' % options.separation)

	encoding = getfilesystemencoding()
	
	if len(args) == 1:
		fd = file(args[0], 'r')
	else:
		fd = stdin

	data, colwidths = load_data(fd, encoding)
	fmt = build_format(colwidths, options.separation)
	for tokens in data:
		print (fmt % tokens).encode(encoding)

if __name__ == '__main__':
	ignore_broken_pipe(main)
