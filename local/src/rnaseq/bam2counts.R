#!/usr/bin/env Rscript
#options(error = function() traceback(2))
suppressMessages(library("optparse"))
suppressMessages(library("Rsamtools")) 
suppressMessages(library("limma"))
suppressMessages(library("Rsubread"))

option_list <- list(
	make_option(c("-p", "--paired_end"), action="store_true", dest="paired",default=FALSE, help="Logical indicating if paired-end reads are used. If TRUE, fragments (templates or read pairs) will be counted instead of individual reads. [default \"%default\"]"),
	make_option(c("-s", "--strandness"), action="store", dest="strandness",default=0, help="'integer' indicating if strand-specific read counting should be performed. It has three possible values: '0' (unstranded), '1' (stranded) and '2' (reversely stranded). [default \"%default\"]"),
	make_option(c("-c", "--cores"), action="store", dest="cores",default=1, help="'integer' indicating the number of threads to be used. [default \"%default\"]"),
	make_option(c("-a", "--gtf_attribute"), action="store",dest="gtf.attribute",default="gene_name", help="select the attribute in the gtf file to summarize the read to: gene_id(ensembl_gene_id), gene_name(gene_symbol). [default \"%default\"]"),
	make_option(c("-l", "--label"), action="store", dest="labels",default="", help="Translate the names of bam files used as labels in output with LABELS, use the format \"label1 label2\". The order of LABELS and bam files given as arguments must be the same. [default \"%default\"]")
)
parser<-OptionParser(usage = "%prog [options] gtf bam_file1 bam_file2 bam_file3 ... bam_fileN
Obtains from a directory that contains bam files (*.merge.bam) and an ensembl gtf (typically that in /lustre1/genomes/$SPECIES/annotation/$SPECIES.ensGene_withGeneName.gtf) an R object counts.rda containing the counts and annotation object for all the bam files with feature counts. We must set -p for the PE/SE, the -s for the STRANDNESS and -a for the attribute, typically gene_name, for the summarization.
...

.DOC: stdout
	counts.rda

", option_list=option_list)

shift_fn <- function(x) {
  if(length(x) == 0) {return(NA)}
  shiftret <- x[1]
  assign(as.character(substitute(x)), x[2:(length(x))], parent.frame())
  return(shiftret)
}

arguments <- parse_args(parser, positional_arguments = c(1,Inf))

sink(stderr())#the stdout ahs to be kept clean form messages

args <- arguments$args
opt<-arguments$options

ann = shift_fn(args)

files<-args

labels=c()
if(nchar(opt$labels) > 0) {
	labels=opt$labels
	labels=strsplit(labels,split=" ",perl=T)[[1]]
	if(length(labels)!=length(files)){
		stop(c("ERROR: inconsistent labels number (-l)\n",labels,"\n",files))
	}
}

# bam="160818_SN859_0322_AHW2J2BCXX_old/align/BAM/"

#ann="/lustre1/genomes/mm10/annotation/mm10.ensGene_withGeneName.gtf"
conteUT = suppressPackageStartupMessages(
	featureCounts(files=files,
                        annot.ext = ann,
                        isGTFAnnotationFile = T,
                        isPairedEnd = opt$paired,
                        strandSpecific = opt$strandness,
                        countChimericFragments=F,
                        allowMultiOverlap = F,
                        reportReads=F,
                        nthreads = opt$cores,
                        useMetaFeatures = T, GTF.featureType = 'exon', GTF.attrType = opt$gtf.attribute,countMultiMappingReads = F
	)
)

if(length(labels)>0){
	files=unlist(lapply(X=files,function(x) if(unlist(strsplit(x,split=""))[1]=="/"){paste("X",x,sep="")}else{x}))
	files=gsub(fixed=T, pattern="/", replacement=".", x=files)
	colnames(conteUT$counts) = labels[ match(colnames(conteUT$counts), files) ]
	colnames(conteUT$stat) = labels[ match(colnames(conteUT$stat), files) ]
	conteUT$targets = labels[ match(colnames(conteUT$targets), files) ]

}

save(conteUT,file="/dev/stdout")

#OUTPUT Warnings in STDERR
w=warnings()
sink(stderr())
if(!is.null(w)){
print(w)
        }
sink()
