ALL+=toptable.xls counts.rda
CLEAN+=toptable.xls counts.rda
conditions_colname=condition
reference_condition=ctrl
STRAND?=0
GTF=/lustre1/genomes/mm10/annotation/mm10.ensGene_withGeneName.gtf
ATTRIBUTE?=gene_name
# serve per il test con metadata di brendolan
.META: metadata.txt
	1	filename
	2	sample
	3	condition
	4	run

# Sara' quello nel make file
RUN_ID=160818_SN859_0322_AHW2J2BCXX_old 
bamfiles = $(shell bawk -v RUN=$(RUN_ID) '$$run==RUN {print $$filename}' metadata.txt)
cores?=12

.PRECIOUS:



toptable.xls: eset_in.rda
	eset2toptable -s $< "~0+condition+batch" "treat-ctrl" > $@
	cmp $@ $@.cfr

toptable.tab.xls: eset.tab.rda
	eset2toptable -s $< "~0+condition+batch" "treat-ctrl" > $@

toptable.fac.xls: eset_in.rda
	eset2toptable -s -f "condition batch" $< "(ctrl.A+ctrl.B+ctrl.C+ctrl.D)-(treat.A+treat.B+treat.C+treat.D)" > $@
	cmp $@ toptable.xls.cfr

counts.label.rda: $(GTF) test_in_1.bam test_in_2.bam 
	bam2counts -l "A B" -c $(cores) -p -a $(ATTRIBUTE) -s $(STRAND) $< $^2 $^3 > $@ 
	
#cmp $@ $@.cfr

counts.rda: $(GTF) test_in_1.bam test_in_2.bam 
	bam2counts -c $(cores) -p -a $(ATTRIBUTE) -s $(STRAND) $< $^2 $^3 > $@
	cmp $@ $@.cfr

eset.rda: counts.rda metadata2.txt
	counts2eset $< $^2 > $@

eset.tab.rda: conte_table.txt metadata3.txt
	../counts_table2eset.R $< $^2 > $@

dictionary: /lustre2/scratch/bioinfotree/common/bioinfotree/task/annotations/dataset/gencode/mmusculus/M11/basic.annotation.ensg2gene_symbol2biotype.dictionary	
	ln -s $< $@

biotypes.plot.png: counts.gab.rda dictionary
	/usr/bin/Rscript biotype.R -p $< $^2 $@


# Test di betaPrior=T con Deseq2
# Uso l'eset.rda copiato da Gabellini_493 con condition KO vs WT

toptable.bp.RData: eset.rda
	eset2toptable -n 4 -t deseq2 $< "~0+genotype" "KO-WT" > $@


