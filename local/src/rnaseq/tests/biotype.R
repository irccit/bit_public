#!/usr/bin/Rscript
#options(error = function() traceback(2))
suppressMessages(library("ggplot2"))
suppressMessages(library("optparse"))
suppressMessages(library("limma"))
suppressMessages(library("edgeR"))
suppressMessages(library("GEOquery"))
suppressMessages(library("Rsubread"))

option_list <- list(
	make_option(c("-e", "--expressed"), action="store_true",default=FALSE, help="Apply a filter for expressed genes that is restrictive to 1 cpm in all samples to avoid 0s. If combined with -n it will be applied in at least n samples [default \"%default\"]"),
	make_option(c("-n", "--number_samples"), action="store", dest="number_samples",default=0, help="'integer' indicating the number of samples as minimum for the filter of 1cpm for defining expressed genes. [default \"%default\"]"),
	make_option(c("-p", "--pie_chart"), action="store_true",default=FALSE, help="Produce a piechart with reads assigned to different biotypes [default \"%default\"]"),
	make_option(c("-l", "--label"), action="store", dest="labels",default="", help="Translate the names of bam files used as labels in output with LABELS, use the format \"label1 label2\". The order of LABELS and bam files given as arguments must be the same. [default \"%default\"]"),
	make_option(c("-v", "--violin"), action="store_true", default=FALSE, help="Produce a violin plot with  with statistics on the coverage obtained on biotypes from a counts.rda. [default \"%default\"]" )
)

parser<-OptionParser(usage = "%prog [options] counts.rda dictionary biotypes_plot.png
It produces different files and plots of statistics on the coverage obtained on biotypes from a counts.rda using an annotation dictionary from task/annotations, e.g. basic.annotation.ensg2gene_symbol2biotype.dictionary
...
.DOC: stdout	
	biotypes_plot.png", 
option_list=option_list)

arguments <- parse_args(parser, positional_arguments = 3)
sink(stderr())#the stdout has to be kept clean from messages
args <- arguments$args
opt<-arguments$options

png.file=args[3]

counts = get(load(args[1]))
counts.c = counts$counts
counts.a = counts$annotation
dict = read.delim(args[2],h=F)

ids = match(row.names(counts.c),dict[,2])
counts.a$biotype = dict[ids,3]

if(opt$number_samples!=0 & opt$expressed){
	n.samples=opt$number_samples
		}
if(opt$expressed & opt$number_samples==0){
	n.samples=ncol(counts.c)
} else {n.samples=ncol(counts.c)}

isexpr = rowSums(cpm(counts.c)>1)>=n.samples	
nexp=as.numeric(table(isexpr)[2])

## Define data for Violin plot
data.use = as.data.frame(cpm(counts.c,log=T))
cell.ident = counts.a$biotype

## Define other functions
SetIfNull <- function (x, default) 
{
    if (is.null(x = x)) {
        return(default)
    }
    else {
        return(x)
    }
}

NoGrid <- function (...) 
{
    no.grid <- theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(), 
        ...)
    return(no.grid)
}
###

## Define Violin Plot
violin <- function (feature, data, cell.ident, do.sort, y.max, size.x.use, 
    size.y.use, size.title.use, adjust.use, point.size.use, cols.use, 
    gene.names, y.log, x.lab.rot, y.lab.rot, legend.position, 
    remove.legend) 
	{
	    feature.name <- colnames(data)
	    colnames(data) <- "feature"
	    feature <- "feature"
	    set.seed(seed = 42)
	    data$ident <- cell.ident
	    if (do.sort) {
		data$ident <- factor(x = data$ident, levels = names(x = rev(x = sort(x = tapply(X = data[, 
		    feature], INDEX = data$ident, FUN = mean)))))
	    }
	    if (y.log) {
		noise <- rnorm(n = length(x = data[, feature]))/200
		data[, feature] <- data[, feature] + 1
	    }
	    else {
		noise <- rnorm(n = length(x = data[, feature]))/1e+05
	    }
	    data[, feature] <- data[, feature] + noise
	    y.max <- SetIfNull(x = y.max, default = max(data[, feature]))
	    plot <- ggplot(data = data, mapping = aes(x = factor(x = ident), 
		y = feature)) + geom_violin(scale = "width", adjust = adjust.use, 
		trim = TRUE, mapping = aes(fill = factor(x = ident))) + 
		theme(legend.position = legend.position, axis.title.x = element_text(face = "bold", 
		    colour = "#990000", size = size.x.use), axis.title.y = element_text(face = "bold", 
		    colour = "#990000", size = size.y.use)) + guides(fill = guide_legend(title = NULL)) + 
		geom_jitter(height = 0, size = point.size.use) + xlab("Identity") + 
		NoGrid() + ggtitle(feature) + theme(plot.title = element_text(size = size.title.use, 
		face = "bold"))
	    plot <- plot + ggtitle(feature.name)
	    if (y.log) {
		plot <- plot + scale_y_log10()
	    }
	    else {
		plot <- plot + ylim(min(data[, feature]), y.max)
	    }
	    if (feature %in% gene.names) {
		if (y.log) {
		    plot <- plot + ylab(label = "Log Expression level")
		}
		else {
		    plot <- plot + ylab(label = "Expression level")
		}
	    }
	    else {
		plot <- plot + ylab(label = "")
	    }
	    if (!is.null(x = cols.use)) {
		plot <- plot + scale_fill_manual(values = cols.use)
	    }
	    if (x.lab.rot) {
		plot <- plot + theme(axis.text.x = element_text(angle = 90, 
		    vjust = 0.5))
	    }
	    if (y.lab.rot) {
		plot <- plot + theme(axis.text.x = element_text(angle = 90))
	    }
	    return(plot)
	}
###

if(!opt$pie_chart && !opt$violin){
	if(opt$expressed){
	png(png.file,width=1800,height=1000)
	par(mar=c(20,5,7,7))
	boxplot(ylab="log2NormalizedExpression",
		cpm(counts.c[isexpr,],log=T)~counts.a[isexpr,"biotype"],
		names=levels(counts.a$biotype[isexpr]),
		las=2,
		cex.axis=1,
		main=paste("Biotypes distribution for",nexp, "expressed genes",sep=" "),
		cex=0.5)
	dev.off()
	} else {
	png(png.file,width=15)
	par(mar=c(15,5,7,7))
	boxplot(ylab="log2NormalizedExpression",
		cpm(counts.c,log=T)~counts.a$biotype,
		names=levels(counts.a$biotype),
		las=2,
		cex.axis=1,
		main="Biotypes distribution",
		cex=0.5)
	dev.off()
	}

}

if(opt$pie_chart){
	biotypes = levels(counts.a$biotype)
	pp = unlist(lapply(levels(counts.a$biotype),function (x) sum(counts.c[which(counts.a$biotype==x),]))) 
	names(pp)=levels(counts.a$biotype)
	png(png.file,width=600,height=600)

	colors=colorRampPalette(c("red","green","blue","yellow","black","orange","pink","grey"))(length(pp)) 
	par(mar=c(10,3,3,3),xpd=T)
	pie(pp,col=colors,labels=ifelse(pp/sum(pp)>0.01,names(pp),""),main="PieChart of Total Reads to Biotypes")
	legend(x=-1.7,y=-1,legend=biotypes,fill=colors,horiz=F,ncol=4,cex=0.5,);
	dev.off()

}

if (opt$violin){
	if(opt$expressed){
	png(png.file,width=1800,height=1000)
	violin(feature="feature", data = data.use,cell.ident=cell.ident,
		do.sort=T,
		y.log=F, 
		y.max=20, 
		adjust.use=1,
		size.x.use=16,
		size.title.use=20,
		size.y.use=16,
		legend.position="right",
		point.size.use=0,
		gene.names=row.names(counts.c),
		cols.use=NULL,
		x.lab.rot=TRUE,
		y.lab.rot=FALSE)
	dev.off()

	
    }
}
