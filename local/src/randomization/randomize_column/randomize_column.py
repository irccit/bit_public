#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict

def numerize(t):
	try:
		return int(t)
	except ValueError:
		try:
			return float(t)
		except ValueError:
			return t

def main():
	usage = format_usage('''
		%prog COL < STDIN
		shuffle the COL column of the input file
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-c', '--convert', action='store_true', default=False, help='convert the fields containing string representing numerical values in number to reduce the memory size at the cost of cpu time')
	parser.add_option('-n', '--number_of_shuffled_output', type=int, default=1, help='output N matrix, each with a indipendend shuffling of the COL column, if N is > 1 then each matrix is separed by a fasta header like ">N"', metavar='N')
	
	options, args = parser.parse_args()

	if len(args) != 1:
		exit('Unexpected argument number.')
	
	col = int(args[0]) - 1
	col_num = None
	row_num = 0
	matrix = []
	for line in stdin:
		row_num += 1
		tokens = safe_rstrip(line).split('\t')
		if options.convert:
			tokens = [ numerize(t) for t in tokens]
				
		if col_num is None:
			col_num = len(tokens)
			for t in tokens:
				matrix.append([t])
		else:
			for i,t in enumerate(tokens):
				matrix[i].append(t)

	stderr.write("LOADED\n")

	for i in xrange(0,options.number_of_shuffled_output):
		if options.number_of_shuffled_output > 1:
			print ">%d" % i

		shuffle(matrix[col])
		for i in range(0,row_num):
			row = []
			for col in matrix:
				row.append(str(col[i]))
			print '\t'.join(row)

if __name__ == '__main__':
	main()

