#include <iostream>
#include <stdlib.h>
#include "gsl/gsl_randist.h"

using namespace std;


int main( int argc, char* argv[] ){
	if(argc!=3){
		cerr<<"usage: tstudent value degre_of_fredom"<<endl;
		exit(-1);
	}
	//cout << argv[1] << "\t" << gsl_ran_tdist_pdf(atof(argv[1]),atof(argv[2])) << endl;
	cout << gsl_ran_tdist_pdf(atof(argv[1]),atof(argv[2])) << endl;
	return(0);
}
