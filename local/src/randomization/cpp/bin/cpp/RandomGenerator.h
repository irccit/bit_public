#ifndef RANDOM_GENERATOR_H
#define RANDOM_GENERATOR_H

# include <iostream>
# include <stdio.h>
# include <stdlib.h>
# include <math.h>
# include <sys/time.h>
# include <getopt.h>
# include <string.h>
# include <gsl/gsl_rng.h>

// to compile
// random_series: random_series.cpp
//         g++ -lgsl -lgslcblas $< -o $@
// 

/*! \brief Generatore di numeri casuali (gsl)
 *	
 *	Questa classe e` utile per generare numeri casuali interi o double,
 *	uniformemente distribuiti in un intervallo, oppure per mescolare array
 *	di qualunque tipo.
 *
 *	Non implementa un vero e proprio generatore, piuttosto funge da
 *	interfaccia per un generatore della GNU Scientific Library:
 *
 * http://www.gnu.org/software/gsl/manual/gsl-ref_17.html#SEC268
 *
 * 
 * reads the environment variables 
 * GSL_RNG_TYPE and GSL_RNG_SEED 
 * and uses their values to set the corresponding library variables
 *
 * If GSL_RNG_TYPE is not set default generator is used (gsl_rng_mt19937); 
 * if GSL_RNG_SEED is not set a time funcion is used to seed.
 *
 * gsl_rng_mt19937
 * The MT19937 generator of Makoto Matsumoto and Takuji Nishimura is a variant of
 * the twisted generalized feedback shift-register algorithm, and is known as the
 * "Mersenne Twister" generator. It has a Mersenne prime period of 2^19937 - 1 
 * (about 10^6000) and is equi-distributed in 623 dimensions. It has passed the 
 * DIEHARD statistical tests. It uses 624 words of state per generator and is 
 * comparable in speed to the other generators. The original generator used a 
 * default seed of 4357 and choosing s equal to zero in gsl_rng_set reproduces 
 * this.
 *
 * For more information see,
 * * Makoto Matsumoto and Takuji Nishimura, 
 * "Mersenne Twister: A 623-dimensionally equidistributed uniform pseudorandom  * number generator". 
 * ACM Transactions on Modeling and Computer Simulation, Vol. 8, No. 1 (Jan. 1998), Pages 3-30 
 *
 * The generator gsl_rng_19937 uses the second revision of the seeding procedure
 * published by the two authors above in 2002. The original seeding procedures could 
 * cause spurious artifacts for some seed values.
 * They are still available through the alternate generators gsl_rng_mt19937_1999 and gsl_rng_mt19937_1998";
 */

class RandomGenerator{
	public:
		/*!\brief Costruttore
		 *
		 * 	si occupa di allocare la memoria per il generatore,
		 * 	decide quale generatore verra' usato, imposta i seed
		 *
		 *  \param verbose se impostato a true vengono restituiti alcuni valori in STDOUT
		 * 
		 */
		RandomGenerator(bool verbose=false);

		/*!\brief Distruttore
		 *
		 * 	disalloca la memoria utilizzata dal generatore gsl
		 *
		 */
		~RandomGenerator(){ gsl_rng_free (GslRng); }//libero la mamoria
		
		/*!
		 *  Restituisce un intero casuale compreso tra imin e imax
		 *  se non sono state chiamate in precedenza
		 *  set_min() e set_max() esce con un errore
		 * 
		 */
		int get_int();

		/*!
		 *  Restituisce un double casuale compreso tra dmin e dmax
		 *  se non sono state chiamate in precedenza
		 *  set_min() e set_max() esce con un errore
		 * 
		 */
		double get_double();

		/*! /brief Randomizza un array
		 *
		 * /param a[] un array contentente dati di qualunque tipo
		 * /param a_size la dimensione dell'array
		 * /return non restituisce nulla ma l'array dopo la chiamata sara' mescolato
		 * 
		 * non e` necessario alcun altro parametro di configurazione: esempio di utilizzo:
		 * 
		 *	double * val = new double[data_cols];
		 *	RandomGenerator * RG = new RandomGenerator(false);	
		 *	RG->shuffle(val,data_cols);
		 *
		 */
		template<class Type>
		void shuffle(Type a[],unsigned int a_size);
		
		/*!
		 *	imposta il massimo dell'intervallo in cui verranno generati i numeri casuali
		 *	alla chiamata di get_double
		 *
		 *	nota l'overloading con set_max(int i)
		 *	
		 *
		 */
		void set_max(double d){
			dmax=d;
			dmax_set=true;
		}

		/*!
		 *	imposta il minimo dell'intervallo in cui verranno generati i numeri casuali
		 *	alla chiamata di get_double
		 *
		 *	nota l'overloading con set_min(int i)
		 *	
		 *
		 */
		void set_min(double d){
			dmin=d;		
			dmin_set=true;
		}
		
		/*!
		 *	imposta il massimo dell'intervallo in cui verranno generati i numeri casuali
		 *	alla chiamata di get_int
		 *
		 *	nota l'overloading con set_max(double d)
		 *	
		 *	/param i deve essere esattamente un intero e non un unsigned per esempio, altrimenti il compilatore non sa se usare questa funzione o l'overloaded set_max(double d)
		 *
		 */
		void set_max(int i){
			imax=i;
			imax_set=true;
		}
		
		/*!
		 *	imposta il minimo dell'intervallo in cui verranno generati i numeri casuali
		 *	alla chiamata di get_int
		 *
		 *	nota l'overloading con set_min(double d)
		 *	
		 *	/param i deve essere esattamente un intero e non un unsigned per esempio, altrimenti il compilatore non sa se usare questa funzione o l'overloaded set_min(double d)
		 *
		 */
		void set_min(int i){
			imin=i;	
			imin_set=true;
		}
		
		void set_int(bool b=true){
			integer=b;
		}
	private:		
		gsl_rng * GslRng;
		double dmin;
			bool dmin_set;
		double dmax;
			bool dmax_set;
		int imin;
			bool imin_set;
		int imax;
			bool imax_set;
		bool integer;
		bool verbose;

		unsigned long int get_seed();
};
#include "RandomGenerator.cpp"
#endif
