<?php
error_reporting(E_ALL);
ini_set('memory_limit',400*1024*1024);

class DiagEvents
{
	var $db_user = "molineri";
	var $db_pass = "pippo123";
	var $db_host = "localhost";
	var $db_name = "cgv_test";
	var $db_conn;
	var $seg_stmt;
	var $reg_stmt;
	
	function __construct($db_name=FALSE)
	{
		/* roba che serviva ad amfphp
		$this->methodTable = array
		(
			'segs' => array
			(
				'access' => 'remote',
				'description' => 'get segs, diags and events',
				'arguments' => array('chr_id_x','x1','x2','chr_id_y','y1','y2')
			),
			'regs' => array
			(
				'access' => 'remote',
				'description' => 'get regions (masks, exons, introns...)',
				'arguments' => array('chr_id','x1','x2')
			)
		);
		*/
		$this->open_db_conn($db_name);
	}

	function open_db_conn($db_name){
		if($db_name){
			$this->db_name=$db_name;
		}
		
		$this->db_conn = new mysqli($this->db_host, $this->db_user, $this->db_pass, $this->db_name);
		/* check connection */
		if (mysqli_connect_errno()) {
			   printf("Connect failed: %s\n", mysqli_connect_error());
			   exit();
		}
	}
	
	function segs_query($chr1, $x1, $x2, $chr2, $y1, $y2)
	{
		$p_info=$this->get_chr_pair_id($chr1,$chr2);
		if($p_info===false){
			return array();
		}

		list($p_id,$reverse)=$p_info;
		
		/* create a prepared statement */
		$stmt = $this->db_conn->prepare('
			SELECT s.x_start, s.x_stop, s.y_start, s.y_stop, t.type, t.subtype
		 	FROM segments s
			JOIN types t ON s.type_id = t.id
			WHERE
				s.chromosome_pair_id = ? AND
				LEAST(s.x_start, s.x_stop) <= ? AND
				GREATEST(s.x_start, s.x_stop) >= ?  AND
				LEAST(s.y_start, s.y_stop) <= ? AND
				GREATEST(s.y_start, s.y_stop) >= ?
		');

		
		if(!$stmt){
			printf("Statement failed2: %s\n", mysqli_connect_error());
			exit();
		}
		

		
		if($reverse){
			$stmt->bind_param("iiiii", $p_id, $y2, $y1, $x2, $x1);
			$stmt->execute();
		}else{
			/*die(htmlentities("	s.chromosome_pair_id = $p_id AND
				LEAST(s.x_start, s.x_stop) <= $x2 AND
				GREATEST(s.x_start, s.x_stop) >= $x1  AND
				LEAST(s.y_start, s.y_stop) <= $y2 AND
				GREATEST(s.y_start, s.y_stop) >= $y1"));*/
			$stmt->bind_param("iiiii", $p_id, $x2, $x1, $y2, $y1);
			$stmt->execute();
		}

		$this->seg_stmt=$stmt;	
		$this->seg_reverse_chr_couple=$reverse;	
		
		if(!$reverse){
			$stmt->bind_result($x_start,$x_stop,$y_start,$y_stop,$type,$subtype);
		}else{
			$stmt->bind_result($y_start,$y_stop,$x_start,$x_stop,$type,$subtype);
		}
		$retval = array();
		while($stmt->fetch()){
			$retval[$type][]=array($x_start,$x_stop,$y_start,$y_stop);
		}
		
		echo('<pre>');
		print_r($retval);
		echo('</pre>');
	}

	function get_next_seg(){
		$this->seg_stmt or die("ERROR: cal segs before of get_next_seg");
		
		if(!$this->seg_reverse_chr_couple){
			$this->seg_stmt->bind_result($x_start,$x_stop,$y_start,$y_stop,$type,$subtype);
		}else{
			$this->seg_stmt->bind_result($y_start,$y_stop,$x_start,$x_stop,$type,$subtype);
		}
		
		if($this->seg_stmt->fetch()){
			return array($x_start,$x_stop,$y_start,$y_stop,$type,$subtype);
		}else{
			$this->seg_stmt->close();
			return NULL;
		}
	}

	function get_chr_pair_id($chr1, $chr2){
		if(! $stmt = $this->db_conn->prepare('
			SELECT p.id 
			FROM chromosome_pairs as p
			JOIN (chromosomes c1, chromosomes c2) ON(
				c1.id=p.chromosome1_id AND
				c2.id=p.chromosome2_id 
			)
			WHERE c1.chromosome=? and c2.chromosome=?')
		){
			printf("Statement failed: %s\n", mysqli_connect_error());
			exit();
		}

		$stmt->bind_param("ss", $chr1, $chr2);
		$stmt->execute();
		$stmt->bind_result($p_id);

		$retval=array();
		if($stmt->fetch()){
			$retval[0]=$p_id;
			$retval[1]=false;
		}else{
			$stmt->close();
			if(!$stmt = $this->db_conn->prepare('
				SELECT p.id, c1.species, c2.species
				FROM chromosome_pairs as p
				JOIN (chromosomes c1, chromosomes c2) ON(
					c1.id=p.chromosome1_id AND
					c2.id=p.chromosome2_id 
				)
				WHERE c1.chromosome=? and c2.chromosome=?')
			){
				printf("Statement failed 3: %s\n", mysqli_connect_error());
				exit();
			}
			
			$stmt->bind_param("ss", $chr2, $chr1);
			
			$stmt->execute();
			$stmt->bind_result($p_id,$species1,$species2);
			
			if($stmt->fetch()){
				if($species2==$species1){
					$retval[0]=$p_id;
					$retval[1]=true;
				}else{
					$retval=false;
				}
			}else{
				die('ERROR: DiagEvents::get_chr_pair_id: invalid chromosome couple.');
				$retval=false;
			}
		}
			
		$stmt->close();
		return $retval;
	}

	function regs($chr,$x1,$x2)
	{
		if (! $stmt = $this->db_conn->prepare('
			SELECT r.start, r.stop, t.type, t.subtype
			FROM regions r
			JOIN (types t, chromosomes c) ON (r.type_id = t.id AND c.id = r.chromosome_id)
			WHERE
				c.chromosome =  ? AND
				r.start <= ? AND
				r.stop >= ?
		')) {
			printf("Statement failed: %s\n", mysqli_connect_error());
			exit();
		}
		
		/* bind parameters for markers */
		$stmt->bind_param("sii", $chr, $x2, $x1);

		/* execute query */
		$stmt->execute();

		/* bind result variables */
		$stmt->bind_result($start,$stop,$type,$subtype);
		
		$retval = array();
		while($stmt->fetch()){
			$idx=strpos($subtype,':');
			if ($idx !== FALSE) {
				$subtype=substr($subtype,0,$idx);
			}
			$retval[$type][$subtype][]=array($start,$stop);
		}
		
		/* close statement */
		$stmt->close();
		return $retval;
		
	}

	/*****
	*
	*	OLD FUNCTION
	*
	function filter_region_by_range($r)
	{
		return !(
			$r[0] > $this->x2
				or
			$r[1] < $this->x1
		);
	}
	function filter_segment_by_coords($d)
	{
		return !(
			$d[2] < $this->x1
				or
			$d[0] > $this->x2
				or
			$d[3] < $this->y1
				or
			$d[1] > $this->y2
		);
	}
*/
}
?>

