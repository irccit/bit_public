#!/usr/bin/Rscript
#options(error = function() traceback(2))
suppressMessages(library("biomaRt"))
# suppressMessages(library("WriteXLS"))
suppressMessages(library("ChIPpeakAnno"))
suppressMessages(library("optparse"))
suppressMessages(library("GenomicRanges"))
options(java.parameters = "-Xmx8000m")
suppressMessages(library("xlsx"))
option_list <- list(
  make_option(c("-v","--version"),action="store", default="grch37.ensembl.org", dest="version", metavar= "VERSION", help="annotation version to be used from biomart [default \"%default\"]"),
  make_option(c("-s","--species"),action="store", default="hsapiens_gene_ensembl", dest="species", metavar= "SPECIES", help="species to be used from biomart to annotate peaks [default \"%default\"]")
)

usage = "%prog [options] peaks.edgeR.RData annotation.rda annotated_peaks.txt
Takes a peaks.edgeR.RData object, loads it and uses the top.list with results to annotate the peaks using ChipAnnot. The annotation.rda contains a GRanges with TSS annotation from biomaRt. It produces a txt file named annotated_peaks.txt
...

.DOC: stdout
	RData
"


parser<-OptionParser(usage = usage, option_list=option_list)

sink(stderr())

arguments <- parse_args(parser, positional_arguments = 3)
opt<-arguments$options
args <- arguments$args

# Use biomaRt for annotation using ENSEMBL mart with the version grch37 by default, and the path. 
# ensembl is giving some problems hence I substitute it, hence it works only for human GRCh37
mart=useMart(biomart="ENSEMBL_MART_ENSEMBL",dataset="hsapiens_gene_ensembl", host = "feb2014.archive.ensembl.org")
#mart = useMart(biomart = "ENSEMBL_MART_ENSEMBL", host=opt$version, path="/biomart/martservice", dataset = opt$species)

# Construct the annotation using TSS and the mart object
#TSS.human.GRCh37 = getAnnotation(mart, featureType = "TSS")

# I'd better have a second argument that is the annotation.rda made by:
# blabla/mart=blabla;TSS.human.GRCh37 = getAnnotation(mart, featureType = "TSS") ; save(TSS.human.GRCh37,file="annotation.rda")
# and then use it as an argument:


load(args[1])
if (opt$version=="grch37.ensembl.org" & opt$species=="hsapiens_gene_ensembl") {
	data('TSS.human.GRCh37')} else { TSS.human.GRCh37=get(load(args[2]))}

filename = args[3]

annotated.peaks.list = lapply(top.list,function (x) {
  .p=x[,1:3]
  .pano = GRanges(ranges = IRanges(start = .p$start,end = .p$end),seqnames =.p$chr)
  annotatedPeak = annotatePeakInBatch(.pano, AnnotationData=TSS.human.GRCh37,output = "both", PeakLocForDistance = "middle", FeatureLocForDistance = "TSS")
  .df = as.data.frame(annotatedPeak)
  
  x$id = paste(x$chr,x$start,x$end,sep="_")
  .df$id = paste("chr",.df$seqnames,"_",.df$start,"_",.df$end,sep="")
  x.df = merge(x,.df,by="id")
  ens_sym = getBM(attributes = c("ensembl_gene_id","external_gene_id","description"), filters = "ensembl_gene_id",values = x.df$feature,mart=mart)
  x.df$symbol = as.character(ens_sym[match(x.df$feature,ens_sym[,1]),2])
  x.df$description = as.character(ens_sym[match(x.df$feature,ens_sym[,1]),3])
  return(x.df)
  })

nn=0
lapply(annotated.peaks.list,function(.l){
  	nn=nn+1
	write.table(x = .l,file = paste(filename,names(annotated.peaks.list)[nn],sep="_"),row.names=TRUE,quote=FALSE,col.names=TRUE,sep="\t")
})

