-b--
title: "Report_QC_Medip"
author: "Jose M. Garcia Manteiga" 
date: "25 October 2016"
output: html_document
	toc: true
	number_sections: true
---

```{r lib, echo=F, cache=F, message=F, warning=F, results='asis'}
library(knitr)
library(limma)
library(GEOquery)
library(Rsubread)
library(mixOmics) #check!
library(edgeR) 
library(RColorBrewer)
library(pheatmap)
```

## QC-counts

# Histogram of CPMs of peak coverage
```{r hist, echo=F, cache=F, message=F, warning=F, results='asis'}
#setwd("/Volumes/Cluster_Lustre2/scratch/bioinfotree/common/bioinfotree/local/src/medip/tests/")
eset=get(load("peaks.eset.rda"))
counts=exprs(eset)

y.all<-DGEList(counts=exprs(eset),genes=fData(eset))
hist(cpm(y.all$counts,log=T))
isexpr= rowSums(cpm(y.all)>1) >= opt$number_replicates
y = y.all[isexpr,]
y <- calcNormFactors(y,method="TMM")
hist(cpm(y,log = T))

eg = dim(y)[1]
lcpm=cpm(y,log = T)
x=y
nsamples <- ncol(x)
col <- brewer.pal(ifelse(nsamples<=12,nsamples,12), "Paired")
plot(density(lcpm[,1]), col=col[1], lwd=2, ylim=c(0,max(density(lcpm)$y*1.1)), las=2,
      main="", xlab="")
title(main="Density plot of Normalized Peak Coverage", xlab="Log-cpm")
abline(v=0, lty=3)
for (i in 2:nsamples){
 den <- density(lcpm[,i])
 lines(den$x, den$y, col=col[i], lwd=2)
}
legend("topright", colnames(lcpm), text.col=col, bty="n",cex=1,text.width = 2)
title(sub=paste(eg," peaks with CPM > 1 coverage in at least 5 samples",sep=""))
```


# Boxplots to check for normalization
```{r boxplots, echo=F, cache=F, message=F, warning=F, results='asis'}
par(mar=c(max(unlist(lapply(colnames(lcpm),nchar))),7,7,7))
boxplot(lcpm, las=2, col=col, main="")
title(main="Boxplots of normalized samples",ylab="Log-cpm")
```

# MDS
A Multi-Dimensional Scaling plot(MDS) plots samples on a two-dimensional scatterplot so that distances on the plot approximate the typical log2 fold changes between the samples. A good clustering MDS/PCA separation of the different conditions is not mandatory to obtain DEGs, but it is a clear sign that our treatment/biological condition produces a significant change in the expression profile and hence will enhance statistical power.  If samples in MDS/PCA are not clustered at all, it is possible that the p.values for our contrasts of interest are rather high, hence producing no statistically significant differentially expressed genes. If other quality features are OK, the main reason for this should be found in the Experimental Design, most likely a higher variance intragroup/replicate than inter-treatment.  

```{r PCA, echo=F, cache=F, message=F, warning=F, results='asis', fig.height=12}
plot.PCA = function(x, intgroup, intgroup2, ntop=500, title="PC1 vs PC2")
{
  require("lattice")
  require("genefilter")
  rv = rowVars(x)
  select = order(rv, decreasing=TRUE)[seq_len(ntop)]
  pca = prcomp(t(x[select,]))

  fac = factor(pData(eset)[, intgroup])
  fac2 = factor(pData(eset)[, intgroup2])
  colours = brewer.pal(nlevels(fac)+1, "Dark2")
  par(mfrow=c(1,2))
  plot(PC2 ~ PC1,data=as.data.frame(pca$x),main=title,
    cex=2,
    pch=14+as.numeric(fac2),
    col=1+as.numeric(fac));
  plot.new()  
  legend("topleft",legend=levels(fac),bty = "n",text.col = 1+as.numeric(as.factor(levels(fac))),cex=2)
  legend("bottom",legend=levels(fac2),bty = "n",pch = as.numeric(levels(as.factor(14+as.numeric(fac2)))),text.width = 1,cex=1.5)
}



if(length(pData(eset)$sex)>0 && length(pData(eset)$family)>0){
plot.PCA(lcpm,intgroup = "sex",intgroup2 = "family",ntop = 500, title="PC1 vs PC2 by sex")
  }

if(length(pData(eset)$condition)>0 && length(pData(eset)$family)>0){
plot.PCA(lcpm,"condition","family",ntop=500, title="PC1 vs PC2 by condition")
}

```

