#!/usr/bin/env python
#
# Copyright 2011 Gabriele Sales <gbrsales@gmail.com>

from __future__ import division
from optparse import OptionParser
from sys import maxint, stdin, stdout
from vfork.util import exit, format_usage
from vfork.draw.plot import CGVPlot
from vfork.draw.surface import PNGSurface


def read_segments(fd, x_min, x_max, y_min, y_max):
	segments = []
	space_bounds = [maxint, 0, maxint, 0]
	
	try:
		for lineno, line in enumerate(fd):
			tokens = line.rstrip().split('\t')
			
			if len(tokens) not in (4, 8):
				raise ValueError
			
			coords = [ int(i) for i in tokens[:4] ]
			
			box_x_min = min(coords[0], coords[1])
			box_x_max = max(coords[0], coords[1])
			box_y_min = min(coords[2], coords[3])
			box_y_max = max(coords[2], coords[3])
			
			if x_min is not None and box_x_max <= x_min:
				continue
			elif x_max is not None and box_x_min >= x_max:
				continue
			elif y_min is not None and box_y_max <= y_min:
				continue
			elif y_max is not None and box_y_min >= y_max:
				continue
			
			if box_x_min < space_bounds[0]:
				space_bounds[0] = box_x_min
			if box_x_max > space_bounds[1]:
				space_bounds[1] = box_x_max
			if box_y_min < space_bounds[2]:
				space_bounds[2] = box_y_min
			if box_y_max > space_bounds[3]:
				space_bounds[3] = box_y_max
			
			if len(tokens) > 4:
				coords.append(tuple(float(c) for c in tokens[4:]))
			else:
				coords.append(None)
			
			segments.append(coords)
	
	except ValueError:
		exit('Malformed input at line %d: %s' % (lineno+1, line))
	
	if x_min is not None:
		space_bounds[0] = x_min
	if x_max is not None:
		space_bounds[1] = x_max
	if y_min is not None:
		space_bounds[2] = y_min
	if y_max is not None:
		space_bounds[3] = y_max
	
   	return segments, space_bounds

def read_stripes(filename):
	with file(filename, 'r') as fd:
		try:
			for lineidx, line in enumerate(fd):
				tokens = line.rstrip().split('\t')
				if len(tokens) not in (2, 6):
					raise ValueError

				start = int(tokens[0])
				stop  = int(tokens[1])

				if len(tokens)>2:
					color = tuple(float(c) for c in tokens[2:])
				else:
					color = None

				yield start, stop, color

		except ValueError:
			exit('Malformed input at line %d of file %s: %s' % (lineidx+1, filename, line))


def main():
	parser = OptionParser(usage=format_usage('''
          Usage: cgvs <SEGMENTS >PNG

          Reads a list of segments with the following format:
          1) start X
          2) stop  X
          3) start Y
          4) stop  Y

          Coordinates can be optionally followed by floats in the 0-1 range
          defining color intensities:
          5) red
          6) green
          7) blue
          8) alpha

          and plots them on the plane producing a PNG image.

          The options "--stripes-horizontal" and "--stripes-vertical" allow you
          to add horizontal and vertical colored bands to the plot. Stripes are
          described by the following format:
          1) start
          2) stop

          A color can be optionally added:
          3) red
          4) green
          5) blue
          6) alpha
        '''))
	parser.add_option('-a', '--x-min', type='int', dest='x_min', help='minimum x coordinate of the drawing rectangle', metavar='X')
	parser.add_option('-b', '--x-max', type='int', dest='x_max', help='maximum x coordinate of the drawing rectangle', metavar='X')
	parser.add_option('-c', '--y-min', type='int', dest='y_min', help='minimum y coordinate of the drawing rectangle', metavar='Y')
	parser.add_option('-d', '--y-max', type='int', dest='y_max', help='maximum y coordinate of the drawing rectangle', metavar='Y')
	parser.add_option('-i', '--width', type='int', dest='width', default=800, help='the width of the PNG image (default: 800)', metavar='W')
	parser.add_option('-j', '--height', type='int', dest='height', default=800, help='the height of the PNG image (default: 800)', metavar='H')
	parser.add_option('-k', '--background', action='store_true', dest='background', default=False, help='use a white background (default: transparent)')
	parser.add_option('', '--stripes-horizontal', type='string', dest='stripes_horizontal', default=None, help='file containing the horizonltal stripes', metavar='FILE')
	parser.add_option('', '--stripes-vertical', type='string', dest='stripes_vertical', default=None, help='file containing the vertical stripes', metavar='FILE')
	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')
	elif options.x_min is not None and options.x_min < 0:
			exit('Invalid minimum x coordinate.')
	elif options.x_max is not None and options.x_max < 1:
		exit('Invalid maximum x coordinate.')
	elif options.x_min is not None and options.x_max is not None and options.x_min >= options.x_max:
		exit('Invalid maximum x coordinate.')
	elif options.y_min is not None and options.y_min < 0:
		exit('Invalid minimum y coordinate.')
	elif options.y_max is not None and options.y_max < 1:
		exit('Invalid maximum y coordinate.')
	elif options.y_min is not None and options.y_max is not None and options.y_min >= options.y_max:
		exit('Invalid maximum y coordinate.')
	
	segments, space_bounds = read_segments(stdin, options.x_min, options.x_max, options.y_min, options.y_max)
	if len(segments) == 0:
		exit('Nothing to do.')
	
	surface = PNGSurface(options.width, options.height)
	if options.background:
		surface.rgba = (1,1,1,1)
		surface.fill_rectangle(0, 0, options.width, options.height)
	
	plot = CGVPlot(space_bounds[0], space_bounds[2], space_bounds[1]-space_bounds[0], space_bounds[3]-space_bounds[2], surface)

	grey = (0.5,0.5,0.5,0.5)
	if options.stripes_horizontal:
		for b, e, color in read_stripes(options.stripes_horizontal):
			if color is None: color = grey
			plot.draw_horizontal_region(b, e-b, color)	

	if options.stripes_vertical:
		for b, e, color in read_stripes(options.stripes_vertical):
			if color is None: color = greyb
			plot.draw_vertical_region(b, e-b, color)

	black = (0,0,0,1)
	for segment in segments:
		color = segment[4]
		if color is None: color = black
		plot.draw_segment(segment[0], segment[1], segment[2], segment[3], color)

	surface.write_to_file(stdout)


if __name__ == '__main__':
	main()
