#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader
import editdistance
from Bio.Seq import Seq




def main():
	usage = format_usage('''
		%prog REFERENCE_SEQUENCE < STDIN

		STDIN
		must contain only sequences, one per line

		STDOUT
		one number for each line of input
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-b', '--both_strand', dest='both_strand', action='store_true', default=False, help='print the minimum edit distance between the reference and the input sequence or its reverse complement [default: %default]')

	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')

	ref_seq = args[0]
	both_strand = options.both_strand
	
	#for id, sample, raw, norm in Reader(stdin, '0u,1s,2i,3f', False):
	for line in stdin:
		seq = line.rstrip()
		e = editdistance.eval(seq, ref_seq)
		if both_strand:
			seq_rev=Seq(seq).reverse_complement().tostring()
			#print "%s\t%d\t%d" % (seq, e, editdistance.eval(seq_rev, ref_seq))
			print min(e, editdistance.eval(seq_rev, ref_seq))
		else:
			print e
			
if __name__ == '__main__':
	main()

