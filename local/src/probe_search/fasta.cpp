// Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

#include "fasta.h"
#include <sstream>
#include <stdexcept>
#include <stdio.h>
#include <string.h>

using namespace std;

FastaReader::FastaReader(const string& filename) : _filename(filename)
{
	_fd = fopen(filename.c_str(), "r");
	if (_fd == NULL)
		throw runtime_error("cannot open file " + filename);
}

FastaReader::~FastaReader()
{
	if (_fd != NULL)
		fclose(_fd);
}

const FastaBlock* FastaReader::get_block()
{
	const int n = 1024*1024;
	char buf[n];
	
	if (!read_line(buf, n))
		return NULL;
	if (buf[0] != '>')
		throw runtime_error("invalid label in file " + _filename);
	string label(buf, 1, strlen(buf)-2);
	
	ostringstream os;
	while (true)
	{
		const long pos = ftell(_fd);
		if (!read_line(buf, n))
			break;
		else if (buf[0] == '>')
		{
			if (fseek(_fd, pos, SEEK_SET) != 0)
				throw runtime_error("cannot seek file " + _filename);
			break;
		}
		else
			os << string(buf, 0, strlen(buf)-1);
	}
	
	string sequence = os.str();
	if (sequence.length() == 0)
		throw runtime_error("missing sequence in file " + _filename);
	
	return new FastaBlock(label, sequence);
}

bool FastaReader::read_line(char* buf, const int size)
{
	if (fgets(buf, size, _fd) == NULL)
	{
		if (feof(_fd))
			return false;
		else
			throw runtime_error("cannot read from file " + _filename);
	}
	
	return true;
}
