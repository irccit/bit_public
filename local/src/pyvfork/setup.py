#!/usr/bin/env python

from setuptools import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext


setup(name='pyvfork',
      version='1.0.2',
      description='vfork bioinformatic library',
      author='Gabriele Sales',
      author_email='gbrsales@gmail.com',
      cmdclass = {'build_ext': build_ext},
      packages=['vfork',
                'vfork.alignment',
                'vfork.config',
                'vfork.draw',
                'vfork.fasta',
                'vfork.fastq',
                'vfork.genbank',
                'vfork.geometry',
                'vfork.io',
                'vfork.microarray',
                'vfork.sequence',
                'vfork.sql',
                'vfork.stat',
                'vfork.ucsc'],
      ext_modules=[Extension('vfork.io.colreader',
                             ['vfork/io/colreader.pyx',
                              'vfork/io/reader.c',
                              'vfork/io/strtok.c'])],
      install_requires=['pandas>=0.5'])
