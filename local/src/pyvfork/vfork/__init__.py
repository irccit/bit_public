''' The top level module of the vfork collection.

All code actually resides in sub-modules.
'''

__author__ = "Gabriele Sales <gbrsales@gmail.com>"
__copyright__ = "2005-2011 Gabriele Sales"
__version__ = "1.0"
