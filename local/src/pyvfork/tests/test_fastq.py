# Copyright 2012 Gabriele Sales <gbrsales@gmail.com>
#
# This file is part of BioinfoTree. It is licensed under the
# terms of the GNU Affero General Public License version 3.

from Bio.SeqIO.QualityIO import FastqGeneralIterator
from itertools import izip
from os import listdir
from os.path import join
from shutil import rmtree
from tempfile import mkdtemp
from vfork.fastq import FastqStreamingReader, FormatError
import tarfile
import unittest as ut


class TestSingleBlockReader(ut.TestSuite):
    def __init__(self):
        ut.TestSuite.__init__(self)

        for filename in self.listCorrectFiles():
            self.createTest(TestReadingOfCorrectFastq, filename)

        for filename in self.listInvalidFiles():
            self.createTest(TestReadingOfInvalidFastq, filename)

    def listCorrectFiles(self):
        return [ n for n in listdir(work_dir) if not n.startswith('error_') ]

    def listInvalidFiles(self):
        return [ n for n in listdir(work_dir) if n.startswith('error_') ]

    def createTest(self, testClass, filename):
        test = testClass()
        test.filename = filename
        self.addTest(test)

class TestReadingOfCorrectFastq(ut.TestCase):
    longMessage = True

    def runTest(self):
        msg = 'reading file ' + self.filename

        full_path = join(work_dir, self.filename)
        with file(full_path, 'r') as biopython_fd:
            biopython_iter = FastqGeneralIterator(biopython_fd)
            streaming_iter = FastqStreamingReader(full_path)

            for biopython_block, streaming_block in izip(biopython_iter, streaming_iter):
                self.assertEquals(biopython_block, streaming_block, msg=msg)

class TestReadingOfInvalidFastq(ut.TestCase):
    longMessage = True

    def runTest(self):
        msg = 'reading file ' + self.filename

        try:
            self.read()
        except Exception, e:
            self.assertIsInstance(e, FormatError, msg=msg)
        else:
            self.fail('no exception raised while ' + msg)

    def read(self):
        streaming_iter = FastqStreamingReader(join(work_dir, self.filename))
        return list(streaming_iter)


work_dir = None

def load_tests(loader, tests, pattern):
    global work_dir
    work_dir = mkdtemp()

    archive = tarfile.open('fastq.tar.gz', 'r')
    archive.extractall(work_dir)
    archive.close()

    return TestSingleBlockReader()

def tearDownModule():
    rmtree(work_dir, ignore_errors=True)


if __name__ == '__main__':
    ut.main()
