from __future__ import with_statement

from itertools import izip
from os import unlink
from vfork.fasta import MultipleBlockReader, MultipleBlockStreamingReader, SingleBlockReader
import unittest as ut

class SampleFasta(object):
	def __init__(self, fd):
		self.fd = fd
	
	@staticmethod
	def makeSection(label, length):
		lines = [ '>%s' % label ]
		
		sampleLine = 'acgt' * 10
		lineno = length / len(sampleLine)
		while lineno > 0:
			lines.append(sampleLine)
			lineno -= 1
		
		lastLine = sampleLine[:length % 80]
		lines.append(lastLine)
		if len(lastLine):
			lines.append('')
		return '\n'.join(lines)
	
	@staticmethod
	def makeSequence(length):
		block = 'acgt'
		repetitions = (length / len(block)) + 1
		return (block * repetitions)[:length]
	
	def writeSection(self, label, length):
		section = self.makeSection(label, length)
		self.fd.write(section)
		return section
	
	def writeSequence(self, length):
		sequence = self.makeSequence(length)
		self.fd.write(sequence)
		return sequence

class TestSingleBlockReader(ut.TestCase):
	FILE = 'test.fasta'
	
	def setUp(self):
		with file(self.FILE, 'w') as fd:
			print >>fd, '>header with spaces'
			print >>fd, 'A' * 80
			print >>fd, 'C' * 80
			print >>fd, 'G' * 80
			print >>fd, 'T' * 80
			print >>fd, 'AC' * 40
		
		self.reader = SingleBlockReader(self.FILE)
	
	def tearDown(self):
		self.reader.close()
		
		try:
			unlink(self.FILE)
		except OSError:
			pass
	
	def testSize(self):
		self.assertEquals(self.reader.size, 400)
		self.assertEquals(len(self.reader), 400)
	
	def testGet(self):
		self.assertEquals(self.reader.get(0, 80), 'A'*80)
		self.assertEquals(self.reader.get(40, 80), ('A'*40)+('C'*40))
		self.assertEquals(self.reader.get(80, 80), 'C'*80)
		self.assertEquals(self.reader.get(80, 120), ('C'*80)+('G'*40))
		self.assertEquals(self.reader.get(321, 40), 'CA'*20)
	
	def testSlice(self):
		self.assertEquals(self.reader[:80], 'A'*80)
		self.assertEquals(self.reader[40:120], ('A'*40)+('C'*40))
		self.assertEquals(self.reader[80:160], 'C'*80)
		self.assertEquals(self.reader[80:200], ('C'*80)+('G'*40))
		self.assertEquals(self.reader[321:361], 'CA'*20)

class TestMultipleBlockReader(ut.TestCase):
	FILE = 'test.fasta'
	IDX_FILE = 'test.fasta.idx'
	BLOCKS = (('label1_' + 'bla'*50, 89), ('label2', 20), ('label3', 182))
	
	def setUp(self):
		offset = 0
		with file(self.FILE, 'w') as fd:
			with file(self.IDX_FILE, 'w') as idx_fd:
				fasta = SampleFasta(fd)
				for label, length in self.BLOCKS:
					section_len = len(fasta.writeSection(label, length))
					print >>idx_fd, '%s\t%d\t%d\t%d' % (label, length, offset, section_len)
					offset += section_len
	
	def tearDown(self):
		for filename in (self.FILE, self.IDX_FILE):
			try:
				unlink(filename)
			except OSError:
				pass
	
	def testSectionTags(self):
		self._testSectionTags(None)
		self._testSectionTags(self.IDX_FILE)
	
	def _testSectionTags(self, index):
		reader = MultipleBlockReader(self.FILE, index=index)
		self.assertEquals([ s[0] for s in self.BLOCKS ], reader.blocks())
	
	def testMissingSection(self):
		self._testMissingSection(None)
		self._testMissingSection(self.IDX_FILE)
	
	def _testMissingSection(self, index):
		reader = MultipleBlockReader(self.FILE, index=index)
		self.assertRaises(KeyError, reader.__getitem__, 'missing')
	
	def testSectionSequences(self):
		self._testSectionSequences(None)
		self._testSectionSequences(self.IDX_FILE)
	
	def _testSectionSequences(self, index):
		reader = MultipleBlockReader(self.FILE, index=index)
		
		for section_info in self.BLOCKS:
			section = reader[section_info[0]]
			self.assertEquals(section_info[0], section.label)
			self.assertEquals(section_info[1], section.size)
			sample_seq = SampleFasta.makeSequence(section_info[1])
			self.assertEquals(sample_seq, section[:])
			
			for num in (10, 30, 50, 90, 120):
				self.assertEquals(min(section.size, num), len(section[:num]))

class TestMultipleBlockStreamingReader(ut.TestCase):
	FILE = 'test.fasta'
	
	def setUp(self):
		with file(self.FILE, 'w') as fd:
			fasta = SampleFasta(fd)
			self.sections = []
			for name, length in (('a', 200), ('b', 120), ('c', 80)):
				content = fasta.writeSection(name, length)
				content = content[content.index('\n')+1:]
				self.sections.append((name, content))
	
	def tearDown(self):
		try:
			unlink(self.FILE)
		except OSError:
			pass
	
	def testJoined(self):
		reader = MultipleBlockStreamingReader(self.FILE)
		for (expectedHeader, expectedContent), (foundHeader, foundContent) in izip(self.sections, reader):
			self.assertEquals(expectedHeader, foundHeader)
			self.assertEquals(expectedContent.replace('\n', ''), foundContent)
	
	def testNotJoined(self):
		reader = MultipleBlockStreamingReader(self.FILE, False)
		for (expectedHeader, expectedContent), (foundHeader, foundContent) in izip(self.sections, reader):
			self.assertEquals(expectedHeader, foundHeader)
			self.assertEquals(expectedContent.split('\n')[:-1], list(foundContent))
	
	def testUnconsumedLines(self):
		sections = len(self.sections) + 1
		for header, lines in MultipleBlockStreamingReader(self.FILE, False):
			sections -= 1
			self.assertTrue(sections >= 0, 'endless loop detected')

if __name__ == '__main__':
	ut.main()
