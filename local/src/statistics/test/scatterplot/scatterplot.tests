#!/usr/bin/env tool_test
#
# Copyright 2012 Davide Risso <risso.davide@gmail.com>

* Simple scatterplot with default pdf name
  In:  matrix.in
  Out: 
  Cmd: scatterplot -e 1,2 <$1

* Simple scatterplot with default pdf name (other columns)
  In:  matrix.in
  Out: 
  Cmd: scatterplot -e 4,2 <$1

* Scatterplot colored with the color passed as an option
  In:  matrix.in
  Out: 
  Cmd: scatterplot -e -c red 1,2 <$1

* scatterplot colored according to a file color 
  In:  matrix.in colors
  Out: 
  Cmd: scatterplot -e -f $2 1,2 <$1

* Xlim and ylim specification
  In:  matrix.in
  Out: 
  Cmd: scatterplot -e --xlim 0:2 --ylim 1:3 1,2 <$1

* Xlim and ylim specification (only one extreme of the interval)
  In:  matrix.in
  Out: 
  Cmd: scatterplot -e --xlim :2 --ylim 1: 1,2 <$1

* Two scatterplots in the same window and different colors
  In:  matrix.in
  Out: 
  Cmd: scatterplot -e -c red,blue 1,2 3,4 <$1

* Two scatterplots in the same window and different pch's
  In:  matrix.in
  Out: 
  Cmd: scatterplot -e -p 1,3 1,2 3,4 <$1

* Log-scale (x-axis)
  In:  exp.in
  Out: 
  Cmd: scatterplot -e -l x 1,2 <$1

* Log-scale (both axes)
  In:  exp.in
  Out: 
  Cmd: scatterplot -e -l xy 1,2 <$1

* Log-scale (both axes), two pairs of columns
  In:  exp.in
  Out: 
  Cmd: scatterplot -e -l xy -c 2,4 1,2 3,4 <$1

* Fancy scatterplot
  In:  matrix.in
  Out:  
  Cmd: scatterplot -e -o fancy-scatterplot.pdf -c red,blue -p 19,19 --cex 0.7 -t "My plot" --xlab x --ylab y 1,2 3,4 <$1

* Only one color, two pairs of columns
  In:  matrix.in
  Fail: yes
  Cmd: scatterplot -e -c 2 1,2 3,4 <$1

* Only one pair of columns, two colors
  In:  matrix.in
  Fail: yes
  Cmd: scatterplot -e -c 2,3 1,2 <$1

* Xlim with negative limit (it does not work due to optparse behavior)
  In:  matrix.in
  Fail: yes
  Cmd: scatterplot -e --xlim -1:1 1,2 <$1
