#!/usr/bin/env tool_test
#
# Copyright 2012 Davide Risso <risso.davide@gmail.com>

* Simple smoothed scatterplot with default pdf name
  In:  matrix.in
  Out:
  Cmd: smooth_scatter -e 1,2 <$1

* Simple smoothed scatterplot with default pdf name (other columns)
  In:  matrix.in
  Out:
  Cmd: smooth_scatter -e 4,2 <$1

* Xlim and ylim specification
  In:  matrix.in
  Out:
  Cmd: smooth_scatter -e --xlim 0:2 --ylim 1:3 1,2 <$1

* Xlim and ylim specification (only one extreme of the interval)
  In:  matrix.in
  Out:
  Cmd: smooth_scatter -e --xlim :2 --ylim 1: 1,2 <$1

* Log-scale (x-axis)
  In:  exp.in
  Out:
  Cmd: smooth_scatter -e -l x 1,2 <$1

* Log-scale (both axes)
  In:  exp.in
  Out:
  Cmd: smooth_scatter -e -l xy 1,2 <$1

* Xlim with negative limit (it does not work due to optparse behavior)
  In:  matrix.in
  Fail: yes
  Cmd: smooth_scatter -e --xlim -1:1 1,2 <$1
