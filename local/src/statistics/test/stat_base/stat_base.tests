#!/usr/bin/env tool_test

* compute the mean of a single column
  In:  value1col.in
  Out: mean.cfr
  Cmd: stat_base -a <$1 >$@

* compute the mean of a single column (explicit index)
  In:  value1col.in
  Out: mean.cfr
  Cmd: stat_base -a 1 <$1 >$@

* compute the mean and variance of a single column
  In:  value1col.in
  Out: mean-variance.cfr
  Cmd: stat_base -as <$1 >$@

* compute the mean of the second column
  In:  value_grp.in
  Out: mean-col2.cfr
  Cmd: stat_base -a 2 <$1 >$@

* compue the mean of multiple columns
  In:  value_grp.in
  Out: mean-2col.cfr
  Cmd: stat_base -a <$1 >$@

* compute the mean of a single column (high precision)
  In:  value1col.in
  Out: mean-high_precision.cfr
  Cmd: stat_base -p10 -a <$1 >$@

* reject negative column index
  In:   value_grp.in
  Fail: yes
  Cmd:  stat_base -a -- -1 <$1

* reject column index too large
  In:   value_grp.in
  Fail: yes
  Cmd:  stat_base -a 10 <$1

* compute the mean of multiple columns
  In:  value_grp.in
  Out: mean-multicol.cfr
  Cmd: stat_base -a <$1 >$@

* compute the mean of multiple columns (explicit indexes)
  In:  value_grp.in
  Out: mean-multicol.cfr
  Cmd: stat_base -a 2 1 <$1 \
       | bawk '{print $$2,$$1}' >$@

* compute the median of a single column
  In:  value1col.in
  Out: median.cfr
  Cmd: stat_base -m <$1 >$@

* compute the mean for groups
  In:  value_grp.in
  Out: mean-groups.cfr
  Cmd: stat_base -a -g <$1 >$@

* compute the mean for groups (explicit index)
  In:  value_grp.in
  Out: mean-groups.cfr
  Cmd: stat_base -a -g 2 <$1 >$@

* reject column index when using group
  In:   value_grp.in
  Fail: yes
  Cmd:  stat_base -a -g 1 <$1

* compute the stdev for groups
  In:  value_grp.in
  Out: stdev-groups.cfr
  Cmd: stat_base -s -g <$1 >$@ 

* compute the stdev for groups (use NA for missing values)
  In:  value_grp.in
  Out: stdev-groups-na.cfr
  Cmd: stat_base -s -g -N NA <$1 >$@

* reject empty input
  In:   /dev/null
  Fail: yes
  Cmd:  stat_base -a <$1

* accept empty input
  In:   /dev/null
  Out:  /dev/null
  Cmd:  stat_base -a -n <$1 >$@

* reject unsorted input
  In:   value_grp.in
  Fail: yes
  Cmd:  tac $1 \
        | stat_base -a -g

* accept unsorted input with -o
  In:  value_grp.in
  Out: mean-groups.cfr
  Cmd: tac $1 \
       | stat_base -a -g -o \
       | tac >$@

* reject NA values
  In:   value1col-na.in
  Fail: yes
  Cmd:  stat_base -a <$1

* accept NA values with -f
  In:  value1col-na.in
  Out: mean-na.cfr
  Cmd: stat_base -a -f <$1 >$@

* interaction between -f and -s
  In:  value1col-na.in
  Out: stdev-na.cfr
  Cmd: tail -n2 $1 \
       | stat_base -s -f -N NA >$@

# * calculate median
#   In:  value1col.in
#   Out: median.cfr
#   Cmd: stat_base -m <$1 >$@

# * calculate stdev
#   In:  value1col.in
#   Out: stdev.cfr
#   Cmd: stat_base -s <$1 >$@

# * calculate min
#   In:  value1col.in
#   Out: min.cfr
#   Cmd: stat_base -l <$1 >$@

# * calculate max
#   In:  value1col.in
#   Out: max.cfr
#   Cmd: stat_base -b <$1 >$@

# * calculate count
#   In:  value1col.in
#   Out: count.cfr
#   Cmd: stat_base -c <$1 >$@

# * calculate tot
#   In:  value1col.in
#   Out: tot.cfr
#   Cmd: stat_base -t <$1 >$@

# * calculate ignoring empty values
#   In:  value1colev.in
#   Out: medianeval.cfr
#   Cmd: stat_base -n <$1 >$@

# * calculate not ignore empty values
#   In:  value1colev.in
#   Fail: yes
#   Cmd: stat_base -m <$1 >$@

# * calculate stdev of one value
#   In:  value1val.in
#   Out: stdev1val.cfr
#   Cmd: stat_base -s <$1 >$@

# * use N option
#   In:  value1val.in
#   Out: stdev1valN.cfr
#   Cmd: stat_base -s -N NA <$1 >$@

# * calculate group median
#   In:  valueGrp.in
#   Out: medianGrp.cfr
#   Cmd: stat_base -g -m <$1 >$@

# * calculate group mean 
#   In:  valueGrp.in
#   Out: meanGrp.cfr
#   Cmd: stat_base -g -a <$1 >$@

# * validate control on args number
#   In:  value1col.in
#   Fail: yes
#   Cmd: stat_base -a 57 <$1 >$@
