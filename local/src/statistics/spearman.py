#!/usr/bin/env python
#
# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

from __future__ import division
from __future__ import with_statement
from itertools import groupby, izip
from math import sqrt
from operator import itemgetter
from optparse import OptionParser
from sys import stdin, stderr
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage

def iter_rows(fd, matrix_name):
	for line_idx, line in enumerate(fd):
		tokens = safe_rstrip(line).split('\t')
		label = tokens[0]

		values = []
		for t in tokens[1:]:
			try:
				values.append(float(t))
			except ValueError:
				exit('Invalid float value at line %d of matrix %s' % (line_idx+1,matrix_name))

		yield label, tuple(values)

def build_ranks(values):
	aux = [ (v,i) for i,v in enumerate(values) ]
	aux.sort()
	aux = [ (r,v,i) for r,(v,i) in enumerate(aux) ]

	res = [None] * len(aux)
	for v, es in groupby(aux, itemgetter(1)):
		es = list(es)
		rs = [ e[0] for e in es ]
		rank = sum(rs) / len(rs)
		for i in (e[2] for e in es):
			res[i] = rank
	
	return res

def main():
	parser = OptionParser(usage=format_usage('''
		%prog [OPTIONS] YS <XS >CORRELATIONS

		Loads two matrices (XS and YS) of values and computes the correlation
		between any two rows.

		The first column of both matrices contains the row label. All other
		entries are treated as floating point numbers.

		The output has the following format:

		  1. row label from XS
		  2. row label from YS
		  3. Spearman correlation
	'''))
	parser.add_option('-z', '--ignore-zero-denom', dest='ignore_zero_denom', action='store_true', help='do not exit on zero denominators')
	options, args = parser.parse_args()

	if len(args) != 1:
		exit('Unexpected argument number.')
	
	with file(args[0], 'r') as fd:
		ys = [ (n, build_ranks(vs)) for (n,vs) in iter_rows(fd, args[0]) ]
	
	for x_name, xs in iter_rows(stdin, '<stdin>'):
		x_ranks = build_ranks(xs)

		for y_name, y_ranks in ys:
			if len(x_ranks) != len(y_ranks):
				exit('Different number of values between %s and %s.' % (x_name, y_name))

			s_x  = sum(x for x in x_ranks)
			s_y  = sum(y for y in y_ranks)
			s_x2 = sum(x*x for x in x_ranks)
			s_y2 = sum(y*y for y in y_ranks)
			s_xy = sum(x*y for x,y in izip(x_ranks, y_ranks))
			n = len(x_ranks)

			num = (n * s_xy) - (s_x * s_y) 
			den = sqrt( (n * s_x2) - (s_x * s_x) ) * sqrt( (n * s_y2) - (s_y * s_y) )

			if den == 0:
				msg = 'Vanishing denominator for pair (%s, %s).' % (x_name, y_name)
				if options.ignore_zero_denom:
					print >>stderr, '[WARNING] ' + msg
				else:
					exit(msg)
			else:
				print '%s\t%s\t%g' % (x_name, y_name, num/den)

if __name__ == '__main__':
	main()

