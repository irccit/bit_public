#!/usr/bin/perl
use warnings;
use strict;
$,="\t";
$\="\n";



while(my $line=<>){
	chomp $line;
	my @row=split /\s+/,$line;

	my $sum=0;
	my $j;
	for ($j=0; $j<scalar @row; $j++) {
		$sum+=$row[$j];
	}
	
	for ($j=0; $j<scalar @row; $j++) {
		$row[$j]/=$sum;
	}
	print @row;
}
