#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\t";
$\="\n";

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 [-h|help] input_file\n
count the symbols (rows) of the input_file,

.META: stdout
	1	min_repetitions
	2	count

The first row of the output equals 
	sort input_file | uniq | wc -l
the following rows count the number of rows that appear more than min_repetitions times.
If you want to count the exact number of repetiotion of each rows use symbol_count and not count_pl
";

my $help=0;
my $quiet=0;
GetOptions (
	'h|help' => \$help,
	'q|quiet' => \$quiet,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

my %C;
my %dups;
while(<>){
	$dups{$C{$_}++}++
}

if($quiet){
	print $dups{0}
}else{
	foreach my $k (sort {$a <=> $b} keys(%dups)){
		my $v=$dups{$k};
		$k++;
		print $k,$v
	}
}
