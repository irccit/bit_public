#!/bin/bash

usage()
{
cat << EOF
usage: $0 file1 file2

This script is a wrapper for wilcoxon.R. Compunte the wilcoxon test on the file1 versus file2.
Input files must contain a single column of numbers.

OPTIONS:
   -h      Show this message
   -s      side: two-site[default], greater, less
   -p      paired test
   -b      brief output
EOF
}

SIDE="two.sided"
PAIRED="FALSE"
BRIEF="FALSE"

while getopts "hpbs:" OPTION
do
     case $OPTION in
         b)
	     BRIEF="TRUE"
	     ;;
     	 p)
	     PAIRED="TRUE"
	     ;;
     	 s)
	     SIDE=$OPTARG
	     ;;
         h)
             usage
             exit 1
             ;;
         ?)
             usage
             exit
             ;;
     esac
done
shift $(($OPTIND - 1))

if [ -z $2 ]
then
	echo "ERROR $0: Wrong argument number"
	usage
	exit 1
fi

Rscript $BIOINFO_ROOT/local/src/statistics/wilcoxon.R $SIDE $PAIRED $BRIEF $*
