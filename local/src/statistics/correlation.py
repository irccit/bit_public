#!/usr/bin/env python
#
# Copyright 2008-2010 Gabriele Sales <gbrsales@gmail.com>

from __future__ import with_statement
from cmath import isnan
from ctypes import cdll, c_double, c_int, c_void_p, create_string_buffer, cast
from itertools import groupby, izip
from multiprocessing import Pool
from optparse import OptionParser
from os.path import basename
from sys import argv, stdin, stderr
from sys import exit as _exit
from copy import deepcopy
from vfork.io.colreader import Reader
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage, safe_import

with safe_import('numpy (http://numpy.scipy.org)'):
    from numpy import ascontiguousarray, double, random
    from numpy.ctypeslib import ndpointer

with safe_import('scipy (http://scipy.org)'):
    from scipy.stats.stats import pearsonr, spearmanr


## Option management
##
def correlation_type(script_name, options):
    bn = basename(script_name)
    known_types = ('pearson', 'spearman', 'knnmi')
    if bn in known_types:
        if options.type is not None and options.type != bn:
            exit('You invoked the %s correlator, but then asked for the %s correlator.' % (bn, options.type))
        else:
            return bn
    elif options.type is None:
        return 'pearson'
    elif options.type in known_types:
        return options.type
    else:
        exit('Unknown correlator type: %s' % options.type)


## Data loading
##
def iter_rows(fd, matrix_name, skip_first, skip_equals):
    if skip_first:
        fd.readline()

    for line_idx, line in enumerate(fd):
        tokens = safe_rstrip(line).split('\t')
        label = tokens[0]

        values = []
        for t in tokens[1:]:
            try:
                values.append(float(t))
            except ValueError:
                exit('Invalid float value at line %d of matrix %s: %s' % (line_idx+1, matrix_name, t))

        if len(set(values))==1:
            if not skip_equals:
                raise Exception("All values are the same at line %d (%s) of matrix %s, consider -e option." % (line_idx+1, label, matrix_name))
            else:
                continue

        yield label, ascontiguousarray(values, dtype=double)

def list_pairs(filename):
    with file(filename, 'r') as fd:
        return set(ns for ns in Reader(fd, '0s,1s', False))

def load_seeds(filename):
    if filename is None:
        return None

    seeds = {}
    with file(filename) as fd:
        reader = Reader(fd, '0s,1u', False)
        for label, seed in reader:
            seeds[label] = seed
    return seeds


## Enumerators
##
def all_pairs(xss, yss):
    for x_name, xs in xss:
        for y_name, ys in yss:
            yield (x_name, xs, y_name, ys)

def listed_pairs(pairs):
    def filter(xss, yss):
        for x_name, xs in xss:
            for y_name, ys in yss:
                if (x_name, y_name) in pairs:
                    yield (x_name, xs, y_name, ys)
    return filter

def corresponding_rows(xss, yss):
    for (x_name, xs), (y_name, ys) in izip(xss, yss):
        yield (x_name, xs, y_name, ys)

def filter_nan(enumerator):
    def filter(xss, yss):
        for x_name, xs, y_name, ys in enumerator(xss, yss):
            fxs = []
            fys = []
            for x, y in izip(xs, ys):
                if not isnan(x) and not isnan(y):
                    fxs.append(x)
                    fys.append(y)
            yield x_name, fxs, y_name, fys

    return filter


## Correlators
##
class NumerosityError(Exception):
    pass

class Correlator(object):
    def __call__(self, x_name, xs, y_name, ys):
        self._check(x_name, xs, y_name, ys, self.min_number)
        res = self.compute(xs, ys)
        return x_name, y_name, res[0], res[1]

    def _check(self, x_name, xs, y_name, ys, min_number):
        if len(xs) != len(ys):
            exit('Different number of values between %s and %s.' % (x_name, y_name))
        elif len(xs) < min_number:
            raise NumerosityError, 'too few values to compute correlation between %s and %s' % (x_name, y_name)

class Pearson(Correlator):
    min_number = 5

    def compute(self, xs, ys):
        return pearsonr(xs, ys)

class Spearman(Correlator):
    min_number = 3

    def compute(self, xs, ys):
        return spearmanr(xs, ys)

class KnnMI(Correlator):
    min_number = 18

    def __init__(self, k, n):
        dll = cdll.LoadLibrary("libknnmi.so")
        sz = c_int.in_dll(dll, "mi_t_size")
        self.buf = create_string_buffer(sz.value)
        self.state = cast(self.buf, c_void_p)
        dll.make_mi(self.state, c_int(n), c_int(k))

        self.mutual_information = dll.mutual_information
        darray = ndpointer(dtype=double)
        self.mutual_information.argtypes = [c_void_p, darray, darray]
        self.mutual_information.restype = c_double

    def compute(self, xs, ys):
        return self.mutual_information(self.state, xs, ys), None

class ShuffleRows(Correlator):
    def __init__(self, base, num, seed_map):
        self.base = base
        self.num = num
        self.seed_map = seed_map

    def __call__(self, x_name, xs, y_name, ys):
        self._check(x_name, xs, y_name, ys, self.base.min_number)

        if self.seed_map:
            try:
                seed = seed_map[x_name]

                # fix problem with NumPy
                assert seed == int(seed)
                seed = int(seed)

                random.seed(seed)

            except KeyError:
                exit('Unknown label: %s' % x_name)

        accum = 0.0
        for i in xrange(self.num):
            random.shuffle(xs)
            accum += self.base.compute(xs, ys)[0]

        return x_name, y_name, accum/self.num, None


## Evaluators
##
def serial_evaluator(handle_error):
    def helper(pairs, correlator):
        c = correlator()
        for p in pairs:
            try:
                display_result(c(*p))
            except NumerosityError, e:
                handle_error(e)
    return helper

def parallel_evaluator(proc_num, handle_error):
    def helper(pairs, correlator):
        p = Pool(proc_num, parallel_setup, [correlator])
        for res in p.imap(parallel_compute, pairs, 1000):
            if isinstance(res, BaseException):
                handle_error(res)
            else:
                display_result(res)
    return helper

parallel_corr = None

def parallel_setup(mk_corr):
    global parallel_corr
    parallel_corr = mk_corr()

def parallel_compute(pair):
    try:
        return parallel_corr(*pair)
    except BaseException, e:
        return e


def print_warning(err):
    if isinstance(err, NumerosityError):
        print >>stderr, '[WARNING]', err
    else:
        exit(err)

def display_result(res):
        if res[3] is None:
            print '%s\t%s\t%g\t' % res[:3]
        else:
            print '%s\t%s\t%g\t%g' % res


def main():
    parser = OptionParser(usage=format_usage('''
	%prog [OPTIONS] YS <XS >CORRELATIONS

	Loads two matrices of values (XS an YS) and computes the correlation
	between corresponding rows.

	The first column of both matrices contains the row label. All other
	entries are treated as floating point numbers.

	You can use the --cross option to compute the correlation between all
	row pairs or --pairs to explicitly list those you are interested in.

	The correlation type is set by using the --type switch followed by
	one of:

	- pearson
	- spearman
	- knnmi

	The output has the following format:

	1. row label from XS
	2. row label from YS
	3. correlation
	4. p-value (missing with knnmi and with --shuffle)

	P-values are not corrected for multiple testing.
    '''))
    parser.add_option('-m', '--seed-map', dest='seed_map', help='map linking row labels to seeds used for the shuffling (requires --shuffle)', metavar='SEED_MAP')
    parser.add_option('-n', '--filter-nan', dest='filter_nan', action='store_true', default=False, help='discard NaN entries (use "nan" and not "NA" in input files)')
    parser.add_option('-1', '--skip-first-row', dest='skip_first', action='store_true', default=False, help='skip the first row of both matrices')
    parser.add_option('-c', '--cross', dest='cross', action='store_true', default=False, help='compute correlation between all row pairs')
    parser.add_option('-p', '--pairs', dest='pairs', default=None, help='compute correlation between row pairs listed in PAIR_FILE', metavar='PAIR_FILE')
    parser.add_option('-s', '--shuffle', dest='shuffle_num', type='int', help='shuffle XS values SHUFFLE_NUM times and report the average correlation', metavar='SHUFFLE_NUM')
    parser.add_option('-t', '--type', dest='type', help='set the correlation type (default: pearson)')
    parser.add_option('-j', '--jobs', dest='jobs', type='int', default=1, help='split the computation into N parallel jobs', metavar='N')
    parser.add_option('-w', '--numerosity-warning', dest='numerosity_warning', default=False, action='store_true', help='print a warning (but do not exit) when two rows have too few values to compute the correlation')
    parser.add_option('-o', '--one_file', dest='one_file', default=False, action='store_true', help='use only one file (stdin), same as using the same file both as firs argument and stdin')
    parser.add_option('-e', '--skip_equals', dest='skip_equals', action='store_true', default=False, help='skip the rows that have the same values in each column')

    options, args = parser.parse_args()

    if len(args) != 1 and not options.one_file or options.one_file and len(args) != 0:
        exit('Unexpected argument number.')
    elif options.cross and options.pairs:
        exit('Cannot use --cross and --pairs together.')
    elif options.shuffle_num is not None and options.shuffle_num <= 0:
        exit('Invalid shuffle number: %d' % options.shuffle_num)
    elif options.shuffle_num is None and options.seed_map is not None:
        exit('--seed-map requires --shuffle.')
    elif options.jobs < 0:
        exit('Invalid number of jobs: %d' % options.jobs)

    tp = correlation_type(argv[0], options)
    if tp == 'knnmi' and options.filter_nan:
        exit('KnnMI is not compatible with --filter-nan.')

    xss = iter_rows(stdin, '<stdin>', options.skip_first, options.skip_equals)
    if options.one_file:
        xss=list(xss)
        yss=list(xss)
        iter(xss)
    else:
        with file(args[0], 'r') as fd:
            yss = list(iter_rows(fd, args[0], options.skip_first, options.skip_equals))
            if len(yss) == 0: return

    if tp == 'pearson':
        correlator = Pearson
    elif tp == 'spearman':
        correlator = Spearman
    else:
        n = len(yss[0][1])
        correlator = lambda: KnnMI(5, n)


    if options.cross:
        enumerator = all_pairs
    elif options.pairs:
        enumerator = listed_pairs(list_pairs(options.pairs))
    else:
        enumerator = corresponding_rows

    if options.filter_nan:
        enumerator = filter_nan(enumerator)


    if options.shuffle_num:
        original = correlator
        correlator = lambda: ShuffleRows(original(), options.shuffle_num, load_seeds(options.seed_map))

    if options.numerosity_warning:
        error_handler = print_warning
    else:
        error_handler = exit

    if options.jobs == 1:
        evaluator = serial_evaluator(error_handler)
    else:
        evaluator = parallel_evaluator(None if options.jobs == 0 else options.jobs, error_handler)

    evaluator(enumerator(xss, yss), correlator)


if __name__ == '__main__':
    main()
