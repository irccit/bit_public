#!/usr/bin/env bRscript
#
# Copyright 2012 Davide Risso <risso.davide@gmail.com
# Copyright 2012 Paolo Martini <paolo.cavei@gmail.com>
# Copyright 2012 Gabriele Sales <gbrsales@gmail.com>
#
# This file is part of BioinfoTree. It is licensed under the
# terms of the GNU Affero General Public License version 3.

import(optparse)

opt_spec <- list(make_option(c("-e", "--no-header"), action="store_true", default=FALSE, help="the input doesn't contain any header"),
                 make_option(c("-r", "--row-names"), action="store_true", default=FALSE, help="take row names from the first input column"),
                 make_option(c("-o", "--output-file"), action="store", type="character", help="name of the PDF file where boxplot will write (default=barplot.pdf)"),
                 make_option(c("-c", "--color"), action="store", type="numeric", default=-1234, help="color-code the plot according to the specified column of VALUES"),
                 make_option(c("-C", "--cex"), action="store", type="numeric", default=1, help="a numerical value giving the amount by which axis labels should be magnified relative to the default"),
                 make_option(c("-f", "--color-file"), action="store", type="character", help="name of the file with one column specifying the colors."),
                 make_option(c("-x", "--xlabel"), action="store", type="character", help="a label for the x axis"),
                 make_option(c("-y", "--ylabel"), action="store", type="character", help="a label for the y axis"),
                 make_option(c("-t", "--title"), action="store", type="character", help="a title to be printed at the top of the plot"),
                 make_option(c("-s", "--side-by-side"), action="store_true", default=FALSE, help="Plot multiple series side by side."))

usage = paste(
  "%prog [OPTIONS] COLUMN.. <MATRIX",
  "",
  "Barplot of the values in columns COLUMN.. of file MATRIX.",
  "",
  "The values in columns COLUMN.. of the input file will be plotted in a single barplot.",
  "An additional column can be used for color coding the plot.",
  "Alternatively, the colors can be supplied using a separate file.",
  "In either case, the colors must be in one of the following formats:",
  "a number between 1 and 8 (see palette()), a string with the name of the color (see colors()), or the RGB code (i.e. \"#RRGGBB\")",
  "The plot is saved to \"barplot.pdf\" by default.",
  sep="\n")

parser <- OptionParser(usage=usage, option_list=opt_spec)
parsed <- parse_args(parser, positional_arguments=TRUE)
opts   <- parsed$options
args   <- parsed$args

if(length(args) == 0)
  error("Unexpected argument number.")

if (opts$color!=-1234 && !is.null(opts$'color-file'))
  error("You can use only one of --color and --color-file.")

columns <- sapply(args, function(a) parseNum(a, "integer", "COLUMN"))

ncolNames <- NULL
if (opts$'row-names')
  ncolNames <- 1

data <- bread(file="stdin", header=!opts$'no-header', row.names=ncolNames)

invalidColumns = columns > ncol(data) | columns < 1
if (any(invalidColumns))
  error(paste("Invalid COLUMN value:", columns[which(invalidColumns)[1]]))

outputFile <- "barplot.pdf"
if (!is.null(opts$'output-file'))
  outputFile <- opts$'output-file'

namesArg <- seq_len(nrow(data))
if (opts$'row-names')
  namesArg <- rownames(data)

if (opts$color==-1234) {
  color <- 1:length(columns)
} else {
  colorCol <- parseNum(opts$color, "integer", "COLOR")

  if (opts$color < 1 | opts$color>ncol(data)) {
    error(paste("Invalid COLOR value:", opts$color))
  } else {
    color <- sapply(data[,colorCol],parseColor)
  }
}

if (!is.null(opts$'color-file')) {
  colorFile <- bread(file=opts$'color-file', header=FALSE)
  color <- sapply(unlist(colorFile), parseColor)
}

data <- t(coerceNumericMatrix(data[,columns, drop=FALSE], "MATRIX"))

pdf(outputFile)
barplot(data, names.arg=namesArg, col=color,cex.names=opts$cex,xlab=opts$xlabel,ylab=opts$ylabel,main=opts$title, beside=opts$'side-by-side')
invisible(dev.off())

if (is.null(opts$'output-file'))
  system(paste(options("pdfviewer"), outputFile, sep=" "))
