#!/usr/bin/env bRscript
#
# Copyright 2012 Davide Risso <risso.davide@gmail.com>
# Copyright 2012 Gabriele Sales <gbrsales@gmail.com>

import(optparse)
import(lattice)

opt_spec <- list(make_option(c("-l", "--log"), action="store", type="character", default="", help="the axis to be plotted in log-scale: 'x', 'y' or 'xy'."),
                 make_option(c("-e", "--no-header"), action="store_true", default=FALSE, help="the input doesn't contain any header"),
                 make_option(c("-r", "--row-names"), action="store_true", default=FALSE, help="take row names from the first input column"),
                 make_option(c("-o", "--output-file"), action="store", type="character", help="name of the PDF file where boxplot will write (default=scatterplot.pdf)"),
                 make_option(c("-t", "--title"), action="store", type="character", help="A title to be printed at the top of the plot"),
                 make_option(c("-X","--xlim"), action="store", type="character", default=":", help="a string representing the limits for the x axis"),
                 make_option(c("-Y","--ylim"), action="store", type="character", default=":", help="a string representing the limits for the y axis"),
                 make_option(c("-x","--xlab"), action="store", type="character", help="a label for the x axis"),
                 make_option(c("-y","--ylab"), action="store", type="character", help="a label for the y axis"),
                 make_option(c("-n", "--remove-na"), action="store_true", default=FALSE, help="Automatically remove NAs from input."))

usage = paste(
  "%prog [OPTIONS] XCOL,YCOL <MATRIX",
  "",
  "Plot the smoothed scatterplot of the selected columns of the input MATRIX.",
  "",
  "The columns XCOL and YCOL of MATRIX will be plotted in a scatterplot.",
  "The plot is saved to \"smooth_scatter.pdf\" by default.",
  "--xlim and --ylim must be of the form: lower:upper, lower: or upper:.",
  sep="\n")

parser <- OptionParser(usage=usage, option_list=opt_spec)
parsed <- parse_args(parser, positional_arguments=TRUE)
opts   <- parsed$options
args   <- parsed$args

if(length(args)!=1) {
  error("Unexpected argument number.")
}

log <- list(x=list(log=FALSE), y=list(log=FALSE))

if (!is.null(opts$log)) {
  if (grepl("x", opts$log)) {
    log$x$log <- TRUE
  }
  if (grepl("y", opts$log)) {
    log$y$log <- TRUE
  }
}

ncolNames <- NULL
if (opts$'row-names')
  ncolNames <- 1

data <- bread(file="stdin", header=!opts$'no-header', row.names=ncolNames)
if (!opts$'remove-na' && any(is.na(data)))
  error("MATRIX contains NA values.")

columns <- unlist(lapply(strsplit(args,","),function(x) sapply(x,parseNum,"integer","COLUMN")))

outputFile <- "smooth_scatter.pdf"
if (!is.null(opts$'output-file'))
  outputFile <- opts$'output-file'

formula <- as.formula(paste(colnames(data)[columns[2]],"~",colnames(data)[columns[1]],sep=""))

pdf(outputFile)
xyplot(formula, data=data, scales=log, xlim=parseInterval(opts$xlim), ylim=parseInterval(opts$ylim), xlab=opts$xlab, ylab=opts$ylab, main=opts$title, cex.axis=0.8,
       panel = "panel.smoothScatter")
invisible(dev.off())

if (is.null(opts$'output-file'))
  system(paste(options("pdfviewer"), outputFile, sep=" "))
