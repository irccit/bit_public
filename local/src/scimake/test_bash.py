from bash import Bash, BashError
import unittest as ut

class TestBash(ut.TestCase):
	def tearDown(self):
		self.shell.close()
	
	def testStaticEval(self):
		self.shell = Bash()
		self.assertEqual(self.shell.eval('dummy'), 'dummy')
	
	def testEnv(self):
		self.shell = Bash()
		self.shell.export('name', 'dummy')
		self.assertEqual(self.shell.eval('$name'), 'dummy')
	
	def testUnquotedEnv(self):
		self.shell = Bash()
		self.shell.export('name', '"dum\\my"')
		self.assertEqual(self.shell.eval('$name'), '"dum\\my"')
	
	def testInvalidCommand(self):
		self.shell = Bash()
		self.assertRaises(BashError, self.shell.eval, '$(cat /dev/aaa)')

if __name__ == '__main__':
	ut.main()