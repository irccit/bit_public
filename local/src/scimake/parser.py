class Reader(object):
	def __init__(self, filename):
		self.fd = file(filename)
		self.lineno = 0
	
	def lines(self):
		while True:
			line = self.fd.readline()
			if len(line) == 0: break
			self.lineno += 1
			yield line.rstrip()

class Rule(object):
	ATTRIBUTES = ('define',
	              'for_each',
	              'source_prefix',
	              'target_prefix',
	              'sources',
	              'targets',
	              'target',
	              'cmd',
	              'custom_deps')
	
	BAD_COMBO = { 'target'  : ('targets',),
	              'targets' : ('target',) }
	
	def __init__(self, name):
		self.name = name
		self.attributes = {}
	
	def isEmpty(self):
		return len(self.attributes) == 0
	
	def addAttribute(self, key, value):
		if key not in self.ATTRIBUTES:
			raise ParseError, "invalid attribute '%s'" % key
		
		if key in ('define', 'for_each'):
			l = self.attributes.get(key, [])
			l.append(value)
			self.attributes[key] = l
			return
		
		if self.attributes.has_key(key):
			raise ParseError, "attribute '%s' set twice" % key
		elif self.BAD_COMBO.has_key(key):
			for attr in self.BAD_COMBO[key]:
				if self.attributes.has_key(attr):
					raise ParseError, "attribute '%s' is conflicting with '%s'" % (attr, key)
		
		self.attributes[key] = value
	
	def checkAttributes(self):
# 		if 'source' not in self.attributes and 'sources' not in self.attributes:
# 			raise ParseError, 'no source for rule closing'
		if 'target' not in self.attributes and 'targets' not in self.attributes:
			raise ParseError, 'no target for rule closing'
		if 'cmd' not in self.attributes:
			raise ParseError, 'not cmd for rule closing'

class IncludeVerbatim(object):
	def __init__(self, filename):
		try:
			f = file(filename)
		except IOError:
			raise ParseError, 'missing file included'
		
		self.filename = filename
		self.content = f.read()
		f.close()

class Parser(object):
	VERBATIM = 'include_verbatim'
	
	def __init__(self, filename):
		self.sections = []
		
		self.rule = None
		self.indent = None
		self.key = None
		self.value = []
		
		try:
			r = Reader(filename)
			for line in r.lines():
				if self.rule is None:
					self.parseHeader(line)
				else:
					self.parseBody(line)
			
			if self.rule is not None:
				self.closeRule()
			
		except ParseError, e:
			e.lineno = r.lineno
			raise e
		
		del self.rule
		del self.indent
		del self.key
		del self.value
	
	##
	## Internal use only
	##
	def parseHeader(self, line):
		idx = line.find('#')
		if idx != -1:
			line = line[:idx]
		
		line = line.rstrip()
		if len(line) == 0:
			return
		
		if line[0] in ' \t':
			raise ParseError, 'unexpected whitespace'
		
		if line.startswith(self.VERBATIM):
			self.parseVerbatim(line)
			return
		
		idx = line.find(':')
		if idx == -1:
			raise ParseError, 'invalid rule header'
		
		if len(line[idx+1:].strip()) > 0:
			raise ParseError, 'found garbage after rule header'
		
		self.rule = Rule(line[:idx].rstrip())
	
	def parseVerbatim(self, line):
		line = line[len(self.VERBATIM):]
		if line[0] not in ' \t':
			raise ParseError, 'invalid include'
				
		self.sections.append(IncludeVerbatim(line.strip()))
	
	def parseBody(self, line):
		# Skip commented lines
		for c in line:
			if c in ' \t': continue
			elif c == '#': return
			break
			
		# Check indentation
		if self.indent is None:
			if line[0] == '\t':
				if line[1] in (' ', '\t'):
					raise ParseError, 'invalid indentation level'
				self.indent = '\t'
				line = line[1:]
			
			elif line[0] == ' ':
				idx = 1
				while idx < len(line) and line[idx] == ' ':
					idx += 1
				self.indent = line[:idx]
				line = line[idx:]
			
			else:
				raise ParseError, 'invalid indentation level'
		
		else:
			if self.key is None:
				if len(line) == 0 or line[0] not in ' \t':
					self.closeRule()
					self.parseHeader(line)
					return
				
				elif line[:len(self.indent)] != self.indent or line[len(self.indent)] in (' ', '\t'):
					raise ParseError, 'invalid indentation level'
				
				line = line[len(self.indent):].rstrip()
			
			else:
				line = line.strip()
		
		if self.key is None:
			idx = line.find('=')
			if idx == -1:
				raise ParseError, 'invalid attribute'
			else:
				self.key = line[:idx].rstrip()
				line = line[idx+1:].lstrip()
		
		if line[-1] == '\\':
			self.value.append(line[:-1].rstrip())
			self.value.append(' ')
		
		else:
			self.value.append(line)
			value = ''.join(self.value)
			
			if self.key == 'for_each':
				self.parseForEach(value)
			elif self.key == 'define':
				self.parseDefine(value)
			else:
				self.rule.addAttribute(self.key, ''.join(self.value))

			self.key = None
			self.value = []
	
	def parseForEach(self, value):
		idx = value.find(' ')
		if idx == -1:
			value = value.find('\t')
		if idx == -1:
			raise ParseError, 'malformed for_each'
		
		var = value[:idx]
		value = value[idx+1:].lstrip()
		if value[:2] != 'in' or value[2] not in (' ', '\t'):
			raise ParseError, 'malformed for_each'
		
		self.rule.addAttribute(self.key, (var, value[3:].lstrip()))
	
	def parseDefine(self, value):
		idx = value.find(' ')
		if idx == -1:
			value = value.find('\t')
		if idx == -1:
			raise ParseError, 'malformed define'
		
		var = value[:idx]
		if var in ('sources', 'target', 'targets', 'cmd'):
			raise ParseError, 'invalid variable name in define'
		
		value = value[idx+1:].lstrip()
		if value[:2] != 'as' or value[2] not in (' ', '\t'):
			raise ParseError, 'malformed define'
		
		self.rule.addAttribute(self.key, (var, value[3:].lstrip()))
	
	def closeRule(self):
		if self.rule.isEmpty():
			raise ParseError, 'empty rule definition closing'
		elif self.key is not None:
			raise ParseError, 'unfinished attribute'
		else:
			self.rule.checkAttributes()

		self.sections.append(self.rule)
		self.rule = None
		self.indent = None

class ParseError(Exception):
	def __init__(self, msg):
		self.msg = msg
		self.lineno = -1
	
	def __str__(self):
		if self.lineno != -1:
			return self.msg + ' at line %d' % self.lineno
		else:
			return self.msg
