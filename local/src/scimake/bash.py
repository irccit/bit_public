from errno import EAGAIN
from os import close, environ, execlp, kill, read, waitpid, write
from pty import fork
from signal import SIGINT
from sys import exit, stdin
from termios import tcgetattr, tcsetattr, ECHO, TCSADRAIN

#DEBUG
from time import time

class Bash(object):
	PS1='PROMPT'
	
	def __init__(self):
		self.pid, self.fd = fork()
		if self.pid == 0:
			try:
				n = stdin.fileno()
				attrs = tcgetattr(n)
				attrs[3] &= ~ECHO
				tcsetattr(n, TCSADRAIN, attrs)
				
				environ['PS1'] = self.PS1
				execlp('bash', 'bash', '--norc')
			finally:
				exit(1)
		else:
			read(self.fd, 0xffff)
		
		self.elapsed = 0
		self.startTime = 0
	
	def close(self, doKill=True):
		close(self.fd)
		if doKill:
			kill(self.pid, SIGINT)
		waitpid(self.pid, 0)
	
	def chdir(self, path):
		write(self.fd, 'cd %s\n' % self.quote(path))
		res = self.readOutput()
		if len(res) != 0:
			raise BashError, "unexpected output: '%s'" % res
	
	def export(self, name, value):
		#DEBUG
		self.startTime = time()
	
		write(self.fd, '%s="%s"\n' % (name, self.quote(value)))
		res = self.readOutput()
		if len(res) != 0:
			raise BashError, "unexpected output: '%s'" % res
			
		#DEBUG
		self.elapsed += time() - self.startTime
	
	def eval(self, cmd):
		#DEBUG
		self.startTime = time()
	
		write(self.fd, '__res="%s"\n' % cmd)
		errmsg = self.readOutput()
		
		write(self.fd, 'echo $?\n')
		code = self.readOutput()
		try:
			code = int(code)
			if code != 0:
				e = BashError('command failure: %s' % errmsg)
				e.errno = code
				e.errmsg = errmsg
				raise e
		
		except ValueError:
			raise BashError, "unexpected exit code: '%s'" % code
		
		write(self.fd, 'echo $__res\n')
		res =  self.readOutput()
		
		#DEBUG
		self.elapsed += time() - self.startTime
		return res
	
	##
	## Internal use only
	##
	def quote(self, text):
		return text.replace('\\', '\\\\').replace('"', '\\"')
	
	def readOutput(self):
		output = []
		while True:
			line = read(self.fd, 0xffff)
			if len(line) == 0:
				self.close(False)
				raise BashError, 'bash died'
			
			if line.endswith(self.PS1):
				output.append(line[:-len(self.PS1)])
				break
			else:
				output.append(line)
		
		return ''.join(output).rstrip()

class BashError(Exception): pass
