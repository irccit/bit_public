================
SciMake tutorial
================

:Author: Gabriele Sales <sales@to.infn.it>
:Version: 1.0 (08/02/2005)

What is it?
-----------

SciMake is a software extending the ``make`` tool so to simplify automation and dependency tracking in scientific computing projects. It is *not* a ``make`` replacement; rather, it allows you to express rules with a much more compact notation.

Basics
------

When you start SciMake, the software looks for a ``scimake`` file into the current directory (you can specify another filename by using the '``-f``' command-line switch). That text file describes a number of *rules*, each one listing *source* files that you want to process into *targets* by means of an external *command*. A minimal ``scimake`` file is therefore::

	transformPs:
		sources=a.ps b.ps
		targets=a.pdf b.pdf
		cmd=ps2pdf $< $@

Running such rule is equivalent to the following::

	ps2pdf a.ps a.pdf
	ps2pdf b.ps b.pdf

As you can see, sources and targets are automatically paired using a many-to-many relation. If, on the contrary, we have many input files and just one target, we can write::

	mergePs:
		sources=a.ps b.ps
		target=book.ps
		cmd=psmerge -o $@ $^

The net result is::

	psmerge -o book.ps a.ps b.ps

Variable expansion
------------------

All the attributes of a rule, with the only exception of 'cmd', are interpreted by the ``bash`` shell before SciMake actually uses them. We can exploit this preprocessing stage to make our ``scimake`` more compact; consider the example already presented in the Basics_ section::

	transformPs:
		sources=a.ps b.ps
		targets=a.pdf b.pdf
		cmd=ps2pdf $< $@

Though perfectly legal, this rule is redundant: we can obtain a target name from its source by replacing the 'ps' suffix with 'pdf'. SciMake can do that automatically for us by means of the ``source`` variable [#]_::

	transformPs:
		sources=a.ps b.ps
		targets=${source%%ps}pdf
		cmd=ps2pdf $< $@

With the target template now in place, we can use a glob instead of the explicit source list::

	transformPs:
		sources=*.ps
		targets=${source%%ps}pdf
		cmd=ps2pdf $< $@

This final form has the big advantage of being extensible: if we add another ps file to our collection, we don't need to change the ``transformPs`` rule.

Paths
-----

It is often desirable to store sources and targets into different directories; SciMake helps in this task by performing automatic path substitution. Consider the following::

	transformPs:
		sources=sources/*.ps
		targets=targets/$(basename $source ps)pdf
		cmd=ps2pdf $< $@

This rule performs the same ps-to-pdf conversion presented in previous examples, but in this case source Postscripts are searched into the ``sources`` directory and resulting pdf files are written into ``targets``. Although the rule may seem simple, it is in fact mixing  static prefixes (directory names) with dynamic suffixes (``*.ls`` and the ``basename`` stuff); we can clearly separate the two by using the attributes ``source_prefix`` and ``target_prefix``::

	transformPs:
		source_prefix=sources/
		target_prefix=targets/
		sources=*.ps
		targets=${source%%ps}pdf
		cmd=ps2pdf $< $@

As you can see there's no longer any reference to directories neither in ``sources`` nor in ``targets``; both attributes are easier to read.  Still this rule is completely equivalent to the previous one.

Please note that the two prefix attributes are independent, so there's no need to use them both at all times.

Loops
-----

Sometimes you want to process the same input file multiple times at the change of a parameter; for example, you may want to extract a number of pages from a single ps file. Here is how SciMake can help::

	dumpPage:
		for_each=page in $(seq 10)
		sources=a.ps
		target=a-$page.ps
		cmd=psselect $$page $< $@

The ``for_each`` attribute declares the ``page`` variable; its values are taken from the exapansion of the ``$(seq 10)`` expression (generally speaking, from the expression following the ``in`` keyword). That same variable can be later referenced from other attributes; please note that ``cmd`` is directly interpreted by ``make``, so references are written prepending ``$$`` instead of a single dollar sign.

-----

**Notes**

.. [#] The ``${varname%%pattern}`` construct is a ``bash`` feature; please consult ``bash`` man page if you need more informations.