#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <unistd.h>
#include <string>
#include <vector>
#include "tnt.h"
#include "StringTokenizer.h"
#include "Histogram.h"

using namespace std;
using namespace TNT;

int setup(int argc, char* argv[]);
bool test_file(char * filename);
void usage(char** argv);

void dump(	Array2D< float > &mtx,
		Array2D< bool > &prensent
	 );
bool parse_next_row(
		float value[],
		bool present[],
		unsigned int array_size,
		unsigned int header_cols_num,
		ifstream* myfile
	);
void read_index(vector<string> &index, char* filename, int file_head_rows );
void fill_matrix(	
		Array2D< float > &mtx,
		Array2D< bool> &present_mtx,
		int file_cols,
		int file_head_rows,
		int file_head_cols,
		char* filename,
		int data_cols,
		bool center,
		bool normalize
	);
float correlate(
		float a[],
		float b[],
		float c[],
		bool present[],
		int k_max 
	);
void center(
		float a[],
		bool present[],
		int data_cols
	);
void normalize(
		float a[],
		bool present[],
		int data_cols
	);


float options_cutoff=-2.;
bool options_center=false;
bool options_normalize=false;
bool options_histogram=false;
int options_header_rows=-1;
int options_header_cols=-1;
int options_index_col=-1;
int options_file_num_cols=-1;
int options_file1_num_rows=-1;
int options_file2_num_rows=-1;
int options_file3_num_rows=-1;
int options_n_min=-1;
char * options_input_filename1;
char * options_input_filename2;
char * options_input_filename3;


int main( int argc, char* argv[] ){

	//controllo le opzioni passate da riga di comando
	if(setup(argc, argv)){
		cout << "main:: termined because setup error"<<endl;
		return(-1);
	}else{
		//atexit(stop);
	}

	vector <string> index1;
	vector <string> index2;
	vector <string> index3;

	read_index(index1, options_input_filename1, options_header_rows);
	read_index(index2, options_input_filename2, options_header_rows);
	read_index(index3, options_input_filename3, options_header_rows);

	//cerr << index1.size() << endl;
	//cerr << index2.size() << endl;
	//return 1;
	
	Array2D< float > mtx1(options_file1_num_rows-options_header_rows,options_file_num_cols-options_header_cols);
	Array2D< bool >  present1(options_file1_num_rows-options_header_rows,options_file_num_cols-options_header_cols);
	Array2D< float > mtx2(options_file2_num_rows-options_header_rows,options_file_num_cols-options_header_cols);
	Array2D< bool >  present2(options_file2_num_rows-options_header_rows,options_file_num_cols-options_header_cols);
	Array2D< float > mtx3(options_file3_num_rows-options_header_rows,options_file_num_cols-options_header_cols);
	Array2D< bool >  present3(options_file3_num_rows-options_header_rows,options_file_num_cols-options_header_cols);

	Histogram *Hist = NULL;
	if(options_histogram){
		Hist = new Histogram(-1., 1., 256*256, options_file1_num_rows * options_file2_num_rows * options_file3_num_rows);
	}
	
	fill_matrix(	mtx1,
			present1, 
			options_file_num_cols, 
			options_header_rows, 
			options_header_cols, 
			options_input_filename1,
			options_file_num_cols-options_header_cols,
			options_center,
			options_normalize
		);
	fill_matrix(	mtx2,
			present2, 
			options_file_num_cols, 
			options_header_rows, 
			options_header_cols, 
			options_input_filename2,
			options_file_num_cols-options_header_cols,
			options_center,
			options_normalize
		);
	fill_matrix(	mtx3,
			present3, 
			options_file_num_cols, 
			options_header_rows, 
			options_header_cols, 
			options_input_filename3,
			options_file_num_cols-options_header_cols,
			options_center,
			options_normalize
		);

	int i_max=mtx1.dim1();
	int j_max=mtx2.dim1();
	int t_max=mtx3.dim1();
	int k_max=mtx2.dim2();
	for(int i=0; i<i_max; i++){
		
		int j_min=0;
		if(strcmp(options_input_filename1,options_input_filename2)==0){
			j_min = i+1;
		}
		
		for(int j=j_min; j<j_max; j++){
			
			int t_min=0;
			if(strcmp(options_input_filename2,options_input_filename3)==0){
				t_min = j+1;
			}
			if(strcmp(options_input_filename1,options_input_filename3)==0){
				if(i >= t_min){
					t_min = i+1;
				}
			}

			for(int t=t_min; t<t_max; t++){
				int n=0;
					
				bool * present  = new bool[k_max];
				float * a  = new float[k_max];
				float * b  = new float[k_max];
				float * c  = new float[k_max];
				for(int k=0; k<k_max; k++){
					if(present1[i][k] and present2[j][k] and present3[t][k]){
						present[k]=true;
						n++;
					}else{
						present[k]=false;
					}
					a[k]=mtx1[i][k];
					b[k]=mtx2[j][k];
					c[k]=mtx3[t][k];
				}
				
				if(!options_histogram){
					if(options_cutoff < -1.){
						cout << index1[i] << "\t" << index2[j] << "\t" << index3[t] << "\t";
						if(n>=options_n_min){
							cout << correlate(a,b,c,present,k_max) << "\t" << n << endl;
						}else{
							cout << "missing" << "\t" << n << endl;
						}
					}else{
						float r = correlate(a,b,c,present,k_max);
						if(r>options_cutoff){
							cout << index1[i] << "\t" << index2[j] << "\t" << index3[t] << "\t";
							if(n>=options_n_min){
								cout << r << "\t" << n << endl;
							}else{
								cout << "missing" << "\t" << n << endl;
							}
						}
					}
				}else{
					if(n>=options_n_min){
						float r = correlate(a,b,c,present,k_max);
						Hist->add(r);
					}
				}
				delete a,b,c;
			}
		}
	}
	if(options_histogram){
		Hist->print();
	}
}

float correlate(float a[], float b[], float c[], bool present[], int k_max ){
	if(options_center and options_normalize){
		center(a,present, k_max);
		center(b,present, k_max);
		center(c,present, k_max);
		normalize(a,present, k_max);
		normalize(b,present, k_max);
		normalize(c,present, k_max);
	}else{
		cerr << "ERROR:cloe_parallel::correlate: this tipe of correlation require booth center -r and normalize -s"<<endl;
		exit(-1);
	}
	
	float s_xyz=0.;
	int n=0;
	for(int k=0; k<k_max; k++){
		if(present[k]){
			s_xyz += a[k] * b[k] * c[k];
			n++;
		}
	}
	
	return s_xyz/(n - 1); 
}

void center(float a[], bool present[], int data_cols){
	float s=0;
	int n=0;
	for(int j=0; j<data_cols; j++){
		if (present[j]){
			s+=a[j];
			n++;
		}
	}
	float m=s/n;
	for(int j=0; j<data_cols; j++){
		a[j] -= m;
	}
}

void normalize(float a[], bool present[], int data_cols){
	float s=0;
	int n=0;
	for(int j=0; j<data_cols; j++){
		if(present[j]){
			s += a[j];
			n++;
		}
	}
	float m=s/n;
	
	float stdev = 0;
	for(int j=0; j<data_cols; j++){
		if(present[j]){
			stdev += (a[j] - m)*(a[j] - m);
			//cerr << (mtx[i][j] - m)*(mtx[i][j] - m) << "\t";
		}
	}
	stdev /= (n-1);
	stdev = sqrt(stdev);

	//cerr << ":\t" << m << "\t" << stdev << endl;
	
	for(int j=0; j<data_cols; j++){
		if(present[j]){
			a[j]/=stdev;
		}
	}
}

void read_index(vector<string> &index, char* filename, int file_head_rows){
	ifstream infile (filename);
	string line;
	for(int i=0;i<file_head_rows;i++){//perdo le righe di intestazione
		getline(infile,line,'\n');
	}
	while(getline(infile,line,'\n')){
	   	StringTokenizer strtok = StringTokenizer(line,"\t",false);
		index.push_back(strtok.nextToken());
	}
	infile.close();
}

void fill_matrix(	
		Array2D< float > &mtx,
		Array2D< bool> &present_mtx,
		int file_cols,
		int file_head_rows,
		int file_head_cols,
		char* filename,
		int data_cols,
		bool center,
		bool normalize
	){

	ifstream myfile (filename);
	for(int i=0;i<file_head_rows;i++){//perdo le righe di intestazione
		string line;
		getline(myfile,line,'\n');
	}

	
	float * value   = new float[data_cols];	//i dati di una singola riga
	bool * present  = new bool[data_cols];	//il dato di una colonna puo` non essere presente per una data riga
	
	int i=0;
	while(	parse_next_row(
				value, 
				present, 
				data_cols, 
				options_header_cols, 
				&myfile
			)
		){

		//for(int j=0; j<data_cols; j++){cerr<<present[j]<<"\t";}; cerr<<endl;
		//for(int j=0; j<data_cols; j++){cerr<<value[j]<<"\t";}; cerr<<endl;


		int n=0;
		float s=0;
		for(int j=0; j<data_cols; j++){
			mtx[i][j]=value[j];
			present_mtx[i][j]=present[j];
			if(present[j]){
				n++;
				s += value[j];
			}

		}
		
		i++;
	}
	
	myfile.close();
	delete [] value;
	delete [] present;
}



int setup(int argc, char* argv[]){
	
	////////////////////////////////////////////////////////////////
	// gestione delle opzioni
	//
	int i;
	while((i=getopt(argc, argv, "C:Hrsi:j:t:c:h:p:n:"))!=-1){
		switch(i){
			case 'C':
				options_cutoff=atof(optarg);
				if(options_cutoff > 1. or options_cutoff < -1.){
					cerr << "cloe_parallel::setup: options_cutoff > 1. or options_cutoff < -1." << endl;
					return -1;
				}
				break;
			case 'H':
				options_histogram=true;
				break;
			case 'r':
				options_center=true;
				break;
			case 's':
				options_normalize=true;
				break;
			case 'i': 
				options_file1_num_rows=atoi(optarg);
				break;
			case 'j': 
				options_file2_num_rows=atoi(optarg);
				break;
			case 't': 
				options_file3_num_rows=atoi(optarg);
				break;
			case 'h': 
				options_header_rows=atoi(optarg);
				break;
			case 'p':
				options_header_cols=atoi(optarg);
				break;
			case 'c':
				options_file_num_cols=atoi(optarg);
				break;
			case 'n':
				options_n_min=atoi(optarg);
				break;
			default:
				cerr<<"wrong options given"<<endl;
				usage(argv);
				return -1;
		}	
	}
	if(
		options_file1_num_rows<0
		or
		options_file2_num_rows<0
		or
		options_header_rows<0 
		or 
		options_header_cols!=1
		or 
		options_file_num_cols<0
	){
		cerr<<"wrong options value. N.B. -p option must be == 1"<<endl;
		usage(argv);
		return -1;
	}
	//if(!options_normalize or !options_center){
	//	cerr << "La funzione di correlazione implementata funziona solo su dati centrati e ridotti, utilizzare le opzioni -r e -s"<< endl << endl << endl;
	//	usage(argv);
	//	return -1;
	//}
	if(argc == optind + 3){
		//vi sono parametri dopo l'elenco delle opzioni
		options_input_filename1=argv[optind];
		optind++;
		options_input_filename2=argv[optind];
		optind++;
		options_input_filename3=argv[optind];
		optind++;
	}else{
		usage(argv);
		return -1;
	}
	if(!test_file(options_input_filename1) or !test_file(options_input_filename2) or !test_file(options_input_filename3)){
		return -1;
	}
	return(0);
}

bool test_file(char * filename){
	ifstream test (filename,ios::in);
	if (!test.is_open()){
		cerr<<"ERROR: cloe_parallel::setup: cant open file '" << filename <<"'" << endl;
		return false;
	}
	test.close();
	return true;
}

void usage(char** argv){
	cout << "usage: " << argv[0] <<  " -r 1430 -h 1 -p 1 -c 100 -n 3 filename1 filename2 filename3"<<endl
		<<"\t-r number of rows in file1"<<endl
		<<"\t\tes: -r `cat file1 | wc -l`"<<endl
		<<"\t-s number of rows in file2"<<endl
		<<"\t\tes: -s `cat file2 | wc -l`"<<endl
		<<"\t-h number of header rows taht will be ignored"<<endl
		<<"\t-p number of header columns (used for indexing)"<<endl
		<<"\t-c total number of columns in filename"<<endl
		<<"\t\t-n minimum common timepoit for calculate correlation"<<endl
		<<"\t\t(head -1 tab_collapsed_homo | tr \"\\t\" \"\\n\" | wc -l) "<<endl
		<<"\t\tdata in filename* must be tab delimited"<<endl
		<<"\t\teach field not in header rows ol colls must be formatted like"<<endl
		<<"\t\t[+|-][nnnnn][.nnnnn][e|E[+|-]nnnn]"<<endl
		<<"\t\tempty string are accepted"<<endl
		<<"\t\tand considered missing values"<<endl;
	exit(-1);
}


bool parse_next_row(
		float value[],
		bool present[],
		unsigned int array_size,
		unsigned int header_cols_num,
		ifstream* infile
	){
	
	unsigned int row_index=0;

	string line;
	if(getline(*infile,line,'\n')){
	   	StringTokenizer strtok = StringTokenizer(line,"\t",false);
	
		unsigned int ct=strtok.countTokens();
		if(ct - header_cols_num>array_size){
			cerr<<"WARNING: cloe_parallel::parse_next_row: array size < file column number"<<endl;
		};
		
		for(unsigned int j=0; j<header_cols_num; j++){//perdo le colonne di intestazione
			string tok=strtok.nextToken();
		}
		
		for(unsigned int j=0; j<array_size; j++){
			//cerr << "j: " << j << "\tj_min: " << j_min << endl; 
			string tok=strtok.nextToken();
			if(
//				(tok=="NaN")
//				or
//				(tok=="nan")
//				or
//				(tok=="X")
//				or
//				(tok=="x")
//				or
				(tok=="")
			){
//				cout<<"token absen: "<<tok<<endl;
				value[j]=0.;
				present[j]=false;
			}else{
				//cout<<"token present: "<<token<<endl;
				value[j]=atof(tok.c_str());
				present[j]=true;
			}
		}
		return true;
	}else{
		return false;
	}
}

