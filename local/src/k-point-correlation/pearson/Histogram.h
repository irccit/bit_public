#ifndef INCLUDE_HISTOGRAM_H
#define INCLUDE_HISTOGRAM_H


#include <stdio.h>
#include <stdlib.h>
#include <iostream>


using namespace std;

class Histogram
{
public:

	Histogram(float min, float max, int bin_n, int expected_number=0);
	~Histogram(){};
	void add(float f);
	void print();

   private:
	float min;
	float max;
	int bin_n;
	int *count;
	float tot_span;
	float span;
};

#endif
