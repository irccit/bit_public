# Copyright 2009-2010,2012,2014 Gabriele Sales <gbrsales@gmail.com>

LOCAL_SHELL    := $(BIOINFO_ROOT)/local/bin/bash-make
#PARALLEL_SHELL := $(BIOINFO_ROOT)/binary/$(BIOINFO_HOST)/local/bin/qbash
PARALLEL_SHELL := $(BIOINFO_ROOT)/local/bin/qbash


## Constants
##
define __bmake_newline


endef


## Extern statement
##
define __bmake_extern
$(eval $(call __bmake_extern_body,$1,$2))
endef

define __bmake_extern_body
ifneq ($2,)

ifndef $2
$1: ; bmake --external $$@
$2 := $1
endif

else
$1: ; bmake --external $$@
endif
endef


## Meta generation
##
define __bmake_meta_body
.PHONY: echo.__bmake_meta_$1
echo.__bmake_meta_$1:
	@echo -e '$(subst $(__bmake_newline),\n,$2)'
endef

define __bmake_meta_emit
$(eval $(call __bmake_meta_body,$1,$2))
endef


# The shell used for bootstrap
SHELL          := $(LOCAL_SHELL)

# Moved here to avoid unexpected pattern capturing
# See commit 5c8a16c6227ee2032f6598fca8a068941d6f1a38
%.__bmake_clean:
	rm -rf '$*'


## Public Functions
##

# drop1 drops the first element of a list.
#
# Arguments:
# $1: the list.
define drop1
$(wordlist 2,$(words $(1)),$(1))
endef

define __enumerate
$(if $(firstword $(1)),
$(call $(2),$(words $(1)),$(firstword $(1)))\
$(call __enumerate,$(call drop1,$(1)),$(2)),)
endef

# enumerate takes a list l and a function f.
# It calls f once for every element of l, passing
# the index of the element and the element itself.
#
# Arguments:
# $1: the list l
# $2: the function f
define enumerate
$(call __enumerate,$(call reverse,$(1)),$(2))
endef

# zip takes two lists (l1 and l2) and a function f.
# It calls f on each pair of elements, one taken from l1
# and the other from l2.
#
# Arguments:
# $1: the list l1
# $2: the list l2
# $3: the function f
define zip
$(if $(firstword $(1)),\
$(call $(3),$(firstword $(1)),$(firstword $(2)))\
$(call zip,$(wordlist 2,$(words $(1)),$(1)),$(wordlist 2,$(words $(2)),$(2)),$(3)),\
)
endef

# printMultilineVar emits a command to print
# the value of a multi-line variable (usually, defined
# within a "define" block) on stdout.
#
# Arguments:
# $1: the variable
define printMultilineVar
echo -e "$(subst $(__bmake_newline),\n,$(1))"
endef
