#ifndef QUEUE_H_
#define QUEUE_H_

#include <pthread.h>

class PthreadError {};

namespace
{
	template <typename T>
	struct Node
	{
		T item;
		Node<T>* next;
	};
	
	class PthreadCond;
	
	class PthreadMutex
	{
	public:
		PthreadMutex()
		{
			if (pthread_mutex_init(&mutex_, NULL) != 0)
				throw PthreadError();
		}
		~PthreadMutex() { pthread_mutex_destroy(&mutex_); }
		
		void lock()
		{
			if (pthread_mutex_lock(&mutex_) != 0)
				throw PthreadError();
		}
		void unlock()
		{
			if (	pthread_mutex_unlock(&mutex_) != 0)
				throw PthreadError();
		}
	
	protected:
		friend class PthreadCond;
		pthread_mutex_t mutex_;
	
	private:
		PthreadMutex(const PthreadMutex& obj);
		PthreadMutex& operator=(const PthreadMutex& rhs);
	};
	
	class PthreadCond
	{
	public:
		PthreadCond()
		{
			if (pthread_cond_init(&cond_, NULL) != 0)
				throw PthreadError();
		}
		~PthreadCond() { pthread_cond_destroy(&cond_); }

		void wait(PthreadMutex& mutex)
		{
			if (pthread_cond_wait(&cond_, &mutex.mutex_) != 0)
				throw PthreadError();
		}
		void signal()
		{
			if (pthread_cond_signal(&cond_) != 0)
				throw PthreadError();
		}
		void broadcast()
		{
			if (pthread_cond_broadcast(&cond_) != 0)
				throw PthreadError();
		}
		
	protected:
		pthread_cond_t cond_;
	
	private:
		PthreadCond(const PthreadCond& obj);
		PthreadCond& operator=(const PthreadCond& rhs);
	};
}

template <typename T>
class Queue
{
public:
	typedef T value_type;
	
	Queue<T>(const unsigned int size) : nodes_(new Node<T>[size]), free_(nodes_), bottom_(NULL), top_(NULL)
	{
		unsigned int i;
		for (i = 0; i < size-1; ++i)
			nodes_[i].next = &nodes_[i+1];
		nodes_[i].next = NULL;
	}
	~Queue<T>() { delete[] nodes_; }
	
	void put(T item)
	{
		mutex_.lock();
	
		Node<T>* node = get_free_node();
		node->item = item;
		set_used_node(node);
		
		mutex_.unlock();
	}
	
	bool put_nowait(T item)
	{
		mutex_.lock();
		
		Node<T>* node = get_free_node(false);
		if (node == NULL)
		{
			mutex_.unlock();
			return false;
		}
		
		node->item = item;
		set_used_node(node);
		
		mutex_.unlock();
		return true;
	}
	
	T get()
	{
		mutex_.lock();
	
		Node<T>* node = get_used_node();
		T item = node->item;
		set_free_node(node);
		
		mutex_.unlock();
		return item;		
	}
	
private:
	Queue<T>(const Queue& obj);
	Queue<T>& operator=(const Queue<T>& rhs);
	
	Node<T>* get_free_node(bool wait=true)
	{
		if (!wait && free_ == NULL)
			return NULL;
		
		while (free_ == NULL)
			cond_.wait(mutex_);
		
		Node<T>* node = free_;
		free_ = node->next;
		node->next = NULL;
		
		return node;
	}
	
	void set_free_node(Node<T>* node)
	{
		bool exhausted = free_ == NULL;
		
		if (exhausted)
			node->next = NULL;
		else
			node->next = free_;
		free_ = node;
		
		if (exhausted)
			cond_.broadcast();
	}

	Node<T>* get_used_node()
	{
		while (bottom_ == NULL)
			cond_.wait(mutex_);
		
		Node<T>* node = bottom_;
		bottom_ = bottom_->next;
	
		if (bottom_ == NULL)
			top_ = NULL;
		
		return node;
	}
	
	void set_used_node(Node<T>* node)
	{
		if (top_ != NULL)
			top_->next = node;
		top_ = node;
		
		if (bottom_ == NULL)
		{
			bottom_ = node;
			cond_.broadcast();
		}
		
		//DEBUG
	/*	int count = 10;
		Node<T>* ptr = free_;
		while (ptr != NULL)
		{
			--count;
			ptr = ptr->next;
		}
		
		if (count > 1)
			cerr << "Queue usage level: " << count << endl;
	*/
	}
	
	Node<T>* nodes_;
	Node<T>* free_;
	Node<T>* bottom_;
	Node<T>* top_;
	PthreadMutex mutex_;
	PthreadCond cond_;
};

#endif /*QUEUE_H_*/
