#ifndef SKIPLIST_H
#define SKIPLIST_H

#include <cmath>
#include <stdlib.h>

namespace skiplist
{
	template <typename V>
	class SkipList
	{
	public:
		class Node
		{
		public:
			Node(long level);
			~Node();
			
			long key;
			V value;
			long level;
			Node** forward;
		};
	
		class ConstCursor
		{
		public:
			ConstCursor(const ConstCursor& obj);
			ConstCursor& operator=(const ConstCursor& rhs);
			
			bool next();
			long key() const;
			const V* value() const;
		
		protected:
			friend class SkipList<V>;
			
			ConstCursor(const Node& head);
			
		private:
			const Node* n;
		};
	
		SkipList<V>(const unsigned long maxSize, const double probLevel=0.5);
		~SkipList<V>();
		
		unsigned long size() const;
		V* get(const long key);
		bool set(const long key, const V& value, V* oldValue=NULL);
		bool del(const long key, V* value=NULL);
		ConstCursor cursor() const;
		
	private:
		SkipList<V>(const SkipList<V>& obj);
		SkipList<V>& operator=(const SkipList<V>& rhs);
		
		long computeMaxLevel() const;
		long randomLevel() const;
	
		const unsigned long maxSize_;
		const double probLevel_;
		unsigned long size_;
		long maxLevel_;
		
		Node head_;
	};
	
	class OutOfRange : public std::exception
	{
	public:
		const char* what() const throw() { return "key value out of range"; }
	};
	
	template <typename V>
	SkipList<V>::SkipList(const unsigned long maxSize, const double probLevel) :
		maxSize_(maxSize), probLevel_(probLevel), size_(0),
		maxLevel_(computeMaxLevel()), head_(maxLevel_)
	{
		head_.key = std::numeric_limits<long>::min();
		for (long level = 0; level < head_.level; ++level)
			head_.forward[level] = NULL;
	}
	
	template <typename V>
	SkipList<V>::~SkipList()
	{
		Node* n = head_.forward[0];
		while (n)
		{
			Node* next = n->forward[0];
			delete n;
			n = next;
		}
	}
	
	template <typename V>
	unsigned long SkipList<V>::size() const
	{
		return size_;
	}
	
	template <typename V>
	V* SkipList<V>::get(const long key)
	{
		if (key == head_.key)
			return NULL;
	
		Node* n = &head_;
		for (long level = maxLevel_-1; level >= 0; --level)
		{
			while (n->forward[level] && n->forward[level]->key < key)
				n = n->forward[level];
		}
		
		n = n->forward[0];
		if (n && n->key == key)
			return &n->value;
		else
			return NULL;
	}
	
	template <typename V>
	bool SkipList<V>::set(const long key, const V& value, V* oldValue)
	{	
		if (key == head_.key)
			throw OutOfRange();
			
		bool updated = false;
		long level;	
		Node* update[maxLevel_];
		Node* n = &head_;
		
		for (level = maxLevel_-1; level >= 0; --level)
		{
			while (n->forward[level] && n->forward[level]->key < key)
				n = n->forward[level];
			
			update[level] = n;
		}
		
		n = n->forward[0];
		if (n && n->key == key)
		{
			if (oldValue != NULL)
			{
				*oldValue = n->value;
				updated = true;
			}
			
			n->value = value;
		}
		else
		{
			n = new Node(randomLevel());
			
			n->key = key;
			n->value = value;
			
			for (level = 0; level < n->level; ++level)
			{
				n->forward[level] = update[level]->forward[level];
				update[level]->forward[level] = n;
			}
			
			++size_;
		}
		
		return updated;
	}
	
	template <typename V>
	bool SkipList<V>::del(const long key, V* value)
	{
		if (key == head_.key)
			throw OutOfRange();
			
		long level;
		Node* update[maxLevel_];
		Node* n = &head_;
		
		for (level = maxLevel_-1; level >= 0; --level)
		{
			while (n->forward[level] && n->forward[level]->key < key)
				n = n->forward[level];
			
			update[level] = n;
		}

		n = n->forward[0];
		if (n && n->key == key)
		{
			for (level = 0; level < maxLevel_; ++level)
			{
				if (update[level]->forward[level] != n)
					break;
				else
					update[level]->forward[level] = n->forward[level];
			}
			
			if (value)
				*value = n->value;
			delete n;
			--size_;
			return true;
		}
		else
			return false;
	}
	
	template <typename V>
	typename SkipList<V>::ConstCursor SkipList<V>::cursor() const
	{
		return ConstCursor(head_);
	}
	
	template <typename V>
	long SkipList<V>::computeMaxLevel() const
	{
		return static_cast<long>(ceil(log(maxSize_) / -log(probLevel_)));
	}
	
	template <typename V>
	long SkipList<V>::randomLevel() const
	{
		const double maxRand = ((unsigned long)1<<31)-1;
		long level = 1;
		
		while (random() / maxRand < probLevel_ && level < maxLevel_)
			++level;
		
		return level;
	}
	
	template <typename V>
	SkipList<V>::ConstCursor::ConstCursor(const Node& head) : n(&head)
	{
	}
	
	template <typename V>
	SkipList<V>::ConstCursor::ConstCursor(const ConstCursor& obj) : n(obj.n)
	{
	}
	
	template <typename V>
	typename SkipList<V>::ConstCursor& SkipList<V>::ConstCursor::operator=(const SkipList<V>::ConstCursor& rhs)
	{
		// no risk in self assignment, here
		n = rhs.n; 
		return *this;
	}

	template <typename V>
	bool SkipList<V>::ConstCursor::next()
	{
		if (n)
		{
			n = n->forward[0];
			return n != NULL;
		}
		else
			return false;
	}
	
	template <typename V>
	long SkipList<V>::ConstCursor::key() const
	{
		return n->key;
	}
	
	template <typename V>
	const V* SkipList<V>::ConstCursor::value() const
	{
		return &n->value;
	}
	
	template <typename V>
	SkipList<V>::Node::Node(long level) : level(level), forward(new Node*[level])
	{
	}
	
	template <typename V>
	SkipList<V>::Node::~Node()
	{
		delete[] forward;
	}
}

#endif //SKIPLIST_H
