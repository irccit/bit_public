#!/usr/bin/env python
#
# Copyright 2011 Gabriele Sales <gbrsales@gmail.com>

from operator import itemgetter
from optparse import OptionParser
from os import close, dup2, execlp, fdopen, fork, pipe, write
from sys import stdin, stdout
from vfork.io.colreader import Reader
from vfork.util import exit, format_usage


def iter_ranges(fd):
    reader = Reader(fd, '0u,1u')
    for start, stop in reader:
        if stop < start:
            exit('Invalid coordinates at line %d.' % reader.lineno())
        yield start, stop

def write_edge(fd, pos, offset):
    buf = '%d\t%d\n' % (pos, offset)

    l = len(buf)
    while l > 0:
        l -= write(fd, buf[-l:])

def iter_edges(fd):
    with fdopen(fd, 'r') as it:
        for line in it:
            tokens = line.rstrip().split('\t')
            if len(tokens) != 2:
                exit('Invalid column number in bsort output.')

            yield int(tokens[0]), int(tokens[1])

def group_edges(it):
    pos = None
    last_level = None
    level = 0
    for coord, shift in it:
        if coord != pos:
            if pos is not None and (last_level is None or last_level != level):
                yield pos, level

            pos = coord
            last_level = level

        level += shift

    if pos is not None:
        yield pos, level


def main():
    parser = OptionParser(usage=format_usage('''
      %prog [OPTIONS] <RANGES >COVERAGE

      Given a list of start-stop coordinates on input computes
      the coverage, ie the number of ranges overlapping each base.

      The output has the following format:
        1) coordinate
        2) coverage level

      When the difference between successive values of "coordinate"
      is larger than one, it is intended that the coverage remains
      constant in between.
    '''))
    options, args = parser.parse_args()
    if len(args) != 0:
        exit('Unexpected argument number.')

    edges_r, edges_w = pipe()
    sorted_r, sorted_w = pipe()

    try:
        pid = fork()
    except OSError:
        exit('Error forking child process: ' + str(e))

    if pid == 0:
        close(edges_w)
        close(sorted_r)
        dup2(edges_r, stdin.fileno())
        dup2(sorted_w, stdout.fileno())

        try:
            execlp('bsort', 'bsort', '-k1,1n')
        except OSError, e:
            exit('Error running bsort: ' + str(e))

        exit('Internal error.')

    else:
        close(edges_r)
        close(sorted_w)

        for start, stop in iter_ranges(stdin):
            write_edge(edges_w, start, 1)
            write_edge(edges_w, stop, -1)

        close(edges_w)

        for coord, level in group_edges(iter_edges(sorted_r)):
            print '%d\t%d' % (coord, level)


if __name__ == '__main__':
    main()
