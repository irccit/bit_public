from parser import DiagonalParser
from reader import BlockReader, BlockMuxer

class Binner(object):
	def __init__(self, size):
		self.size = size

	def load(self, diagonal_file, event_file):
		diagonal_reader = BlockReader(diagonal_file)
		event_reader = BlockReader(event_file)
		muxer = BlockMuxer(diagonal_reader, event_reader)
		
		diagonal_parser = DiagonalParser()
		event_parser = EventParser()
		
		guid = 0
		for block, reader in muxer:
			try:
				if reader is diagonal_reader:
					for line in block[1]:
						alignments, diagonal = diagonal_parser.parse(line)
						self.process_alignments(alignments)
						self.process_diagonal(diagonal)
				
				elif reader is event_reader:
					events = event_parser.parse(block[1])
					for event in events:
						self.process_event(event)
				
				else:
					raise RuntimeError, 'unexpected reader'
			
			except ValueError, e:
				raise ValueError, '%s in block ending at line %d of file %s' % (e.args[0], reader.filename, reader.lineno)
		
		self.close_all()
	
	def segment_spanned_bins(self, segment):
		
		bin_y_start = segment[1] / self.size
		
		bin_x_stop = segment[2] / self.size
		if segment[2] % self.size > 0:
			bin_x_stop += 1
		bin_y_stop = segment[3] / self.size
		if segment[3] % self.size > 0:
			bin_y_stop += 1
		
		segment_dx = segment[2] - segment[0]
		segment_dy = segment[3] - segment[1]
		bin_x = segment[0] / self.size
		
		for x in range(bin_x_start, bin_x_stop+1):
			for y in range(bin_y_start, bin_y_stop+1):
				pass
	
	def open_output(self, bin_x, bin_y):
		raise NotImplementedError
	
	def close_output(self, bin_x, bin_y):
		raise NotImplementedError
	
	def close_all(self):
		raise NotImplementedError
	
	def process_alignments(self, alignments):
		raise NotImplementedError
	
	def process_diagonal(self, diagonal):
		raise NotImplementedError
	
	def process_event(self, event):
		raise NotImplementedError
