class BlockReader(object):
	def __init__(self, filename):
		self.fd = file(filename, 'r')
		self.filename = filename
		self.lineno = 1

		line = self.fd.readline()
		if len(line) == 0:
			self.header = None
		else:
			line = line.rstrip()
			assert line[0] == '>', 'malformed header at line 1 of file %s' % self.filename
			self.parse_header(line)
	
	def __iter__(self):
		return self
	
	def next(self):
		if self.header is None:
			raise StopIteration
	
		lines = []
		for line in self.fd:
			line = line.rstrip()
			self.lineno += 1
			
			if len(line) == 0:
				continue
			elif line[0] == '>':
				retval = (self.header, lines)
				self.parse_header(line)
				return retval
			else:
				lines.append(line)
		
		retval = (self.header, lines)
		self.header = None
		return retval
	
	def parse_header(self, header):
		try:
			self.header = int(header[1:])
		except ValueError:
			raise 'malformed header at line %d of file %s' % (self.lineno, self.filename)

class BlockMuxer(object):
	def __init__(self, *block_readers):
		self.readers = block_readers
	
	def __iter__(self):
		return self.iter_blocks()
	
	def iter_blocks(self):	
		sources = [ (r, r.iter_blocks()) for r in self.readers ]
		round = self.first_round(iterators)
		
		while len(sources):
			hit = min(round)
			idx = hit[2]
			yield hit[1], sources[idx]
			
			try:
				block = sources[idx].next()
				round[idx][:2] = [block[0], block]
			except StopIteration:
				del sources[idx]
				self.remove_round_record(round, idx)
	
	##
	## Internal use only
	##
	def first_round(self, iterators):
		round = []
		to_remove = []
		idx = 0
		for it in iterators:
			try:
				block = it.next()
				round.append([block[0], block, idx])
				idx += 1
			except StopIteration:
				to_remove.append(it)
						
			for it in to_remove:
				iterators.remove(it)
		
		return round

	def remove_round_record(self, round, idx):
		del round[idx]
		for idx in range(idx, len(round)):
			round[idx][2] = idx
