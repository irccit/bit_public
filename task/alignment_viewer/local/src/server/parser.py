class DiagonalParser(object):
	def parse(self, lines):
		alignments = []	
		first_alignment = None
			
		for alignment in line.split('\t'):
			alignment = self.parse_alignment(alignment)
			alignments.append(alignment)
			
			if first_alignment is None:
				first_alignment = alignment
		
		last_alignment = alignment
		return alignments, (first_alignment[0], first_alignment[1], last_alignment[2], last_alignment[3])
	
	def parse_alignment(self, alignment_repr):
		tokens = alignment_repr.split(',')
		
		try:
			if len(tokens) != 4:
				raise ValueError

			tokens = tuple(int(t) for t in tokens)
			return tokens[0], tokens[2], tokens[1], tokens[3]
		
		except ValueError:
			raise ValueError, "malformed alignment '%s'" % alignment_repr
