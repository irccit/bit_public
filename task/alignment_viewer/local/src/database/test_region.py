from math import sqrt
from pprint import pprint
from random import seed, randint
from sys import argv, exit
from time import time
import database

if __name__ == '__main__':
	if len(argv) != 3:
		exit('Unexpected argument number.')
	
	test_num = int(argv[1])
	test_range = int(argv[2])
	if test_range < 1000:
		exit('Test range is too small.')

	print '** Opening database connection'
	cursor = database.Cursor('sales', 'alignmentviewer', 'alignment_viewer_test', 'to441xl')
	type_cache = database.TypeCache(cursor)
	adaptor = database.Adaptor(cursor, type_cache, max_rownum=0)

	print '** Recovering database statistics'
	chromosome_info = []
	cursor.execute('select c.species, c.chromosome, max(r.stop) from regions r join chromosomes c on r.chromosome_id = c.id group by r.chromosome_id')
	while True:
		row = cursor.fetchone()
		if row is None:
			break
		else:
			chromosome_info.append( (row[0], row[1], row[2]) )

	seed()
	elapsed_times = []
	while test_num:
		info = chromosome_info[randint(0, len(chromosome_info)-1)]
		start = randint(1, info[2]/2)
		stop = min(randint(1000, test_range), info[2] - start) + start

		print '** Regions for chromosome %s of %s, between %d and %d' % (info[1], info[0], start, stop)
		start_time = time()
		res = adaptor.get_region_by_range(info[0], info[1], start, stop)
		elapsed_times.append(time() - start_time)

		test_num -= 1

	avg_time = sum(elapsed_times) / len(elapsed_times)
	stdev = 0
	for t in elapsed_times:
		stdev += (t - avg_time) ** 2
	stdev = sqrt(stdev / len(elapsed_times))

	print 'Average: %.2f (+- %.2f)' % (avg_time, stdev)
	print 'Max    : %.2f' % max(elapsed_times)
	print 'Min    : %.2f' % min(elapsed_times)

