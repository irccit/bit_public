# Copyright 2010 Ivan Molineris <ivan.molineris@gmail.com>
# Copyright 2010 Gabriele Sales <gbrsales@gmail.com>

SPECIES            ?= hsapiens
UCSC_VERSION       ?= hg18
REMOTE_URL         ?= http://www.hapmap.org/genotypes/2008-01/fwd_strand/non-redundant
REMOTE_FILE_PREFIX ?= genotypes
REMOTE_FILE_SUFFIX ?= r23_nr.b36_fwd.txt.gz
POPULATIONS        ?= CEU CHB JPT YRI

SEQUENCE_DIR       ?= $(BIOINFO_ROOT)/task/sequences/dataset/ucsc/$(SPECIES)/$(UCSC_VERSION)

param dna as ALL_CHRS_RAW from $(SEQUENCE_DIR)
ALL_CHRS           := $(filter-out chrM,$(ALL_CHRS_RAW))
ALL_CLASSES        := $(foreach p,$(POPULATIONS),$(foreach c,$(ALL_CHRS),$p-$c))
ALL_RAW            := $(addsuffix .raw.gz,$(ALL_CLASSES))
ALL_META           := $(addsuffix .meta,$(ALL_CLASSES))
ALL_SNP            := $(addsuffix .snp,$(ALL_CLASSES))


ALL          += snp.gz
CLEAN        += $(ALL_META)
INTERMEDIATE += $(ALL_RAW) $(ALL_SNP)

$(ALL_RAW):
	IFS=".-"; \
	t="$@"; \
	ps=( $$t ); \
	wget -O$@ $(REMOTE_URL)/$(REMOTE_FILE_PREFIX)_$${ps[1]}_$${ps[0]}_$(REMOTE_FILE_SUFFIX)

$(ALL_META): %.meta: %.raw.gz
	(zcat $< \
	| tr " " "\t" \
	| perl -lane 'BEGIN{$$,="\t"}; my @G=(); push @G, $$F[0]; push @G, $$F[2]; push @G, $$F[3]; push @G, "$*"; push @G,$$F[1]; push @G, splice(@F, 12); print @G'; \
	exit 0) \   * ignore SIGPIPE caused by explain_first_row *
	| explain_first_row >$@

$(ALL_SNP): %.snp: %.raw.gz %.meta
	zcat $< \
	| tr " " "\t" \
	| unhead \
	| perl -lane 'BEGIN{$$,="\t"}; my @G=(); push @G, $$F[0]; push @G, $$F[2]; push @G, $$F[3]; push @G, "$*"; push @G,$$F[1]; push @G, splice(@F, 12); print @G' \
	| sed 's/chr//' \
	| explain_first_row -a \
	| repeat_fasta_pl 'print ">", join("\t",map {$$_=~s/^\d\t//; $$_} splice(@rows,0,5)); print "\n"; print join "\n", @rows; print "\n"' \
	| unhead -n 2 \
	| translate -p '^>' $^2 1 >$@

snp.gz: $(ALL_SNP)
	cat $^ \
	| gzip >$@
