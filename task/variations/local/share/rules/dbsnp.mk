# Copyright 2010 Gabriele Sales <gbrsales@gmail.com>

SPECIES       ?= hsapiens
VERSION       ?= 130
STX_PROCESSOR ?= joost

SPECIES_MAP   := $(TASK_ROOT)/local/share/species-dbsnp.map
DBSNP_SPECIES ?= $(shell translate $(SPECIES_MAP) 1 <<<"$(SPECIES)")

DBSNP_HOST    := ftp.ncbi.nih.gov
DBSNP_BASE    := /snp/organisms/$(DBSNP_SPECIES)
DBSNP_XML     := $(DBSNP_BASE)/XML
SUMMARY_STX   := $(TASK_ROOT)/local/share/dbsnp-summary.stx
FLANKING_STX  := $(TASK_ROOT)/local/share/dbsnp-flanking.stx


define do_list
tr ";" "\n" <<<"user anonymous nothing; cd $(1); ls -l" \
| ftp -p -n $(DBSNP_HOST)
endef

dbsnp.list:
	( \
		set -e; \
		echo -n "ALL_DBSNP:="; \
		$(call do_list,$(DBSNP_XML)) \
		| sed -r 's|.* +||' \
		| grep -F '.xml.gz' \
		| tr "\n" " " \
	) >$@

include dbsnp.list
CLEAN += dbsnp.list


ALL   += $(ALL_DBSNP) summary.gz
CLEAN += flanking.gz

INTERMEDIATE += check.version
check.version:
	@set -e; \
	found="$$($(call do_list,$(DBSNP_BASE)) | sed -r 's|.* +README_$(DBSNP_SPECIES)_B([0-9]+).*|\1|;t;d')"; \
	if [[ $$found  != $(VERSION) ]]; then \
		echo "*** Version mismatch: expected $(VERSION), found $$found" >&2; \
		exit 1; \
	fi

$(ALL_DBSNP): check.version
	wget 'ftp://$(DBSNP_HOST)/$(DBSNP_XML)/$@'

ALL_SUMMARIES := $(addsuffix .summary,$(ALL_DBSNP))
INTERMEDIATE += $(ALL_SUMMARIES)
%.xml.gz.summary: %.xml.gz
	zcat $< \
	| $(STX_PROCESSOR) '-' $(SUMMARY_STX) \
	| xml2tsv -x offset id class chr start stop orientation variation \   * check that the contig offset is defined *
	| cut -f2- >$@                                                        * and then throw it away                  *

summary.gz: $(ALL_SUMMARIES)
	cat $^ \
	| bsort -k3,3 -k4,4n \
	| gzip >$@

.META: summary.gz
	1  id         72477211
	2  class      snp
	3  chr        1
	4  start      259
	5  stop       259
	6  strand     +1
	7  variation  A/G


ALL_FLANKING := $(addsuffix .flanking,$(ALL_DBSNP))
INTERMEDIATE += $(ALL_FLANKING)
%.xml.gz.flanking: %.xml.gz
	zcat $< \
	| $(STX_PROCESSOR) '-' $(FLANKING_STX) \
	| xml2tsv -x id flank5 flank3 >$@

flanking.gz: $(ALL_FLANKING)
	cat $^ | gzip >$@

.META: flanking.gz
	1  id      242
	2  flank5  TGCACCAGA
	3  flank3  GGGCCTGTC
