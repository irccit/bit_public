# Copyright 2010 Gabriele Sales <gbrsales@gmail.com>

SPECIES       ?= hsapiens
VERSION       ?= hg18
TRACK         ?= snp130
FLANKING_SIZE ?= 300

MYSQL_CMD     := mysql --user=genome --host=genome-mysql.cse.ucsc.edu -BCAN
SEQUENCE_DIR  ?= $(BIOINFO_ROOT)/task/sequences/dataset/ucsc/$(SPECIES)/$(VERSION)

param dna as CHR_NAMES from $(SEQUENCE_DIR)
extern $(addprefix $(SEQUENCE_DIR)/,$(addsuffix .fa,$(CHR_NAMES))) as ALL_CHRS


ALL   += summary.gz
CLEAN += flanking.gz

summary.gz:
	$(MYSQL_CMD) $(VERSION) <<<" \
		SELECT REPLACE(name, 'rs', ''), class, REPLACE(chrom, 'chr', ''), \
                       chromStart, chromEnd, CONCAT(strand, '1'), observed \
		FROM snp130 \
		WHERE chrom NOT LIKE '%_random'" \
	| filter_1col 3 <(sed 's|chr||g;y| |\n|' <<<"$(CHR_NAMES)") \
	| bsort -k3,3 -k4,4n \
	| gzip >$@

.META: summary.gz
	1  id         72477211
	2  class      single
	3  chr        1
	4  start      259
	5  stop       260
	6  strand     +1
	7  variation  A/G

flanking.gz: summary.gz $(ALL_CHRS)
	bawk '{print $$id, $$chr, $$start-$(FLANKING_SIZE), $$start; print $$id, $$chr, $$stop, $$stop+$(FLANKING_SIZE)}' $< \
	| seqlist2fasta -c $(SEQUENCE_DIR) \
	| fasta2oneline \
	| bawk 'NR%2==1 {label=$$1; seq5=$$2} \
		NR%2==0 {if($$1!=label) {print "Label mismatch" >"/dev/stderr"; exit 1}; \
		         print label,seq5,$$2}' \
	| gzip >$@

.META: flanking.gz
	1  id      72477211
	2  flank5  taacccta
	3  flank3  taacccta
