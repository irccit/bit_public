#!/usr/bin/env python
#
# Copyright 2009 Gabriele Sales <gbrsales@gmail.com>

from optparse import OptionParser
from vfork.fasta.writer import MultipleBlockWriter
from vfork.util import exit, format_usage, safe_import
from sys import stdin, stdout

with safe_import('BioPython (http://biopython.org)'):
	from Bio.GenBank import Iterator, RecordParser

def main():
	parser = OptionParser(usage=format_usage('''
		%prog [OPTIONS] <GENBANK >FASTA

		Extract sequences from GenBank records.
	'''))
        options, args = parser.parse_args()
        if len(args) != 0:
		exit('Unexpected argument number.')

        w = MultipleBlockWriter(stdout)
        for record in Iterator(stdin, RecordParser()):
		w.write_header(record.version)
                w.write_sequence(record.sequence)
        
        w.flush()

if __name__ == '__main__':
	main()
