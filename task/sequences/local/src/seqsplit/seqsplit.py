#!/usr/bin/env python
from optparse import OptionParser
from sys import stdin, stdout
from vfork.fasta.reader import MultipleBlockStreamingReader
from vfork.util import exit, format_usage
import re

class Output(object):
	def __init__(self, lineLen, hardSplit, indexFile=None):
		self.lineLen = lineLen
		self.hardSplit = hardSplit
		
		if indexFile is None:
			self.indexFile = None
		else:
			self.indexFile = file(indexFile, 'w')
	
	def writeBlock(self, headerPrefix, blockStart, sequence):
		assert '\t' not in headerPrefix, 'tab chars are not allowed inside the header prefix'
		name = '%s_%d' % (headerPrefix, blockStart)
		
		if self.hardSplit:
			fd = file(name + '.fa', 'w')
		else:
			fd = stdout
		
		startOffset = fd.tell()
		
		header = '>%s\n' % name
		fd.write(header)
		writtenBytes = len(header) + self._textwrap(fd, sequence)
		
		if self.hardSplit:
			fd.close()
		elif self.indexFile:
			print >>self.indexFile, '%s\t%d\t%d\t%d' % (name, len(sequence), startOffset, writtenBytes)
	
	def _textwrap(self, fd, text):
		writtenBytes = 0
		for i in xrange(0, len(text), self.lineLen):
			line = text[i:i+self.lineLen] + '\n'
			fd.write(line)
			writtenBytes += len(line)
		return writtenBytes

def main():
	parser = OptionParser(usage=format_usage('''
		%prog [options] <SEQUENCE_FASTA >SPLITTED_FASTA

		Splits a sequence into sub-sequences when it finds
		large masked blocks.
	'''))
	parser.add_option('-i', '--index', dest='indexFile', help='build an INDEX of the blocks', metavar='INDEX')
	parser.add_option('-p', '--prefix', dest='prefix', default='', help='PREFIX for fasta tags', metavar='PREFIX')
	parser.add_option('-m', '--min-size', action='store', type='int', dest='minSize', default=0, help='a filter on the minimum SIZE of a contiguous block of non-N', metavar='SIZE')
	parser.add_option('-g', '--min-gap', action='store', type='int', dest='minGap', default=1000, help='split on blocks at least SIZE masked chars (acgtnN) big (by default: 1000)', metavar='SIZE')
	parser.add_option('-r', '--hard-split', action='store_true', dest='hardSplit', default=False, help='splits blocks into separate output files')
	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Invalid argument number.')
	elif options.hardSplit and options.indexFile is not None:
		exit('-i and -r are mutually exclusive.')
	
	if len(options.prefix):
		options.prefix += '_'

	output = Output(60, options.hardSplit, options.indexFile)
	
	for header, seq in MultipleBlockStreamingReader(stdin):
		m = re.match(r'[acgtnN]+', seq)
		if m is None:
			start = 0
		else:
			start = m.end()
		
		splitRx = re.compile(r'[^acgtnN][acgtnN]{%d,}' % options.minGap)
		block = ''
		block_start = start
	
		for m in splitRx.finditer(seq):
			block += seq[start:m.start()+1]
		
			if options.minSize != 0:
				printable = len(block) > options.minSize or m.end() == len(seq)
			else:
				printable = True
		
			if printable:
				output.writeBlock(options.prefix + header, block_start, block)
				block = ''
				block_start = m.end()
			else:
				block += seq[m.start()+1:m.end()]
		
			start = m.end()
	
		if start < len(seq) or len(block):
			m = re.search(r'[acgtnN]+$', seq[start:])
			if m is None:
				block += seq[start:]
			else:
				block += seq[start:start+m.start()]
		
			if len(block):
				output.writeBlock(options.prefix + header, block_start, block)
	
if __name__ == '__main__':
	main()
