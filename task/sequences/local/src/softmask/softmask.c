#include <stdio.h>
#include <string.h>

int main(int argc, char* argv[])
{
	long i;
	for (i = 1; i < argc; i++)
	{
		if (strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0)
		{
			fprintf(stderr, "Usage: %s MASKED_FA UNMASKED_FA\n", argv[0]);
			return 1;
		}
	}
	
	if (argc != 3)
	{
		fprintf(stderr, "Unexpected argument number.\n");
		return 1;
	}
	
	FILE* masked_fd = fopen(argv[1], "r");
	if (masked_fd == NULL)
	{
		fprintf(stderr, "Cannot open %s\n", argv[1]);
		return 1;
	}
	
	FILE* unmasked_fd = fopen(argv[2], "r+");
	if (unmasked_fd == NULL)
	{
		fprintf(stderr, "Cannot open %s\n", argv[2]);
		return 1;
	}
	
	char masked_seq[4096];
	char unmasked_seq[4096];
	while (1)
	{
		long byte_num = fread(masked_seq, 1, 4096, masked_fd);
		if (byte_num == -1)
		{
			fprintf(stderr, "Error reading from %s\n", argv[1]);
			return 1;
		}
		else if (byte_num == 0)
			break;
		
		if (fread(unmasked_seq, 1, 4096, unmasked_fd) != byte_num)
		{
			fprintf(stderr, "Error reading from %s\n", argv[2]);
			return 1;
		}
		
		for (i = 0; i < byte_num; i++)
		{
			if (masked_seq[i] == 'N' && unmasked_seq[i] != 'N')
				unmasked_seq[i] = unmasked_seq[i] - 'A' + 'a';
		}
		
		fwrite(unmasked_seq, 1, byte_num, stdout);
	}
	
	fclose(masked_fd);
	fclose(unmasked_fd);
	return 0;
}

