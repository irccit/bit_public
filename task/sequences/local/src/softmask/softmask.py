#!/usr/bin/env python
# encoding: utf-8

from array import array
from optparse import OptionParser
from sys import exit
import re

def main():
	parser = OptionParser(usage='%prog MASKED_FA UNMASKED_TARGET_FA')
	options, args = parser.parse_args()
	
	if len(args) != 2:
		exit('Unexpected argument number.')
	
	masked_fd = file(args[0], 'r')
	target_fd = file(args[1], 'r+')
	mask_rx = re.compile('N+')
	
	for masked_line in masked_fd:
		if masked_line.startswith('>'):
			target_fd.seek(len(masked_line), 1)
		else:
			unmasked_line = array('c')
			unmasked_line.fromfile(target_fd, len(masked_line))
			
			updated = False
			for masked_block in mask_rx.finditer(masked_line):
				updated = True
				for i in xrange(masked_block.start(), masked_block.end()):
					current_chr = unmasked_line[i]
					if current_chr != 'N':
						unmasked_line[i] = current_chr.lower()
			
			if updated:
				target_fd.seek(-len(unmasked_line), 1)
				unmasked_line.tofile(target_fd)

if __name__ == "__main__":
	main()
