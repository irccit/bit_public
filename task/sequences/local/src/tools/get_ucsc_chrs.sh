#/bin/bash
mysql --user=genome --host=genome-mysql.cse.ucsc.edu -BCAN $1 <<<"SELECT distinct chrom FROM ensGene" | tr "\n" " " | sed 's/chr//g'
echo
