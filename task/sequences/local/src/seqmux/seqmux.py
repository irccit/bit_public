#!/usr/bin/env python
# encoding: utf-8

from cStringIO import StringIO
from optparse import OptionParser
from sys import exit, stdout
from vfork.fasta.reader import MultipleBlockStreamingReader
from vfork.fasta.writer import MultipleBlockWriter

class BufferedWriter(object):
	def __init__(self, max_block_size, index, separator_size=1000):
		self.max_block_size = max_block_size
		self.separator = 'N' * separator_size
		self.index = index
		self.serial = 1
		self._reset()
		self.writer = MultipleBlockWriter(stdout)
	
	def flush(self):
		if self.cum_size > 0:
			self._output()
		self.writer.flush()
	
	def write(self, header, sequence):
		if self.cum_size != 0 and self.cum_size + len(sequence) > self.max_block_size:
			self._output()
		
		self.index.write('block%d' % self.serial, header, self.cum_size, len(sequence))
		self.buffer.write(sequence)
		self.buffer.write(self.separator)
		self.cum_size += len(sequence) + len(self.separator)
	
	def _reset(self):
		self.cum_size = 0
		self.buffer = StringIO()
	
	def _output(self):
		self.writer.write_header('block%d' % self.serial)
		self.serial += 1
		self.writer.write_sequence(self.buffer.getvalue()[:-len(self.separator)])
		self._reset()

class Index(object):
	def __init__(self, filename):
		self.fd = file(filename, 'w')
	
	def write(self, group_header, header, offset, size):
		print >>self.fd, '\t'.join((group_header, header, str(offset), str(size)))

class NoIndex(object):
	def write(self, group_header, header, offset, size):
		pass

def main():
	parser = OptionParser(usage='%prog SEQUENCE_FASTA MAX_BLOCK_SIZE')
	parser.add_option('-i', '--index', dest='index', help='build and INDEX of the blocks', metavar='INDEX')
	parser.add_option('-n', '--separator-size', dest='separator_size', type='int', default=1000, help='the size of the block of Ns used to separate sequences (default: 1000)', metavar='SIZE')
	options, args = parser.parse_args()
	
	if len(args) != 2:
		exit('Unexpected argument number.')
	elif options.separator_size < 0:
		exit('Invalid separator size.')
	
	try:
		max_block_size = int(args[1])
		#if max_block_size < 100:
		#	raise ValueError
	except ValueError:
		exit('Invalid maximum block size.')
	
	if options.index:
		index = Index(options.index)
	else:
		index = NoIndex()
	writer = BufferedWriter(max_block_size, index, options.separator_size)
	
	for header, sequence in MultipleBlockStreamingReader(args[0]):
		writer.write(header, sequence)
	writer.flush()

if __name__ == "__main__":
	main()
