# Copyright 2015 Gabriele Sales <gbrsales@gmail.com>

VERSION ?= 2015_03

context task/annotations


url.mk:
	( \
	  echo -n 'BASE_URL:='; \
	  uniprot_url_helper $(VERSION); \
	) >$@

include url.mk

sprot.fasta.xz:
	$(call download,$(BASE_URL)/knowledgebase/complete/uniprot_sprot.fasta.gz)

trembl.fasta.xz:
	$(call download,$(BASE_URL)/knowledgebase/complete/uniprot_trembl.fasta.gz)


define download
curl -L '$1' \
| gunzip \
| pixz -9e >$@
endef


ALL        += sprot.fasta.xz trembl.fasta.xz
CLEAN_FULL += url.mk
