# Copyright 2008-2013 Gabriele Sales <gbrsales@gmail.com>

SPECIES ?= hsapiens
VERSION ?= 15

COMMON_PATH := $(TASK_ROOT)/dataset/mirbase/common/$(VERSION)/
SPECIES_MAP := $(TASK_ROOT)/local/share/species-mirbase.map

extern $(COMMON_PATH)/sequences.fa.xz as SEQUENCES_FA
extern $(COMMON_PATH)/hairpins.fa.xz as HAIRPINS_FA


sequences.fa.xz: $(SEQUENCES_FA)
	xzcat $^ \
	| fasta_get -r "$$(translate_value $(SPECIES) $(SPECIES_MAP))-"  \
	| xz -9 >$@

.META: sequences.fa.xz
	>miRNA_id
	mirna_sequence

hairpins.fa.xz: $(HAIRPINS_FA)
	xzcat $< \
	| fasta_get -r "$$(translate_value $(SPECIES) $(SPECIES_MAP))-" \
	| xz -9 >$@

seeds.fa.xz: sequences.fa.xz
	xzcat $< \
	| mirna_seeds \
	| xz -9 >$@

.META: seeds.fa.xz
	>miRNA_id
	seed_sequence


ALL += sequences.fa.xz hairpins.fa.xz seeds.fa.xz
