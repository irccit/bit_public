# Copyright 2009-2011,2013 Gabriele Sales <gbrsales@gmail.com>

VERSION  ?= 15
BASE_URL ?= ftp://mirbase.org/pub/mirbase/$(VERSION)/


major.fa.gz:
	wget -O $@ "$(BASE_URL)/mature.fa.gz"

minor.fa.gz:
	wget -O $@ "$(BASE_URL)/maturestar.fa.gz"


ifdef LEGACY_MATURESTAR
sequences.fa.xz: major.fa.gz minor.fa.gz
else
sequences.fa.xz: major.fa.gz
endif

sequences.fa.xz:
	zcat $^ \
	| sed 's/^\(>[^ \t]*\).*/\1/' \
	| xz -9 >$@

hairpins.fa.xz:
	wget -O- '$(BASE_URL)/hairpin.fa.gz' \
	| zcat \
	| sed 's/^\(>[^ \t]*\).*/\1/' \
	| xz -9 >$@

families.xz:
	wget -O- '$(BASE_URL)/miFam.dat.gz' \
	| zcat \
	| bawk 'BEGIN     {FS=" "} \
	        $$1=="ID" {fam=$$2;next} \
	        $$1=="MI" {print fam,$$3}' \
	| xz -9 >$@

.META: families.xz
	1  family
	2  member

ALL          += sequences.fa.xz hairpins.fa.xz families.xz
INTERMEDIATE += major.fa.gz minor.fa.gz
