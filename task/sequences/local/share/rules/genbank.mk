# Copyright 2009 Gabriele Sales <gbrsales@gmail.com>

GENBANK_HOST    ?= ftp.ncbi.nlm.nih.gov
GENBANK_PATH    ?= /genbank/
GENBANK_VERSION ?= 170

SEQ_SUFFIX      := .seq.gz

define do_list
tr ";" "\n" <<<"user anonymous nothing; cd $(1); ls -l" \
| ftp -p -n $(GENBANK_HOST)
endef

seq.list:
	( \
		set -e; \
		echo -n "ALL_SEQ:="; \
		$(call do_list,$(GENBANK_PATH)) \
		| sed -r 's|.* +||' \
		| grep -F '$(SEQ_SUFFIX)' \
		| tr "\n" " " \
	) >$@

include seq.list
CLEAN += seq.list

INTERMEDIATE += check.version
check.version:
	@set -e; \
	found="$$(wget -O- -q 'ftp://$(GENBANK_HOST)/$(GENBANK_PATH)/GB_Release_Number')"; \
	if [[ $$found  != $(GENBANK_VERSION) ]]; then \
		echo "*** Version mismatch: expected $(GENBANK_VERSION), found $$found" >&2; \
		exit 1; \
	fi

ALL += $(ALL_SEQ)
$(ALL_SEQ): check.version
	wget -O$@ 'ftp://$(GENBANK_HOST)/$(GENBANK_PATH)/$@'
