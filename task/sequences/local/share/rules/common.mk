# Copyright 2011-2012 Gabriele Sales <gbrsales@gmail.com>

ifndef SOURCE
$(error SOURCE must be defined)
endif

include $(TASK_ROOT)/local/share/species/$(SOURCE)/$(SPECIES).mk


CHROMOSOMES := $(addprefix chr,$(SPECIES_CHRS))
SEQUENCES   := $(addsuffix .fa,$(CHROMOSOMES))
LENGTHS     := $(addsuffix .fa.len,$(CHROMOSOMES))

define clean_chromosome_names
sed -r 's/^>(chr)?([^ \t]+).*$$/>\2/'
endef


genome.fa: $(SEQUENCES)
	cat $^ >$@

genome.fa.len: $(SEQUENCES)
	cat $^ \
	| sed '/^>/d' \
	| tr -d '\n' \
	| wc -c >$@


$(LENGTHS): %.fa.len: %.fa
	cat $< \
	| unhead \
	| tr -d "\n" \
	| wc -c >$@

chrs.len: $(addsuffix .fa.len, $(CHROMOSOMES))
	( for i in $^; do \
	    name=$${i%.fa.len}; \
	    name=$${name#chr}; \
	    echo -ne "$$name\t"; \
	    cat $$i; \
	  done ) >$@


.PHONY: dna chromosomes
dna chromosomes: $(SEQUENCES)

echo.chromosomes:
	@echo $(SPECIES_CHRS)


ALL   += $(SEQUENCES)
CLEAN += genome.fa genome.fa.len $(LENGTHS) chrs.len
