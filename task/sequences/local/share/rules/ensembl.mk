# Copyright 2008-2012 Gabriele Sales <gbrsales@gmail.com>
# Copyright 2011-2012 Paolo Martini <paolo.cavei@gmail.com>

SOURCE := ensembl
import common

SPECIES_MAP := $(TASK_ROOT)/local/share/species-ensembl.map
PERMALINK_MAP := $(TASK_ROOT)/local/share/ensembl-permalink.map


ensembl.info:
	info=$$(translate_value $(SPECIES) $(SPECIES_MAP)); \
	IFS=$$'\t' read name repo <<<"$$info"; \
	permalink=$$(translate_value "$$repo-$(VERSION)" $(PERMALINK_MAP)); \
	ensembl_sequence_links "http://$$permalink/info/data/ftp/index.html" "$$name" $(VERSION) >$@

define url_prefix
$$(if ! grep -P '\b$(1)\b' ensembl.info | cut -f2; then \
    echo "*** Cannot find $(1) sequences." >&2; exit 1; \
   fi)
endef

define recompress
zcat | $(1) | gzip -9
endef


$(SEQUENCES): chr%.fa: ensembl.info
	prefix="$(call url_prefix,DNA)"; \
	wget -q -c -O- "$$prefix"'/*.$(CHR_TYPE).chromosome.$*.fa.gz' \
	| zcat \
	| $(clean_chromosome_names) >$@

cdna.fa: ensembl.info
	prefix="$(call url_prefix,cDNA)"; \
	wget -q -c -O- "$$prefix"'/*.cdna.all.fa.gz' \
	| zcat \
	| sed 's| .*||' >$@

ncrna.fa: ensembl.info
	prefix="$(call url_prefix,ncRNA)"; \
	wget -q -c -O- "$$prefix"'/*.ncrna.fa.gz' \
	| zcat \
	| sed 's| .*||' >$@


ALL   += cdna.fa
CLEAN += ncrna.fa ensembl.info

-include override.mk
