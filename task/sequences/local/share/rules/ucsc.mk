# Copyright 2011 Gabriele Sales <gbrsales@gmail.com>

#SOURCE := ucsc
#import common

BASE_URL := ftp://hgdownload.cse.ucsc.edu/goldenPath/$(VERSION)

$(SEQUENCES): chr%.fa:
	wget -q -c -O- "$(BASE_URL)/chromosomes/chr$*.fa.gz" \
	| zcat \
	| $(clean_chromosome_names) >$@

.INTERMEDIATE: genome.fa.gz
genome.fa.gz:
	wget -O $@ $(BASE_URL)/$(VERSION)/BigZips/$(VERSION).fa.gz

genome.fa: genome.fa.gz
	zcat $< > $@
