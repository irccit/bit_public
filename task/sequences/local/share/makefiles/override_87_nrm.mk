cdna.fa.gz:
	wget -O $@ ftp://ftp.ensembl.org/pub/release-87/fasta/mus_musculus/cdna/Mus_musculus.GRCm38.cdna.all.fa.gz

ncrna.fa.gz:
	wget -O $@ ftp://ftp.ensembl.org/pub/release-87/fasta/mus_musculus/ncrna/Mus_musculus.GRCm38.ncrna.fa.gz

ensembl.info:
	echo "NOT SUPPORTED"

%.fa: %.fa.gz
	zcat $< > $@
