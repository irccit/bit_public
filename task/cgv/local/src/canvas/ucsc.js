UCSC = function() {
	function translate_species(species) {
		var supported_species = {
			"hsapiens": "Human",
			"mmusculus": "Mouse"
		};
		
		var sp = supported_species[species];
		if (!sp)
			throw "unsupported species " + species;
		else
			return sp;
	}
	
	function build_step1_url(species, chromosome, start, stop, width)
	{
		var url = "http://genome.ucsc.edu/cgi-bin/hgTracks?org=" + species;
		url += "&position=" + chromosome + "%3A" + start + "-" + stop;
		url += "&pix=" + width;
		return url;
	}
	
	function extract_step2_url(iframe)
	{
		for (elt in iframe.contentDocument.getElementsByTagName("img")) {
			if (elt.usemap == "#map")
				alert("got it: " + elt.src);
		}
	}
	
	return {
		load_image: function(species, chromosome, start, stop, width, callback) {
			species = translate_species(species);
			
			var req = new XMLHttpRequest();
			req.open("GET", "http://www.google.com/", true); //build_step1_url(species, chromosome, start, stop, width), true);
			req.onreadystatechange = function() {
				if (req.readyState == 4)
					alert(req.responseText)
			}
			req.send(null);
		}
	}
}();