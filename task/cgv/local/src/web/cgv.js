function cgv_install(id, params, global_keys)
{
	function process_template(req)
	{
		var root = getElement(id);
		if (root != null)
		{
			root.innerHTML = req.responseText.replace(/\$id\$/g, id);
			
			var controller = cgv_build_controller(id, params);
			if (controller != null)
			{
				var img_id = id + "_img";
				CGVRegister[id + "_form"] = controller;
				CGVRegister[img_id] = controller;
				
				var img_elem = getElement(img_id);
				Zoom.install(img_elem);
				controller.init();
				
				if (global_keys)
					connect(document, "onkeyup",
						function (e) {
							controller.key_up(controller, e);
						});
			}
		}
	}
	
	var d = doSimpleXMLHttpRequest("http://to444xl.to.infn.it/cgv/template.html");
	d.addCallback(process_template);
}

//// PRIVATE ////
CGVRegister = new Array();

Zoom.callback = function(target, start_point, region_dimensions) {
	var controller = CGVRegister[target.id];
	if (controller != null)
		controller.zoom_in(start_point, region_dimensions);
};

function cgv_build_controller(id, params)
{
	var form_elem = getElement(id + "_form");
	if (form_elem == null)
		return null;
	
	var img_elem = getElement(id + "_img");
	if (img_elem == null)
		return null;

	function apply_values_to_form()
	{
		forEach(getElementsByTagAndClassName("INPUT", null, form_elem),
		        function (e) {
		        	var value = params[e.name];
		        	if (value != null)
		        		e.value = value;	
		        });
	}
	
	function build_img_url()
	{
		var url = 'http://to444xl.to.infn.it/cgv/cgv.php?';
		for (var name in params)
			url += name + "=" + params[name] + "&";
		return url.substr(0, url.length-1);
	}
	
	function refresh()
	{
		apply_values_to_form();
		img_elem.src = build_img_url();
	}
	
	function parse_shift(shift, start, stop)
	{
		if (typeof(shift) == "string")
		{
			var last_char = shift.substr(shift.length - 1, 1);
			if (last_char == "%")
			{
				var perc = parseFloat(shift.substr(0, shift.length - 1));
				return Math.round((stop - start) / 100 * perc);
			}
			else
				return parseFloat(shift);
		}
		else if (typeof(shift) == "number")
			return shift;
		
		return null;
	}
	
	var controller = {
		init: function() {
			refresh();
		},
		
		zoom_in: function(start_point, region_dimensions) {
			var x_scale = (params.x_stop - params.x_start) / img_elem.width;
			var y_scale = (params.y_stop - params.y_start) / img_elem.height;
		
			params.x_start += Math.round(start_point.x * x_scale);
			params.x_stop = Math.round(params.x_start + (region_dimensions.w * x_scale));
			params.y_start += Math.round((img_elem.height - start_point.y - region_dimensions.h) * y_scale);
			params.y_stop = Math.round(params.y_start + (region_dimensions.h * y_scale));
			
			refresh();
		},
	
		zoom_out: function() {
			var delta_w = Math.round((params.x_stop - params.x_start) / 2);
			var delta_h = Math.round((params.y_stop - params.y_start) / 2);
			
			params.x_start = Math.max(params.x_start - delta_w, 0);
			params.x_stop += delta_w;
			params.y_start = Math.max(params.y_start - delta_h, 0);
			params.y_stop += delta_h;
			
			refresh();
		},
		
		rescale: function() {
			var w = params.x_stop - params.x_start;
			var h = params.y_stop - params.y_start;
			
			if (w > h)
			{
				var delta_h = Math.round((w - h) / 2);
				params.y_start = Math.max(params.y_start - delta_h, 0);
				params.y_stop += delta_h;
			}
			else
			{
				var delta_w = Math.round((h - w) / 2);
				params.x_start = Math.max(params.x_start - delta_w, 0);
				params.x_stop += delta_w;
			}
			
			refresh();
		},
		
		shift_left: function(shift) {
			shift = parse_shift(shift, params.x_start, params.x_stop);
			if (shift != null)
			{
				if (shift > params.x_start)
					shift = params.x_start;
				
				params.x_start -= shift
				params.x_stop -= shift;
				
				refresh();
			}
		},
		
		shift_right: function(shift) {
			shift = parse_shift(shift, params.x_start, params.x_stop);
			if (shift != null)
			{
				params.x_start += shift;
				params.x_stop += shift;
				
				refresh();
			}
		},
		
		shift_up: function(shift) {
			shift = parse_shift(shift, params.y_start, params.y_stop);
			if (shift != null)
			{
				params.y_start += shift;
				params.y_stop += shift;
				
				refresh();
			}
		},
		
		shift_down: function(shift) {
			shift = parse_shift(shift, params.y_start, params.y_stop);
			if (shift != null)
			{
				if (shift > params.y_start)
					shift = params.y_start;
				
				params.y_start -= shift;
				params.y_stop -= shift;
				
				refresh();
			}
		},
		
		key_up: function(self, e) {
			var code = e.key().code;
			var perc = "30%";
			
			if (code == 37)
				self.shift_left(perc);
			else if (code == 38)
				self.shift_up(perc);
			else if (code == 39)
				self.shift_right(perc);
			else if (code == 40)
				self.shift_down(perc);
			else if (code == 107)
			{
				var start_point = new Coordinates(img_elem.width / 4, img_elem.height / 4);
				var region_dimensions = new Dimensions(img_elem.width / 2, img_elem.height / 2);
				self.zoom_in(start_point, region_dimensions);
			}
			else if (code == 109)
				self.zoom_out();
			else
				return;
			
			e.stop();
		}
	};
	
	return controller;
}

function cgv_get_controller(id)
{
	return CGVRegister[id];
}
