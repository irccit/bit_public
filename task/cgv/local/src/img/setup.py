#!/usr/bin/env python
# encoding: utf-8

from setuptools import setup, find_packages

setup(
	name = 'cgv',
	version = '0.6',
	packages = find_packages(),
	install_requires = ['pycairo>=1.2.6', 'MySQL_python', 'Paste', 'PasteDeploy', 'PasteScript' ],
	entry_points = {
		'paste.app_factory' : [ 'img=cgv.img:app_factory' ]
	}
)
