#!/usr/bin/env python
# encoding: utf-8

from glob import glob
from optparse import OptionParser
from os.path import basename, join
from parse import *
from sys import exit
from tables import *

def main():
	parser = OptionParser(usage='%prog LOCL_DIR SINTENY_DIR')
	options, args = parser.parse_args()
	
	if len(args) != 2:
		exit('Unexpected argument number.')
	
	locl_map = LoclMap()
	chr_pair_table = ChrPairTable('chr_pairs.tbl')
	locl_table = LoclTable('locls.tbl')
	
	type_map = TypeMap('types.tbl')
	try:
		hsp_table = HspTable('hsps.tbl', type_map.get_id('hsp'))
		diagonal_table = DiagonalTable('diagonals.tbl', type_map.get_id('diagonal'))
		spgap_table = SpGapTable('spgaps.tbl', type_map.get_id('spgap'))
	except KeyError:
		import traceback as tb
		tb.print_exc()
		exit('Missing needed type from types.tbl')
	
	for filename in glob(join(args[0], '*.loc_cl')):
		chr1, chr2 = parse_locl_filename(basename(filename))
		chr_pair_id = chr_pair_table.add(chr1, chr2)
		locl_map.add_chr_pair(chr_pair_id, chr1, chr2)
		
		class SegmentScanner(LoclScanner):
			def process_label(self, label):
				coords = parse_locl_label(label, self.filename, self.lineno+1)
				self.locl_id = locl_table.add(chr_pair_id, *coords)
				locl_map.add_locl(chr_pair_id, self.locl_id, *coords)
			
			def process_line(self, line):
				hsp_table.add(self.locl_id, *parse_segment(line, self.filename, self.lineno+1))
		
		SegmentScanner().scan(filename)
	
	for filename in glob(join(args[1], '*.loc_cl.diag')):
		chr1, chr2 = parse_locl_filename(basename(filename))
		
		class DiagonalScanner(LoclScanner):
			def process_label(self, label):
				coords = parse_locl_label(label, self.filename, self.lineno+1)
				try:
					self.locl_id = locl_map[(chr1, chr2) + coords]
				except KeyError:
					exit('Unexpected location cluster at line %d of file %s: %s' % (self.lineno+1, self.filename, label))
			
			def process_line(self, line):
				diagonal = parse_diagonal(line, self.filename, self.lineno+1)
				if diagonal is not None:
					diagonal_table.add(self.locl_id, *diagonal)
		
		DiagonalScanner().scan(filename)
	
	for filename in glob(join(args[1], '*.loc_cl.diag.spgaps')):
		chr1, chr2 = parse_locl_filename(basename(filename))
		
		class SpGapScanner(LoclScanner):
			def process_label(self, label):
				coords = parse_locl_label(label, self.filename, self.lineno+1)
				try:
					self.locl_id = locl_map[(chr1, chr2) + coords]
				except KeyError:
					exit('Unexpected location cluster at line %d of file %s: %s' % (self.lineno+1, self.filename, label))
			
			def process_line(self, line):
				spgap_table.add(self.locl_id, *parse_spgap(line, self.filename, self.lineno+1))
		
		SpGapScanner().scan(filename)

if __name__ == "__main__":
    main()
