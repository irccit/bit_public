TRUNCATE types;
TRUNCATE sequences;
TRUNCATE spans;
TRUNCATE regions;
TRUNCATE sequence_pairs;
TRUNCATE locls;
TRUNCATE segments;

LOAD DATA LOCAL INFILE 'types.tbl' INTO TABLE types;
LOAD DATA LOCAL INFILE 'sequences.tbl' INTO TABLE sequences;
LOAD DATA LOCAL INFILE 'spans.tbl' INTO TABLE spans;
LOAD DATA LOCAL INFILE 'regions.tbl' INTO TABLE regions;
LOAD DATA LOCAL INFILE 'sequence_pairs.tbl' INTO TABLE sequence_pairs;
LOAD DATA LOCAL INFILE 'locls.tbl' INTO TABLE locls;
LOAD DATA LOCAL INFILE 'segments.tbl' INTO TABLE segments;

OPTIMIZE TABLE types;
OPTIMIZE TABLE sequences;
OPTIMIZE TABLE spans;
OPTIMIZE TABLE regions;
OPTIMIZE TABLE sequence_pairs;
OPTIMIZE TABLE locls;
OPTIMIZE TABLE segments;
