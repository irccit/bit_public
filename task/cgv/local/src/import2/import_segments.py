#!/usr/bin/env python
# encoding: utf-8

from glob import glob
from optparse import OptionParser
from os.path import basename, join
from parse import *
from sys import exit
from tables import *

def lookup_type(tp):
	type_map = TypeMap('types.tbl')
	
	try:
		type_id = type_map.get_id(tp)
	except KeyError:
		exit('Invalid type name: %s' % tp)
	
	return type_id

def main():
	parser = OptionParser(usage='%prog LOCL_FILE...')
	parser.add_option('-t', '--type', dest='type', default='hsp', help='segment type (default: hsp)')
	options, args = parser.parse_args()
	
	if len(args) < 1:
		exit('Unexpected argument number.')
	
	chr_pair_table = ChrPairTable('chr_pairs.tbl')
	locl_table = LoclTable('locls.tbl')
	segment_table = SegmentTable('segments.tbl', lookup_type(options.type))
	
	for filename in args:
		chr1, chr2 = parse_locl_filename(basename(filename))
		chr_pair_id = chr_pair_table.add(chr1, chr2)
		
		class SegmentScanner(LoclScanner):
			def process_label(self, label):
				coords = parse_locl_label(label, self.filename, self.lineno+1)
				self.locl_id = locl_table.add(chr_pair_id, *coords)
			
			def process_line(self, line):
				segment_table.add(self.locl_id, *parse_segment(line, self.filename, self.lineno+1))
		
		SegmentScanner().scan(filename)

if __name__ == "__main__":
    main()
