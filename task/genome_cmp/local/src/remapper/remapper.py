#!/usr/bin/env python
# encoding: utf-8

from optparse import OptionParser
from os.path import isfile
from sys import exit, stdin

class Map(object):
	def __init__(self, map_path):
		self.regions = self._load_map(map_path)
		self.last_region_idx = 0
		self.transform = self._transform_fast
	
	def _transform_simple(self, start, stop):
		for region_start, region_stop, offset in self.regions:
			if region_stop >= start:
				break
		
		if start >= region_start and stop <= region_stop:
			return start + offset, stop + offset
		else:
			return None
	
	def _transform_fast(self, start, stop):
		region_start, region_stop, offset = self.regions[self.last_region_idx]
		
		while region_start > stop:
			if self.last_region_idx != 0:
				self.last_region_idx -= 1
				region_start, region_stop, offset = self.regions[self.last_region_idx]
			else:
				return None
		
		while region_stop < start:
			if self.last_region_idx != len(self.regions) - 1:
				self.last_region_idx += 1
				region_start, region_stop, offset = self.regions[self.last_region_idx]
			else:
				return None
		
		if start >= region_start and stop < region_stop:
			return start + offset, stop + offset
		else:
			return None
	
	def _load_map(self, path):
		if not isfile(path):
			raise ValueError, 'invalid map file: ' + path
		
		fd = file(path, 'r')
		last_start = -1
		last_stop = -1
		regions = []
		try:
			fd.readline() #skip chromosome sizes
			for lineno, line in enumerate(fd):
				line = line.rstrip()
				if len(line) == 0:
					continue
				
				tokens = line.split('\t')
				if len(tokens) != 4:
					raise ValueError, 'malformed line %d in file %s' % (lineno+1, path)
				
				try:
					boundaries = tuple(int(t) for t in tokens)
				except ValueError:
					raise ValueError, 'malformed boundaries at line %d in file %s' % (lineno+1, path)
				else:
					start = boundaries[0]
					stop = boundaries[1]
					offset = boundaries[2] - start
					
					#TODO: re-enable this check
					if stop < start or start < last_start or stop < last_stop:
						raise ValueError, 'malformed boundaries at line %d in file %s' % (lineno+1, path)
					else:
						last_start = start
						last_stop = stop
						regions.append((start, stop, offset))
		finally:
			fd.close()
		
		if len(regions) == 0:
			raise ValueError, 'empty map file ' + path
		else:
			return regions

def parse_map_definition(definition):
	tokens = definition.strip().split(':')
	if len(tokens) != 3:
		raise ValueError, 'invalid map definition: ' + definition
	
	try:
		start_col = int(tokens[1])
	except ValueError:
		raise ValueError, 'invalid start column: ' + token[1]
	
	try:
		stop_col = int(tokens[2])
	except ValueError:
		raise ValueError, 'invalid stop column: ' + token[2]
	
	return tokens[0], start_col, stop_col

def load_maps(definitions):
	used_columns = set()
	specs = []
	
	for definition in definitions:
		spec = parse_map_definition(definition)
		
		for column in spec[1:3]:
			if column in used_columns:
				raise ValueError, 'column %d is specified twice' % column
			else:
				used_columns.add(column)
		
		specs.append(spec)
	
	return [ (Map(s[0]), s[1], s[2]) for s in specs ]

def main():
	parser = OptionParser(usage='%prog MAP:START_COL:STOP_COL... <INPUT')
	parser.add_option('-u', '--unmapped', dest='unmapped', help='write unmapped rows to FILE', metavar='FILE')
	options, args = parser.parse_args()

	if len(args) < 1:
		exit('Unexpected argument number.')
	
	map_specs = load_maps(args)
	if options.unmapped is not None:
		unmapped_fd = file(options.unmapped, 'w')
	else:
		unmapped_fd = None
	
	for lineno, line in enumerate(stdin):
		line = line.rstrip()
		if len(line) == 0:
			continue
		
		columns = line.split('\t')
		for map, start_col, stop_col in map_specs:
			try:
				start_boundary = int(columns[start_col])
				stop_boundary = int(columns[stop_col])
			except (IndexError, ValueError):
				raise ValueError, 'malformed input line %d' % (lineno+1)
			
			res = map.transform(start_boundary, stop_boundary)
			if res != None:
				columns[start_col] = str(res[0])
				columns[stop_col] = str(res[1])
			else:
				if unmapped_fd is not None:
					print >>unmapped_fd, line
				break
		else:
			print '\t'.join(columns)

if __name__ == '__main__':
	main()
