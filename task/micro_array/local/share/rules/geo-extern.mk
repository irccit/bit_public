# Copyright 2010 Gabriele Sales <gbrsales@gmail.com>
# Copyright 2010 Paolo Martini <paolo.cavei@gmail.com>

GEO_EXTERN_ROOT := $(BIOINFO_ROOT)/task/micro_array/dataset/geo

define geo-extern-dir
$(GEO_EXTERN_ROOT)/$1
endef

define geo-extern-gsm
$(eval $(call geo-extern-gsm-body,$1,$2,$3,soft))
endef

define geo-extern-gsm-suppl
$(eval $(call geo-extern-gsm-body,$1,$2,$3,suppl))
endef

define geo-extern-gsm-body
$(call __bmake_extern,$(GEO_EXTERN_ROOT)/$1,)
$(foreach m,$2,$(call __bmake_extern,$(GEO_EXTERN_ROOT)/$1/$m.$4.gz,))
$3 := $(foreach m,$2,$(GEO_EXTERN_ROOT)/$1/$m.$4.gz)
endef