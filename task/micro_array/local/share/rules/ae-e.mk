# Copyright 2010 Gabriele Sales <gbrsales@gmail.com>
# Copyright 2010 Enrica Calura <enrica.calura@gmail.com>


define archive_body
$1: ; wget -O $1 '$2'
ALL_ARCHIVES += $1
endef

define archive
$(eval $(call archive_body,$1,$2))
endef

define experiment_body
$1: $2
ALL_EXPERIMENTS += $1
endef

define experiment
$(eval $(call experiment_body,$1,$2))
endef

sdrf:
	e="$$(basename $$PWD)"; \
	wget -O $@ 'http://www.ebi.ac.uk/arrayexpress/files/'$$e'/'$$e'.sdrf.txt'

e.mk: sdrf
	sdrf2mk <$< >$@
include e.mk


$(ALL_EXPERIMENTS):
	unzip $< $@

echo.experiments:
	@echo '$(ALL_EXPERIMENTS)'


ALL   += sdrf $(ALL_EXPERIMENTS)
CLEAN += e.mk $(ALL_ARCHIVES)
