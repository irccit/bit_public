# Copyright 2010 Gabriele Sales <gbrsales@gmail.com>
# Copyright 2010 Enrica Calura <enrica.calura@gmail.com>

E-%:
	mkdir $@; \
	cd $@; \
	ln -s ../makefile; \
	link_install $(TASK_ROOT)/local/share/rules/ae-e.mk rules.mk
