# Copyright 2010 Gabriele Sales <gbrsales@gmail.com>
# Copyright 2010 Enrica Calura <enrica.calura@gmail.com>

AE_EXTERN_ROOT := $(BIOINFO_ROOT)/task/micro_array/dataset/ae


# The full path where an AE dataset is stored.
# $1: the ID of the AE dataset
define ae-extern-dir
$(AE_EXTERN_ROOT)/$1
endef

# Given the ID of an AE dataset ($1) and a space-separated list
# of experiment names ($2), defines a variable $3 containing
# the full paths of the files corresponding to those experiments.
define ae-extern-experiments
$(eval $(call ae-extern-experiments-body,$1,$2,$3))
endef

define ae-extern-experiments-body
$(call __bmake_extern,$(AE_EXTERN_ROOT)/$1,)
$(foreach m,$2,$(call __bmake_extern,$(AE_EXTERN_ROOT)/$1/$m,))
$3 := $(foreach m,$2,$(AE_EXTERN_ROOT)/$1/$m)
endef
