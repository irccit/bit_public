# Copyright 2010 Gabriele Sales <gbrsales@gmail.com>
# Copyright 2010 Paolo Martini <paolo.cavei@gmail.com>

CLEAN        += gse.mk
INTERMEDIATE += gse.brief

gse.brief:
	gse="$$(basename $$PWD)"; \
	R --vanilla <<<"library(GEOquery); \
	                f<-getGEOfile(\"$$gse\", destdir=getwd(), amount=\"brief\");\
                        file.rename(f, \"$@\")" >&/dev/null

gse.mk: gse.brief
	( \
		echo -ne "GSM_LIST:="; \
		grep '^!Series_sample_id' $< \
		| sed 's|.*=[ \t]*||' \
		| tr -d "\r" \
		| tr "\n" " " \
	) >$@
include gse.mk


%.soft:
	R --vanilla <<<"library(GEOquery); getGEOfile(\"$*\", destdir=getwd())"

%.soft.gz: %.soft
	gzip <$< >$@

%.suppl.gz:
	geo_download_suppl $*

echo.gsm:
	@echo "$(GSM_LIST)"

suppl.download: $(addsuffix .suppl.gz,$(GSM_LIST))


ALL += $(addsuffix .soft.gz,$(GSM_LIST))
.PHONY: echo.gsm suppl.download