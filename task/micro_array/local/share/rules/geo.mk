# Copyright 2010 Gabriele Sales <gbrsales@gmail.com>
# Copyright 2010 Paolo Martini <paolo.cavei@gmail.com>

GSE%:
	mkdir GSE$*; \
	cd GSE$*; \
	ln -s ../makefile; \
	link_install $(TASK_ROOT)/local/share/rules/geo-gse.mk rules.mk
