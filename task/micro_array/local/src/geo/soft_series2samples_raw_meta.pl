#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\n";
$\="\n";

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 [-h|help] RAW_META_FILE]\n";

my $help=0;
my $glue=",";
GetOptions (
	'h|help' => \$help,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

my @rows=();
my $header=undef;
while(<>){
	chomp;
	next if length==0 ;
	if(/^\^/){
		my $post_header=$_;
		&proces_sample() if defined $header; #al primo giro non ho ancora nulla
		$header=$post_header;
		@rows=();
		next;
	}
	push @rows, $_ if m/^!/ or m/^#/;
}
&proces_sample();

sub proces_sample
{
	return if not $header=~m/^\^SAMPLE\s+=\s+(.*)/;
	my $s=$1;
	print ">$1";
	print @rows;
}
