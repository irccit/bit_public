#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 [-h|help]\n";

my $help=0;
my $glue=",";
GetOptions (
	'h|help' => \$help,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}


my %samples=();
my %probesets=();
my $m={};

my @rows=();
my $header=undef;
while(<>){
	chomp;
	next if length==0 ;
	if(/^>/){
		my $post_header=$_;
		&proces_sample() if defined $header; #al primo giro non ho ancora nulla
		$header=$post_header;
		@rows=();
		next;
	}
	push @rows, $_;
}
&proces_sample();

exit(0);
my @samples_array = keys(%samples);

print "," . join("\t",@samples_array). "\n";
foreach my $p (keys %probesets){
	print $p;
	foreach my $s (@samples_array){
		print "\t",$m->{$s}->{$p}
	}
	print "\n"
}

sub proces_sample
{
	return if not $header=~m/^>SAMPLE\s+=\s+(.*)/;
	print ">$1\n";
	#$samples{$s}=1;
	for(@rows){
		if(m/^!/ or m/^#/ or m/^ID_REF\s+VALUE\s*$/){
			next;
		}else{
			if(not m/^([-\w\/]+)\s+((?:\d+)?\.?\d*)/){
				die("Malformed input ($_).")
			}
			#$probesets{$1} = 1;
			#$m->{$s}->{$1} = $2;
			print "$1\t$2\n";
		}
	}
}
