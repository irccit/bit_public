#!/usr/bin/env bRscript
#
# Copyright 2010-2011 Gabriele Sales <gbrsales@gmail.com>
# Copyright 2010-2011 Davide Risso <risso.davide@gmail.com>
# Copyright 2010-2011 Enrica Calura <enrica.calura@gmail.com>
# Copyright 2014 Paolo Martini <paolo.cavei@gmail.com>
#
# This file is part of BioinfoTree. It is licensed under the
# terms of the GNU Affero General Public License version 3.


# Needed to suppress import messages
# in normalizeVSN calls.
import(vsn)

import(optparse)
import(impute)
import(limma)
import(pcaMethods)

checkDuplicateColumns <- function (data) {
  for (i in 1:(NCOL(data)-1)) {
    for (j in (i+1):NCOL(data)) {
      if (identical(data[,i], data[,j])) {
        sample.names <- colnames(data)
        error(paste("Found two samples with perfectly equal values:", sample.names[i], "and", sample.names[j], sep=" "))
      }
    }
  }
}

imputeData <- function (data,imputation=c("lls","knn","svd")) {
  imputation <- match.arg(imputation)
  switch(imputation,
         lls = {
	   if(!any(is.na(data))) {
	     data
           } else {
             lls <- llsImpute(t(data))
             t(completeObs(lls))
           }
         },
         knn = {
           sink("/dev/null")
           knn <- impute.knn(data,k=5)$data
           sink()
           knn
         },
         svd = {
           svd <- pca(t(data),method="svd",scale="pareto")
           t(completeObs(svd))
         })
}

prepareData <- function (data, makeLog, impute) {
  if (makeLog) {
    if (length(which(data<=0))) {
      warning("Matrix contains 0 values. NA's introduced.")
      data[data<=0] <- NA
    }
    data <- log2(data)
  }
  if (impute) {
    if (NROW(data) < 5)
      error("Insufficient rows to perform imputation.")
    data <- imputeData(data,opts$impute)
  }
  data
}

prepareOutput <- function (data, isLogged, needLog) {
  if (isLogged & !needLog) {
    2^data
  } else if (!isLogged & needLog) {
    log2(data)
  } else {
    data
  }
}


opt_spec <- list(make_option(c("-m", "--method"), help="The normalization method to be used. This should be one of quantile, loess, vsn"),
                 make_option(c("-l", "--input-log"), action="store_true", type="logical", default=FALSE, help="Assume log input"),
                 make_option(c("-o", "--output-log"), action="store_true", type="logical", default=FALSE, help="Output log-expression"),
                 make_option(c("-i", "--impute"), action="store", type="character", help="Missing data imputation using either 'lls', 'knn' or 'svd'."),
                 make_option(c("-w", "--which-loess"), action="store", type="character", help="Specify which implementation to use: either 'pairs', 'affy' or 'fast'.")
                 )
usage = paste(
    "%prog [OPTIONS] <RAW_EXPRESSION >NORM_EXPRESSION",
    "",
    "Normalize the expression values of a single channel microarray set of experiments.",
    "",
    "The input matrix must have row and column names.","","The supported normalization methods are:",
    "* quantile",
    "* loess (cyclic)",
    "* vsn",
    "",
    "If --impute the missing value are imputed considering one of the following algorithms:",
    "* lls: local least squares",
    "* knn: K nearest neighbours",
    "* svd: singular value decomposition.",
    "Missing values should be specified by NA. Note that loess normalization does not deal with missing values. Therefore, when loess is used and missing values are present, imputation is mandatory.",
    "",
    "The --which-loess option specifies which implementation to use:",
    "* pairs: implements the intuitive idea that each pair of arrays is subjected to loess normalization as for two-color arrays (original limma)",
    "* affy: implements a method similar to 'normalize.loess' in the affy package (default)",
    "* fast: implements the 'fast linear loess' method of Ballman et al (2004), whereby each array is simply normalized to a reference array,
the reference array being the average of all the arrays.",
    sep="\n")

parser   <- OptionParser(usage=usage, option_list=opt_spec)
parsed   <- parse_args(parser, positional_arguments=TRUE)
opts     <- parsed$options
args     <- parsed$args

if (is.null(opts$method)) {
  error("Missing normalization method.")
} else if (!(opts$method %in% c("quantile", "loess", "vsn"))) {
  error(paste("Unknown normalization method:", opts$method, sep=" "))
} else if (opts$method == "vsn" & (opts$"input-log" | opts$"output-log")) {
  error("VSN normalization has to be applied to non-logged expression values and returns expression values on the logaritmic scale")
}

if(!is.null(opts$impute)) {
  if (!(opts$impute %in% c("lls","knn","svd"))) {
    error(paste("Unknown imputation method:", opts$impute, sep=" "))
  }
}

if (opts$method == "loess" & is.null(opts$"which-loess")) {
   opts$"which-loess" <- "affy" 
   } else if (opts$method == "loess" & !is.null(opts$"which-loess")) {
     if (!(opts$"which-loess" %in% c("pairs","affy","fast"))) {
     	error(paste("Unknown loess implementation:", opts$"which-loess", sep=" "))
	}
}

if (opts$method != "loess" & !is.null(opts$"which-loess")) {
  error("The 'which-loess' option is used only when method is 'loess'")
}


data <- coerceNumericMatrix(bread(header=TRUE, row.names=1),
                            allowNA=TRUE)
checkDuplicateColumns(data)

dataLogged <- opts$"input-log"

if (opts$method=="quantile") {
  data <- prepareData(data, FALSE, !is.null(opts$impute))
  dataNorm <- normalizeQuantiles(data)
} else if (opts$method=="loess") {
  if (is.null(opts$impute)) {
    if(any(is.na(data)) | (!dataLogged & any(data<=0))) {
      error("Loess cannot deal with NA values. Consider to impute missing values.")
    }
  }
  data <- prepareData(data, !dataLogged, !is.null(opts$impute))
  dataLogged <- TRUE
  dataNorm <- normalizeCyclicLoess(data,method=opts$"which-loess")
} else if (opts$method=="vsn") {
  data <- prepareData(data, FALSE, !is.null(opts$impute))
  dataNorm <- normalizeVSN(data)
}

bwrite(prepareOutput(dataNorm, dataLogged, opts$"output-log"), file=stdout())
