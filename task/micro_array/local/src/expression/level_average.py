#!/usr/bin/env python
#
# Copyright 2008,2010 Gabriele Sales <gbrsales@gmail.com>

from __future__ import division
from collections import defaultdict
from optparse import OptionParser
from math import isnan
from sys import stdin
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage

nan = float('nan')

def average_levels(levels, probe):
	if len(levels) == 1:
		return levels[0]
	
	m = len(levels[0])
	for l in levels[1:]:
		if len(l) != m:
			exit('Mismatch in the number of samples for probe: ' + probe)

	res = []
	for i in xrange(m):
		column = [ l[i] for l in levels if not isnan(l[i]) ]
		if len(column) == 0:
			res.append(nan)
		else:
			res.append(sum(l for l in column) / len(column))

	return res

def parse_levels(values):
	res = [None] * len(values)
	for i, v in enumerate(values):
		try:
			res[i] = float(v)
		except ValueError:
			if v.lower() == 'na':
				res[i] = nan
			else:
				exit('Invalid expression value: ' + v)
	return res


def main():
	parser = OptionParser(usage=format_usage('''
		%prog <LEVELS >LEVELS

		Reads an expression matrix and computes the average level for each probe
		for each sample.

		Output is sorted by probe name.
	'''))
	parser.add_option('-w', '--with-header', dest='with_header', action='store_true', default=False, help='treat the first input line as an header')
	options, args = parser.parse_args()

	if len(args) != 0:
		exit('Unexpected argument number.')

	levels = defaultdict(list)
	for line_idx, line in enumerate(stdin):
		if line_idx == 0 and options.with_header:
			print safe_rstrip(line)
			continue

		tokens = safe_rstrip(line).split('\t')

		probe = tokens[0]
		try:
			ls = parse_levels(tokens[1:])
		except ValueError:
			exit('Invalid float value at line %d: %s' % (line_idx+1, safe_rstrip(line)))
		levels[probe].append(ls)
	
	probes = levels.keys()
	probes.sort()
	for probe in probes:
		avg = average_levels(levels[probe], probe)
		print '%s\t%s' % (probe, '\t'.join(str(t) for t in avg))

if __name__ == '__main__':
	main()
