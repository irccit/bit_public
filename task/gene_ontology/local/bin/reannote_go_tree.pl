#! /usr/bin/perl -wd

use strict;
use warnings;
use Getopt::Long;

$,="\t";
$\="\n";

my $usage = "
reannote_go_tree.pl -f graph_path -go biol_process.gene_term 
";

GetOptions (
	'graph_path_file|f=s' => \$graph_path,
	'gene_term_file|go=s' => \$gene_term,
) or die($usage);
die($usage) if (!$graph_path);
die($usage) if (!$gene_term);


open GRAPH_PATH, $graph_path or die "Can't open file ($graph_path)\n";

my %tree=();
while (<GRAPH_PATH>) {
	chomp;
	my ($father, $child, $distance) = split;
	$tree{$child}{$father}=1;
}



open GENE_TERM, $gene_term or die "Can't open file ($gene_term)\n";

my %old_words=();
while (<GENE_TERM>) {
	chomp;
	my ($gene, $go_word) = split;
	if (!defined $old_words{$gene}) {
		my @tmp = ($go_word);
		$old_words{$gene} = \@tmp;
	} else {
		push @{$old_words{$gene}},$go_word;
	} 
}


my %new_words=();
foreach my $ensg (keys %old_words) {
	my @aux = @{$old_words{$ensg}};
	foreach my $go_word (@aux) {
		if (!defined $new_words{$ensg}) {
			my @tmp = ($go_word);
			$new_words{$ensg} = \@tmp;
		} else {
			push @{$new_words{$ensg}},keys %{$tree{$go_word}};
		}
	}
}



foreach (keys %new_words) {
	print $_.join (/\n$_/,@{$new_words{$_}})."\n";
}
