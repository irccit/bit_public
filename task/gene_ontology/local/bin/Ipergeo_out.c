# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <math.h>



float sum_log(long n,int estr)
{
	int i;
	float temp;

	if (n>=estr)
	{
		temp=0;
		for (i=1;i<=estr;i++)
		{
			temp=temp+log(n-i+1);
		}
		return (temp);
	}
	else
	{
		printf ("\n\tERRORE !!\tn= %d\testr= %d\n\n",n,estr);
		exit(1);
	}
}


main(int argc, char *argv[])
{
	long N,M,n2;
	int n,x,M2,max_pos;
	int h;
	float sum,a,b,c,d,e,log_F,F;
	char out[100];
	char first_err;




	//printf ("\n%s\n","########################################################################");
	//printf ("%s\n","# Dato un insieme con N elementi e un suo sottoinsieme con M elementi, #");
	//printf ("%s\n","# il programma calcola la probabilit� che, estraendo casualmente       #");
	//printf ("%s\n","# n elementi, almeno x cadano nel sottoinsieme con M elementi.         #");
	//printf ("%s\n","# Per fare ci� si utilizza la distribuzione ipergeometrica.            #");
	//printf ("%s\n\n","########################################################################");
	
	if (argc != 5)
	{
		printf("Uso : ./Ipergeo_out N M n x  \n\n");
		exit(1);
	}

//	strcpy(out,argv[5]);
//	OUT=fopen(out,"w");

/*	printf ("\tn� di elementi dell'insieme pi� grande (N)              : ");
	scanf ("%d",&N);
	printf ("\tn� di elementi del sottoinsieme (M)                     : ");
	scanf ("%d",&M);
	printf ("\tn� di elementi estratti casualmente (n)                 : ");
	scanf ("%d",&n);
	printf ("\tn� di elementi estratti che cadono nel sottoinsieme (x) : ");
	scanf ("%d",&x);
*/
	N=atol(argv[1]);
	M=atol(argv[2]);
	n=atoi(argv[3]);
	x=atoi(argv[4]);

	if (! ((M>=x) && (N>=M) && (n>=x) && (N>=n)) ) {
		printf ("ERRORE !!\n\tM>=x , N>=M , n>=x , N>=n\n\n");
		exit(1);
	}

	if (n==0) {
		printf ("n deve essere > 0 !!\n\n");
		exit(1);
	}

	if (x!=0) {
	
		
                if (n>M) {
			max_pos=M;
			// il massimo numero di estrazioni positive e' M (il totale di positivi)
		} else {
			max_pos=n;
		}

		sum=0;


		for (h=x;h<=max_pos;h++)
		{
			if (h!=0) {
				if (h!=n) {
					a=sum_log(M,h);
					b=sum_log(N-M,n-h);
					c=sum_log(n,h);
					d=sum_log(h,h);
					e=sum_log(N,n);
					log_F=a+b+c-d-e;
				}
				else {
					a=sum_log(M,n);
					b=sum_log(N,n);
					log_F=a-b;
				}
			}
			else {
				a=sum_log(N-M,n);
				b=sum_log(N,n);
				log_F=a-b;
			}

			F=exp(log_F);
			sum+=F;
		}

		if (sum>1)	/* ci sono errori di arrotondamento */
			sum=1;

	}
	else 
		sum=1;

	printf ("%s%d\t","Tot: ", N);
	printf ("%s%d\t","Tot_positiv: ", M);
	printf ("%s%d\t","Extractions: ", n);
	printf ("%s%d\t","Extractions_positiv: ", x);
	
//	printf ("%s%11.10f\n","Pvalue: ",sum);
	printf ("%s%11.10e\n","Pvalue: ",sum);

//	printf("Probabilit� che, estraendo casualmente n elementi da un insieme di N elementi,\n");
//	printf("almeno x cadano in un sottoinsieme con M elementi.\n\n");
//	printf("\tn� di elementi dell'insieme pi� grande (N)              : %d\n",N);
//	printf("\tn� di elementi del sottoinsieme (M)                     : %d\n",M);
//	printf("\tn� di elementi estratti casualmente (n)                 : %d\n",n);
//	printf("\tn� di elementi estratti che cadono nel sottoinsieme (x) : %d\n\n",x);
//	printf("\n\t%s%11.10f\n\n","Probabilit� : ",sum);
	
//	printf("%11.10f\n",sum);
}



