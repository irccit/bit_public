#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;

$\="\n";
$,="\t";

my $usage = "zcat qualocosa.loc_cl.gz | $0 -c";

my $option_count = undef;
GetOptions(
	'count|c' => \$option_count
) or die $usage;


my @list = ();
my $locl_id = undef;
while (<>){
	chomp;
	if ($_ =~ /^>/){
		if (defined $locl_id) {
			&overlap(\@list,$locl_id);
			@list = ();
		}
		$locl_id = $';
		next;
	}
	
	my @F = split /\t/;
	push @list,\@F;
}

&overlap(\@list,$locl_id) if (defined $locl_id);

sub overlap
{
	my $block_ref = shift;
	my $block_id = shift;

	my $block_len = scalar @{$block_ref};
	my @sorted_blocks = sort {${$b}[8]<=>${$a}[8]} @{$block_ref};
	for (my $i=0; $i<$block_len - 1; $i++) {
		next if (!defined $sorted_blocks[$i]);
		my @hsp1 = @{$sorted_blocks[$i]};
		for (my $j=$i + 1; $j<$block_len; $j++){
			next if (!defined $sorted_blocks[$j]);
			my @hsp2 = @{$sorted_blocks[$j]};
			next if ( ($hsp1[2] < $hsp2[1]) or ($hsp2[2] < $hsp1[1]) );
			next if ( ($hsp1[6] < $hsp2[5]) or ($hsp2[6] < $hsp1[5]) );
			$sorted_blocks[$j] = undef;
		}
	}
	@sorted_blocks = sort {&sort_undef($a,$b)} @sorted_blocks;
	my $left_b = undef;
	my $left_e = undef;
	my $right_b = undef;
	my $right_e = undef;
	for (my $i=0; $i<$block_len; $i++) {
		last if (!defined $sorted_blocks[$i]);
		my @hsp = @{$sorted_blocks[$i]};
		$left_b = $hsp[1] if (!defined $left_b or ($hsp[1] < $left_b)); 
		$left_e = $hsp[2] if (!defined $left_e or ($hsp[2] > $left_e)); 
		$right_b = $hsp[5] if (!defined $right_b or ($hsp[5] < $right_b)); 
		$right_e = $hsp[6] if (!defined $right_e or ($hsp[6] > $right_e)); 
	}
	print ">".$left_b.'_'.$left_e.'_'.$right_b.'_'.$right_e;
	for (my $i=0; $i<$block_len; $i++) {
		last if (!defined $sorted_blocks[$i]);
		print @{$sorted_blocks[$i]};
	}
}


sub sort_undef
{
	my $a = shift;
	my $b = shift;

	return 0 if (!defined $a and !defined $b);
	return 1 if !defined $a;
	return -1 if !defined $b;
	my @c = @{$a};
	my @d = @{$b};
	return 1 if ($c[1]>$d[1]);
	return 0 if ($c[1]==$d[1]);
	return -1;

}
