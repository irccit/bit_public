use warnings;
use strict;
$,="\t";
$\="\n";

sub foreach_fasta
{
	my $id = shift;
	$id =~ s/^>//;
	$_=shift;
	my @rows = @{$_};
	$_=shift;
	my %opt = %{$_};
	
	my $x_min=undef;
	my $x_max=undef;
	my $y_min=undef;
	my $y_max=undef;
	my $chrx =undef;
	my $chry =undef;
	for(@rows){
		my ($chrx_,$xb,$xe,$chry_,$yb,$ye) = split;
		die("ERROR: maybe strand problem?") if $xb >= $xe;
		die("ERROR: in chr column") if defined $chrx and $chrx ne $chrx_ or defined $chry and $chry ne $chry_;
		$chrx = $chrx_;
		$chry = $chry_;

		my $ym = &min($yb,$ye);
		my $yM = &max($yb,$ye);
		
		$x_min = $xb if !defined $x_min or $x_min > $xb;
		$x_max = $xe if !defined $x_max or $x_max < $xe;
		$y_min = $ym if !defined $y_min or $y_min > $ym;
		$y_max = $yM if !defined $y_max or $y_max < $yM;
	}
	print $chrx, $x_min, $x_max, $chry, $y_min, $y_max, $id;
}

sub max
{
	return $_[0] > $_[1] ? $_[0] : $_[1];
}

sub min
{
	return $_[0] < $_[1] ? $_[0] : $_[1];
}

1;
