#!/usr/bin/env python
#
# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

from __future__ import with_statement
from collections import defaultdict
from optparse import OptionParser
from sys import stdin
from vfork.io.colreader import Reader
from vfork.util import exit, format_usage

def load_cdna_groups(filename):
	groups = defaultdict(set)
	with file(filename, 'r') as fd:
		for grp, cdna in Reader(fd, '0s,1s', False):
			groups[grp].add(cdna)
	
	res = defaultdict(list)
	for grp, cdnas in groups.iteritems():
		for cdna in cdnas:
			res[cdna].append(cdnas)
	
	return res

class TranscriptUniq(object):
	def __init__(self):
		self.processed_oligos = {}
	
	def add(self, oligo, cdna):
		if oligo in self.processed_oligos:
			self.processed_oligos[oligo] = 'degenerate'
		else:
			self.processed_oligos[oligo] = cdna
	
	def uniq(self):
		aux = [ i for i in self.processed_oligos.iteritems() if i[1] != 'degenerate' ]
		aux.sort()
		self.processed_oligos = aux
	
	def iter_oligos(self):
		return self.processed_oligos.__iter__()

class TranscriptGroupUniq(object):
	def __init__(self, groups):
		self.processed_oligos = defaultdict(set)
		self.groups = groups
	
	def add(self, oligo, cdna):
		self.processed_oligos[oligo].add(cdna)
	
	def uniq(self):
		aux = []
		for oligo, cdnas in self.processed_oligos.iteritems():
			if not self._is_degenerate(cdnas):
				aux.append( (oligo, cdnas) )
		
		aux.sort()
		self.processed_oligos = aux
	
	def _is_degenerate(self, cdnas):
		for cdna in cdnas:
			for grp in self.groups.get(cdna, [set([cdna])]):
				if len(cdnas - grp) > 0:
					return True
		return False
	
	def iter_oligos(self):
		for oligo, cdnas in self.processed_oligos:
			for cdna in sorted(cdnas):
				yield oligo, cdna

def main():
	parser = OptionParser(usage=format_usage('''
		%prog [OPTIONS] <OLIGO-TRANSCRIPT.MAP >OLIGO-TRANSCRIPT-NR.MAP
		
		Filters out those transcript that are not specific to a single oligo.
		Input is not required to be sorted.
		
		If the -g option is used, the software does not filter out transcripts of the
		same group even if they match the same oligo.
	'''))
	parser.add_option('-g', '--transcript-groups', dest='cdna_groups', help='a map associating groups to transcripts', metavar='GROUP-TRANSCRIPT.MAP')
	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')
	
	if options.cdna_groups:
		u = TranscriptGroupUniq(load_cdna_groups(options.cdna_groups))
	else:
		u = TranscriptUniq()
	
	for oligo, cdna in Reader(stdin, '0s,1s', False):
		u.add(oligo, cdna)
	u.uniq()
	
	for oligo, cdna in u.iter_oligos():
		print '%s\t%s' % (oligo, cdna)

if __name__ == '__main__':
	main()
