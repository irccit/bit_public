# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

TRANSCRIPT_GROUPS ?= 

oligo-transcript-nr.map: oligo-transcript.map $(TRANSCRIPT_GROUPS)
	uniq-oligo-transcript -g $^2 <$< \
	| collapsesets 2 \
	| sort -S40% >$@

.META: oligo-transcript-nr.map
	1  oligo_ID       M000002_01
	2  transcript_IDs ENSMUST00000000962;ENSMUST00000000080
