#!/usr/bin/perl
use strict;
use warnings;
$,="\t";
$\="\n";

my $usage="usage: $0 homology_sp1_sp2 upstream_sp1 upstream_sp2\n";

my $filename=shift @ARGV;
die($usage) if !defined($filename);
open HOM, $filename or die("Can't open filename ($filename)");

$filename=shift @ARGV;
die($usage) if !defined($filename);
open SP1, $filename or die("Can't open filename ($filename)");

$filename=shift @ARGV;
die($usage) if !defined($filename);
open SP2, $filename or die("Can't open filename ($filename)");

my %hom;
while(<HOM>){
	chomp;
	my ($id_sp1,$id_sp2) = split;
	$hom{$id_sp1}=$id_sp2;
}

my %sp1;
while(<SP1>){
	chomp;
	my @F = split;
	$sp1{$F[0]}=$_;
}

my %sp2;
while(<SP2>){
	chomp;
	my @F = split;
	$sp2{$F[0]}=$_;
}

foreach(keys %sp1){
	print $sp1{$_},$sp2{$hom{$_}} if defined($hom{$_}) and defined($sp2{$hom{$_}});
}
