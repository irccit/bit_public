#!/usr/bin/perl -w
## $ARGV[0] -> Exons annotation file (xxx_all_exons_ens42.txt)
## $ARGV[1] -> Macroregions coordinates file 

use warnings;
use strict;

use Bit::Vector;
use DBI ;

#use lib '/wrk04/martigne/upstream_work/upstream_ortologhi/Ensembl_perl_api_v31/ensembl/modules/';     # Load the Ensembl perl API
#use lib '/wrk04/martigne/upstream_work/upstream_ortologhi/Ensembl_perl_api_v31/bioperl-live/' ;	# Load bioperl

use Bio::EnsEMBL::DBSQL::DBAdaptor;
use Bio::Seq;
use Bio::SeqIO;

#####################################
##Configure Ensembl query parameters

my $sth ;	# mysql statmemt handle
my $dbh ;	# mysql database handle

my $query = '' ;
my $array_ref = '' ;

my $host   = 'ensembldb.ensembl.org';
my $user   = 'anonymous';
my $dbname = 'homo_sapiens_core_42_36d';
#my $dbname = 'mus_musculus_core_42_36c';

	
my $db = new Bio::EnsEMBL::DBSQL::DBAdaptor(	-host   => $host,
               	                   	 	-user   => $user,
                       	            		-dbname => $dbname);
my $slice_adaptor = $db->get_SliceAdaptor();

	
#############################################
## Define chr list

open IN_CHR, "awk '{print \$1}' $ARGV[0] | sort -u |" or die ("Cant open pipe chr\n");

my @chr_list=();

while (my $line=<IN_CHR>)
{
	chomp($line);
	push @chr_list, $line; 

}	

close IN_CHR;

################################################

for my $i (0 .. $#chr_list)
{
	my $chromo = Bit::Vector->new(500000000);
	
	################################################
	### Creating Bit vector for each chromosome with coding exons positions ON

	open IN_EX, "grep -P \"^$chr_list[$i]\\t\" $ARGV[0] |" or die ("Cant open file in exons\n");
	
	while (my $line=<IN_EX>)
	{
		chomp($line);
	        my @tmp= split /\t/, $line;
		
		if ($#tmp > 6)
		{
			my $start = $tmp[7];
			my $stop = $tmp[8];
			
			$chromo->Interval_Fill($start,$stop);
    		}
	
	}
	close(IN_EX);

	################################################
	## Define macroregions list for each chromosome

	open IN_MAC, "grep -P \"\t$chr_list[$i]\\t\" $ARGV[1] | awk '{print \$1\"\\t\"\$3\"\\t\"\$4}' |" or die ("Cant open file in macroregions\n");
	
	my %macroregions_list=();
	
	while ( my $line=<IN_MAC>)
	{
		chomp($line);

		my @tmp = split /\t/, $line;

		my $start = $tmp[1];
		my $stop = $tmp[2];
		
		$macroregions_list{$line} =[$start,$stop]; 
	}	

	close IN_MAC;
	
	foreach my $k (keys(%macroregions_list))
	{
		
		##################################################################
		## Creating Bit vector for each macroregion with macroregion position ON
	
		my $macroregion = Bit::Vector->new(500000000);
		my $intersection = Bit::Vector->new(500000000);
		
		my $start = $macroregions_list{$k}[0]; 
		my $stop = $macroregions_list{$k}[1]; 

		$macroregion->Interval_Fill($start,$stop);
	
		print STDOUT "$k\t$chr_list[$i]\n";

		##################################################################
		## Intersection macroregion AND coding exons
		
		$intersection->Intersection($chromo,$macroregion);
		
		## Compute @start, @stop intersection ON
		my $begin=0;
		my @start_exon=();
		my @stop_exon=();
		
		
		while (($begin < $intersection->Size()) && ((my $min, my $max) = $intersection->Interval_Scan_inc($begin)))
    		{
					
			push @start_exon, $min;
			push @stop_exon, $max;		

		       	$begin = $max + 2;
		}
		
		
		##################################################################
		# It extracts coding-exons-masked macroregion sequence from Repeat-masked chromosome 
	
		
		if ($#start_exon != -1)
		{
		
			my $slice = '' ;
			my $sequence = '';
			my $masked_sequence = '';
		
					
			$slice = $slice_adaptor->fetch_by_region( 'chromosome', $chr_list[$i] , $start+1, $stop );
			$sequence = $slice->seq();
			$masked_sequence = $slice->get_repeatmasked_seq()->seq();
		
			for my $n (0 .. $#start_exon)
			{
				my $length_exon = $stop_exon[$n] - $start_exon[$n]+1;
				my $exon = 'E'x $length_exon;
				my $q = $start_exon[$n] - $start-1;
				substr($masked_sequence,$q,$length_exon,$exon);
				
			}
			write_seq_as_fasta($masked_sequence,80);
					
		}
		else
		{
			my $slice = '' ;
			my $sequence = '';
			my $masked_sequence = '';
		
			$slice = $slice_adaptor->fetch_by_region( 'chromosome', $chr_list[$i] , $start+1, $stop );
			$sequence = $slice->seq();
			$masked_sequence = $slice->get_repeatmasked_seq()->seq();
			
			write_seq_as_fasta($masked_sequence,80);
		
		}
			
	}
	
}

sub write_seq_as_fasta{

        my ( $sequence , $length ) = @_ ;

        for ( my $pos = 0 ; $pos < length( $sequence ) ; $pos += $length )
	{
                print STDOUT substr( $sequence , $pos, $length ) , "\n" ;
        }

        return ;

}
