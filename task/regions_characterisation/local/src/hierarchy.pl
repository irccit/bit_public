#!/usr/bin/perl

use warnings;
use strict;
$,="\t";
$\="\n";

my $usage="$0 CUTOFF < blast_file";
my $cutoff = shift;
die if $cutoff > 1;

while(<>){
	chomp;
	my ($id1, $l1, $b1, $e1, $strand, $id2, $l2, $b2, $e2, $lunghezza, $score, $pident, $evalue ) = split;
	next if $id1 >= $id2;

	if($l1 < $l2){
		print $id2, $id1 if ($e1-$b1) / $l1 > $cutoff;
	}else{
		print $id1, $id2 if ($e2-$b2) / $l2 > $cutoff;
	}
}
