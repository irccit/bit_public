#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;


my $usage = "$0 -c 0.9 [-t] < qualcosa.consensus_matrix
	-c cutoff on letter frequency
	-t if file in STDIN is already transposed";

my $cutoff = undef;
my $transpose = undef;
GetOptions (
	'cutoff|c=f' => \$cutoff,
	'transp|t'   => \$transpose
) or die ($usage);
die $usage if !defined $cutoff;


my $FH=undef;
if (!$transpose) {
	my $consensus_file = shift @ARGV;
	die $usage if (!defined $consensus_file);
	open $FH, "transpose $consensus_file |" or die "Can't open file ($consensus_file)";
} else {
	$FH=*STDIN;
}


chomp (my $intest = <$FH>);
my @letters = split /\t/,$intest;
my $l = scalar @letters;

my $seq = undef;
my $colora = undef;
while (my $line = <$FH>) {
	chomp $line;
	my @F = split /\t/,$line;

	my $max = 0;
	my $idx = 0;
	for (my $i=0; $i<$l; $i++) {
		if ($F[$i] > $max) {
			$max = $F[$i];
			$idx = $i;
		}
	}

	if (!defined $seq) {
		$seq = $letters[$idx];
		$colora = 1 if ($max >= $cutoff);
		next;
	}

	if (defined $colora) {
		if ($max >= $cutoff) {
			$seq .= $letters[$idx];
		} else {
			print "\e[4;40m$seq\e[0m";
			$seq = $letters[$idx];
			$colora = undef;
		}
	} else {
		if ($max < $cutoff) {
			$seq .= $letters[$idx];
		} else {
			print $seq;
			$seq = $letters[$idx];
			$colora = 1;
		}
	}
}

if (defined $seq) {
	if (defined $colora) {
		print "\e[4;40m$seq\e[0m";
	} else {
		print $seq;
	}
}

print "\n";
