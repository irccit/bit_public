include $(BIOINFO_ROOT)/local/share/makefile/global-rules.mk

BASE_URL?="http://cbio.mskcc.org/microrna_data/"
VERSION?=sept2008
SPECIES?=hsapiens

SPECIES_MAP:=$(BIOINFO_ROOT)/task/mirna/local/share/microrna_org-species.map

all: targets.gz mirna_mrna.map
clean:
	rm -f predictions.txt.gz
clean.full:
	rm -f targets.gz mirna_mrna.map

%.txt.gz:
	wget -O - "$(BASE_URL)/$$(translate $(SPECIES_MAP) 1 <<<"$(SPECIES)")_$*_$(VERSION).txt.gz" >$@

.INTERMEDIATE: predictions.txt.gz
targets.gz: predictions.txt.gz
	mv $< $@

.META: targets.gz
	1   UCSC_ID          uc001abs.1
	2   mRNA             AK091100
	3   gene_ID          LOC643837
	4   mirna_acc        MIMAT0000062
	5   miRNA            hsa-let-7a
	6   miRNA_align      uuGAUAUGUUGGAUGAUGGAGu
	7   alignment        ||:  || |:| ||:||||
	8   gene_align       guCUGCUCACCUUCCUGCCUCa
	9   align_score      144
	10  conservation     0.615759
	11  miRNA_start      2
	12  miRNA_end        21
	13  gene_start       396
	14  gene_end         417	
	15  %ID              63
	16  %Similar         78
	17  energy           0
	18  organism         9606
	19  prediction_date  2008-05-16

mirna_mrna.map: targets.gz
	zcat $< \
	| bawk '{ print $$5,$$1 }' \
	| collapsesets 2 >$@

.META: mirna_mrna.map
	1  miRNA     hsa-let-7a
	2  mRNA      AK091100;...
