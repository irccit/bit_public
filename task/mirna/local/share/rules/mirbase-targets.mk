# Copyright 2009 Gabriele Sales <gbrsales@gmail.com>

SPECIES               ?= hsapiens
TARGETS_VERSION       ?= 5
TARGETS_BASE_URL      ?= ftp://ftp.sanger.ac.uk/pub/mirbase/targets/v$(TARGET_VERSION)/arch.v$(TARGET_VERSION).txt

SEQUENCES_SPECIES_MAP := $(TASK_ROOT)/local/share/mirbase_sequence-species.map
TARGET_SPECIES_MAP    := $(TASK_ROOT)/local/share/mirbase_target-species.map


ALL += targets.gz mirna_mrna.map.gz

INTERMEDIATE += targets.txt.download
targets.txt.download:
	wget -O $@ "$(TARGETS_BASE_URL).$$(translate $(TARGET_SPECIES_MAP) 1 <<<"$(SPECIES)").zip"

targets.gz: targets.txt.download
	unzip -p $< \
	| unhead -n 5 \
	| grep -F "$$(translate $(SEQUENCES_SPECIES_MAP) 1 <<<"$(SPECIES)")-" \
	| bawk '$$5 !~ /_/ {$$2=tolower($$2); $$6-=1; print}' \
	| gzip >$@

.META: targets.gz
	1   group          Similarity
	2   mirna_ID       mmu-mir-707
	3   method         miRanda
	4   feature        mirna_mrna
	5   chr            2
	6   start          120824620
	7   stop           120824640
	8   strand         +
	9   phase          .
	10  score          15.3548
	11  pvalue         2.796540e-02
	12  transcript_ID  ENST00000295228
	13  external_name  INHBB

mirna_mrna.map.gz: targets.gz
	zcat $< \
	| cut -f 2,12 \
	| collapsesets 2 \
	| gzip >$@

.META: mirna_mrna.map.gz
	1  miRNA          hsa-let-7a
	2  transcript_ID  ENST00000295228
