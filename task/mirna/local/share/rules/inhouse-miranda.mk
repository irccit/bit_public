# Copyright 2008-2009 Gabriele Sales <gbrsales@gmail.com>

SCORE_CUTOFF   ?= 1000
MIRANDA_FILTER ?= sed 's/>>//;t;d'

import inhouse-prediction-common

define miranda_scan
	+miranda <(get_fasta @$1:$2 <$<) $^3 \
	| $(MIRANDA_FILTER) \
	| gzip >$@
endef

define filter_targets
	bawk '$$3>=$(SCORE_CUTOFF)'
endef

.META: targets.gz
	1   mirna_ID              mmu-let-7g
	2   transcript_ID         ENSMUST00000000080
	3   total_score           108.00
	4   total_energy          -23.42
	5   max_score             108.00
	6   max_energy            -23.42
	7   strand                9
	8   length_on_mirna       22
	9   length_on_transcript  3110
	10  positions             1605
