# Copyright 2009 Gabriele Sales <gbrsales@gmail.com>

SPECIES ?= hsapiens
VERSION ?= 50

BASE_URL       := http://www.targetscan.org/vert_$(VERSION)/vert_$(VERSION)_data_download
SPECIES_TAXON   = $(shell translate $(TASK_ROOT)/local/share/targetscan-species.map 1 <<<"$(SPECIES)")
ALL_TXT        := miR_Family_Info.txt Predicted_Targets_Info.txt
ALL_ZIP        := $(addsuffix .zip,$(ALL_TXT))

ALL += family.gz targets.gz mirna_gene.map.gz

CLEAN += $(ALL_ZIP)
%.zip:
	wget -O$@ '$(BASE_URL)/$@'

INTERMEDIATE += $(ALL_TXT)
%.txt: %.txt.zip
	unzip -p $< \
	| unhead >$@

family.gz: miR_Family_Info.txt
	bawk '$$3=="$(SPECIES_TAXON)" {$$1=tolower($$1);$$4=tolower($$4);print}' $< \
	| cut -f3 --complement \
	| bsort -k1,1 -k3,3 \
	| gzip >$@

.META: family.gz
	1  family             mir-548d-3p
	2  seed               AAAAACC
	3  mirbase_ID         hsa-mir-548d-3p
	4  mature_sequence    CAAAAACCACAGUUUCUUUUGC  
	5  conservation       0
	6  mirbase_accession  MIMAT0003323

targets.gz: Predicted_Targets_Info.txt family.gz
	bawk '$$4=="$(SPECIES_TAXON)" {$$1=tolower($$1);print}' $< \
	| cut -f4 --complement \
	| translate -k -d <(zcat $^2 | cut -f1,3) 1 \   * filter out mirna families lacking a representative in this species *
	| expandsets 1 \
	| bsort -k1,1 -k2,2 \
	| gzip >$@

.META: targets.gz
	1  mirna_ID     hsa-mir-320
	2  gene_ID      201626
	3  gene_symbol  2-PDE
	4  UTR_start    893
	5  UTR_end      899
	6  MSA_start    941
	7  MSA_end      947
	8  seed_match   1A
	9  PCT          0.1764

mirna_gene.map.gz: targets.gz
	zcat $< \
	| cut -f-2 \
	| bsort -u \
	| collapsesets 2 \
	| bsort -k1,1 \
	| gzip >$@

.META: mirna_target.map
	1  mirna_ID  hsa-mir-331
	2  gene_IDs  160622;23291;9256
