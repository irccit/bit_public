# Copyright 2008-2009 Gabriele Sales <gbrsales@gmail.com>

ANNOTATION_DIR     ?= $(BIOINFO_ROOT)/task/annotations/dataset/ensembl/mmusculus/50
SEQUENCE_DIR       ?= $(BIOINFO_ROOT)/task/sequences/dataset/ensembl/mmusculus/50
MIRNA_SEQUENCE_DIR ?= $(BIOINFO_ROOT)/task/mirna/dataset/mirbase/sequences/mmusculus/12.0-5
ANNOTATION_FILE    ?= cdnas.gz

extern $(ANNOTATION_DIR)/$(ANNOTATION_FILE) as CDNA_COORDS
extern $(SEQUENCE_DIR)/cdna.fa as CDNA_FA

ALL   += 3utrs.fa mirnas.fa
CLEAN += 3utrs-delta 3utrs-delta.fa mirnas-delta mirnas-delta.fa 

3utrs.fa: $(CDNA_COORDS) $(CDNA_FA)
	zcat $< \
	| bawk '$$6!="" {print $$1,$$1,$$4,$$6}' \
	| seqlist2fasta_singlefile $^2 >$@

ifeq ($(ALGORITHM),targetscan)
mirnas.fa: $(MIRNA_SEQUENCE_DIR)/seeds.fa.gz
	zcat $< >$@
else
mirnas.fa: $(MIRNA_SEQUENCE_DIR)/sequences.fa.gz
	zcat $< >$@
endif

ifdef INCREMENTAL_REF

REF_UTRS    := $(INSTANCE_ROOT)/../$(INCREMENTAL_REF)/sequences/3utrs.fa
REF_MIRNAS  := $(INSTANCE_ROOT)/../$(INCREMENTAL_REF)/sequences/mirnas.fa
REF_TARGETS := $(INSTANCE_ROOT)/../$(INCREMENTAL_REF)/predictions/targets.gz

ifeq ($(if $(and $(shell ls $(REF_UTRS)),$(shell ls $(REF_MIRNAS)),$(shell ls $(REF_TARGETS))),yes,),yes)

ALL += 3utrs-delta 3utrs-delta.fa mirnas-delta mirnas-delta.fa

3utrs-delta: $(REF_UTRS) 3utrs.fa
	fasta_cmp $^ >$@

mirnas-delta: $(REF_MIRNAS) mirnas.fa
	fasta_cmp $^ >$@

%-delta.fa: %.fa %-delta
	if [[ -s $^2 ]]; then \
		get_fasta -f <(bawk '$$1!="-" {print $$2}' $^2) <$< >$@; \
	else \
		touch $@; \
	fi

endif

endif