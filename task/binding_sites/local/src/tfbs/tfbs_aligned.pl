#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;
use lib $ENV{'BIOINFO_ROOT'}.'/task/binding_sites/local/src/tfbs/';
use Tfbs;

$SIG{__WARN__} = sub {die @_};

our($opt_h, $opt_m, $opt_c, $opt_b, $opt_a, $opt_C, $opt_s);
getopts('hm:c:b:aC:s');

my $usage = <<EOF;
usage tfbs_aligned -m matrix_file {-c cutoff| -C relative_cutoff}  -b background_file aligned_sequence_file (axt format)
 [-a: report only aligned matches]
 [-s use -c and -C at the same time]
EOF

if ($opt_h || ! defined $opt_m || ! defined $opt_b) {
    die $usage;
}
if ( defined $opt_c && defined $opt_C && ! defined $opt_s) {
    die "Options -c and -C are mutually exclusive without -s option\n";
}


my @base = ('A', 'C', 'G', 'T');

my @background = ();
open BKG, $opt_b or die "Error opening $opt_b: $!\n";
my @bkg = <BKG>;
chomp @bkg;
my @check = ();
for (my $i = 0; $i <= 3; ++$i) {
    my @line = split("\t", $bkg[$i]);
    push @check, $line[0];
    push @background, $line[1];
}
for (my $i = 0; $i <= 3; ++$i) {
    if ($check[$i] ne $base[$i]) {
        die "Error in $opt_b: $bkg[$i]\n";
    }
}
close BKG;


my $tot = sum(@background);
foreach (@background) {
    $_ /= $tot;
}

@background = log2(@background);

# load matrices
my %matrix;
open MAT, $opt_m or die "Error opening $opt_m";
while (<MAT>) {
    chomp;
    my @line = split("\t");
    my $mat_id = shift @line;
    my $pos = shift @line;
    ${ $matrix{$mat_id} }[$pos] = join("\t", @line);
}
close MAT;

my %cutoff;
my %best;
foreach my $mat_id (keys %matrix) {
    @{ $matrix{$mat_id} } = normalized(@background,  @{ $matrix{$mat_id} });
    $best{$mat_id} =  best_score( @{ $matrix{$mat_id} } );
    if (defined $opt_C and defined $opt_c) {
	die("error in checking options") if not defined $opt_s;
	$cutoff{$mat_id} = $opt_C * $best{$mat_id};
	if ($cutoff{$mat_id} < $opt_c){
		$cutoff{$mat_id} = $opt_c;
	}
    }elsif (defined $opt_C) {
        $cutoff{$mat_id} = $opt_C * $best{$mat_id};
    }
    else {
        $cutoff{$mat_id} = $opt_c;
    }
}

while (1) {


    my $id_line = <>;
    last unless $id_line;
    next if $id_line =~ /^#/;
    chomp $id_line;
    my ($n, $primary_chr, $primary_start, $primary_end, $aligned_chr, $aligned_start, $aligned_end, $relative_strand,
        $blast) = split(/\s+/, $id_line);
    my $primary_seq = <>;
    chomp $primary_seq;
    my @map = mapping($primary_seq);
    my $aligned_seq = <>;
    chomp $aligned_seq;
    my @al_map = mapping($aligned_seq);
    if (length($primary_seq) != length($aligned_seq)) {
        die "Primary and aligned sequences have different length\n";
    }
    my $empty = <>;
    if ($empty !~ /^\s*$/) {
        die "Empty line expected after $n\n";
    }
  MAT:
    foreach my $mat_id (keys %matrix) {
        my @norm = @{ $matrix{$mat_id} };
        my $mat_len = scalar @norm;
        for (my $i = 0; $i <= length($primary_seq) - $mat_len; ++$i) {
            my $primary_sub = substr($primary_seq, $i, $mat_len);
            if ($primary_sub =~ /^-/) {
                next;
            }
            my $ngaps = 0;
            my $j = $i;
            while ($primary_sub =~ /-/) {
                $primary_sub =~ s/-//;
                ++$ngaps;
                $primary_sub = $primary_sub . substr($primary_seq, $j + $mat_len, 1);
                ++$j;
                if ($j > length($primary_seq) - $mat_len) {
                    last;
                }
            }
            next MAT if ((length($primary_sub) < $mat_len) || ($j > length($primary_seq) - $mat_len)) ;
            my ($score, $strand) = maxscore(uc($primary_sub), @norm);
            next unless $score >= $cutoff{$mat_id};
            my $aligned_sub = substr($aligned_seq, $j, $mat_len);
            my $aligned_score;
            my $aligned_strand;
            my $aligned;
            my $rel_al_score;
            if ($ngaps  || $aligned_sub =~ /-/) {
                $aligned = 0;
                $aligned_score = 'NA';
                $rel_al_score= 'NA';
                $aligned_strand = 'NA';
                $aligned_sub = 'NA';
            }
            else {
                ($aligned_score, $aligned_strand) = maxscore(uc($aligned_sub), @norm);
                if ($aligned_score > $cutoff{$mat_id}) {
                    $aligned = 1;
                    $rel_al_score = $aligned_score / $best{$mat_id};
                }
                else {
                    $aligned = 0;
                    $aligned_score = 'NA';
                    $rel_al_score = 'NA';
                    $aligned_sub = 'NA';
                }
            }
            my $identical = 0;
            if (uc($aligned_sub) eq uc($primary_sub) || revc(uc($aligned_sub)) eq uc($primary_sub)) {
                $identical = 1;
            }
            if (! defined $opt_a || ($opt_a && $aligned)) {
                print join("\t", ($primary_chr, $mat_id, $primary_start + $map[$j-$ngaps], $strand, $primary_sub,  $score,
                                  $score/$best{$mat_id}, $aligned, $identical, $aligned_chr, $relative_strand, 
                                  $aligned_start + $al_map[$j-$ngaps],  $aligned_strand, $aligned_sub, $aligned_score,
                                  $rel_al_score)), "\n";
            }
        }
    }

}

