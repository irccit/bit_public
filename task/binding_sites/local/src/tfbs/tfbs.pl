#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;
use lib $ENV{'BIOINFO_ROOT'}.'/task/binding_sites/local/src/tfbs/';
use Tfbs;

$SIG{__WARN__} = sub {die @_};

my $wikidoc =  <<END;

===== tfbs =====


[[http://www.to.infn.it/~provero/data_exchange/scripts/tfbs/tfbs|tfbs]]:

identification of matches of a PWM on a sequence

needs [[http://www.to.infn.it/~provero/data_exchange/scripts/tfbs/Tfbs.pm|Tfbs.pm]]\\
(change the location of this in the source)

usage:  tfbs OPTIONS sequence_file

==== OPTIONS ==== 

=== -m matrix file ===

//required//

=== -b background file ===
//required//


=== -c cutoff ===
use this cutoff for all matrices (retain only sequences scoring better than cutoff)\\
=== -C relative_cutoff ===
for each matrix, retain only sequences scoring better than relative_cutoff * best, where best is the best possible score for the matrix\\ \\
//Options -c and -C  are mutually exclusive, one of them must be specified//
=== -M ===
if both a sequence and its revcompl score better than the cutoff, retain only the one with the higher score (default: retain both)
=== -o offset ===
add offset to the relative coordinate of the matching sequence in output the relative coordinate start from 1 (default offset: 0)


==== INPUT FILES ====
 (all tab-delimited, no headers, no comments):

=== matrix file ===

column 1: matrix id\\
column 2: position (starting from 0)\\
column 3: A count\\
column 4: C count\\
column 5: G count\\
column 6: T count\\

zero counts will produce errors - provide pseudocounts\\
counts can be real counts or probabilities: the program normalizes each line to have sum 1

=== background file ===
4 lines\\
column 1: base (must be A,C,G,T in this order in the four lines)\\
column 2: count or probability\\

=== sequence file ===

column 1: unique sequence id\\
last column: sequence\\
all other columns are ignored\\



==== OUTPUT ====
on stdout\\
column 1: id of the matching sequence (from column1 of sequence file)\\
column 2: matrix id\\
column 3: coordinate of the match (see option -o above)\\
column 4: strand (relative to the input sequence)\\
column 5: matching subsequence; this is always reported as it appears in the input file, even if it is the reverse complement that matches\\
column 6: score\\
column 7: relative score (score/best)\\

==== SAMPLE INPUT FILES ====

[[http://www.to.infn.it/~provero/data_exchange/scripts/tfbs/stat3.txt|matrix file]]\\
[[http://www.to.infn.it/~provero/data_exchange/scripts/tfbs/bg.txt|background file]]\\
[[http://www.to.infn.it/~provero/data_exchange/scripts/tfbs/chr2_71691329_71760731.txt|sequence file]]\\
END

our($opt_h, $opt_m, $opt_c, $opt_b, $opt_M, $opt_o, $opt_C, $opt_s);
getopts('hm:c:b:Mo:C:s');

my $usage = <<EOF;
usage tfbs -m matrix_file {-c cutoff | -C fractional_cutoff}  -b background_file sequence_file
 [-M return only sequence on best scoring strand]
 [-o offset to be added to coordinates]
 [-s use -c and -C at the same time]
EOF

if ($opt_h || ! defined $opt_m || !defined $opt_b) {
    die $usage;
}

if (! defined $opt_c && ! defined $opt_C) {
    die $usage;
}

if ( defined $opt_c && defined $opt_C && ! defined $opt_s) {
    die "Options -c and -C are mutually exclusive without -s option\n";
}

if (! defined $opt_o) {
    $opt_o = 0;
}

my @base = ('A', 'C', 'G', 'T');



open BKG, $opt_b or die "Error opening $opt_b: $!\n";
my @background = ();
my @bkg = <BKG>;
chomp @bkg;
my @check = ();
for (my $i = 0; $i <= 3; ++$i) {
    my @line = split("\t", $bkg[$i]);
    push @check, $line[0];
    push @background, $line[1];
}
for (my $i = 0; $i <= 3; ++$i) {
    if ($check[$i] ne $base[$i]) {
        die "Error in $opt_b: $bkg[$i]\n";
    }
}



my $tot = sum(@background);
foreach (@background) {
    $_ /= $tot;
}

#print join("\t", @background), "\n";

@background = log2(@background);

# load matrices
my %matrix;
open MAT, $opt_m or die "Error opening $opt_m";
while (<MAT>) {
    chomp;
    my @line = split("\t");
    my $mat_id = shift @line;
    my $pos = shift @line;
    ${ $matrix{$mat_id} }[$pos] = join("\t", @line);
}
close MAT;

my %cutoff;
my %best;
foreach my $mat_id (keys %matrix) {
    #print STDERR join("\t",@{ $matrix{$mat_id} }),"\n";
    @{ $matrix{$mat_id} } = normalized(@background,  @{ $matrix{$mat_id} });
    $best{$mat_id} =  best_score( @{ $matrix{$mat_id} } );
    if (defined $opt_C and defined $opt_c) {
	die("error in checking options") if not defined $opt_s;
	$cutoff{$mat_id} = $opt_C * $best{$mat_id};
	if ($cutoff{$mat_id} < $opt_c){
		$cutoff{$mat_id} = $opt_c;
	}
    }
    elsif (defined $opt_C) {
        $cutoff{$mat_id} = $opt_C * $best{$mat_id};
    }
    else {
        $cutoff{$mat_id} = $opt_c;
    }
}

while (<>) {
    chomp;
    my @line = split("\t");
    my $id = $line[0];
    my $sequence = $line[$#line];
    foreach my $mat_id (keys %matrix) {
        my @norm = @{ $matrix{$mat_id} };
        my $mat_len = scalar @norm;
        for (my $i = 0; $i <= length($sequence) - $mat_len; ++$i) {
            my $sub = substr($sequence, $i, $mat_len);
            my $sub_uc = uc($sub);
            my $score_plus = score($sub_uc, @norm);
            my $score_minus = score(revc($sub_uc), @norm);
            if (! $opt_M) {
                if ($score_plus >= $cutoff{$mat_id}) {
                    my $rel_score = $score_plus /  $best{$mat_id};
                    print $id, "\t", $mat_id, "\t", $i+1+$opt_o, "\t+\t",  $sub, "\t", $score_plus, "\t", $rel_score, "\n";
                }
                if ($score_minus >= $cutoff{$mat_id}) {
                    my $rel_score = $score_minus /  $best{$mat_id};
                    print $id, "\t", $mat_id, "\t", $i+1+$opt_o, "\t-\t",  $sub, "\t", $score_minus, "\t", $rel_score, "\n";
                }
            }
            else {
                my $score = $score_plus;
                my $strand = '+';
                if ($score_minus > $score) {
                    $score = $score_minus;
                    $strand = '-';
                }
                if ($score > $cutoff{$mat_id}) {
                    my $rel_score = $score /  $best{$mat_id};
                    print $id, "\t", $mat_id, "\t", $i + 1+$opt_o, "\t$strand\t",  $sub, "\t", $score, "\t", $rel_score, "\n";
                }
            }

        }
    }
}
