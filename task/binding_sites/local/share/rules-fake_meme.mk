	(\
		echo MEME version 4.0.0;\
		echo ALPHABET= ACGT;\
		echo strands: + -;\
		echo 'Background letter frequencies (from dataset with add-one prior applied):';\
		tr "\t" "\n" < $^2 | transpose\
	) > $@
	id2count 1 < $< | sort -k 1,1\
	| repeat_group_pipe '\
		echo "MOTIF  $$1";\
		echo "BL   MOTIF $$1 width=0 seqs=0";\
		echo letter-probability matrix: alength= 4 w= len_$$1 nsites= 1 E= 1;\
		cut -f 3-\
	' 1 \
	| translate -v -w <(id2count 1 < $< | find_best 1 2 | bawk '{print "len_" $$1,$$2+1}') >> $@
