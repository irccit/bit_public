#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\t";
$\="\n";

my $k=undef;
my %words=();

my $usage="$0 -k 8 < nucloetides";

GetOptions (
	'k=i' 	=> \$k,
) or die($usage);

die($usage) if !defined($k);
 
$_=<>;
chomp;
while( 1 ){
	
	if(length($_) < $k){
		my $new_row=<>;
		#print STDERR length($_),$new_row;
		if(defined($new_row)){
			chomp $new_row;
			$_.=$new_row;
		}else{
			last;
		}
	}

	my $w=substr($_,0,$k);
	substr($_,0,1,'');
	next if $w!~/^[ACGT]+$/;
	$words{$w}++;


}

for(keys %words){
	print $_,$words{$_};
}
