#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
my @char=('A','T','G','C');

my $usage="$0 -l 100000 -w AAAAA -n 100 [-m]";

my $seq_len=undef;
my $word=undef;
my $word_occurrencies=undef;
my $mismatch=0;

GetOptions (
	'L=i' 	=> \$seq_len,
	'w=s'	=> \$word,
	'n=i'	=> \$word_occurrencies,
	'm'	=> \$mismatch
) or die($usage);

die($usage) if !defined($seq_len) or !defined($word) or !defined($word_occurrencies);


my $word_length=length($word);

my $seq='';
for(my $i=0; $i<$seq_len; $i++){
	$seq.=$char[int(rand(4))];
}

for(my $i=0; $i<$word_occurrencies; $i++){
	my $offset = int(rand($seq_len - $word_length));
	my $my_word = $word;
	if($mismatch){
		while($my_word eq $word){
			substr($my_word, int(rand($word_length)), 1, $char[int(rand(4))]);
		}
		#print STDERR "$my_word\n";
	}
	substr(	$seq, $offset, $word_length, $my_word);
}

print $seq;
print "\n";
