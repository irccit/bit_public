SEQ_DIR ?= $(BIOINFO_ROOT)/task/sequences/dataset/ensembl/hsapiens/42
BIN_DIR ?= $(BIOINFO_ROOT)/task/word_count/local/bin
WORD_LENGTH ?= 8
MIN_OCCURRENCE ?= 50
MIN_RATIO ?= 0.3
MARKOV_K ?= 4
RANDOM_TRIALS ?= 1000

.DELETE_ON_ERROR:

#%.mmfreq: $(SEQ_DIR)/%.fa
#	$(BIN_DIR)/mismatch_word_count.pl $(WORD_LENGTH) $< > $@
#

all_chr_accoda.%:
	for i in $(ALL_CHR); do \
		accoda -name $$i.$(WORD_LENGTH).$(MARKOV_K).$(NAME_SUFFIX) -- "make $$i.$* WORD_LENGTH=$(WORD_LENGTH) MARKOV_K=$(MARKOV_K) RANDOM_TRIALS=$(RANDOM_TRIALS)"; \
	done;

%.mmfreq: %.freq
	$(BIN_DIR)/count_neighbour.pl $< > $@

cpg_test.freq:
	$(BIOINFO_ROOT)/task/seq_random_models/local/bin/cpg.pl 100000000 | $(BIN_DIR)/word_count.pl -k $(WORD_LENGTH) > $@


%.$(WORD_LENGTH).freq: $(SEQ_DIR)/%.fa
	$(BIN_DIR)/word_count.pl -k $(WORD_LENGTH) $< > $@\

%.$(WORD_LENGTH).random_0.freq: $(SEQ_DIR)/%.fa
	$(BIOINFO_ROOT)/task/seq_random_models/local/bin/markov_step_0_masked.pl $< \
	| $(BIN_DIR)/word_count.pl -k $(WORD_LENGTH) > $@

%.$(WORD_LENGTH).random_$(MARKOV_K).freq: $(SEQ_DIR)/%.fa
	$(BIOINFO_ROOT)/task/seq_random_models/local/bin/markov_step_k_masked.pl -k $(MARKOV_K) -m $*.$(MARKOV_K).mm -l `unhead $< | tr -d "N\n" | wc -c` $< \
	| $(BIN_DIR)/word_count.pl -k $(WORD_LENGTH) > $@

%.$(WORD_LENGTH).random_cpg.freq: $(SEQ_DIR)/%.fa
	$(BIOINFO_ROOT)/task/seq_random_models/local/bin/cpg.pl `unhead $< | tr -d "N\n" | wc -c` | $(BIN_DIR)/word_count.pl -k $(WORD_LENGTH) > $@


%.ratio_test.$(WORD_LENGTH).random_$(MARKOV_K).$(RANDOM_TRIALS):
	for i in `seq $(RANDOM_TRIALS)`; do \
		echo ">$$i" >> $@; \
		rm -f $*.$(WORD_LENGTH).random_$(MARKOV_K).freq; \
		rm -f $*.$(WORD_LENGTH).random_$(MARKOV_K).mmfreq; \
		rm -f $*.$(WORD_LENGTH).random_$(MARKOV_K).freq.both.gz; \
		make $*.$(WORD_LENGTH).random_$(MARKOV_K).freq.both.gz WORD_LENGTH=$(WORD_LENGTH) MARKOV_K=$(MARKOV_K); \
		zcat $*.$(WORD_LENGTH).random_$(MARKOV_K).freq.both.gz | awk '$$2>10' | sort -k 4,4nr \
		| head -n 100 >> $@; \
	done;

genome.ratio_test.$(WORD_LENGTH).random_$(MARKOV_K).$(RANDOM_TRIALS): $(addsuffix .ratio_test.$(WORD_LENGTH).random_$(MARKOV_K).$(RANDOM_TRIALS),$(ALL_CHR))
	grep -A1 '^>' $^ | awk '$$4 {print $$4}' | binner -b r > $@

significative_ratio.$(WORD_LENGTH).$(MIN_OCCURRENCE).$(MIN_RATIO): $(addsuffix .$(WORD_LENGTH).freq.both.gz,$(ALL_CHR))
	(for i in $^; do \
		echo ">$$i"; \
		zcat chr*.11.freq.both.gz | awk '$$2>$(MIN_OCCURRENCE) && $$4>$(MIN_RATIO)'; \
	done;) > $@

%.ratio_test.$(WORD_LENGTH).cpg_random.$(RANDOM_TRIALS):
	for i in `seq $(RANDOM_TRIALS)`; do \
		echo ">$$i" >> $@; \
		rm -f $*.$(WORD_LENGTH).random_cpg.freq; \
		rm -f $*.$(WORD_LENGTH).random_cpg.mmfreq; \
		rm -f $*.$(WORD_LENGTH).random_cpg.freq.both.gz; \
		make $*.$(WORD_LENGTH).random_cpg.freq.both.gz WORD_LENGTH=$(WORD_LENGTH); \
		zcat $*.$(WORD_LENGTH).random_cpg.freq.both.gz | awk '$$2>10' | sort -k 4,4nr \
		| head -n 100 >> $@; \
	done;

%.$(MARKOV_K).mm: $(SEQ_DIR)/%.fa
	$(BIOINFO_ROOT)/task/seq_random_models/local/bin/markov_step_k_masked.pl -k $(MARKOV_K) -m $*.$(MARKOV_K).mm -l 1 > /dev/null

%.freq.both.gz: %.freq %.mmfreq
	sort -S20% -k 1,1 $< > $@.tmp1
	sort -S20% -k 1,1 $(word 2,$^) > $@.tmp2
	join3_pl -i -u $@.tmp1 $@.tmp2 | awk 'BEGIN{OFS="\t"} {print $$0,$$2/$$3}' | sort -k 4,4nr \
	| gzip> $@
	rm -f $@.tmp*
	rm -f $^

%.random.freq.both.gz: %.random.freq %.random.mmfreq
	sort -S30% -k 1,1 $< > $@.tmp1
	sort -S30% -k 1,1 $(word 2,$^) > $@.tmp2
	join3_pl -i -u $@.tmp1 $@.tmp2 | awk 'BEGIN{OFS="\t"} {print $$0,$$2/$$3}' | sort -k 3,3nr \
	| gzip> $@`
	rm -f $@.tmp*
	rm -f $^

genome.$(WORD_LENGTH).freq.both: $(addsuffix .$(WORD_LENGTH).freq.both, $(addprefix chr, $(ALL_CHR)))
	cp $< $@;
	for i in $(wordlist 2,$(words $^),$^); do \
		echo $$i; \
		join3_pl $@ $$i | awk 'BEGIN{OFS="\t"}{print $$1, $$2+$$6, $$3+$$7}' > $@.tmp; \
		mv  $@.tmp $@; \
	done;

%.freq.both.dot_plot.png: %.freq.both
	echo "\
		set terminal png size 800 600;\
		plot '$<' u 2:3 w p pt 6\
	" \
	| gnuplot > $@

##################
#	mmfreq_test
#################

mmfreq_test.source:
	(for i in `seq 1000`; do\
		$(BIN_DIR)/build_model_sequence.pl -l 100000 -w AAAAAAAA -n 10 -m;\
	done;) > $@

mmfreq_test.exact: mmfreq_test.source
	set -e; \
	cat $< \
	| ( while read line; do\
		echo $$line | $(BIN_DIR)/word_count.pl -k 8 | sort -k 2,2nr | head | $(BIN_DIR)/polia_position.pl; \
	done; ) > $@

mmfreq_test.neighbour: mmfreq_test.source
	set -e; \
	cat $< \
	| ( while read line; do\
		echo $$line | $(BIN_DIR)/word_count.pl -k 8 | $(BIN_DIR)/count_neighbour.pl | sort -k 2,2nr | head | $(BIN_DIR)/polia_position.pl; \
	done; ) > $@
		

%.pippo:
	echo $(SEQ_DIR)/$*.fa
