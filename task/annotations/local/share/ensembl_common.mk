ENS_MART_PORT = 3316
ENS_CORE_PORT = 3316
#MYSQL_MART = mysql -u anonymous -h martdb.ensembl.org -P 3316 -BCAN
#MYSQL_CORE = mysql -u anonymous -h ensembldb.ensembl.org -P 3306 -BCAN
MYSQL_MART = mysql -u anonymous -h martdb.ensembl.org -P $(ENS_MART_PORT) -BCAN
MYSQL_CORE = mysql -u anonymous -h ensembldb.ensembl.org -P $(ENS_CORE_PORT) -BCAN
ENS_MART = ensembl_mart_$(ENS_VERSION)
