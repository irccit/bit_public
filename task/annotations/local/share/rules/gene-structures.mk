#Copyright 2010 Gabriele Sales <gbrsales@gmail.com>

CLEAN        += gene_structures.gz
INTERMEDIATE += exon-gene.map


exon-gene.map: exon-transcript.map.gz transcript-gene.map.gz
	zcat $< \
	| translate <(zcat $^2) 2 >$@

gene_structures.gz: exons.gz exon-gene.map
	zcat $< \
	| translate -d $^2 1 \
	| expandsets 1 \
	| bawk '{print $$1 ":" $$2 ":" $$5, $$3, $$4}' \
	| bsort -k1,1 -k2,2n \
	| union \
	| tr ':' '\t' \
	| bawk '{ if (!($$1 in seen)) { seen[$$1]=1; print ">"$$1, $$2, $$3 } \
	        ; print $$4,$$5 }' \
	| gzip >$@

.META: gene_structures.gz
	FASTA
	> gene_ID chr strand
	exon_start exon_stop
