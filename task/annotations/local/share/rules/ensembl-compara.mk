# Copyright 2010 Gabriele Sales <gbrsales@gmail.com>
# Copyright 2010 Paolo Martini <paolo.cavei@gmail.com>

VERSION   ?= 56
SORT_OPTS ?= -S60%


define convert_species_name
bawk '{split($$$(1),ps," "); $$$(1)=tolower(substr(ps[1],1,1))ps[2]; print}'
endef

FTP_BASE := ftp://ftp.ensembl.org/pub/release-$(VERSION)/mysql/ensembl_compara_$(VERSION)/


genome.gz:
	wget -O$@ '$(FTP_BASE)/genome_db.txt.gz'

.META: genome.gz
	1  id
	2  taxon
	3  species
	4  assembly
	5  default_assembly
	6  genebuild
	7  locator


methods.gz: method_link_species_set.gz species_set.gz genome.gz
	bawk '{print $$id,$$name,$$species_set}' $< \
	| translate -d <(bawk '{print $$id,$$genome}' $^2) 3 \
	| expandsets 3 \
	| translate <(bawk '{print $$id,$$species}' $^3) 3 \
	| $(call convert_species_name,3) \
	| collapsesets 3 \
	| gzip >$@

.META: methods.gz
	1  id
	2  name
	3  species


dnafrag.gz:
	wget -O$@ '$(FTP_BASE)/dnafrag.txt.gz'

.META: dnafrag.gz
	1  id
	2  length
	3  name
	4  genome
	5  coord_system


blocks.gz: genomic_align.gz dnafrag.gz genome.gz
	bawk '{print $$block,$$method,$$dnafrag,$$start,$$stop,$$strand,$$cigar}' $< \
	| translate <(bawk '{print $$id,$$name,$$genome}' $^2) 3 \
	| translate <(bawk '{print $$id,$$species}' $^3 | $(call convert_species_name,2)) 4 \
	| bawk '{print $$1,$$2,$$4,$$3,$$5-1,$$6~8}' \
	| bsort -k1,1n -T. $(SORT_OPTS) \
	| gzip >$@

.META: blocks.gz
	1  id
	2  method
	3  species
	4  chr
	5  start
	6  stop
	7  strand
	8  cigar


%.gz:
	wget -O$@ '$(FTP_BASE)/$*.*.txt.gz'

.META: method_link_species_set.gz
	1  id
	2  link
	3  species_set
	4  name
	5  source
	6  url

.META: species_set.gz
	1  id
	2  genome

.META: genomic_align_block.gz
	1  id
	2  method
	3  score
	4  perc_ident
	5  length
	6  group_id

.META: genomic_align.gz
	1  id
	2  block
	3  method
	4  dnafrag
	5  start
	6  stop
	7  strand
	8  cigar
	9  level


ALL          += methods.gz blocks.gz
INTERMEDIATE += genome.gz \
                method_link_species_set.gz species_set.gz \
                genomic_align_block.gz genomic_align.gz dnafrag.gz blocks.raw.gz
