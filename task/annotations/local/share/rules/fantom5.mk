FANTOM_CAT.lv3_robust.gtf.gz:
	wget -O $@ -c http://fantom.gsc.riken.jp/5/suppl/Hon_et_al_2016/data/assembly/lv3_robust/FANTOM_CAT.lv3_robust.gtf.gz: 

FANTOM_CAT.lv2_permissive.gtf.gz: 
	wget -O $@ -c http://fantom.gsc.riken.jp/5/suppl/Hon_et_al_2016/data/assembly/lv2_permissive/FANTOM_CAT.lv2_permissive.gtf.gz

FANTOM_CAT.overlay_GENCODEv25.transcript.tsv.gz:
	wget -O $@ -c http://fantom.gsc.riken.jp/5/suppl/Hon_et_al_2016/data/other_data/GENCODEv25_overlay/FANTOM_CAT.overlay_GENCODEv25.transcript.tsv.gz: 

%.gtf: %.gtf.gz
	zcat $< > $@

gencode_merged/transcripts.gtf: FANTOM_CAT.lv2_permissive.gtf ../../../gencode/hsapiens/29/primary_assembly.annotation.gtf
	cuffmerge -g $^2 <(echo $<) -o `dirname $@` -p $(CORES)


