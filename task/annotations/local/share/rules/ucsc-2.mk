# Copyright 2008,2010 Gabriele Sales <gbrsales@gmail.com>

SPECIES            ?= hsapiens
VERSION            ?= 18
UPSTREAM_EXTENSION ?= 10000

SPECIES_MAP        := $(BIOINFO_ROOT)/task/annotations/local/share/ucsc_species.map
UCSC_SPECIES        = $(shell translate $(SPECIES_MAP) 1 <<<"$(SPECIES)")
UCSC_DATABASE      ?= $(UCSC_SPECIES)$(VERSION)
SEQUENCE_DIR       ?= $(BIOINFO_ROOT)/task/sequences/dataset/ucsc/$(SPECIES)/$(UCSC_DATABASE)
DATABASE_URL       := http://hgdownload.cse.ucsc.edu/goldenPath/$(UCSC_DATABASE)/database
MYSQL_CMD          := mysql --user=genome --host=genome-mysql.cse.ucsc.edu -BCAN


extern $(SEQUENCE_DIR)/all_chr.len as ALL_CHR_LEN


ALL_XREF_MAPS := $(addsuffix .map,$(addprefix transcript-,$(UCSC_XREF_DBS)))

import ucsc_gene_tracks

define transcript_xref
	$(MYSQL_CMD) $(UCSC_DATABASE) <<<" \
		SELECT name, '$*' as db, value FROM $1" \
	| sort -S40% >$@
endef

xref_dbs.mk:
	( \
		set -e; \
		echo -n "UCSC_XREF_DBS := "; \
		$(MYSQL_CMD) $(UCSC_DATABASE) <<<" \
		SHOW TABLES LIKE 'knownTo%'" \
		| sed 's|knownTo||' \
		| bawk '$$1!="super"' \
		| bawk '$$1!="known"' \
		| sort \
		| tr "\n\t" "  "; \
	) >$@
#include xref_dbs.mk


all: transcripts.gz transcript-gene.map.gz transcript-xref.map.gz \
     transcript_structures.gz 5utr_structures.gz 3utr_structures.gz
clean:
	rm -f xref_dbs.mk
	rm -f transcript.info
	rm -f $(ALL_XREF_MAPS)
clean.full:
	rm -f transcripts.gz transcript-gene.map.gz transcript-xref.map.gz
	rm -f transcript_structures.gz 5utr_structures.gz 3utr_structures.gz

transcripts.gz: transcript.info
	cut -f1,3-7 $< \
	| gzip >$@

.META: transcripts.gz
	1  transcript_ID  uc001aaa.2
	2  chr            1
	3  start          1115
	4  stop           4121
	5  strand         +
	6  biotype        noncoding
	7  status         KNOWN

transcript-gene.map.gz: transcript.info
	cut -f-2 $< \
	| gzip >$@

.META: transcript-gene.map.gz
	1  transcript_ID  uc001aaa.2
	2  gene_ID        1

.INTERMEDIATE: transcript.info
transcript.info:
	$(MYSQL_CMD) $(UCSC_DATABASE) <<<" \
		SELECT t.name, g.clusterId, t.chrom, t.txStart, t.txEnd, t.strand, ti.category, t.cdsStart, t.cdsEnd, t.exonStarts, t.exonEnds \
		FROM knownGene AS t \
			LEFT JOIN knownIsoforms AS g ON t.name = g.transcript \
			LEFT JOIN kgTxInfo AS ti ON t.name = ti.name \
		WHERE t.chrom NOT LIKE \"%\_%\"" \
	| bawk '{sub(/^chr/,"",$$3); \
	         sub(/^\+$$/,"+1",$$6); \
	         sub(/^-$$/,"-1",$$6); \
	         print $$0,"KNOWN"}' \
	| sort -S40% -k1,1 -k2,2 >$@

.META: transcript.info
	1       transcript_id	uc001aaa.2
	2       transcrip_cluster_id		1
	3       chr		1
	4       b		1115
	5       e		4121
	6       strand		+1
	7       type		noncoding
	8       coding_b	1115
	9       coding_e	1115
	10      exon_bs		1115,2475,3083,
	11      exon_es		2090,2584,4121,
	12      other		KNOWN

.DOC: transcript.info
		transcrip_cluster_id
	First, the Known Genes are clustered based on if they have overlapping
	exons.
	Then, within a cluster, the gene with the highest number of coding bases is
	chosen as the representative canonical gene.
	The details can be found in the source code of hgClusterGenes.c under
		kent/src/hg/near/hgClusterGenes

transcript-xref.map.gz: $(ALL_XREF_MAPS)
	sort -S40% -k1,1 -k2,2 $^ \
	| gzip >$@

.META: transcript-xref.map.gz
	1  transcript_ID  ENST00000000233
	2  database_ID    AFFY_HG_Focus
	3  external_ID    201526_at

.INTERMEDIATE: $(ALL_XREF_MAPS)
transcript-%.map:
	$(call transcript_xref,knownTo$*)

transcript_structures.gz: transcript.info
	ucsc-transcript_structures <$< \
	| gzip >$@

.META: transcript_structures.gz
	FASTA
	> transcript_ID chromosome coding_start coding_stop strand
	exon_ID exon_start exon_stop

%_genes.whole: %_genes
	bawk '{print $$1,$$2,$$4,$$5,$$3}' $< > $@

.META: *_genes.whole
	1       id	ENST00000404059
	2       chr	1
	3       b	1872
	4       e	3533
	5       strand	+1

%_genes.intergenic: %_genes.whole
	bawk '{print $$2,$$3,$$4,$$1}' $< \
	| bsort -k1,1 -k2,2n \
	| id2count -b 4 \
	| union | complement > $@

.META: %_genes.intergenic
	1	chr	1
	2	b	3661579
	3	e	4280926
	4	sx_ids	uc007aet.1;uc007aeu.1;uc007aev.1
	5	dx_ids	uc007aew.1;uc007aex.1


%_genes.coding: %_genes
	cat $< \
	| perl -lane '$$,="\t"; $$\="\n"; $$F[8]=~s/,^//g; $$F[9]=~s/,^//g; @ex_b=split /,/,$$F[8]; @ex_e=split /,/,$$F[9]; for (my $$i=0; $$i < scalar @ex_b; $$i++) { next if $$ex_e[$$i]<$$F[5]; next if $$ex_b[$$i]>$$F[6]; my $$b= $$ex_b[$$i] < $$F[5] ? $$F[5] : $$ex_b[$$i]; my $$e= $$ex_e[$$i] < $$F[6] ? $$ex_e[$$i] : $$F[6]; print $$F[1],$$b,$$e,$$F[0],"C",$$F[2] if ($$b < $$e); }' \
	| sort -k 1,1 -k2,2n \
	> $@

.META: %_genes.coding
	1       chr		1
	2       b		58953
	3       e		59871
	4       trans_id	ENST00000326183
	5       type		C


%_genes.5utr: %_genes
	cat $< \
	| perl -lane '$$,="\t"; $$\="\n"; $$F[8]=~s/,^//g; $$F[9]=~s/,^//g; @ex_b=split /,/,$$F[8]; @ex_e=split /,/,$$F[9]; my $$utr_b=$$F[3]; my $$utr_e=$$F[5]; if ($$F[2]=~/^-$$/) {$$utr_b=$$F[6]; $$utr_e=$$F[4];} for (my $$i=0; $$i < scalar @ex_b; $$i++) {next if $$ex_e[$$i]<$$utr_b; next if $$ex_b[$$i]>$$utr_e; my $$b= $$ex_b[$$i] < $$utr_b ? $$utr_b : $$ex_b[$$i]; my $$e=$$ex_e[$$i] < $$utr_e ? $$ex_e[$$i] : $$utr_e; print $$F[1],$$b,$$e,$$F[0],"5",$$F[2] if ($$b < $$e);}' \
	| sort -k 1,1 -k2,2n \
	> $@

.META: %_genes.5utr
	1       chr		1
	2       b		58953
	3       e		59871
	4       trans_id	ENST00000326183
	5       type		5

%_genes.3utr: %_genes
	cat $< \
	| perl -lane '$$,="\t"; $$\="\n"; $$F[8]=~s/,^//g; $$F[9]=~s/,^//g; @ex_b=split /,/,$$F[8]; @ex_e=split /,/,$$F[9]; my $$utr_b=$$F[6]; my $$utr_e=$$F[4]; if ($$F[2]=~/^-$$/) {$$utr_b=$$F[3]; $$utr_e=$$F[5];} for (my $$i=0; $$i < scalar @ex_b; $$i++) {next if $$ex_e[$$i]<$$utr_b; next if $$ex_b[$$i]>$$utr_e; my $$b= $$ex_b[$$i] < $$utr_b ? $$utr_b : $$ex_b[$$i]; my $$e=$$ex_e[$$i] < $$utr_e ? $$ex_e[$$i] : $$utr_e; print $$F[1],$$b,$$e,$$F[0],"3",$$F[2] if ($$b < $$e);}' \
	| sort -k 1,1 -k2,2n \
	> $@

.META: %_genes.3utr
	1       chr		1
	2       b		58953
	3       e		59871
	4       trans_id	ENST00000326183
	5       type		3

%_genes.3utr.strand: %_genes
	cat $< \
	| perl -lane '$$,="\t"; $$\="\n"; $$F[8]=~s/,^//g; $$F[9]=~s/,^//g; @ex_b=split /,/,$$F[8]; @ex_e=split /,/,$$F[9]; my $$utr_b=$$F[6]; my $$utr_e=$$F[4]; if ($$F[2]=~/^-1$$/) {$$utr_b=$$F[3]; $$utr_e=$$F[5];} for (my $$i=0; $$i < scalar @ex_b; $$i++) {next if $$ex_e[$$i]<$$utr_b; next if $$ex_b[$$i]>$$utr_e; my $$b= $$ex_b[$$i] < $$utr_b ? $$utr_b : $$ex_b[$$i]; my $$e=$$ex_e[$$i] < $$utr_e ? $$ex_e[$$i] : $$utr_e; print $$F[1],$$b,$$e,$$F[0],$$3,$$F[2] if ($$b < $$e);}' \
	| sort -k 1,1 -k2,2n \
	> $@

.META: %_genes.3utr.strand
	1	chr	1
	2	b	58953
	3	e	59871
	4	trans_id	ENST00000326183
	5	strand	+1


%_genes.exons: %_genes.5utr %_genes.coding %_genes.3utr
	cat $^ | sort -k 1,1 -k 2,2n > $@

.META: %_genes.exons
	1       chr		1
	2       b		58953
	3       e		59871
	4       trans_id	ENST00000326183
	5       type		3

%_genes.introns: %_genes 
	cat $< \
	| perl -lane '$$,="\t"; $$\="\n"; $$F[8]=~s/,^//g; $$F[9]=~s/,^//g; @ex_b=split /,/,$$F[8]; @ex_e=split /,/,$$F[9]; if ($$F[3]<$$ex_b[0]) {print $$F[1],$$F[3],$$ex_b[0],$$F[0],"I",$$F[2];} shift @ex_b; for (my $$i=0; $$i < scalar @ex_b; $$i++) {print $$F[1],$$ex_e[$$i],$$ex_b[$$i],$$F[0],"I",$$F[2] if ($$ex_e[$$i] < $$ex_b[$$i]);} print $$F[1],$$ex_e[$$#ex_e],$$F[4],$$F[0],"I",$$F[2] if ($$ex_e[$$#ex_e]<$$F[4]);' \
	| sort -k 1,1 -k2,2n \
	> $@

#%_genes.nr_introns: %_genes.introns %_genes.exons
#	intersection \
#		<(cut -f -3 $<  | append_each_row "garbage" | union --allow-duplicates | enumerate_rows -r | sort -k 1,1 -k 2,2n) \
#		<(cut -f -3 $^2 | append_each_row "garbage" | union --allow-duplicates | enumerate_rows -r | sort -k 1,1 -k 2,2n) \
#	| sort -k 8,8 -k 2,2n \
#	| complement -i \
#	| sort -k 1,1 -k 2,2n \
#	| intersection - $^2 | bawk '{print $$1,$$2,$$3,$$9}' \
#	| sort | uniq | collapsesets 4 \
#	| sort -k 1,1 -k 2,2n > $@

%_genes.upstream: %_genes.whole $(ALL_CHR_LEN)
	upstreams $^2 $(UPSTREAM_EXTENSION) <$< >$@

repeat_rmsk.txt.gz:
	wget -O $@ http://hgdownload.soe.ucsc.edu/goldenPath/$(UCSC_DATABASE)/database/rmsk.txt.gz

.META: repeat_rmsk.txt.gz
	1	bin
	2	swScore
	3	milliDiv
	4	milliDel
	5	milliIns
	6	genoName
	7	genoStart
	8	genoEnd
	9	genoLeft
	10	strand
	11	repName
	12	repClass
	13	repFamily
	14	repStart
	15	repEnd
	16	repLeft
	17	id

repeat_rmsk.bed.gz: repeat_rmsk.txt.gz
	bawk '{print $$genoName,$$genoStart,$$genoEnd,$$repName ";" $$repFamily ";" $$repClass,$$swScore,$$strand}' $< | gzip > $@


repeat.gz:
	( \
	if [ `echo "show tables like '%rmsk%';" | $(MYSQL_CMD) $(UCSC_DATABASE) | wc -l` != 1 ]; then \
		for i in $(ALL_CHRS); do \
			echo "SELECT genoName, genoStart, genoEnd, repName, repClass, repFamily  FROM chr$${i}_rmsk" | $(MYSQL_CMD) $(UCSC_DATABASE); \
		done; \
	else \
		echo "SELECT genoName, genoStart, genoEnd, repName, repClass, repFamily  FROM rmsk" | $(MYSQL_CMD) $(UCSC_DATABASE); \
	fi;\
	) \
	| sed 's/^chr//' \
	| sort -k 1,1 -k 2,2n \
	| gzip > $@

transposon.gz: repeat.coords.gz
	zcat $< \
	| awk '$$5=="DNA" || $$5=="LINE" || $$5=="LTR" || $$5=="SINE"' \
	| gzip > $@

knownGene.txt.gz:
	wget -O $@ http://hgdownload.cse.ucsc.edu/goldenPath/$(UCSC_DATABASE)/database/$@

knownToLocusLink.txt.gz:
	wget -O $@ http://hgdownload.cse.ucsc.edu/goldenPath/$(UCSC_DATABASE)/database/knownToLocusLink.txt.gz

###################################
#
#	Conservation
#
###################################

conservation_phast17.gz conservation_phast28.gz: conservation_phast%.gz:
	wget -O - $(DATABASE_URL)/phastConsElements$*way.txt.gz \
	| zcat \
	| cut -f 2-4,6 \
	| grep '^chr[0-9XY]*	' \
	| sed 's/^chr//' \
	| sort -k 1,1 -k 2,2n \
	| gzip > $@

.META: conservation_phast%.gz
	1	1	chr
	2	1865	b
	3	1943	e
	4	307	score


.DOC: conservation_phast17.gz
	Predictions of conserved elements produced by the phastCons program.
	This track shows a measure of evolutionary conservation in 17 vertebrates.
	http://genome.ucsc.edu/cgi-bin/hgTables?clade=mammal&org=Human&db=hg18&hgta_group=compGeno&hgta_track=multiz17way&hgta_table=phastCons17way&hgta_doSchema=describe+table+schema&hgta_regionType=genome&position=chr14%3A52394436-52487518&hgta_outputType=wigData&boolshad.sendToGalaxy=1&hgta_outFileName=&hgta_compressType=none

.DOC: conservation_phast28.gz
	Predictions of conserved elements produced by the phastCons program, trak also called "Most Conserved".
	This track shows a measure of evolutionary conservation in 28 vertebrates.
	http://genome.ucsc.edu/cgi-bin/hgTables?clade=mammal&org=Human&db=hg18&hgta_group=compGeno&hgta_track=mostConserved28way&hgta_table=phastConsElements28wayPlacMammal&hgta_doSchema=describe+table+schema&hgta_regionType=genome

###########
#
# POLY_A_DB
#
########### #               bin,chrom,chromStart,chromEnd,strand,name,score,strand,thickStart,thickEnd \

polyA_DB:
	echo "SELECT DISTINCT \
		name,bin,chrom,strand,chromStart,chromEnd,score \
		FROM polyaDb" \
	| $(MYSQL_CMD) $(UCSC_DATABASE) \
	| $(call convert_strand) >$@

.META: polyA_DB
	1	poly.name	p.79854.1
	2	poly_bin	590
	3	chr	chr1
	4	strand	+1
	5	start	751452
	6	stop	751453
	7	score	1000

###########
#
# targetScanS
#
########### 

targetscanS_track:
	echo "SELECT DISTINCT \
		name,bin,chrom,strand,chromStart,chromEnd,score \
		FROM targetScanS" \
	| $(MYSQL_CMD) $(UCSC_DATABASE) \
	| $(call convert_strand) >$@

.META: targetscanS_track
	1	name:miRNA	AGRN:miR-144
	2	TS_bin	592
	3	chr	chr1
	4	strand	+1
	5	start	980692
	6	stop	980699
	7	score	18

chrom.sizes:
	wget -O $@ http://hgdownload.cse.ucsc.edu/goldenPath/$(UCSC_DATABASE)/bigZips/$(UCSC_DATABASE).chrom.sizes

genome:
	mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A -e "select chrom, size from $(UCSC_DATABASE).chromInfo" | unhead | sed 's/^chr//' | bsort > $@

genome_chr:
	mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A -e "select chrom, size from $(UCSC_DATABASE).chromInfo" | unhead > $@

%.no_exons: %.exons genome
	bedtools merge -i $< | bedtools complement -i - -g $^2 > $@

%.nr_introns: %.no_exons %.introns
	bedtools intersect -a $< -b $^2 -loj | bawk '$$4!="." {print $$1~3,$$7~9}' | perl -pe 's/1$$//' | bedtools merge -c 4,6 -o collapse \
	| perl -lane 'BEGIN{$$,="\t"}$$F[4]=~s/.*([\+-])$$/\1/; print @F' >$@               *the strand is the same, this row "decollapase sting as +,+,+ or -,-,-*

repeat_rmsk.ribosomal.bed: repeat_rmsk.txt.gz
	bawk '$$13=="rRNA" {print $$6,$$7,$$8,$$11,$$2,$$10}' $< > $@
repeat_rmsk.L1.bed: repeat_rmsk.txt.gz
	bawk '$$13=="L1" {print $$6,$$7,$$8,$$11,$$2,$$10}' $< > $@

genconde%_comp_annotation_genes_name2transcript:
	echo "SELECT DISTINCT name2, name FROM wgEncodeGencodeCompV$*" | $(MYSQL_CMD) $(UCSC_DATABASE) > $@

known_genes_name2transcript:
	echo "SELECT DISTINCT alignID, name FROM knownGene" | $(MYSQL_CMD) $(UCSC_DATABASE) > $@

genconde%_comp_annotation_genes.nr_introns.gene_names: genconde%_comp_annotation_genes.nr_introns genconde%_comp_annotation_genes_name2transcript
	expandsets 4 -s ',' < $< | translate -a -n $^2 4 > $@
genconde27_comp_annotation_genes.nr_introns.gene_names.saf: genconde27_comp_annotation_genes.nr_introns.gene_names
	bawk 'BEGIN{print "GeneID","Chr","Start","End","Strand"}{print $$5,"chr"$$1~3,$$6}' $< > $@
