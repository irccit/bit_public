# Copyright 2010 Gabriele Sales <gbrsales@gmail.com>

SPECIES          ?= hsapiens
VERSION          ?= 100302
KEGG_SPECIES     ?= $(shell translate $(KEGG_SPECIES_MAP) 1 <<<"$(SPECIES)")

KEGG_HOST        := ftp://ftp.genome.jp
KEGG_PATH        := /pub/kegg/genes/organisms/
KEGG_SPECIES_MAP := $(TASK_ROOT)/local/share/species-kegg.map

KEGG_URL         := $(KEGG_HOST)/$(KEGG_PATH)/$(KEGG_SPECIES)


define check_date
	@set -e; \
	date="$$(ftp_mdate '$(KEGG_URL)/$1')"; \
	if [[ $$date -gt $(VERSION) ]]; then \
		echo "Version mismatch: remote data is more recent than this dataset." >&2; \
		exit 1; \
	fi
endef

define download_list
	$(call check_date,$1)
	wget -O- '$(KEGG_URL)/$1' \
	| bawk '{gsub(/^[^:]+:/,"",$$2); print}' \
	| gzip >$@
endef


ALL += gene-ensembl.map.gz gene-ncbi.map.gz


gene-ensembl.map.gz:
	$(call download_list,$(KEGG_SPECIES)_ensembl-$(KEGG_SPECIES).list)

gene-ncbi.map.gz:
	$(call download_list,$(KEGG_SPECIES)_ncbi-geneid.list)