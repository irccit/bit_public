SPECIES?=human
SPECIES2?=hsapiens
SPECIES_1L?=h
UCSC_VERSION?=hg38_20180212

STAR_VERSION?=STAR_2.5.3a
VERSION := $(shell basename `pwd`)
FTP_PREFIX=ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_$(SPECIES)/release_$(VERSION)
CORES=32

ALL+=basic.annotation.ensg2gene_symbol2biotype.map tRNAs.gtf.gz 2wayconspseudos.gtf.gz polyAs.gtf.gz long_noncoding_RNAs.gtf.gz chr_patch_hapl_scaff.basic.annotation.gtf.gz basic.annotation.gtf.gz primary_assembly.annotation.gtf.gz chr_patch_hapl_scaff.annotation.gtf.gz annotation.gtf.gz GRC$(SPECIES_1L)38.primary_assembly.star_index/$(STAR_VERSION)/SA primary_assembly.annotation.rRNA_complete.bed


# If you don't agree we should preserve the original .gz, remove the .PRECIOUS line
.PRECIOUS: %.genome.fa.gz %.gtf.gz

%.gtf: %.gtf.gz
	zcat $< > $@

%.gtf.bed12: %.gtf
	gtf2bed12 -i < $< > $@

tRNAs.gtf.gz 2wayconspseudos.gtf.gz polyAs.gtf.gz long_noncoding_RNAs.gtf.gz chr_patch_hapl_scaff.basic.annotation.gtf.gz basic.annotation.gtf.gz primary_assembly.annotation.gtf.gz chr_patch_hapl_scaff.annotation.gtf.gz annotation.gtf.gz: %.gtf.gz:
	wget -O $@ $(FTP_PREFIX)/gencode.v$(VERSION).$@

gencode.v$(VERSION)lift37.basic.annotation.gtf.gz:
	wget -O $@ $(FTP_PREFIX)/GRCh37_mapping/$@

%.transcritp_len: %.gtf.gz
	bawk '$$0!~/^#/ && $$3=="exon"' $< | gtf2tab transcript_id | bawk '{print $$9,$$5-$$4}' | bsort -k1,1 -S10% | stat_base -g -t > $@

%.ERCC.genome.fa: ../../../../local/share/data/ERCC.fa %.genome.fa
	cat $^ > $@

%.ERCC.gtf.gz: %.gtf.gz $(TASK_ROOT)/local/share/data/ERCC.gtf
	(cat $^2; zgrep -v '^#' $<) | gzip > $@

#transcripts.fa.gz pc_transcripts.fa.gz pc_translations.fa.gz lncRNA_transcripts.fa.gz: %.fa.gz:
#	wget -O $@ $(FTP_PREFIX)/gencode.v$(VERSION).$@
	
%.genome.fa.gz:
	wget -O $@ -c "$(FTP_PREFIX)/$@"


#customed in makefile:
#.DOC: %.genome.fa.gz
#	GRCm38.p4.genome.fa.gz
#	GRCm38.primary_assembly.genome.fa.gz

%: %.gz
	gunzip $<

%.splitted: %.fa
	mkdir -p $@
	split_fasta -p -o -e -f .fa $< 0 $@

%.clean_id.fa: %.fa
	perl -pe 's/^(>[^\s]+).*/\1/' $< > $@ 


### Genome indices -- in HSAPIENS
#could be improved to be more general #since the genome can be the same or not, and name syntax differs form file to file, I just created the rules I needed

# NB: basic.annotation.gtf.gz.bak is kept as proof, since it's not basic.annotation somehow
# Since:
# 'diff <(less basic.annotation.gtf.gz.bak) <(zcat basic.annotation.gtf.gz)' gives plenty of differences
# 'diff <(less basic.annotation.gtf.gz.bak) less primary_assembly.annotation.gtf' gives no result (=no difference),
# so somehow primary_assembly gtf was used instead of the basic annotation

# NB: previously, in all 3 map *rules, we used cut -f [field nums] to select the fields instead of grep_columns, but from version to version, the order of fields changes so grepping is safer. still TODO, the final bawk should contain a check, maybe postponing the sed that removes "gene_[a-z]"

primary_assembly.annotation.ensg2biotype.map basic.annotation.ensg2biotype.map: %.annotation.ensg2biotype.map: %.annotation.gtf
	grep -v '#' $< | awk '($$3=="gene"){print}' | cut -f9 | sed 's/;\s/\t/g' | grep_columns gene_id gene_type | sed -e 's/gene_[a-z]\+\s//g' | tr -d "\"" > $@

primary_assembly.annotation.ensg2gene_symbol2biotype.map basic.annotation.ensg2gene_symbol2biotype.map: %.annotation.ensg2gene_symbol2biotype.map: %.annotation.gtf
	grep -v '#' $< | awk '($$3=="gene"){print}' | cut -f9 | sed 's/;\s/\t/g' | grep_columns gene_id gene_type gene_name | sed -e 's/gene_[a-z]\+\s//g' | tr -d "\"" | bawk '{print $$1,$$3,$$2}' > $@


primary_assembly.annotation.enst2transcript_name2biotype.map basic.annotation.enst2transcript_name2biotype.map: %.annotation.enst2transcript_name2biotype.map: %.annotation.gtf
	grep -v '#' $< | awk '($$3=="transcript"){print}' | cut -f9 | sed 's/;\s/\t/g' | grep_columns transcript_id transcript_type transcript_name | sed -e 's/transcript_[a-z]\+\s//g' | tr -d "\"" | bawk '{print $$1,$$3,$$2}' > $@

bowtie2_index/GRCh38.primary_assembly.genome.1.bt2 bowtie2_index/GRCm38.primary_assembly.genome.1.bt2: bowtie2_index/%.1.bt2: %.fa
	mkdir -p bowtie2_index
	cd bowtie2_index; bowtie2-build ../$< $*

bowtie1_index/GRCh38.primary_assembly.genome.1.ebwt bowtie1_index/GRCm38.primary_assembly.genome.1.ebwt: bowtie1_index/%.1.ebwt: %.clean_id.fa
	mkdir -p bowtie1_index
	cd bowtie1_index; bowtie-build --threads=$(CORES) ../$< $*

#GRCh38.primary_assembly.star_index/STAR_2.5.3a

pippo:
	echo $(STAR_VERSION)

%.star_index/$(STAR_VERSION)/SA: %.genome.fa primary_assembly.annotation.gtf
	echo PBS -N starIndexing -l select=1:ncpus=$(CORES):mem=32gb;\
        mkdir -p $$(dirname $@);\
	test `STAR --version` = $(STAR_VERSION);\
	STAR --runThreadN $(CORES) --runMode genomeGenerate --genomeDir $$(dirname $@) --genomeFastaFiles $< --sjdbGTFfile $^2 --sjdbOverhang 100 

star.primary-assembly_GTF_FA.done: GRC$(SPECIES_1L)38.primary_assembly.star_index/$(STAR_VERSION)/SA
	touch $@

%.appris_principal.gtf: %.gtf
	bawk '$$3=="transcript"' $< | grep "appris_principal" > $@

## bwa index
bwa/GRCm38.primary_assembly.genome.fa.sa: GRCm38.primary_assembly.genome.fa
	echo PBS -N bwaIndexing -l select=1:ncpus=$(CORES):mem=32gb;\
	mkdir -p $$(dirname $@);\
	condactivate; \
	cd $@;\
	bwa index $<; \
	link_install ../$< $<
##### Genome with EYFGP added # 

EYFP.%.genome.fa: ../../../../local/share/data/EYFP.fa %.genome.fa
	cat $^ > $@


EYFP.%.gtf.gz: %.gtf.gz $(TASK_ROOT)/local/share/data/EYFP.gtf
	(cat $^2; zgrep -v '^#' $<) | gzip > $@


%.bed: %.gtf
	gtf2bed_Aronesty $< | bsort -k1,1 -k2,2n > $@

%.annotation.rRNA.bed: %.annotation.gtf
	perl -lne 'print if m/gene_type "rRNA"/ or m/gene_type "Mt_rRNA"/ or m/gene_type "Mt_tRNA"/' $< | gtf2bed_Aronesty - > $@

repeat_rmsk.ribosomal.bed: ../../../ucsc/$(SPECIES2)/$(UCSC_VERSION)/repeat_rmsk.ribosomal.bed
	cat $< | perl -lane 'if($$F[0]=~m/_/){$$chr=shift(@F); $$chr=~m/chr[^_]+_([^_]+)_?/; $$chr=$$1; $$chr=~s/v(\d+)$$/.\1/; $$_=$$chr."\t".join("\t",@F);} print ' \
	| bawk '{print $$0,$$2,$$3,0,1,$$3-$$2",",0","}' > $@                  * to bed12*

%.annotation.rRNA_complete.bed: %.annotation.rRNA.bed repeat_rmsk.ribosomal.bed
	bsort -k1,1 -k2,2n $^ >$@

%.gtf.most_3P_exon: %.gtf
	 grep -v '^#' $< | gtf2tab gene_name | bawk '$$3=="UTR" || $$3=="exon" {if($$7=="+"){b=$$4}else{b=-$$5} print $$9, b, $$0}' | find_best 1 2 | cut -f 3- | bsort -k1,1V -k4,4n -S5% > $@

%.fa.polyA: %.fa
	repeat_fasta_pipe 'tr -d "\n" | perl -lne "BEGIN{\$$,="\""\t"\""} while(m/(?:A+.A{4,}.A+)/g){\$$l=length(\$$&); print pos()-\$$l,pos(),\$$&}" | append_each_row  -B $$HEADER' < $<\
	| union --allow-duplicates -s | bawk '$$3-$$2>12' > $@
%.fa.polyT: %.fa
	repeat_fasta_pipe 'tr -d "\n" | perl -lne "BEGIN{\$$,="\""\t"\""} while(m/(?:T+.T{4,}.T+)/g){\$$l=length(\$$&); print pos()-\$$l,pos(),\$$&}" | append_each_row  -B $$HEADER' < $<\
	| union --allow-duplicates -s | bawk '$$3-$$2>12' > $@

%.fa.polyAT_extended: %.fa.polyA %.fa.polyT
	cat $^ | bsort -k 1,1 -k2,2n | bawk '{b=$$2-100; if(b<0){b=0} print $$1,b,$$3+100,"polyAT"}' > $@

%.fa.fai: %.fa
	samtools faidx $<

basic.annotation.gene_positions: basic.annotation.gtf.gz
	bawk '$$0!~/^#/ && $$3=="gene"' $< | gtf2tab gene_name | bawk '{print $$1,$$4,$$5,$$9}' > $@
