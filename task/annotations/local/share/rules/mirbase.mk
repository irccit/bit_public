# Copyright 2013 Gabriele Sales <gbrsales@gmail.com>

SPECIES  ?= hsapiens
VERSION  ?= 15
BASE_URL ?= ftp://mirbase.org/pub/mirbase/$(VERSION)

SPECIES_MAP := $(BIOINFO_ROOT)/task/sequences/local/share/species-mirbase.map


ifeq ($(call lt,$(VERSION),18),$(true))
GFF_EXT := gff
else ifeq ($(call lt,$(VERSION),21),$(true))
GFF_EXT := gff2
else
GFF_EXT := gff3
endif

coords.gff.download:
	wget -O $@ "$(BASE_URL)/genomes/$$(translate_value $(SPECIES) $(SPECIES_MAP)).$(GFF_EXT)"

coords.genome: coords.gff.download
	grep -P "Genome assembly:|genome-build-id" $< \
	| quote_exec 'at_least_rows 1 <[sed -r <[s|^.*\s+||]>]>' >$@

ifeq ($(GFF_EXT),gff3)
coords.xz: coords.gff.download
	sed '/^#/d' $< \
	| bawk '{ chr=$$1; start=$$4-1; stop=$$5; \
	          if ($$7=="+") strand=1; else strand=-1; \
	          split($$9, tagged, ";"); \
	          for (t in tagged) { \
	            split(tagged[t], kv, "="); \
	            if (kv[1]=="ID") id=kv[2]; else \
	            if (kv[1]=="Name") name=kv[2]; \
	          }; \
	          print name,chr,start,stop,strand,id }' \
	| xz -9 >$@
else
coords.xz: coords.gff.download
	sed '/^#/d' $< \
	| cut -f 1,4,5,7,9,10 \
	| sed 's|ACC="||; s|ID="||; s|";\s*|\t|; s|";||' \
	| bawk '{print $$6,$$1,$$2-1,$$3,$$4,$$5}' \
	| bawk '{if ($$5=="-") $$5=-1; else $$5=1; print}' \
	| xz -9 >$@
endif

.META: coords.xz
	1  ID         hsa-mir-1302-2
	2  chr        1
	3  start      20229
	4  stop       20366
	5  strand     1
	6  accession  MI0006363

ALL          += coords.xz coords.genome
INTERMEDIATE += coords.gff.download

ifeq ($(call gte,$(VERSION),19),$(true))

aliases.xz:
	wget -O- '$(BASE_URL)/database_files/mirna_mature.txt.gz' \
	| zcat \
	| select_columns 2 3 4 \
	| grep "^$$(translate_value $(SPECIES) $(SPECIES_MAP))-" \
	| bsort -u \
	| xz -9 >$@

.META: aliases.xz
	1  ID         hsa-miR-103a-2-5p
	2  aliases    hsa-miR-103-2*;hsa-miR-103a-2*
	3  accession  MIMAT0009196


ALL += aliases.xz

endif
