VERSION=$(shell basename $(PWD))

obo.gz:
	wget -O - ftp://ftp.geneontology.org/go/ontology-archive/gene_ontology_edit.obo.$(VERSION).gz\
	| zcat \
	| perl -pe "if(m/occurs_in:/){print STDERR 'ERROR: this ontology seem not to be a basig ontology (see .DOC)'; exit(1)}"\
	| gzip > $@

.DOC: obo.gz
	from: http://geneontology.org/page/download-ontology 
	go-basic.obo
	This is the basic version of the GO filtered such that the graph is guaranteed to be acyclic, and annotations can be propagated up the graph. The relations included are is_a, part_of, regulates, negatively_regulates and positively_regulates. This version excludes relationships that cross the 3 main GO hierarchies. This is the version that should be used with most GO-based annotation tools.
	We download the file from ftp://ftp.geneontology.org/go/ontology-archive/ ho be versioned but we need to check that the downloaded file is "basic".
	We do this by checking for the presence of the "malicious" occurs_in relationship

id2name.gz: obo.gz
	zcat $< \
	| perl -ne 'chomp;if(m/^id: (GO:\d+)/){ print "$$1\t"; $$_=<>; if(m/^name: (.*)/){print "$$1\n"}else{die("ERROR: id not followed by name")}}'\
	| gzip > $@

id2parents.gz: obo.gz
	zcat $< \
	| awk -F" " '   $$1=="[Term]" {printf "\n"}\
			$$1=="id:" || $$1=="alt_id:" || $$1=="is_a:" {printf $$2 "\t"} \
			$$1~/^relationship:/ {printf $$3 "\t"}'\
	| bawk '$$2'\                *child without $$2 are obsolete or top level terms*
	| perl -lane '$$child = shift(@F); for(@F){print "$$child\t$$_"}'\
	| bawk '$$2~/^GO:/'\         *remove spourious output from [typedef] entities*
	| gzip > $@

id2anchestors.gz: id2parents.gz
	zcat $< | full_anchestors \
	| bawk '{print $$2,$$2; print $$1,$$1; print $$1,$$2}' | bsort | uniq \
	| gzip > $@

.META: id2anchestors.gz
	1 child
	2 anchestor
