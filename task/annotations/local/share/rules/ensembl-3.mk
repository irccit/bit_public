# Copyright 2008-2011,2016 Gabriele Sales <gbrsales@gmail.com>
# Copyright 2009 Ivan Molineris <ivan.molineris@gmail.com>

SPECIES            ?= hsapiens
VERSION            ?= 50
UPSTREAM_EXTENSION ?= 10000

SPECIES_MAP     := $(BIOINFO_ROOT)/task/annotations/local/share/species-ensembl.map
ENSEMBL_SPECIES := $(shell translate_value $(SPECIES) $(SPECIES_MAP))
MYSQL_CMD       := mysql --user=anonymous --host=ensembldb.ensembl.org --port=5306 -BCAN
MYSQL_CMD_MART  := mysql --user=anonymous --host=martdb.ensembl.org -P 5316 -BCAN ensembl_mart_$(VERSION)
SEQUENCE_DIR    ?= $(BIOINFO_ROOT)/task/sequences/dataset/ensembl/$(SPECIES)/$(VERSION)
FTP_URL         ?= ftp://ftp.ensembl.org/pub/release-$(VERSION)/
GO_TREE_VERSION ?= 2016-11-01
#ENSEMBL_ARCHIVE ?= www
ENSEMBL_ARCHIVE ?= $(shell grep $(VERSION) $(PRJ_ROOT)/local/share/data/ensembl.archive_url | cut -f 2).archive
### NB: NOT ALL VERSIONS ARE AVAILABLE!! SEE http://www.ensembl.org/info/website/archives/assembly.html
MARTSERVICE_URL ?= http://$(ENSEMBL_ARCHIVE).ensembl.org/biomart/martservice
GO_FILTER	?= 
#GO_FILTER	?= curated.
#GO_FILTER	?= strong.

pluto:
	echo $(MARTSERVICE_URL); echo $(SPECIES)

SPECIES_TO_REMOVE_ORTHOLOGS=phamadryas

import gene-structures
extern $(SEQUENCE_DIR)/all_chr.len as ALL_CHR_LEN


database.name:
	set -e; \
	pattern="$(ENSEMBL_SPECIES)_core_$(VERSION)_%"; \
	$(MYSQL_CMD) <<<"SHOW DATABASES LIKE '$$pattern'" \
	| at_least_rows -e 1 >$@


## External references
##

ifeq ($(call lt,$(VERSION),67),$(true))

define xref_sql
SELECT stable_id, db_name, x.dbprimary_acc, x.display_label, x.description \
FROM object_xref AS ox \
  LEFT JOIN $(1)_stable_id AS gs ON ox.ensembl_id = gs.$(1)_id \
  LEFT JOIN xref AS x ON ox.xref_id = x.xref_id \
  LEFT JOIN external_db AS ed ON x.external_db_id = ed.external_db_id \
WHERE ensembl_object_type='$(1)' AND stable_id is not NULL
endef

else # VERSION >= 67

define xref_sql
SELECT stable_id, db_name, x.dbprimary_acc, x.display_label, x.description \
FROM object_xref AS ox \
  LEFT JOIN $(1) AS g ON ox.ensembl_id = g.$(1)_id \
  LEFT JOIN xref AS x ON ox.xref_id = x.xref_id \
  LEFT JOIN external_db AS ed ON x.external_db_id = ed.external_db_id \
WHERE ensembl_object_type='$(1)' AND stable_id is not NULL
endef

endif


define xref
	$(MYSQL_CMD) $$(cat $<) <<<"$(call xref_sql,$(1))" \
	| bsort -u \
	| gzip >$@
endef


## Genes
##

ifeq ($(call lt,$(VERSION),67),$(true))

define gene_sql
SELECT g.stable_id, name, seq_region_start-1, seq_region_end, seq_region_strand, biotype, display_label, g.description \
FROM gene AS g \
  LEFT JOIN gene_stable_id AS gs ON g.gene_id = gs.gene_id \
  LEFT JOIN seq_region AS sr ON g.seq_region_id = sr.seq_region_id \
  LEFT JOIN xref AS xr ON g.display_xref_id = xr.xref_id
endef

else # VERSION >= 67

define gene_sql
SELECT g.stable_id, name, seq_region_start-1, seq_region_end, seq_region_strand, biotype, display_label, g.description \
FROM gene AS g \
  LEFT JOIN seq_region AS sr ON g.seq_region_id = sr.seq_region_id \
  LEFT JOIN xref AS xr ON g.display_xref_id = xr.xref_id
endef

endif

genes.gz: database.name
	$(MYSQL_CMD) $$(cat $<) <<<"$(gene_sql)" \
	| bsort -u \
	| gzip >$@

.META: genes.gz
	1  gene_ID      ENSG00000000003
	2  chr          X
	3  start        99770450
	4  stop         99778450
	5  strand       -1
	6  biotype      protein_coding
	7  label        TSPAN6
	8  description  Tetraspanin-6 (Tspan-6)(Transmembrane 4 superfamily member 6)(T245 protein)(Tetraspanin TM4-D)(A15 homolog) [Source:UniProtKB/Swiss-Prot;Acc:O43657]


gene-xref.map.gz: database.name
	$(call xref,gene)

.META: gene-xref.map.gz
	1	gene_ID  ENSG00000000003
	2	database_ID    EMBL
	3	external_ID    AC100821
	4	external_NAME	PINCO
	5	external_DESC	pingo gene involved in pallino proces

#gene-entrez.map.gz: gene-xref.map.gz
#	bawk '$$database_ID == "EntrezGene" {print $$gene_ID, $$external_ID}' $< \
#	| bsort -u \
#	| gzip -9 >$@

#.META: gene-entrez.map.gz
#	1  gene_ID    ENSG00000000003
#	2  entrez_ID  7105


## Transcripts
##

ifeq ($(call lt,$(VERSION),67),$(true))

define transcript_sql
SELECT ts.stable_id, gs.stable_id, sr.name, seq_region_start-1, seq_region_end, seq_region_strand, biotype, status \
FROM transcript AS t \
  LEFT JOIN transcript_stable_id AS ts ON t.transcript_id = ts.transcript_id \
  LEFT JOIN gene_stable_id AS gs ON t.gene_id = gs.gene_id \
  LEFT JOIN seq_region AS sr ON t.seq_region_id = sr.seq_region_id \
WHERE is_current = 1 AND \
  sr.name NOT LIKE '%\_%'            * ignore non-standard chromosomes *
endef

else # VERSION >= 67

define transcript_sql
SELECT t.stable_id, g.stable_id, sr.name, t.seq_region_start-1, t.seq_region_end, t.seq_region_strand, t.biotype, t.status \
FROM transcript AS t \
  LEFT JOIN gene AS g ON t.gene_id = g.gene_id \
  LEFT JOIN seq_region AS sr ON t.seq_region_id = sr.seq_region_id \
WHERE t.is_current = 1 AND \
  sr.name NOT LIKE '%\_%'            * ignore non-standard chromosomes *
endef

endif

transcript.info: database.name
	$(MYSQL_CMD) $$(cat $<) <<<"$(transcript_sql)" \
	| bsort -u >$@


transcripts.gz: transcript.info
	cut -f1,3- $< \
	| gzip >$@

.META: transcripts.gz
	1  transcript_ID  ENSMUST00000000001
	2  chr            3
	3  start          107910199
	4  stop           107949007
	5  strand         -1
	6  biotype        protein_coding
	7  status         KNOWN


ifeq ($(call lt,$(VERSION),67),$(true))

define transcript_gene_sql
SELECT ts.stable_id, gs.stable_id \
FROM transcript AS t \
  LEFT JOIN transcript_stable_id AS ts ON t.transcript_id = ts.transcript_id \
  LEFT JOIN gene_stable_id AS gs ON t.gene_id = gs.gene_id \
WHERE is_current = 1
endef

else # VERSION >= 67

define transcript_gene_sql
SELECT t.stable_id, g.stable_id \
FROM transcript AS t \
  LEFT JOIN gene AS g ON t.gene_id = g.gene_id \
WHERE t.is_current = 1
endef

endif

transcript-gene.map.gz: database.name
	$(MYSQL_CMD) $$(cat $<) <<<"$(transcript_gene_sql)" \
	| bsort -u \
	| gzip >$@

.META: transcript-gene.map.gz
	1  transcript_ID  ENSMUST00000000001
	2  gene_ID        ENSMUSG00000000001


transcript-xref.map.gz: database.name
	$(call xref,transcript)

.META: transcript-xref.map.gz
	1  transcript_ID   ENST00000000412
	2  database_ID     HGNC_transcript_name
	3  external_ID     M6PR-001
	4  external_name   M6PR-001
	5  external_descr  integrin, alpha 2 (CD49B, alpha 2 subunit of VLA-2 receptor)


transcript-refseq.map.gz: transcript-xref.map.gz translation-xref.map.gz translation-transcript.map.gz
	( bawk '$$2~/RefSeq_dna/' $<; \
	  bawk '$$2~/RefSeq_dna/' $^2 \
	  | translate <(zcat $^3) 1 \
	) \
	| cut --complement -f2 \
	| bsort -u \
	| gzip >$@


transcript_structures.gz:
	with_ensembl_api $(VERSION) fetch-ensembl-transcript_structures $(ENSEMBL_SPECIES) \
	| gzip >$@

.META: transcript_structures.gz
	FASTA
	> transcript_ID chromosome coding_start coding_stop strand
	exon_ID exon_start exon_stop


cdnas.gz:
	with_ensembl_api $(VERSION) fetch-ensembl-cdnas $(ENSEMBL_SPECIES) \
	| bsort -u \
	| gzip >$@

.META: cdnas.gz
	1  ID            ENSMUST00000000001
	2  length        3204
	3  coding_start  84
	4  coding_stop   1149
	5  5utr_start    0
	6  3utr_stop     3204


transcripts.gtf.gz:
	if [[ $$(curl -s '$(FTP_URL)/gtf/$(ENSEMBL_SPECIES)/' | grep '$(VERSION).gtf.gz$$' | wc -l) -eq 1 ]]; then\
		wget -O- '$(FTP_URL)/gtf/$(ENSEMBL_SPECIES)/*.$(VERSION).gtf.gz' \
		| zcat \
		| gzip -9 >$@;\
	else\
		echo "ERROR: more than one file matching the pattern $(FTP_URL)/gtf/$(ENSEMBL_SPECIES)/*.$(VERSION).gtf.gz" >&2;\
		exit 2;\
	fi;

%.dexseq.gff: %.gtf.gz
	zcat $< > $@.tmp
	python $(BIOINFO_ROOT)/binary/$(BIOINFO_HOST)/local/lib/R/DEXSeq/python_scripts/dexseq_prepare_annotation.py $@.tmp $@
	rm $@.tmp


## Exons
##

ifeq ($(call lt,$(VERSION),67),$(true))

define exon_sql
SELECT es.stable_id, sr.name, seq_region_start-1, seq_region_end, seq_region_strand, phase, end_phase \
FROM exon AS e \
  LEFT JOIN exon_stable_id AS es ON e.exon_id = es.exon_id \
  LEFT JOIN seq_region AS sr ON e.seq_region_id = sr.seq_region_id \
WHERE is_current = 1 AND \
  sr.name NOT LIKE '%\_%'
endef

else # VERSION >= 67

define exon_sql
SELECT e.stable_id, sr.name, seq_region_start-1, seq_region_end, seq_region_strand, phase, end_phase \
FROM exon AS e \
  LEFT JOIN seq_region AS sr ON e.seq_region_id = sr.seq_region_id \
WHERE is_current = 1 AND \
  sr.name NOT LIKE '%\_%'
endef

endif

exons.gz: database.name
	$(MYSQL_CMD) $$(cat $<) <<<"$(exon_sql)" \
	| bsort -u \
	| gzip >$@

.META: exons.gz
	1  exon_ID    ENSMUSE00000097910
	2  chr        10
	3  start      6794511
	4  stop       6794667
	5  strand     -1
	6  phase      2
	7  end_phase  0


ifeq ($(call lt,$(VERSION),67),$(true))

define exon_transcript_sql
SELECT es.stable_id, ts.stable_id \
FROM exon_transcript AS et \
  LEFT JOIN exon_stable_id AS es ON et.exon_id = es.exon_id \
  LEFT JOIN transcript_stable_id AS ts ON et.transcript_id = ts.transcript_id
endef

else # VERSION >= 67

define exon_transcript_sql
SELECT e.stable_id, t.stable_id \
FROM exon_transcript AS et \
  LEFT JOIN exon AS e ON et.exon_id = e.exon_id \
  LEFT JOIN transcript AS t ON et.transcript_id = t.transcript_id
endef

endif

exon-transcript.map.gz: database.name
	$(MYSQL_CMD) $$(cat $<) <<<"$(exon_transcript_sql)" \
	| bsort -u \
	| gzip >$@

.META: exon-transcript.map.gz
	1  exon_ID        ENSMUSE00000097910
	2  transcript_ID  ENSMUST00000019896


introns.gz: exons.gz exon-transcript.map.gz
	zcat $<\
	| translate -a -d <(zcat $(word 2,$^)) 1 \
	| expandsets -s ',' 2 \
	| bawk '{print $$3,$$4,$$5,$$2}' \
	| introns_coords \
	| bsort -u \
	| gzip > $@

.META: introns.gz
	1  chr           1
	2  start         141594868
	3  stop          141598375
	4  transcrit_ID  ENSMUST00000111986


## Translations (proteins)
##

ifeq ($(call lt,$(VERSION),67),$(true))

define translation_transcript_sql
SELECT ps.stable_id, ts.stable_id \
FROM translation AS p \
  LEFT JOIN translation_stable_id AS ps ON p.translation_id = ps.translation_id \
  LEFT JOIN transcript_stable_id AS ts ON p.transcript_id = ts.transcript_id \
WHERE ps.stable_id is not NULL AND \
  ts.stable_id is not NULL
endef

else # VERSION >= 67

define translation_transcript_sql
SELECT p.stable_id, t.stable_id \
FROM translation AS p \
  LEFT JOIN transcript AS t ON p.transcript_id = t.transcript_id \
WHERE p.stable_id is not NULL AND \
  t.stable_id is not NULL
endef

endif

translation-transcript.map.gz: database.name
	$(MYSQL_CMD) $$(cat $<) <<<"$(translation_transcript_sql)" \
	| bsort -u \
	| gzip >$@

.META: translation-transcript.map.gz
	1  translation_ID  ENSP00000000233
	2  transcript_ID   ENST00000000233


translation-gene.map.gz: translation-transcript.map.gz transcript-gene.map.gz
	zcat $< \
	| translate <(zcat $^2) 2 \
	| bsort -u \
	| gzip >$@

.META: translation-gene.map.gz
	1  translation_ID  ENSP00000000233
	2  gene_ID         ENSG00000004059


translation-xref.map.gz: database.name
	$(call xref,translation)

.META: translation-xref.map.gz
	1  translation_ID  ENSP00000000412
	2  database_ID     goslim_goa
	3  external_ID     GO:0005886
	4  external_name   GO:0005886
	5  external_descr  plasma membrane


## Repeats
##
repeats.gz: database.name
	$(MYSQL_CMD) $$(cat $<) <<<" \
		SELECT sr.name, seq_region_start-1, seq_region_end, seq_region_strand, repeat_name, repeat_class \
		FROM repeat_feature AS rf \
			LEFT JOIN repeat_consensus AS rc ON rf.repeat_consensus_id = rc.repeat_consensus_id \
			LEFT JOIN seq_region AS sr ON rf.seq_region_id = sr.seq_region_id \
		WHERE sr.name NOT LIKE '%\_%'" \
	| bsort -k1,1 -k2,2n \
	| uniq \
	| gzip -9 >$@

.META: repeats.gz
	1  chr     1
	2  start   3000001
	3  stop    3000156
	4  strand  -1
	5  name    L1_Mur2
	6  class   LINE/L1


## Other
##
gene-readable.map.gz:
	sed 's/__SPECIES__/$(SPECIES)/g' < $(TASK_ROOT)/local/share/queries/mart_readable_$(VERSION).xml > $@_query.xml
	wget -q -O - --post-file $@_query.xml $(MARTSERVICE_URL) \
	| gzip > $@
	rm $@_query.xml

.META: gene-readable.map.gz
	1  gene_id      ENSG00000208234
	2  gene_name	AC019043.8
	3  description  cippo lippo
	4  biotype      scRNA_pseudogene
	5  transcript_gencode_basic
	6  status       NOVEL

gene-name.map.gz: gene-readable.map.gz
	bawk '{print $$gene_id,$$gene_name}' $< | bsort | uniq | gzip >$@

gene-readable.map.clean.gz: gene-readable.map.gz
	zcat $< \        * separo la source dalla description, perche` contiene ; e spesso fa casino*
	| perl -lne 'BEGIN{$$,="\t"} @F=split(/\t/); $$source=""; $$source = $$1 if $$F[2]=~s/(\[.*\])//; $$F[2].="\t$$source"; print @F ' \
	| gzip >$@

.META: gene-readable.map.clean.gz
	1  gene_id      ENSG00000208234
	2  gene_name    AC019043.8
	3  description  cippo lippo
	4  gene_name_source
	5  biotype      scRNA_pseudogene
	6  transcript_gencode_basic
	7  status       NOVEL

transcript-readable.map.gz: mart_gene-transcript-exon-coords.gz gene-readable.map.gz
	zcat $< | cut -f -2 | uniq | translate -a -d <(bawk '{print $$gene_id,$$gene_name,$$biotype}' $^2 | bsort | uniq) 1 | gzip > $@

GENE_SYMBOL.chip: gene-readable.map.gz
	echo -e "Probe Set ID\tGene Symbol\tGene Title\tAliases" > $@
	bawk '{print $$gene_name,$$gene_name,$$description,"NA"}' $< | bsort -S5% | uniq >> $@

.DOC: GENE_SYMBOL.chip
	analogous to ftp://ftp.broadinstitute.org/pub/gsea/annotations/GENE_SYMBOL.chip
	used in gsea cli

mart_ensp2ensg.gz:
	sed 's/__SPECIES__/$(SPECIES)/g' < $(TASK_ROOT)/local/share/queries/mart_ensp2ensg.xml > $@_query.xml
	wget -q -O - --post-file $@_query.xml $(MARTSERVICE_URL) \
	| gzip > $@
	rm $@_query.xml

.META: mart_ensp2ensg.gz
	1	ensg
	2	enst
	3	ensp

mart_gene-transcript-exon-coords.gz:
	sed 's/__SPECIES__/$(SPECIES)/g' < $(TASK_ROOT)/local/share/queries/mart_gene-transcript-exon-coords.xml > $@_query.xml
	wget -q -O - --post-file $@_query.xml $(MARTSERVICE_URL) \
	| gzip > $@
	rm $@_query.xml

.META: mart_gene-transcript-exon-coords.gz
	1       gene_id         ENSG00000276385
	2       transcript_id   ENST00000618935
	3       exon_id         ENSE00003733355
	4       chr             20
	5       gene_b          8934984
	6       gene_e          8935481
	7       transcript_b    8934984
	8       transcript_e    8935481
	9       exons_b         8934984
	10      exons_e         8935481
	11      strand          +


gene_transcript_tss_strand.gz: mart_gene-transcript-exon-coords.gz
	bawk '{ print $$gene_id, $$transcript_id, $$chr, $$strand * $$transcript_b, $$strand; \
		print $$gene_id, $$transcript_id, $$chr, $$strand * $$transcript_e, $$strand}' $<\
	| uniq \
	| find_best -r 2 4 \
	| bawk '{if($$5==-1){$$4=-$$4} print}' | gzip > $@

.META: mart_gene-transcript-exon-coords.gz
	1	gene_id		ENSG00000276385
	2	transcript_id	ENST00000618935
	3	chr		20
	4	tss	8934984
	5	strand		+

gene-refseq.map.gz: 
	sed 's/__SPECIES__/$(SPECIES)/g' < $(TASK_ROOT)/local/share/queries/mart_gene-refseq_map.xls > $@_query.xml
	wget -q -O - --post-file $@_query.xml $(MARTSERVICE_URL) \
	| gzip > $@
	rm $@_query.xml

.META: gene-refseq.map.gz
	1	gene_id
	2	transcript_id
	3	refseq_mrna
	4	refseq_ncrna
	5	refseq_peptide

INTERMEDIATE += gene-omim.query
gene-omim.query: $(TASK_ROOT)/local/share/queries/mart_omim.xml
	sed 's/__SPECIES__/$(SPECIES)/g' <$< >$@

gene-omim.map.gz: gene-omim.query
	wget -q -O - --post-file $< $(MARTSERVICE_URL) \
	| bawk '$$2' \    *select only genes whit a omimi disease associated *
	| gzip >$@

gene-homologous.map.gz:
	(\
	for i in $$( $(MYSQL_CMD_MART) <<< "SHOW TABLES LIKE '$(SPECIES)_gene_ensembl__homolog_%'" | unhead -n 2 ); do \
		species=` echo $$i | sed 's/$(SPECIES)_gene_ensembl__homolog_//' | sed 's/__dm$$//'`; \
		$(MYSQL_CMD_MART) <<< 	"SELECT	\
		stable_id_4016_r3, stable_id_4016, stable_id_4016_r1, stable_id_4016_r2, description_4014 stable_id_4016_r1 FROM $$i WHERE stable_id_4016_r3 is not NULL or stable_id_4016 is not NULL or stable_id_4016_r2 is not NULL ;" \
		| append_each_row $$species; \
	done; \
	) | gzip > $@

gene-homologous.one2one.to_clean: gene-homologous.map.gz
	zcat $<\
	| bawk '$$5=="ortholog_one2one" {print $$4}'\
	| symbol_count | bawk '$$2>1' > $@

gene-homologous.one2one_clean.map.gz: gene-homologous.map.gz gene-homologous.one2one.to_clean
	zcat $< \
	| bawk '$$5=="ortholog_one2one" {print $$2,$$4,$$6}'\
	| filter_1col -v 2 <(cut -f 1 $^2) \
	| gzip > $@


## Upstreams
##

upstreams.gz: genes.gz $(SEQ_DIR)/chrs.len 
	zcat genes.gz \
	| upstreams $^2 $(UPSTREAM_EXTENSION) \
	| gzip > $@

nr_upstreams.gz: upstreams.gz genes.gz
	intersection -l $@.missing \
		<(zcat $< \
			| bawk '$$3 != 0' \					* in the MT there is this case *
			| nsort -k 2,2 -k3,3n \
		):2:3:4 \
		<(zcat $^2 | bsort -k2,2 -k3,3n):2:3:4 \
	| cut -f -9 \
	| bawk '{if($$9==1){print $$1,$$3,$$5,$$8,$$9 }else{ print $$1,$$4,$$2,$$8,$$9}}' \
	| bawk '{print $$0,$$3-$$2}' \
	| find_best -r 4 6 \    * the shortest *
	| cut -f -5 > $@.tmp
	cat $@.missing $@.tmp \
	| bawk '{print $$4,$$1,$$2,$$3,$$5}'| gzip > $@
	rm $@.missing $@.tmp

%.mart.homolog.gz:
	sed 's/__SPECIES__/$(SPECIES)/g; s/__HOMOLOG_SPECIES__/$*/g' < $(PRJ_ROOT)/local/share/queries/mart_hortologous.xml > $@.tmp
	wget -q -O - --post-file $@.tmp $(MARTSERVICE_URL) | gzip > $@
	rm $@.tmp

.META: *.mart.homolog.gz *.mart.homolog.one2one_through_gene_name.gz *.mart.homolog.one2one_through_gene_name.missing_with_description.gz
	1	ensembl_gene                 
	2	homolog_gene
	3	orthology_type              
	4	subtype                      
	5	orthology_confidence         
	6	perc_id                      
	7	canonical_transcript_protein 
	8	ensembl_peptide              
	9	chromosome                 
	10	chrom_start                  
	11	chrom_end                    
	12	perc_id_r1                   
	13	gene_name
	14	homolog_gene_name

%.mart.homolog.one2one_through_gene_name.gz: %.mart.homolog.gz gene-name.map.gz ../../%/$(VERSION)/gene-name.map.gz
	(\
		bawk '$$homolog_gene && $$orthology_type=="ortholog_one2one"' $<;\
		bawk '$$homolog_gene && $$orthology_type!="ortholog_one2one"' $<\
		| translate -a <(zcat $^2) 1 \
		| translate -a -k <(	bawk '{print $$2,$$1}' $^3 \
					| collapsesets 2 | grep -v ';'\            *take only names associated to only one homolog_gene*
					| bawk '{print $$2,$$1}'\
			 	  ) 3 \
		| bawk 'BEGIN{IGNORECASE = 1} $$2==$$4 {print $$1,$$3,$$5"_uniq_through_gene_name",$$6~14}' \
	) \
	| translate -a -r <(zcat $^2) 1 \
	| translate -a -r <(zcat $^3) 2 \
	| gzip > $@


%.mart.homolog.one2one_through_gene_name.missing_with_description.pre.gz: %.mart.homolog.gz gene-readable.map.gz ../../%/$(VERSION)/gene-readable.map.gz 
	bawk '!$$homolog_gene' $<\
	| translate -k -a <(bawk '$$description {print $$gene_id,$$gene_name,$$description}' $^2 | perl -pe 's/\s+\[.*//' | sort | uniq \
				| translate -a -f 3 -d -k  <(bawk '{print $$gene_id,$$gene_name,$$description}' $^3 | perl -pe 's/\s+\[.*//' | sort | uniq) 3 \
		) 1 \
	| grep -v ';' \         *take only description associated to only one gene*
	| gzip > $@

%.mart.homolog.one2one_through_gene_name.missing_with_description.gz: %.mart.homolog.one2one_through_gene_name.gz %.mart.homolog.one2one_through_gene_name.missing_with_description.pre.gz
	(zcat $<;\
	 bawk '{print $$1,$$4,"ortholog_through_description","NA","NA","NA","NA","NA","NA","NA","NA","NA",$$2,$$5}' $^2;\
	) | gzip > $@


ALL.mart.homolog.gz: $(addsuffix .mart.homolog.gz, $(shell cut -f 1 $(BIOINFO_ROOT)/task/annotations/local/share/ucsc_species.map | grep -v $(SPECIES) | grep -v SPECIES_TO_REMOVE_ORTHOLOGS))
	(for i in `cut -f 1 $(BIOINFO_ROOT)/task/annotations/local/share/ucsc_species.map | grep -v $(SPECIES)`; do \
		echo ">$$i";\
		zcat $$i.mart.homolog.gz;\
	done) | gzip -6 > $@

gene-phenotypes.map.gz:
	sed 's/__SPECIES__/$(SPECIES)/g; s/__HOMOLOG_SPECIES__/$*/g' < $(PRJ_ROOT)/local/share/queries/mart_pheno.83.xml > $@.tmp
	wget -q -O - --post-file $@.tmp $(MARTSERVICE_URL) | gzip > $@
	rm $@.tmp

gene-GCcontent.map.gz:
	sed 's/__SPECIES__/$(SPECIES)/g; s/__HOMOLOG_SPECIES__/$*/g' < $(PRJ_ROOT)/local/share/queries/mart_gene-GCcontent.xml > $@.tmp
	wget -q -O - --post-file $@.tmp $(MARTSERVICE_URL) | gzip > $@
	rm $@.tmp



ALL          += genes.gz gene-xref.map.gz \
                transcripts.gz transcript-gene.map.gz transcript-xref.map.gz transcripts.gtf.gz \
                transcript_structures.gz exons.gz exon-transcript.map.gz \
                translation-transcript.map.gz translation-xref.map.gz
CLEAN        += gene-entrez.map.gz \
                transcript-refseq.map.gz \
	        translation-gene.map.gz \
                cdnas.gz introns.gz \
                upstreams.gz nr_upstreams.gz \
                repeats.gz
INTERMEDIATE += database.name transcript.info

ensg2entrez.gz:
	sed 's/__SPECIES__/$(SPECIES)/g' < $(TASK_ROOT)/local/share/queries/mart_ensg2entrez.$(ENSEMBL_ARCHIVE).xml > $@_query.xml
	wget -q -O - --post-file $@_query.xml $(MARTSERVICE_URL) \
	| gzip >$@
	rm $@_query.xml

.META: ensg2entrez.gz
	1	ensg
	2	enst
	3	entrez
	4	entrez_transcript_id

ensg2entrez.clean.gz: ensg2entrez.gz
	bawk '$$entrez {print $$ensg,$$entrez}' $< \
	| gzip > $@

.META: ensg2entrez.clean.gz
	1	ensg
	2	entrez

ensg_go.gz:
	sed 's/__SPECIES__/$(SPECIES)/g' < $(TASK_ROOT)/local/share/queries/mart_go.$(ENSEMBL_ARCHIVE).xml > $@_query.xml
	wget -q -O - --post-file $@_query.xml $(MARTSERVICE_URL) \
	| bawk '$$2' \
	| gzip >$@
	rm $@_query.xml

.META: ensg_go.gz
	1	ensg		ENSG00000281614
	2	go_id		GO:0005886
	3	go_name		plasma membrane
	4	go_desc		"The membrane surrounding a cell that separates the cell from its external environment. It consists of a phospholipid bilayer and associated proteins." [ISBN:0716731363]
	5	go_linkage_type IEA
	6	go_namespace	cellular_component

ensg_go_no_definition.gz:
	sed 's/__SPECIES__/$(SPECIES)/g' < $(TASK_ROOT)/local/share/queries/mart_go_no_definition.$(ENSEMBL_ARCHIVE).xml > $@_query.xml
	wget -q -O - --post-file $@_query.xml $(MARTSERVICE_URL) \
	| bawk '$$2' \
	| gzip >$@
	rm $@_query.xml

.META: ensg_go.gz ensg_go.strong.gz ensg_go.curated.gz ensg_go.all.gz
	1	ensg		ENSG00000281614
	2	go_id		GO:0005886
	3	go_name		plasma membrane
	4	go_desc		plasma membrane
	5	go_linkage_type IEA			http://www.geneontology.org/page/guide-go-evidence-codes
	6	go_namespace	cellular_component

ensg_go.strong.gz ensg_go.curated.gz: ensg_go.%.gz: ensg_go.gz ../../../../local/share/data/GO_evidence_code.%.txt
	zcat $< | filter_1col 5 $^2 | gzip > $@
ensg_go.all.gz: ensg_go.gz
	ln -sf $< $@

ensg_go.inclusive.molecular_function.$(GO_FILTER)gz ensg_go.inclusive.cellular_component.$(GO_FILTER)gz ensg_go.inclusive.biological_process.$(GO_FILTER)gz: ensg_go.inclusive.%.$(GO_FILTER)gz: ensg_go.$(GO_FILTER)gz $(BIOINFO_ROOT)/task/annotations/dataset/GO/geneontology.org/$(GO_TREE_VERSION)/id2anchestors.gz
	bawk '$$go_namespace=="$*"' $< \
	| bawk '$$2!="GO:0005575" && $$2!="GO:0003674" && $$2!="GO:0008150"'\       * removing non translable molecular_function cellular_component biological_process *
	| cut -f 1,2 \
	| translate -a -j -d <(zcat $^2) 2 \             *TODO remove the -v and recover 21 obsolete terms*
	| bawk '$$3!="NA"{print $$1,$$3; print $$1,$$2;}'\
	| bsort | uniq \
	| gzip > $@







##########################
#
#	rules related to CTGB pipelines
#

ensGenes.gtf: transcripts.gtf.gz
	zcat $< > $@

ensGenes.with_chr.gtf: ensGenes.gtf
	bawk '{if($$1!~/^#/){\
		if($$1!~/^GJ/){\
			if($$1=="MT"){\
				$$1="M"\
			} \
			print "chr" $$0\
		}else{\
			print $$0\
		}\
	}}' $< >$@
#bawk '{if($$1=="chrM"){$$1="chrMT"} print}' $< > $@

%.protein_coding.gtf: %.gtf
	perl -lne 'my $$bt="NA";   if(m/gene_biotype\s+"([^\s"]+)"/){$$bt=$$1;}    if($$bt eq "NA"){die("ERROR: can not found gene_biotype")}   if($$bt eq "protein_coding"){print}' $< > $@
	
%.bed: %.gtf
	/lustre1/tools/bin/gtf2bed $< > $@ 


-include override.mk
## include it only if it exists: it contains rules that need to be different in some annotation versions, e.g. right now (oct2016) we created it because local/share/queries/mart_readable.xml needs a different format for older versions (such as 75, hence local/share/queries/mart_readable_75.xml), and it should be linked in all the versions that require such format # probably at least all rules before version 75

go_name_desc.gz: ensg_go.gz
	zcat $< | cut -f 2,3,4 | bsort | uniq | gzip > $@

%.header_added.gz: %.gz
	(bawk -M $< | cut -f 2 | transpose; zcat $<) | gzip > $@

mmusculus.mart.homolog.name.gz: mmusculus.mart.homolog.gz gene-readable.map.clean.gz ../../mmusculus/$(VERSION)/gene-readable.map.gz
	bawk '$$2' $< | translate -a <(bawk '{print $$gene_id,$$gene_name}' $^2 | sort | uniq) 1 | translate -a <(bawk '{print $$gene_id,$$gene_name}' $^3 | sort | uniq) 3 | gzip > $@

.META: mmusculus.mart.homolog.name.gz 
	1	ensembl_gene         
	2	ensembl_gene_name	
	3	homolog_gene
	4	homolog_gene_name
	3	orthology_type              
	4	subtype                      
	5	orthology_confidence         
	6	perc_id                      
	7	canonical_transcript_protein 
	8	ensembl_peptide              
	9	chromosome                 
	10	chrom_start                  
	11	chrom_end                    
	12	perc_id_r1                   
	13	gene_name
	14	homolog_gene_name

gene-refseq.name.map.gz: gene-refseq.map.gz gene-readable.map.gz
	zcat $< | translate -a <(bawk '{print $$gene_id,$$gene_name}' $^2 | uniq | sort | uniq ) 1 | bawk '$$4 {print $$1~3,$$4} $$5 {print $$1~3,$$5} $$6 {print $$1~3,$$6}' | gzip > $@

.META: gene-refseq.name.map.gz
	1	gene_id	ENSG00000198888
	2	gene_name	MT-ND1
	3	transcript_id	ENST00000361390
	4	refseq	YP_003024026

