# Copyright 2008-2010 Gabriele Sales <gbrsales@gmail.com>


# utr_coords
# $1: prefix
# $2: side (5 or 3)
# $3: annotation dir
#
# Will define a target $(1).coords and some variables with prefix $(1)-$(2)utr.coords

define utr_coords
$(eval $(call utr_coords_body,$(1),$(2),$(3)))
endef

define utr_coords_body
$$(call __bmake_extern,$(3)/transcript_structures.gz,$(1)_STRUCTURES)

$(1)-$(2)utr.coords: $$($(1)_STRUCTURES)
	zcat $$< \
	| genome2cdna --print --$(2)utr >$$@
endef


# utr_sequences
# $1: prefix
# $2: side (5 or 3)
# $3: sequence dir
# $4: annotation dir
#
# Will define the target $(1)-$(2)utr.fa and some variables with prefix $(1)

define utr_sequences
$(eval $(call utr_sequences_body,$(1),$(2),$(3),$(4)))
endef

define utr_sequences_body
$$(call __bmake_extern,$(4)/transcript_structures.gz,$(1)_STRUCTURES)
$$(call __bmake_extern,$$(addsuffix .fa,$$(addprefix $(3)/,$(shell cd $(3); bmake_query dna))),$(1)_SEQUENCES)

$(1)-$(2)utr.fa: $$($(1)_STRUCTURES) $$($(1)_SEQUENCES)
	zcat $$< \
	| genome2cdna --$(2)utr $(3) >$$@
endef
