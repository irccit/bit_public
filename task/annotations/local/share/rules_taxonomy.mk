include $(BIOINFO_ROOT)/local/share/makefile/global-rules.mk

URL_PREFIX = ftp://ftp.ncbi.nih.gov/pub/taxonomy

nucleotide-taxid.map.gz:
	wget -O $@ $(URL_PREFIX)/gi_taxid_nucl.dmp.gz

.META: taxid_nucl.gz
	gi_nucleotide_id
	tax_id

taxdump.tar.gz:
	wget -O $@ ftp://ftp.ncbi.nih.gov/pub/taxonomy/$@

%.dmp: taxdump.tar.gz
	tar -xzf $< $@

names: names.dmp
	sed 's/\t|\t/\t/g' $< | sed 's/\t|$$//' \
	| bawk '{print $$1,$$4,$$2; if($$3){print $$1,$$4,$$3}}' > $@
	
taxid-parent_taxid: nodes.dmp
	sed 's/\t|\t/\t/g' $< | sed 's/\t|$$//' | cut -f 1,2 | sort -n | uniq > $@
