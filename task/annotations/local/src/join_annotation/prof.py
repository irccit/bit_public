from collections import defaultdict
from sys import stderr
from time import time

_accum = None

def init_stat():
	global _accum
	_accum = defaultdict(lambda: 0.)

def print_stat():
	global _accum

	for k in sorted(_accum.iterkeys()):
		print >>stderr, '%s\t%g' % (k, _accum[k])
	
def elapsed(func):
	global _accum

	def wrapper(*args, **kwargs):
		start = time()
		res = func(*args, **kwargs)
		delta = time() - start

		print >>stderr, '%s\t%g' % (func.func_name, delta)
		_accum[func.func_name] += delta

		return res
	
	return wrapper
