#!/usr/bin/env python
from __future__ import with_statement

from itertools import groupby
from optparse import OptionParser
from sys import exit, stdin, stderr
from vfork.io.colreader import Reader
from vfork.io.util import safe_rstrip

def load_tree(path):
	child_parent = {}

	with file(path, 'r') as fd:
		reader = Reader(fd, '0s,1s', False)
		while True:
			tokens = reader.readline()
			if tokens is None:
				break
			
			parents = child_parent.get(tokens[1], set())
			if tokens[0] in parents:
				exit('Duplicated parent-child relation at line %d of file %s' % (reader.lineno(), path) )
			parents.add(tokens[0])
			child_parent[tokens[1]] = parents

	return child_parent

def parse_lines(fd):
	last_key = None
	for lineno, line in enumerate(fd):
		tokens = safe_rstrip(line).split('\t')
		
		if len(tokens) != 12:
			exit('Insufficient token number at line %d' % (lineno + 1) )
		
		key = (tokens[0], tokens[8])
		if last_key > key:
			exit('Disorder foud at line %d (col1,col9)' % (lineno + 1))
		last_key = key

		tokens[11] = set(tokens[11].split(',')) 
		
		yield tokens

def main():
	parser = OptionParser(usage='%prog GO_TREE < JOIN_ANNOTATION_OUT')
	parser.add_option('-d', '--deeper', default=False, dest='deeper', action='store_true', help='print the number of gene with deeper annotation after x')
	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')
	
	child_parents = load_tree(args[0])

	for (family, ontology), lines in groupby(parse_lines(stdin), lambda l: (l[0], l[8]) ):
		go_genes = {}
		go_line = {}
		gos = set()
		for line in lines:
			go = line[1]
			gos.add(go)
			go_genes[go] = line[11]
			go_line[go] = line

		for go, genes in go_genes.iteritems():
			try:
				for pgo in child_parents[go].intersection(gos):
					go_genes[pgo] -= genes
			except KeyError:
				if go_line[go][9] == '1':
					print >>stderr, '[WARNING] hierarchy root found'
				else:
					raise

		for go, genes in go_genes.iteritems():
			if len(genes):
				line = go_line[go]
				line[11] = ','.join(genes)
				line.append(str(len(genes)))
				if options.deeper:
					line.insert(6, str(int(line[5]) - len(genes)))
				print '\t'.join(line)

if __name__ == '__main__':
	main()
