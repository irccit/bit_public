#!/usr/bin/env python
from __future__ import with_statement
from __future__ import division
from itertools import groupby
from optparse import OptionParser
from subprocess import Popen, PIPE
from sys import exit, stdin, stderr
#from vfork.io.colreader import Reader
from collections import defaultdict
from math import log, floor, fabs, exp, pow 
from random import shuffle

N_max = None
universe_max = None
M_keys_max = None

class Hyper(object):
	def __init__(self, exename):
		self.exe = Popen(exename, stdin=PIPE, stdout=PIPE, close_fds=True, shell=False)
	
	def close(self):
		self.exe.stdin.close()
		self.exe.wait()
	
	def calc(self, N, M, n, x):
		print >>self.exe.stdin, '%d\t%d\t%d\t%d' % (N, M, n, x)
		self.exe.stdin.flush()
		
		tokens = self.exe.stdout.readline().split(None)
		assert len(tokens) == 5, repr(tokens)
		return float(tokens[4])

def bonferroni_correction(family_regions,go_descr):
	return len(family_regions)*len(go_descr)

def read_descriptions(filename):
	with file(filename, 'r') as fd:
		go_descr = {}
		
		for line in fd:
			tokens = line.rstrip().split('\t', 1)
			assert len(tokens) == 2
			go_descr[tokens[0]] = tokens[1]
			
	return go_descr

def conn_comp(pairs): # prende una lista, i.e. gli edge, e restituisce una lsita di set, i.e. le componenti connesse
	conn_comp = {}
	for a,b in pairs:
		conn_a = conn_comp.get(a, None)
		conn_b = conn_comp.get(b, None)
		
		if conn_a == conn_b and conn_a is not None:
			continue

		if conn_a is not None and conn_b is not None:
			for node, c in conn_comp.iteritems():
				if c == conn_b:
					conn_comp[node] = conn_a
		elif conn_a is not None:
				conn_comp[b] = conn_a
		elif conn_b is not None:
				conn_comp[a] = conn_b
		else:
			conn_comp[a]=a
			conn_comp[b]=a

	retval = defaultdict(set) 
	for node,c in conn_comp.iteritems():
		if node[0]=='g':
			retval[c].add(node)
	return retval.itervalues()
	
def update(region_gene_pairs, family, family_key_x, gene_keys, family_n):
	for c in conn_comp(region_gene_pairs):
		family_n[family].append(c)
		keys = set()
		for g in c:
			for k in gene_keys[g]:
				keys.add(k)
				# non ho ancora letto tutto, quindi gene_keys e` incompleto
				# ma lo assumo completo per quanto riguarda i geni
				# associati alla famiglia che ho gia` letto tutta
		for k in keys:
			family_key_x[family][k] += 1

def my_append(list,el):
	present = 0
	for l in list:
		if el == l:
			present = 1
			break
	if not present:
		list.append(el)

def compute_universe(all_conn_comp, family_conn_comp):
	universe = []
	for c1 in all_conn_comp:
		to_append = True
		for c2 in family_conn_comp:
			if c1 != c2 and len( c1 & c2 ) != 0:
				to_append = False
				break
		if to_append:
			universe.append(c1)
	return universe
	
def compute_M_keys(universe, gene_keys):
	M_keys = defaultdict( lambda: 0 )
	for c in universe:
		keys = set()
		for g in c:
			for k in gene_keys[g]:
				keys.add(k)
		for k in keys:
			M_keys[k] += 1
	return M_keys


def compute_pvalues(region_list, family_list, region_genes, gene_keys, go_descr, hyper, rnd):
	global N_max	
	global universe_max
	global M_keys_max
	region_gene_pairs = set ()
	family_n = defaultdict( lambda: [] ) 
	# family_n conterra`, per ogni famiglia, la lista delle sue componenti connesse /(lista di set di geni)
	family_key_x  = defaultdict( lambda: defaultdict( lambda: 0))
	family_last  = None
	all_conn_comp  = []

	family_pvalues = defaultdict( lambda: [] )

	i=0
	while i<len(region_list):
		region = region_list[i]
		family = family_list[i]
		
		########## family change
		if family is not None and family_last != family:
			update(region_gene_pairs, family_last, family_key_x, gene_keys, family_n)
			region_gene_pairs = set()
		family_last = family
		
		### fill the region_gene_pairs set
		for gene in region_genes[region]:
			region_gene_pairs.add( (region,gene) )
		i=i+1

	# faccio i calcoli per l'ultima famiglia
	if(len(region_gene_pairs) > 0):
		update(region_gene_pairs, family, family_key_x, gene_keys, family_n)
		region_gene_pairs = set()
	##########
	bonferroni = bonferroni_correction(family_n, go_descr)
	#print '%d\t%d\t%d' % (len(family_n), len(go_descr), bonferroni)

	if rnd==False:
		N_max = 0

	for set_c in family_n.itervalues():
		for c in set_c:
			my_append(all_conn_comp, c)

	for family, key_count in family_key_x.iteritems():
		#elimino dall'universo del set le con_con comp vietate
		if rnd==True:
			N = N_max
			universe = universe_max
			M_keys = M_keys_max
		else:
			universe = compute_universe(all_conn_comp, family_n[family])
			N = len(universe)
			M_keys = compute_M_keys(universe, gene_keys)
			if N>N_max:
				universe_max = universe
				N_max = N
				M_keys_max = M_keys

		n = len(family_n[family])
		for key, x in key_count.iteritems():
			M = M_keys[key]
			if M>=x and N>=M and n>=x and N>=n:
				pvalue = hyper.calc(N, M, n, x)
			else:
				print >> stderr, "One of these condiction is not satisfied by family (%s) in GO (%s): M>=x, N>=M, n>=x and N>=n" % (family, key);
			if pvalue > 1.0:
				print "ERROR: pvalue>1.0 -- %d\t%d\t%d\t%d\t%e" % (N, M, n, x, pvalue)
				exit(0)
			bonferroni_pvalue = pvalue * bonferroni	
			family_pvalues[family].append((key, N, M, n, x, pvalue, bonferroni_pvalue, go_descr[key]))
	return family_pvalues


def update_histo_pvalues(histo_pvalues,best_pvalue,dp):
	log_pvalue = log(best_pvalue,10)
	x = fabs(log_pvalue)
	x = floor(x/dp)
	histo_pvalues[x] += 1 

def cumulate_histo(histo_pvalues,nbin):
	cumulative_histo_pvalues = defaultdict( lambda: 0 )
	cumsum = 0
	x = nbin-1
	while x>=0:
		cumsum += histo_pvalues[x]
		cumulative_histo_pvalues[x] = cumsum
		x = x - 1
	return cumulative_histo_pvalues

def print_results(rounds, dp, family_pvalues, cumulative_histo_pvalues, nbin, throws, cutoff, fn_go_rounds, fn_commty_go, fn_cum_histo):
	frounds = open(fn_go_rounds,'a')
	print >>frounds, 'ROUND %d' % (rounds)
	frounds.close()

	
	fgo = open(fn_commty_go,'w')
	for family in family_pvalues:
		for pars_hypergeo in family_pvalues[family]:
			log_pvalue = log(pars_hypergeo[5],10)
			x = floor(fabs(log_pvalue)/dp)
			rnd_pvalue = cumulative_histo_pvalues[x]/throws
			if pars_hypergeo[5] < cutoff:
				print >> fgo, '%s\t%s\t%d\t%d\t%d\t%d\t%e\t%e\t%e\t%s' % (family, pars_hypergeo[0], pars_hypergeo[1], pars_hypergeo[2], pars_hypergeo[3], pars_hypergeo[4], pars_hypergeo[5], rnd_pvalue, pars_hypergeo[6], pars_hypergeo[7])
	fgo.close()

	fcumhisto= open(fn_cum_histo,'w')
	i=0
	while i<nbin :
		print >> fcumhisto, '%e\t%e' % (pow(10,-i*dp), cumulative_histo_pvalues[i]/throws)
		i+=1
	print >> fcumhisto, 'throws %d' % (throws)
	fcumhisto.close()

def main():
	parser = OptionParser(usage='''%prog GO_DESCR_MAP fn_commty_go_prernd  fn_go_rounds fn_commty_go fn_cum_histo <REGION_FAMILY_ENSG_GO 

GO_DESCR_MAP:
GO:0000001	mitochondrion inheritance
GO:0000002	mitochondrial genome maintenance
GO:0000003	reproduction

STDIN:
region_id	family_id	ENSG00000172987	GO:0003674

The Bonferroni correction is calculated as tot number of GO in go_descr * number of families whit at least 1 GO association.

''')
	parser.add_option('-c', '--cutoff', type=float, dest='cutoff', default=1e-4, help='do not report items with a p-value larger than CUTOFF', metavar='CUTOFF')
	parser.add_option('-p', '--dump-params', dest='params_file', help='dump hypergeometric parameters to FILE', metavar='FILE')
	parser.add_option('-r', '--max-randomization-rounds', dest='rounds', default=1000, type='int', help='maximum number of randomization rounds (default: 100)')
	options, args = parser.parse_args()
	
	if len(args) != 5:
		exit('Unexpected argument number.')
	elif options.cutoff < 0:
		exit('Invalid cutoff value.')
	elif options.rounds < 1:
		exit('Required at least 1 randomization round.')
		
	
	go_descr = read_descriptions(args[0])
	
	# all these are filenames (fn)
	fn_commty_go_prernd = args[1]
	fn_go_rounds = args[2]
	fn_commty_go = args[3]
	fn_cum_histo = args[4]
	
	#setup external process
	hyper = Hyper('ipergeo')

	########## leggo tutti i dati
	region_list = []
	family_list = []
	region_last = None
	family_last = None

	region_genes = defaultdict( set )
	gene_keys = defaultdict( set )

	for line in stdin:
		region, family, gene, key = line.rstrip().split('\t')
		### check sull'ordine delle famiglie
		if family_last is not None and family_last > family:
			exit("Disorder found")
		family_last = family

		### rinomino id geni
		gene = "g"+gene
		if region[0] == 'g':
			exit("region intentifiers cannot begin with 'g'");
		
		### fill dictionaries and lists
		gene_keys[gene].add(key)
		region_genes[region].add(gene)
		if region_last is not None and region_last != region:
			region_list.append(region)
			family_list.append(family)	
		region_last = region
	
	region_list.append(region)
	family_list.append(family)
	
	family_pvalues = compute_pvalues(region_list, family_list, region_genes, gene_keys, go_descr, hyper, False)
	f = open(fn_commty_go_prernd,'w')
	for family in family_pvalues:
		for pars_hypergeo in family_pvalues[family]:
			if pars_hypergeo[5] < options.cutoff:
				print >> f, '%s\t%s\t%d\t%d\t%d\t%d\t%e\t%e\t%s' % (family, pars_hypergeo[0], pars_hypergeo[1], pars_hypergeo[2], pars_hypergeo[3], pars_hypergeo[4], pars_hypergeo[5], pars_hypergeo[6], pars_hypergeo[7])
	f.close()
	
	############### RANDOMIZATION 
	histo_pvalues = defaultdict( lambda: 0 )
	maxlogp = 100
	minlogp = 0
	nbin = 1000
	dp = (maxlogp - minlogp)/nbin

	rounds = 1
	throws = 0
	while rounds <= options.rounds:
		#### randomizing
		shuffle(region_list)
		
		#### computing new pvalues
		rnd_family_pvalues = compute_pvalues(region_list, family_list, region_genes, gene_keys, go_descr, hyper, True)
		#### finding best pvalues for each family
		for family in rnd_family_pvalues:
			pvalues = []
			for pars_hypergeo in rnd_family_pvalues[family]:
				pvalues.append(pars_hypergeo[5])
			best_pvalue = min(pvalues)
		#### updating pvalues histogram 
			throws +=1
			update_histo_pvalues(histo_pvalues,best_pvalue,dp)
	
		#### printing results
		if rounds%100==0 or rounds==options.rounds :
			cumulative_histo_pvalues = cumulate_histo(histo_pvalues, nbin)
			print_results(rounds, dp, family_pvalues, cumulative_histo_pvalues, nbin, throws, options.cutoff, fn_go_rounds, fn_commty_go, fn_cum_histo)
		rounds += 1


if __name__ == '__main__':
	main()
