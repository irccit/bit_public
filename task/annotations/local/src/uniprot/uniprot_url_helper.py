#!/usr/bin/env python
#
# Copyright 2010 Gabriele Sales <gbrsales@gmail.com>

from ftplib import FTP
from ftplib import all_errors as FTPError
from optparse import OptionParser
from vfork.util import exit, format_usage
import re


host = 'ftp.uniprot.org'

def printUrl(path):
    print 'ftp://%s%s' % (host, path)

class RemoteFile(object):
    def __init__(self):
        self.lines = []

    def __call__(self, line):
        self.lines.append(line)

def extract_release(lines):
    rx = re.compile(r'Release\s+(.*)$', re.I)
    for line in lines:
        m = rx.search(line)
        if m is not None:
            return m.group(1) 

def main():
    parser = OptionParser(usage=format_usage('''
	Usage: %prog [OPTIONS] VERSION

	Scans the UniProt FTP server looking for the directory
	containing the data for the given VERSION. Prints the
	corresponding URL.
    '''))
    options, args = parser.parse_args()
    if len(args) != 1:
        exit('Unexpected argument number.')

    try:
        conn = FTP(host)
        conn.login()
    except FTPError, e:
        exit('Error logging to UniProt FTP server: ' + str(e))

    try:
        path = '/pub/databases/uniprot/previous_releases/release-' + args[0]
        conn.cwd(path)
    except FTPError:
        pass
    else:
        printUrl(path)
        return

    try:
        content = RemoteFile()
        path = '/pub/databases/uniprot/current_release/'
        conn.retrlines('RETR %s/relnotes.txt' % path, content)
    except FTPError, e:
        exit ('Error retrieving relnotes: ' + str(e))
    
    release = extract_release(content.lines)
    if release is None:
        exit('Cannot parse release number from relnotes.')
    elif release == args[0]:
        printUrl(path)
    else:
        exit('Cannot find URL for release: ' + args[0])


if __name__ == '__main__':
    main()
