#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;

our ($opt_a, $opt_A, $opt_o, $opt_t, $opt_h, $opt_n, $opt_w);
getopts('t:a:A:o:n:wh');

my $usage = <<EOF;
usage inclusive.pl -t term_file -a ancestor_file -A alternate_file -o obsolete_file direct_anno
  [-n namespace: examples -n biological_process
                         -n biological_process|cellular_component
   default: include terms from all namespaces
   -w: warn about obsolete terms etc.]

EOF

die $usage if ($opt_h || ! $opt_a || ! $opt_A || ! $opt_t || ! $opt_o);

if (! $opt_n) {
  $opt_n = 'ALL';
}

my $ancfile = $opt_a;
my $obsfile = $opt_o;
my $altfile = $opt_A;
my $termfile = $opt_t;

my %select_namespace;
my %namespace;

if ($opt_n ne 'ALL') {
  my @namespace = split(/\|/, $opt_n);
  foreach my $namespace (@namespace) {
    $select_namespace{$namespace} = 1;
  }
  open TERM, $termfile or die "Error opening $termfile: $!\n";
  while (<TERM>) {
    next if /^#/;
    chomp;
    my @line = split("\t");
    $namespace{$line[0]} = $line[2];
  }
  close TERM;
}


my $term = '';
my %parent;
open ANC, $ancfile or die "Error opening $ancfile: $!\n";
while (my $line = <ANC>) {
  next if ($line =~ /^#/);
  chomp $line;
  my @line = split(/\s+/, $line);
  my $term = $line[0];
  next if ($opt_n ne 'ALL' && ! $select_namespace{$namespace{$term}});
  my $anc  = $line[1];
  next if ($opt_n ne 'ALL' && ! $select_namespace{$namespace{$anc}});
  push @{$parent{$term}}, $anc;
}
close ANC;

# foreach (@{ $parent{'FBcv0000440'} }) {
#   print STDERR "\t$_\n";
# }

my %obs;
open OBS, $obsfile or die "Error opening $obsfile: $!\n";
while (my $line = <OBS>) {
  next if ($line =~ /^#/);
  chomp $line;
  $obs{$line} = 1;
}
close OBS;

my %alt;
open ALT, $altfile or die "Error opening $altfile: $!\n";
while (my $line = <ALT>) {
  next if ($line =~ /^#/);
  chomp $line;
  my @line = split("\t", $line);
  $alt{$line[0]} = $line[1];
}
close ALT;


my %incl;
while (my $line = <>) {
  next if ($line =~ /^#/);
  chomp $line;
  my @line = split("\t", $line);
  my $gene = $line[0];
  $term = $line[1];
  if ($opt_n ne 'ALL' && !  $select_namespace{$namespace{$term}} ) {
    next;
  }
  if (defined $alt{$term}) {
    $term = $alt{$term};
  }
  if (defined $obs{$term} && $opt_w) {
    print STDERR "Warning: obsolete term $term ignored\n";
    next;
  }
  if (!defined $parent{$term} && $opt_w) {
    print STDERR "Warning: no parent for term $term\n";
  }
  if (!defined $incl{$gene}) {
    @{$incl{$gene}} = ();
  }
  if (isnew($term, @{$incl{$gene}})) {
    push @{$incl{$gene}}, $term;
  }
  foreach my $x (@{$parent{$term}}) {
    if (isnew($x, @{$incl{$gene}})) {
      push @{$incl{$gene}}, $x;
    }
  }
}

foreach my $gene (keys %incl) {
  foreach my $x (@{$incl{$gene}}) {
    print "$gene\t$x\n";
  }
}

sub isnew {

  my $x = shift @_;

  my $isnew = 1;

  foreach my $y (@_) {
    if ($x eq $y) {
      $isnew = 0;
      last;
    }
  }

  return $isnew;
}
