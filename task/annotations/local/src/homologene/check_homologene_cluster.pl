#!/usr/bin/perl -w

#FIRST STEP: check if the two entrez (Hs and Mm) are present in the homologene file, and if they are in the same cluster

chomp($species1 = $ARGV[0]);
chomp($entrez1 = $ARGV[1]);
#chomp($species2 = $ARGV[2]);
#chomp($entrez2 = $ARGV[3]);

$species{$species1} = 1;
#$species{$species2} = 1;
#$entrez{$entrez2} = 1;
$entrez{$entrez1} = 1;

open (IN, "$ARGV[2]") || die;

while (<IN>) {
	chomp ($_);
	@array = split (/\t/, $_, 4);
	if ($entrez{$array[2]} && $species{$array[1]}) {
		open (OUT, ">homol.cluster") || die;

		$homol = $array[0];
		$homol{$array[0]} = 1;
		print OUT "$array[0]\n";

		close (OUT);
	}
}

close (IN);

if ($homol) {
	open (IN, "$ARGV[2]") || die;

	while (<IN>) {
		chomp ($_);
		@array = split (/\t/, $_, 4);
		if ($homol{$array[0]}) {
			print "$_\n";
		}
	}

	close (IN);
} else {
	print "\nThis entrez is not present in homologene.data\n\n";
}
