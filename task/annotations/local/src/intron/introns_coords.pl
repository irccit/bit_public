#!/usr//bin/perl
use warnings;
use strict;

my $usage = "$0 < input_file > intron.coords\n
	input_file:
	1	chr
	2	exon_begin
	3	exon_end
	4	transcript_id
";

if($ARGV[0] and $ARGV[0] =~ /^-h/){
	print $usage;
	exit(0);
}


my %transcript=();
while(<>){
	chomp;
	my @F = split /\t/;
	my $chr	   		= $F[0];
	my $exon_start  	= $F[1];
	my $exon_stop   	= $F[2];
	my $transcript_id	= $F[3];
	if(!defined($transcript{$transcript_id})){
		my @tmp1=();
		$transcript{$transcript_id}=\@tmp1;

		my @tmp2=($chr,$exon_start,$exon_stop);
		push @{$transcript{$transcript_id}},\@tmp2;
	}else{
		my @tmp=($chr,$exon_start,$exon_stop);
		push @{$transcript{$transcript_id}},\@tmp;
	}
}

$,="\t";
$\="\n";

my @keys= keys(%transcript);
for(@keys){

	my @exons=@{$transcript{$_}};
	my $first=1;
	my ($chr,$start, $stop, $pre_start, $pre_stop);


	foreach my $seg ( sort {${$a}[1] <=> ${$b}[1]} @exons ){

		if($first){
			($chr,$pre_start,$pre_stop)=@{$seg};
			$first=0;
		}else{
			($chr,$start,$stop)=@{$seg};
			#print $chr,$start,$stop,'a';
			die "ERROR: pre_stop > start" if ($pre_stop>$start);
			if($pre_stop<$start){
				print $chr,$pre_stop,$start,$_;
			}
			$pre_start=$start;
			$pre_stop=$stop;
		}
	}
}
