PHASES:=sequences alignments statistics
include $(BIOINFO_ROOT)/local/share/makefile/global-rules.mk

%/rules.mk: ../../local/share/rules-%.mk %
	cd $(word 2,$^) && ln -sf ../$< rules.mk

%/makefile: makefile %
	cd $* && ln -sf ../$$(basename $@)
