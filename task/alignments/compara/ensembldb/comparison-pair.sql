use ensembl_compara_33;

select @source_genome_id := genome_db_id
	from genome_db
	where
		genome_db.name like "Homo sapiens";

select name
	from genome_db, method_link_species_set
	where
		# filter comparisons by genome & chromosomes
		method_link_species_set.method_link_species_set_id in 
			( select method_link_species_set.method_link_species_set_id
				from genome_db, method_link, method_link_species_set
				where
					# select genome
					genome_db.genome_db_id = @source_genome_id and
				
					# select chromosomes
					method_link.type like "BLASTZ_NET" and
					method_link_species_set.method_link_id = method_link.method_link_id and
					method_link_species_set.genome_db_id = genome_db.genome_db_id
			) and
		
		#genome_db.genome_db_id != @source_genome_id and
		method_link_species_set.genome_db_id = genome_db.genome_db_id;
