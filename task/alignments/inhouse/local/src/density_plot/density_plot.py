#!/usr/bin/env python
# encoding: utf8

from __future__ import division

from array import array
from optparse import OptionParser
from sys import exit, stdin
from vfork.draw.surface import PNGSurface
from vfork.io.colreader import Reader
import cairo

def parse_size(repr, name):
	try:
		size = int(repr)
		if size <= 0:
			raise ValueError
	except ValueError:
		exit('Invalid %s.' % name)
	else:
		return size

def build_coord_transformer(stride, space):
	def transformer(coord):
		return min(int(round(coord / stride)), space-1)
	return transformer

class Grid(object):
	def __init__(self, width, height):
		self.stride = width
		self.values = array('L', (0 for i in xrange(width * height)))

	def inc(self, x, y):
		idx = y * self.stride + x;
		assert 0 <= idx < len(self.values)
		self.values[idx] += 1
	
	def max_value(self):
		max_value = 0
		for value in self.values:
			if value > max_value:
				max_value = value
		return max_value
	
	def iter_values(self):
		idx = 0
		y = 0
		while idx < len(self.values):
			for x in xrange(self.stride):
				yield x, y, self.values[idx]
				idx += 1
			y += 1

def main():
	parser = OptionParser(usage='%prog XSIZE YSIZE OUTPUT_PNG <PDB')
	parser.add_option('-a', '--width', dest='width', type='int', default=800, help='plot width (default: 800)', metavar='PIXELS')
	parser.add_option('-b', '--height', dest='height', type='int', default=800, help='plot height (default: 800)', metavar='PIXELS')
	options, args = parser.parse_args()
	
	if len(args) != 3:
		exit('Unexpected argument number.')
	elif options.width <= 0:
		exit('Invalid plot width.')
	elif options.height <= 0:
		exit('Invalid plot height.')
	
	xstride = parse_size(args[0], 'x size') / options.width
	ystride = parse_size(args[1], 'y size') / options.height
	xtransform = build_coord_transformer(xstride, options.width)
	ytransform = build_coord_transformer(ystride, options.height)
	grid = Grid(options.width, options.height)
	
	reader = Reader(stdin, '1u,2u,5u,6u', False)
	while True:
		item = reader.readline()
		if item is None:
			break
		
		for x in xrange(xtransform(item[0]), xtransform(item[1])+1):
			for y in xrange(ytransform(item[2]), ytransform(item[3])+1):
				grid.inc(x, y)
	
	surface = PNGSurface(options.width, options.height)
	surface.rgba = (1, 1, 1, 1)
	surface.fill_rectangle(0, 0, options.width, options.height)
	
	norm_factor = 1 / grid.max_value()
	for x, y, value in grid.iter_values():
		surface.rgba = (1, 0, 0, value*norm_factor)
		surface.fill_rectangle(x, y, 1, 1)
	
	surface.write_to_file(args[2])

if __name__ == '__main__':
	main()
