#!/bin/bash

rm -f *.qerr
rm -f *.qout

cat *.out | sort -k1 -k2n -k4 -k5n -k8 -k9n -S$((500*1024)) > res.txt 
rm -f *.out
