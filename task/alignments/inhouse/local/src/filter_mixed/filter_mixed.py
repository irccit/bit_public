#!/usr/bin/env python
# encoding: utf-8

from itertools import izip
from optparse import OptionParser
from sys import exit, stdin, stdout

def read_fasta_headers(filename):
	fd = file(filename, 'r')
	for line in fd:
		if line.startswith('>'):
			yield line[1:].rstrip()
	fd.close()

def main():
	parser = OptionParser(usage='%prog LEFT_FASTA RIGHT_FASTA <INPUT_ALIGN >OUTPUT_ALIGN')
	options, args = parser.parse_args()

	if len(args) != 2:
		exit('Unexpected argument number.')
	
	valid_pairs = set(izip(read_fasta_headers(args[0]), read_fasta_headers(args[1])))
	for lineno, line in enumerate(stdin):
		tokens = line.split('\t')
		if len(tokens) < 6:
			exit('Malformed input at line %d.' % (lineno+1))
		elif (tokens[0], tokens[3]) in valid_pairs:
			stdout.write(line)

if __name__ == '__main__':
	main()
