#!/usr/bin/perl

use warnings;
use strict;

my $usage = "$0 seq_dir1 seq_dir2 < qualcosa.pdb";

my $seq1_dir = shift @ARGV;
my $seq2_dir = shift @ARGV;
die $usage if (!defined $seq1_dir or !defined $seq2_dir);

my $seq1_len = undef;
my $seq2_len = undef;

my $n = 0;
while (<>){
	$n++;
	chomp;
	my @F = split /\t/;
	if (!defined $seq1_len or !defined $seq2_len) {
		$seq1_len = &seq_file($F[0],$seq1_dir);
		$seq2_len = &seq_file($F[4],$seq2_dir);
	}
	die "query_stop > seq_len at line $n" if ($F[2]>$seq1_len);
	die "target_stop > seq_len at line $n" if ($F[6]>$seq2_len);
	die "query_start >= query_stop at line $n" if ($F[1]>=$F[2]);
	die "target_start >= target_stop at line $n" if ($F[5]>=$F[6]);

	if ($F[0] eq $F[4]){
		die "identity found at line $n" if (($F[1]==$F[5]) and ($F[2]==$F[6]));
		die "identity found at line $n" if ($F[2]>$F[5]);
	}
}
		

sub seq_file
{
	my $chr = shift;
	my $seq_dir = shift;

	my $file = $seq_dir."/".$chr.".fa.len";
	$file = $seq_dir."/chr".$chr.".fa.len" if !(-e $file);
	die "file $file is missing" if !(-e $file);
	die "file $file is empty" if (-z $file);
	open FILE,$file or die "Can't open file $file";
	my $seq_len = <FILE>;
	chomp $seq_len;
	return $seq_len;
}
