#!/usr/bin/env python
from optparse import OptionParser
from sys import exit, stdin
from vfork.alignment.parser import WuBlastParser, ParseError

def format_gaps(gaps):
	return ';'.join(['%d,%d' % g for g in gaps])

if __name__ == '__main__':
	parser = OptionParser(usage='%prog <WUBLAST_OUTPUT')
	parser.add_option('-g', '--group', dest='report_group', action='store_true', help='report grup data (see topcomboN option of wublast)')
	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')
	
	blast_parser = WuBlastParser()
	
	for alignment in blast_parser.parse(stdin):
		assert alignment.strand is not None, 'strand is missing'

		query_gaps = format_gaps(alignment.query_gaps)
		target_gaps = format_gaps(alignment.target_gaps)
		
		line = [
			alignment.query_label,
			str(alignment.query_start),
			str(alignment.query_stop),
			alignment.strand,
			alignment.target_label,
			str(alignment.target_start),
			str(alignment.target_stop),
			str(alignment.length),
			str(alignment.score),
			str(alignment.identity),
			str(alignment.evalue),
			query_gaps,
			target_gaps
		]
		
		if options.report_group:
			line.insert(3, str(alignment.group))
		
		print '\t'.join(line)
