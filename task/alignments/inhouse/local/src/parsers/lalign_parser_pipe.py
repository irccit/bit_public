#!/usr/bin/env python
from optparse import OptionParser
from sys import exit, stdin
from vfork.alignment.parser import LalignParser

def format_gaps(gaps):
	return ';'.join(['%d,%d' % g for g in gaps])

if __name__ == '__main__':
	opt_parser = OptionParser(usage='%prog CHR1 CHR2')
	options, args = opt_parser.parse_args()
	
	if len(args) != 2:
		exit('Unexpected argument number.')
	
	parser = LalignParser()
	for alignment in parser.parse(stdin):
		query_gaps = format_gaps(alignment.query_gaps)
		target_gaps = format_gaps(alignment.target_gaps)
		
		print '\t'.join((
			args[0],
			str(alignment.query_start),
			str(alignment.query_stop),
			alignment.strand,
			args[1],
			str(alignment.target_start),
			str(alignment.target_stop),
			str(alignment.length),
			str(alignment.score),
			str(alignment.identity),
			str(alignment.evalue),
			query_gaps,
			target_gaps
		))
