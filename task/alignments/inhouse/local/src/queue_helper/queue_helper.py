#!/usr/bin/env python
from __future__ import with_statement
from optparse import OptionParser
from os import system
from os.path import dirname
from string import Template
from sys import exit
from time import sleep
import re

def main():
	parser = OptionParser(usage='%prog MAKEFILE TARGET_VAR SPECIES1 SPECIES2 ALIGN_TYPE')
	parser.add_option('-c', '--command', dest='command', default='accoda --name $name -- make $target', help='the queue command (by default accoda; you can use $name, $makefile and $target placeholders)') 
	parser.add_option('-d', '--delay', dest='delay', type='float', default=0, help='delay between successive calls to accoda', metavar='SECONDS')
	parser.add_option('-n', '--dry-run', dest='dry_run', action='store_true', default=False, help='print the commands, do no execute them')
	parser.add_option('-r', '--reverse', dest='reverse', action='store_true', default=False, help='reverse the order of submission')
	options, args = parser.parse_args()

	if len(args) != 5:
		exit('Unexpected argument number.')
	elif len(dirname(args[0])) > 0:
		exit('Absolute makefile paths are not supported.')

	var_rx = re.compile(r'%s[ \t:=]+([^\r\n]*)' % args[1])
	with file(args[0], 'r') as fd:
		for line in fd:
			m = var_rx.match(line)
			if m:
				targets = m.group(1).split()
				break
		else:
			exit('Can\'t find target list in file %s' % args[0])
	
	if options.reverse:
		targets.reverse()

	pattern1 = '%s_' % args[2]
	pattern2 = '%s_' % args[3]
	for target in targets:
		idx = target.index('.$(ALIGN_TYPE)')
		name = target[:idx].replace(pattern1, '').replace(pattern2, '')
		target = target.replace('$(ALIGN_TYPE)', args[4])
		cmd = Template(options.command).substitute(name=name, target=target, makefile=args[0])

		if options.dry_run:
			print cmd
		elif system(cmd) != 0:
			exit('*** Error running accoda.')
		elif options.delay > 0:
			sleep(options.delay)

if __name__ == '__main__':
	main()
