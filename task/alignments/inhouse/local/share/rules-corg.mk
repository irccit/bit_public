SHELL = /bin/bash
BIN_DIR := $(BIOINFO_ROOT)/task/alignments/inhouse/local/bin
BIN_DIR_PARENT := $(BIOINFO_ROOT)/task/alignments/local/bin
BIN_DIR_COVERAGE := $(BIOINFO_ROOT)/task/align_coverage/local/bin

BLOCK_NUM?=300
REDUCE_SCORE_FILTER=2200
PAIR_LIST?=$(BIOINFO_ROOT)/task/corg/dataset/2006/upstream_homology_hsapiens_mmusculus

MEGA_FLAGS ?= -W 11 -t 16 -N 1 -g T -F T -f T -m 8 -e 10000000 -s 16 -z 1 -U -M 3000000000 -v 1000000000 -b 1000000000 -H 0

CHAOS_FLAGS ?= -b

WUBLAST_FLAGS ?= W=10 matrix=pam10 Q=2046 R=18 S2=2232 gapS2=2232 S=2232 Y=3080436051 Z=3080436051 kap hspmax=0 cpus=1
wublast_DEPS=$$i.xns $$j

LALIGN_FLAGS ?= -n -I -E 10 -K 10000000
lalign_DEPS=$$i $$j

.PHONY: all clean \
        all.$(ALIG_TYPE) clean.$(ALIGN_TYPE)

.SECONDARY:
.DELETE_ON_ERROR:

all : reduced.left.shift.gz  reduced.right.shift.gz
clean : clean.$(ALIGN_TYPE)
distclean : clean
	rm -f pairs.combo
include pairs.combo

reduced.left.gz : $(ALL_ALIGNS)
	$(call reduce_by_coverage,1,2,3,1)

reduced.right.gz : $(ALL_ALIGNS)
	$(call reduce_by_coverage,4,5,6,1)

reduced.%.shift.gz: reduced.%.gz $(PAIR_LIST)
	zcat $< | $(BIN_DIR)/shift_upstream.pl $(PAIR_LIST) | gzip > $@

align.shift.gz: $(ALL_ALIGNS)
	set -e;\
	(for i in $(ALL_ALIGNS); do\
	 	zcat $< | $(BIN_DIR)/shift_align.pl $(PAIR_LIST);\
	done;) \
	| gzip > $@

reduced.%.shift.annote: reduced.%.shift.gz
	zcat $< \
	| awk 'BEGIN{OFS="\t"}{print $$2,$$3,$$4,$$1}' > $@.tmp
	annote_dir=$(ANNOTE_DIR_LEFT);\
	if [[ "$*" == right ]]; then annote_dir=$(ANNOTE_DIR_RIGHT); fi;\
	annote `for i in $$annote_dir/coords_nonredundant.*; do echo -n "-a $$i "; done;` $@.tmp > $@;
	rm $@.tmp;

reduced.left.shift.fa.gz: reduced.left.shift.gz
	zcat $< | sort -S40% -k 2,2 -k 3,3n | $(BIN_DIR)/seqlist2fasta -r $(SEQ_DIR1) | gzip > $@

reduced.right.shift.fa.gz: reduced.right.shift.gz
	zcat $< | sort -S40% -k 2,2 -k 3,3n | $(BIN_DIR)/seqlist2fasta -r $(SEQ_DIR2) | gzip > $@
	
left.fa: $(PAIR_LIST)
	set -o pipefail; \
	set -e; \
	cut -f 1-4 $< \
	| $(BIN_DIR)/seqlist2fasta $(SEQ_DIR1) > $@

right.fa: $(PAIR_LIST)
	set -o pipefail; \
	set -e; \
	cut -f 5-8 $< \
	| $(BIN_DIR)/seqlist2fasta $(SEQ_DIR2) > $@

pairs.txt: left.fa right.fa
	(\
		left_blocks=( $$(split_fasta  $(word 1,$^) $(BLOCK_NUM) .) ); \
		right_blocks=( $$(split_fasta $(word 2,$^) $(BLOCK_NUM) .) ); \
		for ((i=0; i<$${#left_blocks[*]}; i+=1)); do \
			echo -e "$${left_blocks[$$i]}\t$${right_blocks[$$i]}"; \
		done \
	) > $@


#pairs.combo : makefile $(SEQ_PAIR_LIST)
pairs.combo : pairs.txt
	( \
		targets=""; \
		while read i j; do \
			i_reduced=$$(basename $$i); \
			i_reduced=$${i_reduced%%.fa}; \
			j_reduced=$$(basename $$j); \
			j_reduced=$${j_reduced%%.fa}; \
			target="$${i_reduced}_$${j_reduced}.$(ALIGN_TYPE).gz"; \
			targets="$$targets $$target"; \
			echo -n "$$target : "; \
			echo "$($(ALIGN_TYPE)_DEPS)"; \
			echo -e "\t\$$(call $(ALIGN_TYPE)_single_run)"; \
		done; \
		echo "ALL_ALIGNS=$$targets"; \
		echo "all.$(ALIGN_TYPE) : \$$(ALL_ALIGNS)"; \
		echo "clean.$(ALIGN_TYPE) :"; \
		echo -e "\trm -f \$$(ALL_ALIGNS)"; \
	) <$< >$@

%.fa.xns: %.fa
	xdformat -n -o $< $<

define wublast_single_run
	logname=$(patsubst %.gz,%.log,$@); \
	( \
		set -o pipefail; \
		set -e; \
		database=$(word 1,$^); \
		database=$${database%.xns}; \
		echo "starting at $$(date) on host $$(hostname --fqdn)"; \
		echo "running wublast on $(word 2,$^) and $$database"; \
		echo "using the following flags: $(WUBLAST_FLAGS)"; \
		( \
			source $$(which wublast-env);\
			blastn $$database $(word 2,$^) $(WUBLAST_FLAGS) \
			| $(BIN_DIR)/wublast_parser_pipe \
			| $(BIN_DIR)/filter_mixed $(word 2,$^) $$database \
			| gzip >$@; \
		) 2>&1 \
		| while read line; do echo "[ERR] $$line"; done; \
		exit_code=$$?; \
		if [ $$exit_code -ne 0 ]; then \
			echo "aborted at $$(date) with code $$exit_code"; \
		else \
			echo "completed at $$(date)"; \
		fi; \
		exit $$exit_code; \
	) >$$logname; \
	exit $$?
endef

define lalign_single_run
	logname=$(patsubst %.gz,%.log,$@); \
	( \
		set -o pipefail; \
		set -e; \
		query=$(word 1,$^); \
		database=$(word 2,$^); \
		id1=$$(head -n 1 $$query | sed 's/^>//'); \
		id2=$$(head -n 1 $$database | sed 's/^>//'); \
		echo "starting at $$(date) on host $$(hostname --fqdn)"; \
		echo "running lalign on $$query and $$database"; \
		echo "using the following flags: $(LALIGN_FLAGS)"; \
		( \
			lalign $(LALIGN_FLAGS) $$query $$database \
			| $(BIN_DIR)/lalign_parser_pipe $$id1 $$id2 \
			| $(BIN_DIR)/filter_mixed $$query $$database \
			| gzip; \
			lalign $(LALIGN_FLAGS) -i $$query $$database \
			| $(BIN_DIR)/lalign_parser_pipe $$id1 $$id2 \
			| $(BIN_DIR)/filter_mixed $$query $$database \
			| gzip; \
		) 2>&1 >$@ \
		| while read line; do echo "[ERR] $$line"; done; \
		exit_code=$$?; \
		if [ $$exit_code -ne 0 ]; then \
			echo "aborted at $$(date) with code $$exit_code"; \
		else \
			echo "completed at $$(date)"; \
		fi; \
		exit $$exit_code; \
	) >$$logname; \
	exit $$?
endef

aborted_alignments:
	(for i in *.log; do grep -q aborted $$i && echo $$i || true; done) > $@

length_distrib.txt : $(ALL_ALIGNS)
	(find . -name '*.$(ALIGN_TYPE).gz' | xargs zcat) \
	| cut -f 7 \
	| sort -n \
	| binner --min 10 --max 15000 --bin-num 30 --log --out-of-memory >$@

define reduce_by_coverage
	set -o pipefail; \
	set -e; \
	( \
		(find . -name '*.$(ALIGN_TYPE).gz' | xargs zcat) \
		| awk '$$8>=$(REDUCE_SCORE_FILTER)' \
		| cut -f $1,$2,$3 \
		| sort -k1,1 -S80% \
		| repeat_group_pipe 'cut -f 2,3 | $(BIN_DIR_COVERAGE)/coverage | $(BIN_DIR_COVERAGE)/coverage_trigger $4 | awk '\''{print "$$1\t" $$$$0}'\' 1 \
	) | gzip >$@
endef

