SHELL = /bin/bash
BIN_DIR := $(BIOINFO_ROOT)/task/alignments/inhouse/local/bin
BIN_DIR_PARENT := $(BIOINFO_ROOT)/task/alignments/local/bin

QUERY?=$(CORG_DIR)/sp1.macroregions.masked.sort.fa
TARGHET?=sp2.macroregions.masked.sort.fa.xns

MEGA_FLAGS ?= -W 11 -t 16 -N 1 -g T -F T -f T -m 8 -e 10000000 -s 16 -z 1 -U -M 3000000000 -v 1000000000 -b 1000000000 -H 0
WUBLAST_FLAGS ?= W=10 matrix=pam10 Q=2046 R=18 S2=2232 gapS2=2232 S=2232 Y=3080436051 Z=3080436051 kap hspmax=0 cpus=1
wublast_DEPS=$$i.xns $$j

BLOCK_NUM ?= 15

.PHONY: all clean \
		all.$(ALIG_TYPE) clean.$(ALIGN_TYPE)

.SECONDARY:
.DELETE_ON_ERROR:

include rules-algo.mk
include all_wublast_block.mk

all: all_wublast



all_wublast_block.mk:
	tot=`grep '>' $(QUERY) | wc -l`;\
	$(BIN_DIR)/splitter.pl $$tot $(BLOCK_NUM) \
	| awk "{print NR \".wublast.gz:\"; print \"\t\$$(call wublast_single_run, \" \$$1 \", \" \$$2 \", 0, $$tot)\"; print \"\" }" \
	> $@

all_wublast:
	make all_wublast_block.mk
	let "max_block=$(BLOCK_NUM)+1"; \
	for i in `seq 1 $$max_block`; do \
		make -n $$i.wublast.gz; \
	done


wublast_block:
	for i in `seq $(A) $(B)`; do\
		$(call wublast_single_run,$*,$*,$*,$*)
		make $$i.wublast.gz;\
	done;

%.fa.xns: $(CORG_DIR)/%.fa
	tr 'E' 'N' < $< | xdformat -n -o `basename $<` -

#%.wublast.gz: $(CORG_DIR)/sp1-%.fa sp2-%.fa.xns
#	@echo pippo;
#	$(call wublast_single_run)
%.wublast.gz: $(QUERY) $(TARGHET)
	$(call wublast_single_run,$*,$*,$*,$*)
