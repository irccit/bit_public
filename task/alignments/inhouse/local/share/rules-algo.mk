MEGABLAST_FLAGS ?= -W 11 -t 16 -N 1 -g T -F T -f T -m 8 -e 10000000 -s 16 -z 1 -U -M 3000000000 -v 1000000000 -b 1000000000 -H 0
CHAOS_FLAGS     ?= -b
WUBLAST_FLAGS   ?= W=10 matrix=pam10 Q=2046 R=18 S2=2232 gapS2=2232 S=2232 Y=3080436051 Z=3080436051 kap hspmax=0 cpus=1
WUBLASTX_FLAGS  ?= W=3 cpus=1
LALIGN_FLAGS    ?= -n -I -E 10 -K 10000000
OUTPUT_FILTER   ?=

BIN_DIR := $(BIOINFO_ROOT)/task/alignments/inhouse/local/bin

define megablast_single_run
	logname=$(patsubst %.gz,%.log,$@); \
	( \
		set -o pipefail; \
		set -e; \
		query=$(word 1,$^); \
		database=$(word 2,$^); \
		database=$${database%.nsq}; \
		echo "starting at $$(date) on host $$(hostname --fqdn)"; \
		echo "running megablast on $$query and $$database"; \
		echo "using the following flags: $(MEGABLAST_FLAGS)"; \
		( \
			repeat_fasta_pipe -g100 "megablast $(MEGABLAST_FLAGS) -i /dev/stdin -d $$database" <$$query \
			| $(BIN_DIR)/megablast_parser_pipe \
			$(OUTPUT_FILTER) \
			| gzip >$@; \
		) 2>&1 \
		| while read line; do echo "[ERR] $$line"; done; \
		exit_code=$$?; \
		if [ -e error.log ]; then \
			cat error.log | while read line; do echo "[ERR] $$line"; done; \
			rm -f error.log; \
		fi; \
		if [ $$exit_code -ne 0 ]; then \
			echo "aborted at $$(date) with code $$exit_code"; \
		else \
			echo "completed at $$(date) (exit code $$exit_code)"; \
		fi; \
		exit $$exit_code; \
	) >$$logname; \
	exit $$?
endef

define wublast_single_run
	logname=$(patsubst %.gz,%.log,$@); \
	( \
		set -o pipefail; \
		set -e; \
		query=$(word 1,$^); \
		database=$(word 2,$^); \
		database=$${database%.xns}; \
		echo "starting at $$(date) on host $$(hostname --fqdn)"; \
		echo "running wublast on $$query and $$database"; \
		echo "using the following flags: $(WUBLAST_FLAGS) globalexit"; \
		failed=0; \
		( \
			source $$(which wublast-env);\
			blastn $$database $$query $(WUBLAST_FLAGS) globalexit \
			| $(BIN_DIR)/wublast_parser_pipe \
			$(OUTPUT_FILTER) \
			| gzip >$@; \
		) 2>&1 \
		| while read line; do \
			echo "[ERR] $$line"; \
			if [[ "$$line" =~ "EXIT CODE [0-9]+" ]]; then \
				code=$$(sed 's|.*EXIT CODE \([0-9]*\)|\1|' <<<"$$line"); \
				[ $$code -ne 0 -a $$code -ne 12 -a $$code -ne 17 -a $$code -ne 23 ] && failed=1; \
			fi; \
		  done; \
		exit_code=$$?; \
		if [ $$exit_code -eq 0 ]; then \
			echo "completed at $$(date) (exit code $$exit_code)"; \
		elif [ $$exit_code -lt 127 -a $$failed -eq 0 ]; then \
			echo "completed at $$(date) (exit code $$exit_code)"; \
			exit_code=0; \
		else \
			echo "aborted at $$(date) with code $$exit_code"; \
		fi; \
		exit $$exit_code; \
	) >$$logname; \
	exit $$?
endef

define wublastNoparse_single_run
	logname=$(patsubst %.gz,%.log,$@); \
	( \
		set -o pipefail; \
		set -e; \
		query=$(word 1,$^); \
		database=$(word 2,$^); \
		database=$${database%.xns}; \
		echo "starting at $$(date) on host $$(hostname --fqdn)"; \
		echo "running wublast on $$query and $$database"; \
		echo "using the following flags: $(WUBLAST_FLAGS) globalexit"; \
		failed=0; \
		( \
			source $$(which wublast-env);\
			blastn $$database $$query $(WUBLAST_FLAGS) globalexit \
			| gzip >$@; \
		) 2>&1 \
		| while read line; do \
			echo "[ERR] $$line"; \
			if [[ "$$line" =~ "EXIT CODE [0-9]+" ]]; then \
				code=$$(sed 's|.*EXIT CODE \([0-9]*\)|\1|' <<<"$$line"); \
				[ $$code -ne 0 -a $$code -ne 12 -a $$code -ne 17 -a $$code -ne 23 ] && failed=1; \
			fi; \
		  done; \
		exit_code=$$?; \
		if [ $$exit_code -eq 0 ]; then \
			echo "completed at $$(date) (exit code $$exit_code)"; \
		elif [ $$exit_code -lt 127 -a $$failed -eq 0 ]; then \
			echo "completed at $$(date) (exit code $$exit_code)"; \
			exit_code=0; \
		else \
			echo "aborted at $$(date) with code $$exit_code"; \
		fi; \
		exit $$exit_code; \
	) >$$logname; \
	exit $$?
endef

define wublastx_single_run
	logname=$(patsubst %.gz,%.log,$@); \
	( \
		set -o pipefail; \
		set -e; \
		query=$(word 1,$^); \
		database=$(word 2,$^); \
		database=$${database%.xps}; \
		echo "starting at $$(date) on host $$(hostname --fqdn)"; \
		echo "running wublastx on $$query and $$database"; \
		echo "using the following flags: $(WUBLASTX_FLAGS) globalexit "; \
		( \
			source $$(which wublast-env);\
			blastx $$database $$query $(WUBLASTX_FLAGS) globalexit \
			| $(BIN_DIR)/wublast_parser_pipe \
			$(OUTPUT_FILTER) \
			| gzip >$@; \
		) 2>&1 \
		| while read line; do \
			echo "[ERR] $$line"; \
			if [[ "$$line" =~ "EXIT CODE [0-9]+" ]]; then \
				code=$$(sed 's|.*EXIT CODE \([0-9]*\)|\1|' <<<"$$line"); \
				[ $$code -ne 0 -a $$code -ne 12 -a $$code -ne 17 -a $$code -ne 23 ] && failed=1; \
			fi; \
		  done; \
		exit_code=$$?; \
		if [ $$exit_code -eq 0 ]; then \
			echo "completed at $$(date) (exit code $$exit_code)"; \
		elif [ $$exit_code -eq 12 -a $$failed -eq 0 ]; then \
			echo "completed at $$(date) (exit code $$exit_code)"; \
			exit_code=0; \
		else \
			echo "aborted at $$(date) with code $$exit_code"; \
		fi; \
		exit $$exit_code; \
	) >$$logname; \
	exit $$?
endef

define lalign_single_run
	logname=$(patsubst %.gz,%.log,$@); \
	( \
		set -o pipefail; \
		set -e; \
		query=$(word 1,$^); \
		database=$(word 2,$^); \
		id1=$$(head -n 1 $$query | sed 's/^>//'); \
		id2=$$(head -n 1 $$database | sed 's/^>//'); \
		echo "starting at $$(date) on host $$(hostname --fqdn)"; \
		echo "running lalign on $$query and $$database"; \
		echo "using the following flags: $(LALIGN_FLAGS)"; \
		( \
			lalign $(LALIGN_FLAGS) $$query $$database \
			| $(BIN_DIR)/lalign_parser_pipe $$id1 $$id2 \
			$(OUTPUT_FILTER) \
			| gzip; \
			lalign $(LALIGN_FLAGS) -i $$query $$database \
			| $(BIN_DIR)/lalign_parser_pipe $$id1 $$id2 \
			$(OUTPUT_FILTER) \
			| gzip; \
		) 2>&1 >$@ \
		| while read line; do echo "[ERR] $$line"; done; \
		exit_code=$$?; \
		if [ $$exit_code -ne 0 ]; then \
			echo "aborted at $$(date) with code $$exit_code"; \
		else \
			echo "completed at $$(date) (exit code $$exit_code)"; \
		fi; \
		exit $$exit_code; \
	) >$$logname; \
	exit $$?
endef

define chaos_single_run
	(source /home_PG/molineri/local/bin/chaos_env; /home_PG/molineri/local/bin/chaos $^ $(CHAOS_FLAGS) | gzip >$@ ) 2>$@.err
endef
