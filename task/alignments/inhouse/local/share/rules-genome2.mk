SEQDIR1           ?= $(BIOINFO_ROOT)/task/sequences/dataset/ensembl/hsapiens/39
SEQDIR2           ?= $(BIOINFO_ROOT)/task/sequences/dataset/ensembl/hsapiens/39
SPECIES1          ?= hsapiens
SPECIES2          ?= hsapiens
ALIGN_TYPE        ?= wublast
BLOCK_TARGET_SIZE ?= 50M

megablast_DEPS = \$$(SEQDIR1)/$${seq1}.fa.splitted \$$(SEQDIR2)/$${seq2}.fa.nsq
chaos_DEPS     = \$$(SEQDIR1)/$${seq1}.fa \$$(SEQDIR2)/$${seq2}.fa
wublast_DEPS   = \$$(SEQDIR1)/$${seq1}.fa.splitted \$$(SEQDIR2)/$${seq2}.fa.splitted.xns

BIN_DIR_PARENT := $(BIOINFO_ROOT)/task/alignments/local/bin
BIN_DIR        := $(BIOINFO_ROOT)/task/alignments/inhouse/local/bin

define list_sequences
	grep '^ALL_DNA\s*.*=' $(word 3,$^) | sed 's|^ALL_DNA\s*.*=\s*||' | sed 's|\.fa||g'
endef

define list_seq_by_ext
	$(addsuffix .fa.$(2),$(addprefix $(1)/,$(shell grep '^ALL_DNA\s*.*=' $(1)/makefile | sed 's|^ALL_DNA\s*.*=\s*||' | sed 's|\.fa||g')))
endef

define get_first_sequence
	$(addsuffix .fa,$(addprefix $(1)/,$(shell sed 's|^$(SPECIES1)_\([^_]*\)_.*|\1|' <<<"$(2)")))
endef

define get_second_sequence
	$(addsuffix .fa,$(addprefix $(1)/,$(shell sed 's|^$(SPECIES1)_[^_]*_$(SPECIES2)_\([^.]*\).*|\1|' <<<"$(2)")))
endef

define compute_hashes
	set -o pipefail; \
	set -e; \
	( \
		for i in $(wordlist 2,$(words $^),$^); do \
			md5sum $$i \
			| while read md5 rest; do \
				echo -e "$$(basename $$i)\t$$md5"; \
			done; \
		done \
	) >$@; \
	if [ ! -f $@.stable ]; then \
		cp $@ $@.stable; \
	elif ! cmp $@ $@.stable >/dev/null 2>&1; then \
		echo "*** WARNING ***" >&2; \
		echo "*** Detected changes in sequences." >&2; \
		exit 1; \
	fi
endef

define emit_counts
	uniq -c | awk '{OFS="\t"; print $$2,$$1}'
endef

.DELETE_ON_ERROR:
all: all.pdb

sequences-1.list: makefile rules.mk $(SEQDIR1)/makefile 
	$(list_sequences) >$@

sequences-2.list: makefile rules.mk $(SEQDIR2)/makefile
	$(list_sequences) >$@

sequences-1.hashes: sequences-1.list $(call list_seq_by_ext,$(SEQDIR1),splitted)
	$(call compute_hashes)

sequences-2.hashes: sequences-2.list $(call list_seq_by_ext,$(SEQDIR2),splitted)
	$(call compute_hashes)

sequences.combo: sequences-1.list sequences-1.hashes sequences-2.list sequences-2.hashes
	set -e; \
	align_stem="$(ALIGN_TYPE)"; \
	align_stem="$${align_stem%_noparse}"; \
	if [[ "$$align_stem" == "megablast" ]]; then \
		$(ALGO_BIN_DIR)/mkcombo -2 -s nsq $(SPECIES1) $(SEQDIR1) "$$(cat $(word 1,$^))" $(SPECIES2) $(SEQDIR2) "$$(cat $(word 3,$^))" $(BLOCK_TARGET_SIZE) $(ALIGN_TYPE)_single_run >$@; \
	elif [[ "$$align_stem" == "wublast" ]]; then \
		$(ALGO_BIN_DIR)/wublast_mkcombo $(SPECIES1) $(SEQDIR1) "$$(cat $(word 1,$^))" $(SPECIES2) $(SEQDIR2) "$$(cat $(word 3,$^))" $(BLOCK_TARGET_SIZE) $(ALIGN_TYPE)_single_run >$@; \
	elif [[ "$$align_stem" == "blastz" ]]; then \
		$(ALGO_BIN_DIR)/blastz_mkcombo $(SPECIES1) $(SEQDIR1) "$$(cat $(word 1,$^))" $(SPECIES2) $(SEQDIR2) "$$(cat $(word 3,$^))" $(ALIGN_TYPE)_single_run >$@; \
	else \
		echo "Unsupported alignment type." >&2; \
		exit 1; \
	fi

include rules-algo.mk
include sequences.combo

.PHONY: all.align all.pdb clean clean.align clean.pdb
all.align: $(ALL_ALIGN)
all.pdb: $(ALL_PDB)
clean : clean.align clean.pdb
clean.align:
	rm -f $(ALL_ALIGN)
clean.pdb:
	rm -f $(ALL_PDB)
distclean : clean
	rm -f sequences.combo sequences-1.list sequences-2.list

align.accoda: sequences.combo
	opts=""; \
	if [ "$(ACCODA_REVERSE)" == "y" ]; then \
		opts+="-r"; \
	fi; \
	$(ALGO_BIN_DIR)/queue_helper $$opts -d1 $< ALL_ALIGN $(SPECIES1) $(SPECIES2) $(ALIGN_TYPE)

align.bsub: sequences.combo
	set -e; \
	$(ALGO_BIN_DIR)/queue_helper -d1 -c 'bsub make $$target' $< ALL_ALIGN $(SPECIES1) $(SPECIES2) $(ALIGN_TYPE)

align.accoda_gentile: sequences.combo
	set -e; \
	$(ALGO_BIN_DIR)/queue_helper -c '--name $$name -- make $$target' -n $< ALL_ALIGN $(SPECIES1) $(SPECIES2) $(ALIGN_TYPE) >accoda_gentile.tmp; \
	accoda_gentile accoda_gentile.tmp; \
	rm -f accoda_gentile.tmp

%.fa.splitted %.fa.nsq %.fa.splitted.xns %.fa.len:
	cd $$(dirname $@) && $(MAKE) $$(basename $@)

$(ALL_PDB):
	set -o pipefail; \
	set -e; \
	pdb_opts=""; \
	if [ "$(SPECIES1)" == "$(SPECIES2)" ]; then \
		pdb_opts="$$pdb_opts -s"; \
	fi; \
	if [ "$(ALIGN_TYPE)" == "wublast" ]; then \
		pdb_opts="$$pdb_opts -i"; \
	fi; \
	zcat $^ \
	| $(ALGO_BIN_DIR)/per_database $$pdb_opts \
	| gzip >$@
	rm $^

%.log.check_log: %.log
	@grep -q 'code 0' $< || echo $<

.PHONY: check.logs
check.logs: $(ALL_ALIGN:.gz=.log.check_log)

%.gz.check_archive: %.gz
	@gzip -t $< &>/dev/null || echo $<

.PHONY: check.archives
check.archives: $(addsuffix .check_archive,$(ALL_ALIGN))

%.raw.bz2.check_raw_archive: %.raw.bz2
	@bzip2 -t $< &>/dev/null || echo $<

.PHONY: check.raw_archives
check.raw_archives: $(ALL_ALIGN:.gz=.raw.bz2.check_raw_archive)

.PHONY: check.coords
check.coords: $(addsuffix .check_coords,$(ALL_PDB))

%.gz.reparsed: %.raw.bz2
	set -o pipefail; \
	set -e; \
	bzcat $< \
	| $(BIN_DIR)/blastz_parser_pipe \
	| gzip >$@; \
	dest=$@; \
	mv $@ $${dest%%.reparsed}

.PHONY: blastz.reparse
blastz.reparse: $(addsuffix .reparsed,$(ALL_ALIGN))

.PHONY: blastz.rescore
blastz.rescore: $(addsuffix .rescored,$(ALL_PDB))

strand.distrib: $(addsuffix .strand_distrib,$(ALL_PDB))
	sort -m -k1,1 $^ \
	| sum_groups >$@

.INTERMEDIATE: $(addsuffix .strand_distrib,$(ALL_PDB))
%.pdb.gz.strand_distrib: %.pdb.gz
	zcat $< \
	| cut -f4 \
	| sort -S40% -k1,1 \
	| $(emit_counts) >$@

score.distrib: $(addsuffix .score_distrib,$(ALL_PDB))
	sort -m -k1,1n $^ \
	| sum_groups >$@

.INTERMEDIATE: $(addsuffix .score_distrib,$(ALL_PDB))
%.pdb.gz.score_distrib: %.pdb.gz
	zcat $< \
	| cut -f9 \
	| sort -S40% -k1,1n \
	| $(emit_counts) >$@

.SECONDEXPANSION:
%.pdb.gz.check_coords: %.pdb.gz $$(call get_first_sequence,$(SEQDIR1),$$*) $$(call get_first_sequence,$(SEQDIR1),$$*).len $$(call get_second_sequence,$(SEQDIR2),$$*) $$(call get_second_sequence,$(SEQDIR2),$$*).len
	@set -e; \
	if ! zcat $< | $(BIN_DIR)/coords_check $(SEQDIR1) $(SEQDIR2); then \
		echo $<; \
	fi

%.pdb.gz.rescored: %.pdb.gz $$(call get_first_sequence,$(SEQDIR1),$$*) $$(call get_second_sequence,$(SEQDIR2),$$*)
	@if [ "$(MATRIX)" == "" -o "$(Q)" == "" -o "$(R)" == "" ]; then \
		echo "*** Need the following vars: MATRIX, Q, R." >&2; \
		exit 1; \
	fi
	set -o pipefail; \
	set -e; \
	zcat $< \
	| $(BIN_DIR_PARENT)/alignment_score $(SEQDIR1) $(SEQDIR2) $(MATRIX) $(Q) $(R) \
	| gzip >$@; \
	dest=$@; \
	mv $@ $${dest%%.rescored}
