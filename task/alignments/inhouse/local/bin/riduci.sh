#!/bin/bash

for i in mmusculus_chr1_hsapiens_chr* mmusculus_chr10_hsapiens_chr* mmusculus_chr11_hsapiens_chr* mmusculus_chr12_hsapiens_chr*; do
	echo $i;
	cd $i;
	zcat blast.gz | awk '$4>=16' | gzip > blast.gz.mv && mv -f blast.gz.mv blast.gz && touch ridotto;
	cd ..;
done;
