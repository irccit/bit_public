#!/usr/bin/perl
use warnings;
use strict;
$,="\t";
$\="\n";

my $up_coords = shift @ARGV;

open UP_COORDS , $up_coords or die("Can't open file ($up_coords)");

my %coords=();
while(<UP_COORDS>){
	chomp;
	my @F=split;
	my ($id1, $chr1, $b1, $e1, $id2, $chr2, $b2, $e2 ) = split;
	my @tmp1=($chr1, $b1, $e1);
	my @tmp2=($chr2, $b2, $e2);


	$coords{$id1}=\@tmp1;
	$coords{$id2}=\@tmp2;
}

while(<>){
	chomp;
	my ($id, $b, $e) = split;
	
	die("No coordinates for id ($id)") if !defined($coords{$id});
	my @tmp=@{$coords{$id}};

	print $id, $tmp[0], $b + $tmp[1], $e + $tmp[1];

	die("Overflow in coordinates") if $e + $tmp[1] > $tmp[2];
}
