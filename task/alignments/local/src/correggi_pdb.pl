#!/usr/bin/perl

use warnings;
use strict;

my $usage = "zcat qualcosa.pdb.gz | $0
REGOLA PER CORREGGERE TUTTI I pdb
	for i in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 X Y; do rm -f hsapiens_chr\${i}_hsapiens_chr\${i}.wublast.pdb.gz; zcat hsapiens_chr\${i}_hsapiens_chr\${i}.wublast.pdb.gz.old > hsapiens_chr\${i}_hsapiens_chr\${i}.wublast.pdb.tmp; ../../../local/bin/correggi_pdb < hsapiens_chr\${i}_hsapiens_chr\${i}.wublast.pdb.tmp | sort -k2,2n -k3,3n -k6,6n -k7,7 | gzip > hsapiens_chr\${i}_hsapiens_chr\${i}.wublast.pdb.gz; rm hsapiens_chr\${i}_hsapiens_chr\${i}.wublast.pdb.tmp; done
";


$,="\t";
$\="\n";


while (my $line = <>){
	chomp $line;
	my @F = split /\t/, $line;

	die "ERRORE: file non da correggere: cromosomi diversi" if $F[0] ne $F[4];

	next if (($F[1]==$F[5]) and ($F[2]==$F[6]));

	$F[11] = "" if (!defined $F[11]);
	$F[12] = "" if (!defined $F[12]);

	if ( ($F[5]<$F[1]) or (($F[5]==$F[1]) and ($F[6]<$F[2])) ) {
		my $tmp=$F[1];
		$F[1]=$F[5];
		$F[5]=$tmp;
		$tmp=$F[2];
		$F[2]=$F[6];
		$F[6]=$tmp;
		$tmp=$F[11];
		$F[11]=$F[12];
		$F[12]=$tmp;
	}
	print @F;
}
