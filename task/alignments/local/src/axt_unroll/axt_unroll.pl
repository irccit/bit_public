#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\t";

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 [-uc] < file.axt
convert the atx in a multi fasta, in which each block
is a single line of a axt alignment and the other
informations are stored in the header of the block

\t-uc convert the sequences in uppercase
\n";

my $help=0;
my $uc=0;
GetOptions (
	'h|help' => \$help,
	'uc|u'	=> \$uc
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

$\="\n";
while(<>){
	chomp;
	next if m/^#/;
	next if !$_;
	my ($id,$chr1,$b1,$e1,$chr2,$b2,$e2,$strand,$score) = split /\s+/,$_,-1;
	my $s1 = <>;
	chomp($s1);
	my $s2 = <>;
	chomp($s2);
	print ">$id"."_s1",$chr1,$b1,$e1,'+',$score;
	if($uc){
		print uc($s1);
	}else{
		print $s1;
	}
	print ">$id"."_s2",$chr2,$b2,$e2,$strand,$score;
	if($uc){
		print uc($s2);
	}else{
		print $s2;
	}
}
