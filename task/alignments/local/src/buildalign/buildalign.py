#!/usr/bin/env python

from __future__ import with_statement

from optparse import OptionParser
from os import close, system, unlink
from os.path import join
from shutil import rmtree
from sys import exit
from tempfile import mkdtemp
from vfork.fasta import SingleBlockReader, SingleBlockWriter

default_options = {
	'bl2seq'  : '-p blastn',
	'splign'  : '-strand both',
	'lalign'  : '-n -I',
	'tblastn' : '--help',
	'blastn'  : ''
}

default_formatter = {
	'tblastn' : 'xdformat -p -o $< $<',
	'blastn' : 'xdformat -n -o $< $<'
}

cmdline_template = {
	'bl2seq'  : '$(prog) $(opts) -i $(query) -j $(target)',
	'splign'  : '$(prog) $(opts) -query $(query) -subj $(target)',
	'lalign'  : '$(prog) $(opts) $(query) $(target); $(prog) $(opts) -i $(query) $(target)',
	'tblastn' : '$(prog) $(target) $(query) $(opts)',
	'blastn'  : '$(prog) $(target) $(query) $(opts)'
}

class TemporaryDir(object):
	def __init__(self, parent_dir, delete_after=True):
		self.path = parent_dir
		self.delete_after = delete_after
	
	def __enter__(self):
		self.path = mkdtemp(dir=self.path)
		return self
	
	def __exit__(self, type, value, tb):
		if self.delete_after:
			try:
				rmtree(self.path)
			except:
				pass
		return False

def extract_sequence(input_path, output_path, region):
	assert region[0] >= 0
	assert region[1] > region[0]
	
	r = SingleBlockReader(input_path)
	w = SingleBlockWriter(output_path, '%s:%d:%d' % (r.header, region[0], region[1]))
	w.write(r.get(region[0], region[1]-region[0]))
	r.close()
	w.close()

def safe_path(path):
	return "'%s'" % path.replace("'", "\\'")

def format_sequence(sequence, options):
	if options.formatter is None:
		formatter = default_formatter.get(options.program, '')
	else:
		formatter = options.formatter
	
	if len(formatter) == 0:
		return
	elif '$<' not in formatter:
		exit('Malformed formatter: missing $< spec.')
	
	cmdline = formatter.replace('$<', safe_path(sequence))
	if options.show_cmdline:
		print '*** Formatter command line:'
		print cmdline
	
	if system(cmdline) != 0:
		exit('Error running the formatter.')

def format_cmdline(program, options, query, target):
	template = cmdline_template[program]
	
	for name, value in zip(('prog', 'opts', 'query', 'target'), (program, options, query, target)):
		tag = '$(%s)' % name
		if tag in template:
			template = template.replace(tag, value)
		else:
			exit('Malformed command template.')
	
	return template

def main():
	parser = OptionParser(usage='%prog [OPTIONS] QUERY_FASTA QUERY_START QUERY_STOP TARGET_FASTA TARGET_START TARGET_STOP')
	parser.add_option('-f', '--formatter', dest='formatter', default=None, help='the command to format the database sequence; $< will be replaced with the input FASTA path')
	parser.add_option('-k', '--keep-temp', dest='keep_temp', action='store_true', default=False, help='keep temporary files needed to compute the alignments')
	parser.add_option('-o', '--program-options', dest='program_options', default=None, help='options for the alignment program')
	parser.add_option('-p', '--program', dest='program', default='bl2seq', help='the program to compute alignments; defaults to bl2seq')
	parser.add_option('-s', '--show-cmdline', dest='show_cmdline', action='store_true', default=False, help='show the command line of the alignment program')
	options, args = parser.parse_args()
	
	if len(args) != 6:
		exit('Unexpected argument number.')
	elif options.program not in default_options.keys():
		exit('Unknown program %s.' % options.program)
	
	query_region = (int(args[1]), int(args[2]))
	target_region = (int(args[4]), int(args[5]))
	
	with TemporaryDir('.', not options.keep_temp) as work_dir:
		query_path = join(work_dir.path, 'query.fa')
		extract_sequence(args[0], query_path, query_region)
		
		target_path = join(work_dir.path, 'target.fa')
		extract_sequence(args[3], target_path, target_region)
		format_sequence(target_path, options)
		
		if options.program_options is None:
			program_options = default_options.get(options.program, '')
		else:
			program_options = options.program_options
		
		cmdline = format_cmdline(options.program, program_options, query_path, target_path)
		if options.show_cmdline:
			print '*** Command line:'
			print cmdline
		
		if system(cmdline) != 0:
			exit('Error running %s.' % options.program)

if __name__ == '__main__':
	main()
