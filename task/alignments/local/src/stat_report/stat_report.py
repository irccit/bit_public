#!/usr/bin/env python
# encoding: utf-8
from __future__ import with_statement

from Cheetah.Template import Template
from gzip import GzipFile
from optparse import OptionParser
from os import makedirs
from os.path import abspath, dirname, isdir, join
from socket import gethostname
from sys import exit
from time import ctime
from vfork.io.util import safe_rstrip
import plugins

def common_path(paths):
	base = dirname(paths[0])
	for path in paths[1:]:
		if dirname(path) != base:
			return None
	return abspath(base)

def sort_plugins_by_title(plugins):
	def plugin_name(plugin):
		return plugin.title
	plugins.sort(key=plugin_name)

def iter_alignments(filename):
	if filename.endswith('.gz'):
		fd = GzipFile(filename, 'r')
	else:
		fd = file(filename, 'r')
	
	for lineno, line in enumerate(fd):
		tokens = safe_rstrip(line).split('\t')
		#if len(tokens) != 13:
		#	raise ValueError, 'malformed row at line %d of file %s' % (lineno+1, filename)
		tokens[8] = float(tokens[8])
		
		for col, descr in ((1, 'query start'), (2, 'query stop'), (5, 'target start'), (6, 'target stop'), (7, 'length'), (8, 'score')):
			try:
				value = int(tokens[col])
				if value < 0:
					raise ValueError
				else:
					tokens[col] = value
			except ValueError:
				raise ValueError, 'invalid %s at line %d of file %s' % (descr, lineno+1, filename)
		
		try:
			if tokens[3] not in ('+', '-'):
				raise ValueError, 'invalid strand'
			elif tokens[1] >= tokens[2]:
				raise ValueError, 'invalid query stop'
			elif tokens[5] >= tokens[6]:
				raise ValueError, 'invalid target stop'
		except ValueError, e:
			raise ValueError, '%s at line %d of file %s' % (e.args[0], lineno+1, filename)
		
		yield tokens

def main():
	parser = OptionParser(usage='%prog PDB...')
	parser.add_option('-o', '--output-dir', dest='output_dir', default='report', help='the directory used to store the report (by default: report)', metavar='DIR')
	options, args = parser.parse_args()
	
	if len(args) < 1:
		exit('Unexpected argument number.')
	
	basedir = common_path(args)
	if basedir is None:
		exit('All input files must reside in the same directory.')
	
	if not isdir(options.output_dir):
		makedirs(options.output_dir)
	
	report = Template(file='report.tmpl')
	report.pdb_num = len(args)
	report.pdb_dir = basedir
	report.pdb_host = gethostname()
	report.processing_date = ctime()
	report.sections = []
	
	plugs = plugins.load('plugins')
	if len(plugs) == 0:
		exit('No usable plugin found.')
	else:
		sort_plugins_by_title(plugs)
	
	plugs.reverse()
	while len(plugs):
		plug = plugs.pop()
		for arg in args:
			plug.process_alignments(iter_alignments(arg))
		
		report.sections.append((plug.title, plug.build_report(options.output_dir)))
		del plug
	
	with file(join(options.output_dir, 'index.html'), 'w') as fd:
		print >>fd, report

if __name__ == "__main__":
	main()
