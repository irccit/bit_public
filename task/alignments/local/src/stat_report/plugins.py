# encoding: utf-8
from glob import glob
from os.path import basename, join
import sys

def load(path):
	classname = 'Plugin'
	sys.path.append(path)
	
	try:
		plugins = []
		for modpath in glob(join(path, 'mod_*.py')):
			modname = basename(modpath).replace('.py', '')
			mod = __import__(modname)
			
			try:
				plugin_class = getattr(mod, classname)
			except AttributeError:
				print >>sys.stderr, '[WARNING] the plugin %s is missing the Plugin class; it will be ignored' % modpath
				continue
			
			plugins.append(plugin_class())
		
		return plugins
	
	finally:
		sys.path.remove(path)
