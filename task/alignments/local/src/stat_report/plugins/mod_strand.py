# encoding: utf-8
from __future__ import division
from Cheetah.Template import Template

template = '''
<ul>
	<li>plus: <code>$plus_count</code></li>
	<li>minus: <code>$minus_count</code></li>
</ul>
<p>The asymmetry level is: <code>$asymmetry_level%</code>
'''

class Plugin(object):
	title = 'Strands'
	plus_count = 0
	minus_count = 0
	
	def process_alignments(self, iter):
		ps = ms = 0
		for alignment in iter:
			if alignment[3] == '+':
				ps += 1
			else:
				ms += 1
		
		self.plus_count += ps
		self.minus_count += ms
	
	def build_report(self, report_path):
		t = Template(template)
		t.plus_count = self.plus_count
		t.minus_count = self.minus_count
		t.asymmetry_level = '%.2f' % ((self.plus_count - self.minus_count) / (self.plus_count + self.minus_count) * 100)
		return str(t)
