# encoding: utf-8
from __future__ import division
from __future__ import with_statement
from binner import OutOfMemoryCounter
from Cheetah.Template import Template
from itertools import izip
from os import popen, unlink
from os.path import join

template = '''
<p>Alignment length varies from <code>$min_length</code> to <code>$max_length</code>.</p>
<p>Length distribution plot for lengths ranging from $min_plot_length to $max_plot_length:<br>
<img src="$distrib_plot"></img></p>
'''

class LengthIterator(object):
	def __init__(self):
		self.min_length = None
		self.max_length = None
		self.plus_strand_lengths = []
		self.minus_strand_lengths = []
	
	def iter_lengths(self, iter):
		i = None
		a = None
		pl = []
		ml = []
		
		for alignment in iter:
			length = alignment[7]
			
			if i is None or length < i:
				i = length
			if a is None or length > a:
				a = length
			
			if alignment[3] == '+':
				pl.append(length)
			else:
				ml.append(length)
			
			yield length
		
		if self.min_length is None or self.min_length > i:
			self.min_length = i
		if self.max_length is None or self.max_length < a:
			self.max_length = a
		
		self.plus_strand_lengths = pl
		self.minus_strand_lengths = ml

class Plugin(object):
	title = 'Lenghts'
	min_plot_length = 16
	max_plot_length = 1000
	distrib_plot = 'length_distrib.png'
	
	def __init__(self):
		self.liter = LengthIterator()
		self.counter = OutOfMemoryCounter(bin_num=20, coord_min=self.min_plot_length, coord_max=self.max_plot_length)
		self.plus_counter = OutOfMemoryCounter(bin_num=20, coord_min=self.min_plot_length, coord_max=self.max_plot_length)
		self.minus_counter = OutOfMemoryCounter(bin_num=20, coord_min=self.min_plot_length, coord_max=self.max_plot_length)
	
	def process_alignments(self, iter):
		self.counter.load_values(self.liter.iter_lengths(iter))
		self.plus_counter.load_values(self.liter.plus_strand_lengths)
		self.minus_counter.load_values(self.liter.minus_strand_lengths)
		
	def build_report(self, report_path):
		datafile = 'gnuplot.data'
		with file(datafile, 'w') as fd: #TODO: use temporary name
			for (bin_start, count), (a, plus_count), (b, minus_count) in izip(self.counter.iter_counts('l'), self.plus_counter.iter_counts('l'), self.minus_counter.iter_counts('l')):
				print >>fd, '\t'.join(str(v) for v in (bin_start, count, plus_count, minus_count))
		
		gnuplot_fd = popen('gnuplot', 'w')
		print >>gnuplot_fd, 'set terminal png'
		print >>gnuplot_fd, 'set output "%s"' % join(report_path, self.distrib_plot)
		print >>gnuplot_fd, 'plot "%s" using 1:2 w steps title "total", "%s" using 1:3 w steps title "plus strand", "%s" using 1:4 w steps title "minus strand"' % (datafile, datafile, datafile)
		res = gnuplot_fd.close()
		
		unlink(datafile)
		if res is not None:
			raise RuntimeError, 'error running gnuplot'
		
		t = Template(template)
		t.min_length = self.liter.min_length
		t.max_length = self.liter.max_length
		t.min_plot_length = self.min_plot_length
		t.max_plot_length = self.max_plot_length
		t.distrib_plot = self.distrib_plot
		return str(t)
