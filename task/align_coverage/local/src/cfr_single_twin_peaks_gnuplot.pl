#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;

$\="\n";

my $usage="$0 \$@ -l label_index -chr '\$(ALL_CHR)' \$@.tmp";

my $label_idx = undef;
my $chrs = undef;

GetOptions(
	'label|l=s'      => \$label_idx,
	'chr|chr=s'      => \$chrs
) or die $usage;

die $usage if ( (!defined $label_idx) or (!defined $chrs) );
die $usage if !$ARGV[0];



print "set terminal postscript enhanced color \"Helvetica\" 10;";
print "set grid mytics ytics xtics;";
print "set xlabel 'chrs';";

my @chrs_list = split /\s+/,$chrs;
my @xtics = ();
for (my $i=0; $i< scalar @chrs_list; $i++) {
	push @xtics, "\"$chrs_list[$i]\" $i";
}
print "set xtics (", (join ', ',@xtics),");";

print "plot '$ARGV[0]' using 4 w point pt 6 ps 1 title '$label_idx', 0 w line lw 2;";
