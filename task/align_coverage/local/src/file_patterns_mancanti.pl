#!/usr/bin/perl

use warnings;
use strict;

$\="\n";

my $usage = "$0 rules.mk < patterns_file
	rules.mk must be a file!";

my $rules_file = shift @ARGV;
die $usage if !defined $rules_file;

my @patterns = ();
while (<>) {
	if (m/^>/) {
		chomp;
		s/^>//;
		push @patterns,$_;
	}
}


my $list = `grep ':' $rules_file | grep -v '^#' | grep -v '=' | grep -v '^\\\.'`;
my @F = split /\n/,$list;

foreach my $line (@F) {
	my ($file_name) = $line =~ /^(.*)\s*:/;
	next if ( ($file_name =~ /^all\./) or ($file_name =~ /\.all$/) );

	$file_name =~ s/%/\.\*/;
	$file_name =~ s/\./\\\./;
	$file_name =~ s/\$/\\\$/;

	next if ($file_name !~ /ucsc/);
	print 'file: ',$file_name;
	my $found = 0;
	foreach my $pattern (@patterns) {
		print 'pattern: ',$pattern;
		die;
		if ($file_name =~ /$pattern/) {
			$found = 1;
			last;
		}
	}
	print $file_name if !$found;
}


