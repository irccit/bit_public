#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Long;

use constant ID1      => 0;
use constant ID2      => 2;
use constant SCORE    => 4;
use constant CONNECT1 => 1;
use constant CONNECT2 => 3;

$,="\t";
$\="\n";


my $usage = " $0 score_connectivity communities -hr header_rows -hc header_cols [-id]
	-id if ids in file communities don't contain chr,b,e";

my $id_only = undef;
my $header_rows = undef;
my $header_cols = undef;
GetOptions (
	'header_rows|hr=n' => \$header_rows,
	'header_cols|hc=n' => \$header_cols,
	'id_only|id' => \$id_only
) or die ($usage);

my $score_file = shift @ARGV;
die $usage if (!$score_file or !defined $header_rows or !defined $header_cols);


my $conn_comp_id=0;
my @all_conn_comp_id=();

my @void = ();
for (my $j=0; $j<10; $j++) {
	push @void,'N';
}


####### linee di intestazione ########
for (my $j=0; $j<$header_rows; $j++) {
	my $line = <>;
}
######################################

my $tmp_file = 'tmp';

while(<>){
	chomp;
	my @F = split /\t/,$_;

	for (my $i=0; $i<$header_cols; $i++) {
		shift @F;
	}


	if (scalar @F == 1) {
		print $conn_comp_id, '1',@void;
		next;
	}

	open TMP,">$tmp_file" or die "Can't write on temporary file $tmp_file";
	my %conn_comp=();
	my @score = ();
	my %connectivity = ();
	for(@F){
		print TMP $_;
		$conn_comp{$_} = 1; 
	}
	close TMP;
	my $scores = `grep -wF -f $tmp_file $score_file`;
	unlink $tmp_file;
	my @lines = split /\n/,$scores;

	foreach my $row (@lines) {
		my @f = split /\t/,$row;
		if ($id_only) {
			$f[ID1] =~ s/:.*//g;
			$f[ID2] =~ s/:.*//g;
		}
		next if (!defined $conn_comp{$f[ID1]} or !defined $conn_comp{$f[ID2]});

		push @score, $f[SCORE];
		$connectivity{$f[ID1]} = $f[CONNECT1];
		$connectivity{$f[ID2]} = $f[CONNECT2];
	}

	next if !@score;

	my $score_mean = &mean(\@score);
	my $score_stdev = &stdev(\@score,$score_mean);
	my $score_ref = &median_min_max(\@score);
	my ($score_median, $score_min, $score_max) = @{$score_ref};

	my @a = values %connectivity;
	my $conn_mean = &mean(\@a);
	my $conn_stdev = &stdev(\@a,$conn_mean);
	my $conn_ref = &median_min_max(\@a);
	my ($conn_median, $conn_min, $conn_max) = @{$conn_ref};
	
	print 	$conn_comp_id, scalar(@a), 	
		$score_mean,$score_median,$score_stdev,$score_min,$score_max,
		$conn_mean, $conn_median, $conn_stdev, $conn_min, $conn_max;

	$conn_comp_id++;
}


sub mean
{
	my $sum=0;
	foreach my $value (@{$_[0]}){
		$sum += $value;
	}
	return $sum/scalar(@{$_[0]});
}

sub median_min_max
{
	my $i = int(scalar(@{$_[0]})/2);
	my @a = sort {$a<=>$b} @{$_[0]};
	my @tmp=($a[$i],$a[0],$a[scalar(@a)- 1]);
	return \@tmp;
}

sub stdev
{
	my $mean = pop @_;
	my $stdev=0;

	foreach my $value (@{$_[0]}){
		$stdev += ($value - $mean)**2;
	}

	return sqrt($stdev / scalar(@{$_[0]}));
}


