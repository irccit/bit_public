#!/usr/bin/perl

use strict;
use warnings;
no warnings "recursion";
use Getopt::Long;

use constant ID1 => 0;
use constant ID2 => 1;
use constant STRAND => 3;

$,="\t";
$\="\n";

my $usage = "cat qualcosa.score | $0 -c regions.1_50.coverage_correct_triggered.conn_comp [-id] [-ccomp]
	-id if ids in group_file don't contain chr,b,e
	-ccomp if groups are connected components (e.g. no links between nodes from 2 different groups)";

my $group_file = undef;
my $id_only = undef;
my $is_conn_comp = undef;
GetOptions (
	'group_file|c=s' => \$group_file,
	'id_only|id'     => \$id_only,
	'ccomp|ccomp'    => \$is_conn_comp
) or die ($usage);
die $usage if !defined $group_file;


open GROUP_FILE,$group_file or die "Can't open file $group_file";


my %network = ();
while (<>) {
	chomp;
	my @F = split /\t/,$_;
	if ($id_only) {
		$F[ID1] =~ s/:.*//g;
		$F[ID2] =~ s/:.*//g;
	}
	my @tmp1=($F[ID2],$F[STRAND]);
	my @tmp2=($F[ID1],$F[STRAND]);
	push @{$network{$F[ID1]}},\@tmp1;
	push @{$network{$F[ID2]}},\@tmp2;
}


my %group = ();
my @group_ids = ();
while (<GROUP_FILE>) {
	chomp;
	my @F = split /\t/,$_;
	my $g_id = shift @F;
	$g_id =~ s/^>//;
	foreach my $node (@F) { 
		$group{$g_id}{$node} = 1;
	}
	push @group_ids, $g_id;
}


my %already_seen = ();
foreach my $g_idx (@group_ids){
	my @nodes = keys %{$group{$g_idx}};

	$already_seen{$nodes[0]} = 1;
	print ">$g_idx";
	print $nodes[0],'+';

	&neighbours($g_idx,$nodes[0],'+');
}


sub neighbours
{
	my $g_idx = shift;
	my $id = shift;
	my $strand = shift;


	my @neighbours = @{$network{$id}};
	foreach(@neighbours){
		if ($is_conn_comp) { 
			die ("ERROR: edge between nodes from different connected components!!") if !defined $group{$g_idx}{${$_}[0]};
		} else {
			next if !defined $group{$g_idx}{${$_}[0]};
		}

		if(!defined($already_seen{${$_}[0]})){
			my $out_strand;
			if($strand eq ${$_}[1]){
				$out_strand='+';
			}else{
				$out_strand='-';
			}
			print ${$_}[0],$out_strand;
			$already_seen{${$_}[0]} = 1;
			&neighbours($g_idx,${$_}[0],$out_strand);
		}
	}
}


