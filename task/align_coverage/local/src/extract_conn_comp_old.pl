#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;


my $usage = "$0 -n conn_comp_number -c regions.1_50.coverage_correct_triggered.conn_comp -tmp \$@.tmp [-line]
	regions.1_50.coverage_correct_triggered.score
	-line option to print the entire line of score_file\n";

my $conn_comp_file  =  undef;
my $conn_comp_number = undef;
my $tmp_file = undef;
my $full_score_line = undef;
GetOptions (
	'conn_comp_file|c=s'   => \$conn_comp_file,
	'conn_comp_number|n=s' => \$conn_comp_number,
	'temporary_file|tmp=s' => \$tmp_file,
	'print_line|line' => \$full_score_line
) or die ($usage);
die $usage if (!defined $conn_comp_file or !defined $conn_comp_number or !defined $tmp_file);

my $score_file = shift @ARGV;
die $usage if !defined $score_file;

open CONN_COMP,$conn_comp_file or die "Can't open file $conn_comp_file\n";

my $i=0;
my $line = undef;
while ($i <= $conn_comp_number) {
	$line = <CONN_COMP>;
	die "ERROR: line $i not defined\n" if !$line;
	$i++;
}

chomp $line;
my $nodes = $line;
$nodes =~ s/\t/\n/g;


open TMP,">$tmp_file" or die "Can't write on temporary file $tmp_file\n";
print TMP $nodes;

if (defined $full_score_line) {
	print `grep -wF -f $tmp_file $score_file`;
} else {
	print `grep -wF -f $tmp_file $score_file | tr ":" "\t" | cut -f 1,3`; # prints only ids
}
`rm -f $tmp_file`;
