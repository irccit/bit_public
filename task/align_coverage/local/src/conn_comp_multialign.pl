#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;

$,="\t";
$\="\n";

my $usage = "cat regions.1_50.coverage_correct_triggered.conn_comp | $0 -t tmp_file regions.1_50.coverage_correct_triggered.fa";

my $tmp_file = undef;
GetOptions (
	'temporary_file|c=s' => \$tmp_file
) or die ($usage);
die $usage if !defined $tmp_file;

my $conn_comp_file = shift @ARGV;
die $usage if ($conn_comp_file); 


while (<>) {
	chomp;
	my @conn_comp = split /\t/,$_;

	open TMP, $tmp_file or die "Can't open temporary file $tmp_file";
	foreach my $region (@conn_comp) {
		print TMP `get_fasta $region $conn_comp_file`;
	}
	close TMP;

	print `probcons $tmp_file`;

}

