#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;

my $usage = "get_fasta -n 5 graph | cut -f -2 | $0 -tmp tmp_file [-n 5] [-s 15]
	-n max number of cliques to extract
	-s minimum clique size\n";

my $tmp_file = undef;
my $max_cliques_n = undef;
my $min_size = 3;
GetOptions (
	'tmp_file|tmp=s' => \$tmp_file,
	'max_n|n'        => \$max_cliques_n,
	'min_size|s'     => \$min_size
) or die($usage);
die ($usage) if !defined $tmp_file;

die ("ERROR: min_size -s must be > 0") if ($min_size <= 0); 
die ("ERROR: max_n -n must be > 0") if (defined $max_cliques_n and $max_cliques_n <= 0); 


my %node2id = ();
my %id2node = ();
my %edges = ();
my $node_num = 1;
my $edge_num = 0;
while (<>) {
	chomp;
	my ($node1,$node2) = split /\t/,$_;
	die "ERROR in input file\n" if (!defined $node1 or !defined $node2);
	
	if (!defined $node2id{$node1}) {
		$node2id{$node1} = $node_num;
		$id2node{$node_num} = $node1;
		$node_num++;
	}
	if (!defined $node2id{$node2}) {
		$node2id{$node2} = $node_num;
		$id2node{$node_num} = $node2;
		$node_num++;
	}
	$edges{$node1}{$node2} = 1;
	$edges{$node2}{$node1} = 1;
	$edge_num += 2;
}
$node_num--;


my $clique_intest = "";
my @clique_nodes = ();
my $clique_number = 0;
while (1) {
	&print_dimacs;

	my $new_clique = `cl -x -1 -u -q -q $tmp_file`;
	die ("ERROR: cl\n") if (!defined $new_clique or $?!=0);

	my ($clique_size, $clique_weight) = ($new_clique =~ /^\s*size\s*=\s*(\d+)\s*,\s+weight\s*=\s*(\d+)\s*:\s+/);
	last if ($clique_size < $min_size);

	$clique_intest = $&;
	@clique_nodes = split /\s+/,$';

	&print_clique;
	&delete_clique_nodes;

	$clique_number++;
	last if ( ($node_num < $min_size) or ($clique_number >= $max_cliques_n) );

	&initialise_id;
}

`rm $tmp_file`;



sub print_dimacs
{
	open TMP,">$tmp_file" or die ("Can't write on temporary file ($tmp_file)\n");
	print TMP "p edge $node_num $edge_num\n";
	for my $n1 (keys %edges) {
		for my $n2 (keys %{$edges{$n1}}) {
			print TMP "e ".$node2id{$n1}." ".$node2id{$n2}."\n";
		}
	}
	close TMP;

	return;
}


sub print_clique
{
	$clique_intest =~ s/\s+$/\t/;
	print $clique_intest;

	my @out = ();
	for (my $i=0; $i<scalar @clique_nodes; $i++) {
		push @out,$id2node{$clique_nodes[$i]};
	}
	print (join ' ',@out);
	print "\n";

	return;
}


sub delete_clique_nodes
{
	foreach my $id (@clique_nodes) {
		my $n1 = $id2node{$id};
		for (keys %{$edges{$n1}}) {
			delete $edges{$n1}{$_};
			delete $edges{$_}{$n1};
			$edge_num -= 2;
		}
		$node_num--;
	}
	@clique_nodes = ();
}


sub initialise_id
{
	%node2id = ();
	%id2node = ();

	my $n_num = 1;
	for my $n1 (keys %edges) {
		for my $n2 (keys %{$edges{$n1}}) {
			if (!defined $node2id{$n1}) {
				$node2id{$n1} = $n_num;
				$id2node{$n_num} = $n1;
				$n_num++;
			}
			if (!defined $node2id{$n2}) {
				$node2id{$n2} = $n_num;
				$id2node{$n_num} = $n2;
				$n_num++;
			}
		}
	}
	$n_num--;

#	die ("ERROR in new node_num\t$n_num\t$node_num\n") if ($n_num != $node_num);

	return;
}
