#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;
use IO::Handle;


my $input_file = undef;

my $usage="$0 -in all_chr.rand.annote.density \$(COORDS_NONREDUNDANT)\n";

GetOptions (
        'input|in=s' => \$input_file
) or die ($usage);


my @labels = @ARGV;
die ("ERROR: no labels in input") if !@labels;


print "set terminal postscript enhanced color \"Helvetica\" 10
set grid mytics ytics xtics
set key spacing 1 # in legend
#set logscale y
set xlabel \"cromosomi\"
set ylabel \"\" 1.8,0
#set label \"\" at 0.06,30 font \"Helvetica,12\"\n";

my $point_style="w p pt 7 ps 0.8";
my $command =  "plot \\\n";
my @tmp = ();
for (my $i=0; $i<scalar (@labels); $i++) {
	my $col = $i + 2;
	push @tmp, "'$input_file' using 1:$col title \"$labels[$i]\" $point_style";
}
$command .= join ", \\\n", @tmp;
$command .= "\n";

print $command;
