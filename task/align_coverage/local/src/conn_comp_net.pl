#!/usr/bin/perl

use strict;
use warnings;
no warnings "recursion";
use Getopt::Long;

use constant ID1 => 0;
use constant ID2 => 1;
use constant STRAND => 3;

$,="\t";
$\="\n";

my $usage = "cat network | $0 -c regions.1_50.coverage_correct_triggered.conn_comp [-id]
	-id if ids in conn_comp_file don't contain chr,b,e";

my $conn_comp_file = undef;
my $id_only = undef;
GetOptions (
	'conn_comp_file|c=s' => \$conn_comp_file,
	'id_only|id' => \$id_only
) or die ($usage);
die $usage if !defined $conn_comp_file;


open CONN_COMP,$conn_comp_file or die "Can't open file $conn_comp_file";


my %network = ();
while (<>) {
	chomp;
	my @F = split /\t/,$_;
	if ($id_only) {
		$F[ID1] =~ s/:.*//g;
		$F[ID2] =~ s/:.*//g;
	}
	my @tmp1=($F[ID2],$F[STRAND]);
	my @tmp2=($F[ID1],$F[STRAND]);
	push @{$network{$F[ID1]}},\@tmp1;
	push @{$network{$F[ID2]}},\@tmp2;
}


my @conn_comp = ();
while (<CONN_COMP>) {
	chomp;
	my @F = split /\t/,$_;
	push @conn_comp, \@F;
}

my $i=0;

my %already_seen = ();
for(@conn_comp){
	$already_seen{${$_}[0]} = 1;
	print ">$i";
	print ${$_}[0],'+';
	$i++ ;
	&neighbours(${$_}[0],'+');
}


sub neighbours
{
	my $id = shift;
	my $strand = shift;

#	$i++;

	my @neighbours = @{$network{$id}};
	foreach(@neighbours){
		if(!defined($already_seen{${$_}[0]})){
			my $out_strand;
			if($strand eq ${$_}[1]){
				$out_strand='+';
			}else{
				$out_strand='-';
			}
			print ${$_}[0],$out_strand;
			$already_seen{${$_}[0]} = 1;
			&neighbours(${$_}[0],$out_strand);
#			$i--;
		}
	}
#	print STDERR $i;
}


