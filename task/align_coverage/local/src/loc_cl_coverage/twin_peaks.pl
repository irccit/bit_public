#!/usr/bin/perl 

use strict;
use warnings;
use Getopt::Long;


$,="\t";
$\="\n";


my $usage = "$0 -b \$(BIN_DIR) -h 5 -chr 10 -t 10_1_50.coverage_correct_triggered -m 60 [-min_l 16] 10_1.coverage_correct
	h passo\tm min_coverage_level\tmin_l min_word_length\n";
my $chr_n = undef;
my $h = undef;
my $BIN_DIR = undef;
my $fixed_coverage_file = undef;
my $min_coverage_level = undef;
my $min_seq_length = 0;


GetOptions (
        'bin_dir|b=s'     => \$BIN_DIR,
        'chr|chr=s'       => \$chr_n,
        'passo|h=i'       => \$h,
        'cov_tr_file|t=s' => \$fixed_coverage_file,
        'min_cov|m=i'     => \$min_coverage_level,
        'min_len|min_l=i' => \$min_seq_length
) or die ($usage);

die ($usage) if !$BIN_DIR;
die ($usage) if !$chr_n;
die ($usage) if !$h;
die ($usage) if !$fixed_coverage_file;
die ($usage) if !$min_coverage_level;

my $coverage_file = shift @ARGV;
die $usage if !$coverage_file;

-s $coverage_file or die ("ERROR: $0: File ($coverage_file) is empty!!");

my $fixed_coverage_level = ($fixed_coverage_file =~ /.+_(\d+)\.coverage_correct_triggered/);
die ("ERROR: $0: \$fixed_coverage_level must be lower than \$min_coverage_level") if ($fixed_coverage_level >= $min_coverage_level);



my $max_coverage = `awk '{print \$2}' $coverage_file | sort -n -k 1,1 -r | head -1`;
chomp $max_coverage;

my %regioni = ();	
my %count_old_regioni = ();	

for (my $i=$max_coverage; $i > $min_coverage_level; $i-= $h) {

	if (!%regioni) {
		if (!%count_old_regioni) {
			foreach (keys %regioni) {
				$count_old_regioni{$_} = scalar @{$regioni{$_}};
			}
		}
	}
			
	%regioni = ();

	my $tmp = "/dev/shm/tmp";
	`cat $coverage_file | $BIN_DIR/coverage_trigger $i | awk 'OFS=\"\\t\" {print $chr_n,\$1,\$2}' > $tmp`;
	
	my $intersec = `cat $fixed_coverage_file | intersection -- - $tmp | awk 'BEGIN{OFS=\"\\t\"} \$3!=\$2 {print \$2,\$3,\$4,\$5}'`;
	# contenuto di $intersec :
	# 1. start regione a livello di coverage $i
	# 2. stop regione a livello di coverage $i
	# 3. start regione a livello di coverage basso $fixed_coverage_level
	# 4. stop regione a livello di coverage basso $fixed_coverage_level
	
	my @lines = split /\n/,$intersec;
	my $n_lines = scalar @lines;

	for (my $j = 0; $j < $n_lines; $j++) {
		my @temp = split /\t/,$lines[$j];
		my $low_coverage_start = $temp[2];
		my $high_coverage_start = $temp[0];
		if (!defined $regioni{$low_coverage_start}) {
			my @aux = ($j);
			$regioni{$low_coverage_start} = \@aux;
		} else {
			push @{$regioni{$low_coverage_start}},$j;
		}
	}
	
	foreach my $start (sort {$a <=> $b} keys %regioni) {
		if (! defined $count_old_regioni{$start}) {
			if (scalar @{$regioni{$start}} > 1) {
				$count_old_regioni{$start} = @{$regioni{$start}};
				#################
				my @new_ind = ();
				my $pre_stop = -1;
				foreach my $ind (@{$regioni{$start}}) {
					my ($start,$stop,$reg_start,$reg_stop) = split /\t/,$lines[$ind];
					if ( ($stop - $start > $min_seq_length) and ($start - $pre_stop > $min_seq_length) ) {
						push @new_ind,$ind;
					}
					$pre_stop = $stop;
#					print $chr_n,$lines[$ind],$i;
				}
				next if (scalar @new_ind < 2);
				foreach my $idx (@new_ind) {
					print $chr_n,$lines[$idx],$i;
				}
				##################
#				foreach my $ind (@{$regioni{$start}}) {
#					print $chr_n,$lines[$ind],$i;
#				}
			}

		} else {
			if ( (scalar @{$regioni{$start}} > 1) and (scalar @{$regioni{$start}} > $count_old_regioni{$start}) ) {
				$count_old_regioni{$start} = @{$regioni{$start}};
				#################
				my @new_ind = ();
				my $pre_stop = -1;
				foreach my $ind (@{$regioni{$start}}) {
					my ($start,$stop,$reg_start,$reg_stop) = split /\t/,$lines[$ind];
					if ( ($stop - $start > $min_seq_length) and ($start - $pre_stop > $min_seq_length) ) {
						push @new_ind,$ind;
					}
					$pre_stop = $stop;
#					print $chr_n,$lines[$ind],$i;
				}
				next if (scalar @new_ind < 2);
				foreach my $idx (@new_ind) {
					print $chr_n,$lines[$idx],$i;
				}
				##################
			}
		} 
	} 
			
	unlink $tmp;

}
	
# cat 22.coverage.trigger_10 | intersection -- - a
