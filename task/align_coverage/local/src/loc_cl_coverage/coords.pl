#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;
use AlignTools;

$,="\t";
$\="\n";
$| = 1;

my $usage = "cat chr_b_e | $0 -f \$(BIOINFO_ROOT)/task/alignments/inhouse/dataset/homo_homo_ensembl39_wublast30/hsapiens_chr9_hsapiens_chr18.wublast.pdb.gz -s sx";
my $gap_coods_file = undef;
my $side = undef;

GetOptions (
        'gene_coods_file|f=s' => \$gap_coods_file,
        'side|s=s' => \$side
) or die ($usage);

die ($usage) if !$gap_coods_file;
die ($usage) if !$side;

open PDB,$gap_coods_file or die ("ERROR: $0: Cant' open file ($gap_coods_file)\n");


my @pdb_coords = ();

while (<PDB>) {
	chomp;
	my @tmp = split /\t/;
	my @aux = ();

	my @gaps_start_l=();
	my @gaps_length_l=();
	if($tmp[10]){
		@gaps_start_l  = ($tmp[10] =~ /(\d+),\d+;*/g);
		@gaps_length_l = ($tmp[10] =~ /\d+,(\d+);*/g);
		my $s_ref_l = &correct_gaps(\@gaps_start_l,\@gaps_length_l);
		@gaps_start_l = @{$s_ref_l};
		if($tmp[3] eq '-'){
#			print "original gaps_start : @gaps_start_l";
#			print "original gaps_length: @gaps_length_l";
			@gaps_length_l = reverse @gaps_length_l;
#			my $sum_gaps = 0;
#			foreach my $l_gap (@gaps_length_l) {
#				$sum_gaps += $l_gap;
#			}
#			my $l_tot_align = $tmp[2] + $sum_gaps;
#			@gaps_start_l  = reverse map{$_ = $l_tot_align - $_ } @gaps_start_l;
			@gaps_start_l  = reverse map{$_ = $tmp[2] - $_ } @gaps_start_l;
#			print "new gaps_start : @gaps_start_l";
#			print "new gaps_length: @gaps_length_l";
		}
	}

	my @gaps_start_r=();
	my @gaps_length_r=();
	if($tmp[11]){
		@gaps_start_r  = ($tmp[11] =~ /(\d+),\d+;*/g);
		@gaps_length_r = ($tmp[11] =~ /\d+,(\d+);*/g);
		my $s_ref_r = &correct_gaps(\@gaps_start_r,\@gaps_length_r);
		@gaps_start_r = @{$s_ref_r};
	}
	

	if ($side eq 'sx') {
		push @aux, @tmp[0..9];
#		print @aux,@gaps_start_l,@gaps_length_l,@gaps_start_r,@gaps_length_r;
		push @aux, \@gaps_start_l;
		push @aux, \@gaps_length_l;
		push @aux, \@gaps_start_r;
		push @aux, \@gaps_length_r;
	}else{
		push @aux, @tmp[4..6], $tmp[3], @tmp[0..2], @tmp[7..9];
#		print @aux,@gaps_start_l,@gaps_length_l,@gaps_start_r,@gaps_length_r;
		push @aux, \@gaps_start_r;
		push @aux, \@gaps_length_r;
		push @aux, \@gaps_start_l;
		push @aux, \@gaps_length_l;
	}
		
	push @pdb_coords,\@aux;

}	


while (<>) {
	my ($r_chr,$r_start,$r_stop) = split;
	print '>'.$r_chr,$r_start,$r_stop;
	foreach my $t (@pdb_coords) {
		
		my @tmp = @{$t};
		my $align_start = $tmp[1];
		my $align_stop = $tmp [2];
		my $strand = $tmp[3];

		next if ($r_stop <= $align_start);
		next if ($align_stop <= $r_start);
		# if ( ($r_stop <= $align_start) or ($r_start >= $align_stop) ) non c'e' intersezione tra la regione e l'allineamento
	
		# coordinate assolute dell'intersezione tra regione e allineamento
		my $int_start = $align_start < $r_start ? $r_start : $align_start; 
		my $int_stop = $align_stop < $r_stop ? $align_stop : $r_stop; 
#		print $int_start,$int_stop;
	
		# coordinate di allineamento sul cromosoma sx
		my $int_align_coords_left_start;
		my $int_align_coords_left_stop;
		my $int_align_length;
		my $align_length = $align_stop - $align_start;

		if (@{$tmp[10]}) {
#		if (${$tmp[10]}[0] !~ /^N$/) {
#			print @{$tmp[10]},'aaa',@{$tmp[11]};
			$int_align_coords_left_start = 
				&genomic2align ($tmp[10],$tmp[11], $align_start, $int_start); 
			$int_align_coords_left_stop =
				&genomic2align ($tmp[10],$tmp[11], $align_start, $int_stop); 
			$align_length = 
				&genomic2align ($tmp[10],$tmp[11], $align_start, $align_stop) - $align_start; 
		} else {
#			print 'ciao';
			$int_align_coords_left_start = $int_start; 
			$int_align_coords_left_stop  = $int_stop;
		}
		$int_align_length = $int_align_coords_left_stop - $int_align_coords_left_start;
#		print $int_align_coords_left_start,$int_align_coords_left_stop,$int_align_length;

		# coordinate di allineamento sul cromosoma dx
		my $dist = $int_align_coords_left_start - $align_start;
		my $dist_align = &genomic2align ($tmp[10],$tmp[11], $align_start, $dist); 
		my $int_align_coords_right_start = $tmp[5] + $dist_align;
		my $int_align_coords_right_stop  = $int_align_coords_right_start + $int_align_length;
#		print $dist_align,$int_align_coords_right_start,$int_align_coords_right_stop;

		# coordinate assolute sul cromosoma dx
		my $int_abs_coords_right_start;
		my $int_abs_coords_right_stop;

		if (@{$tmp[12]}) {
#		if (${$tmp[12]}[0] !~ /^N$/) {
#			print @{$tmp[12]},'aaa',@{$tmp[13]};
			$int_abs_coords_right_start = 
				&align2genomic ($tmp[12],$tmp[13], $tmp[5], $int_align_coords_right_start); 
			$int_abs_coords_right_stop =
				&align2genomic ($tmp[12],$tmp[13], $tmp[5], $int_align_coords_right_stop); 
		} else {
			$int_abs_coords_right_start = $int_align_coords_right_start;
			$int_abs_coords_right_stop  = $int_align_coords_right_stop;
		}	
	
		print $int_abs_coords_right_start,$int_abs_coords_right_stop, $strand;
	}
}


