#!/usr/bin/perl  

use strict;
use warnings;
use Getopt::Long;



my $usage = "plateaux_coverage.pl -b \$(BIN_DIR) -c chromosome\n";
my $BIN_DIR = undef;
my $chr_n = undef;


GetOptions (
        'bin_dir|b=s' => \$BIN_DIR,
        'chr|c=s' => \$chr_n
) or die ($usage);

die ($usage) if !$BIN_DIR;
die ($usage) if !$chr_n;




my $max_coverage = `awk '{print \$2}' $chr_n.coverage | sort -n -k 1,1 -r | head -1`;

# per ogni livello di coverage $i, stampa il punto medio delle regioni con coverage >= $i
for (my $i=1; $i<= $max_coverage; $i++) {
	print `cat $chr_n.coverage | $BIN_DIR/coverage_trigger $i | awk 'OFS=\" \"{print (\$1+\$2)/2,$i}'`;
}


