#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;


$,="\t";
#$\="\n";
$| = 1;

my $usage = "$0 -f LOC_CL_FILE -b \$(BIN_DIR) -c chromosome 22_50.coverage_triggered";
my $filename = undef;
my $BIN_DIR = undef;
my $chr_n = undef;

#my $j=0;

GetOptions (
        'loc_cl_file|f=s' => \$filename,
        'bin_dir|b=s' => \$BIN_DIR,
        'chr|c=s' => \$chr_n
) or die ($usage);

die ($usage) if !$filename;
die ($usage) if !$BIN_DIR;
die ($usage) if !$chr_n;

die ("ERROR: $0: file ($filename) is void") if (!-s $filename);
if ($filename =~ /\.gz/) {
	my $new_file_name = $filename;
	$new_file_name =~ s/.*\///g;
	$new_file_name =~ s/\.gz$/\.tmp/;
	`zcat $filename > $new_file_name`;
	open FILE, $new_file_name or die ("Cant' open file ($new_file_name)\n");
	`rm $new_file_name`;
} else {	
	open FILE,$filename or die ("ERROR: $0: Cant' open file ($filename)\n"); 
}



# dal file coi location clusters estraggo start,stop,offset degli header fasta
my @loc_cl_pos = ();
my $pos_in_starts_max;

my @list = ();

while (my $line = <FILE>) {
	next if (!($line =~ /^>/));

	my ($start1,$stop1,$start2,$stop2) = ($line =~ /^>(\d+)_(\d+)_(\d+)_(\d+)\s*$/);
	my $offset = tell (FILE);
	die $offset if ( (!defined $start1) || (!defined $stop1) );
	die $offset if ( (!defined $start2) || (!defined $stop2) );

	my @temp = ($start1,$stop1,$start2,$stop2,$offset);
	push @list, \@temp;
}

@loc_cl_pos = sort { ${$a}[0] <=> ${$b}[0] } @list;
$pos_in_starts_max = scalar(@loc_cl_pos);

while (<>) {
	chomp;
	my ($chr,$r_start,$r_stop) = split /\t/;

	# apertura pipe per la lista dei location cluster
	open PIPE,"| $BIN_DIR/coverage" or die ("ERROR: $0: Can't open pipe\n");

	&process_loc_cl($r_start, $r_stop);

	close PIPE;
	print $r_stop,"0\n";
}



sub process_loc_cl
{
	my $r_start = shift @_;
	my $r_stop = shift @_;

	for (my $i = 0; $i < $pos_in_starts_max; $i++) {
		my $start_sx  = ${$loc_cl_pos[$i]}[0];
		my $stop_sx   = ${$loc_cl_pos[$i]}[1];
		my $start_dx  = ${$loc_cl_pos[$i]}[2];
		my $stop_dx   = ${$loc_cl_pos[$i]}[3];
		my $offset    = ${$loc_cl_pos[$i]}[4];

		last if ($start_sx >= $r_stop);
		next if ($stop_sx <= $r_start);
		next if ($stop_dx <= $r_start);
		next if ($start_dx >= $r_stop);

		seek FILE,$offset,0;
		my $align = <FILE>;
		#$j++;
		#print STDERR "$j\r";
		while ( (defined $align) and ($align !~ /^>/) ) {
			chomp $align;
			#my ($start1,$stop1,$start2,$stop2) = ($align =~ /^\s*(\d+)\t(\d+)\t(\d+)\t(\d+)\s+/);
			my ($start1,$stop1,$start2,$stop2) = ($align =~ /^\s*\w+\t(\d+)\t(\d+)\t[+-]\t\w+\t(\d+)\t(\d+)\s+/);
			#22      14457106        14457297        +       22      21397819        21398006        193     123     83.42
			die("ERROR: $0:  $align") if (!defined($start1) || !defined($stop1) || !defined($start2) || !defined($stop2) );
			
			$align = <FILE>;

			next if ($start1 >= $r_stop);
			next if ($stop1 <= $r_start);
			next if ($stop2 <= $r_start);
			next if ($start2 >= $r_stop);

			$start1 = $start1 >= $r_start ? $start1 : $r_start;
			$stop1 = $stop1 <= $r_stop ? $stop1 : $r_stop;
			$start2 = $start2 >= $r_start ? $start2 : $r_start;
			$stop2 = $stop2 <= $r_stop ? $stop2 : $r_stop;
			print PIPE $start1,$stop1."\n";
			print PIPE $start2,$stop2."\n"; 
			

		}
	}
}
