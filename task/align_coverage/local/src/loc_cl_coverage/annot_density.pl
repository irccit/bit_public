#!/usr/bin/perl 

use strict;
use warnings;
use Getopt::Long;


$,="\t";
$\="\n";

my $usage = "cat b1_e1_b2_e2 | $0 -d \$(BIOINFO_ROOT)/task/sequences/dataset/ensembl/hsapiens/40";

my $seq_dir = undef;

GetOptions (
	'seq_dir|d=s' => \$seq_dir,
) or die ($usage);

$seq_dir =~ s/\/$//;



while (my $line = <>) {
	my @F = split /\t/,$line;
	my $chr = shift @F;
	-s "$seq_dir/chr$chr.fa.len" or die "ERROR: $seq_dir/chr$chr.fa.len is an empty file!!";
	my $chr_len = `cat $seq_dir/chr$chr.fa.len`;
	@F = map {$_ /= $chr_len} @F;
	print $chr,@F;
}
