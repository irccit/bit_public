#!/usr/bin/perl 

use strict;
use warnings;
use Getopt::Long;

$,="\t";
$\="\n";



my $autocoverage_file = undef;

my $usage = "cat 22.coverage | $0 -a 22_10.autocoverage\n";


GetOptions (
	'autocov_file|a=s'      =>  \$autocoverage_file
) or die ($usage);

$autocoverage_file or die ($usage);


	
my $x_c =-1;
my $y_c = 0;
my $x_change_c = undef;
my $y_change_c = undef;

my $x_a = 0;
my $y_a = 0;
my $x_change_a = undef;
my $y_change_a = undef;

open AUTOCOV,$autocoverage_file or die ("ERROR: $0: Can't open file ($autocoverage_file)");


my $first_line = <>; 	# tolgo la prima linea del coverage perche' e' sempre 0\t0
&get_next_xy;		# faccio girare una volta per inizializzare i valori

my $xy_a_ref = &get_line_autocov;	# faccio girare una volta per inizializzare i valori
($x_change_a,$y_change_a) = @{$xy_a_ref};

while ( &get_next_xy ) {

	while ($x_c > $x_change_a) {
		my $xy_a_ref = &get_next_autocov;
		($x_change_a,$y_change_a) = @{$xy_a_ref};
	}

	while ($x_c < $x_a) {
		print $x_c,$y_c;
	}
	$y_c -= $y_a;
}

	
	

sub get_next_xy
{

	my @xy = ();

	$x_c++;

	if(!defined($x_change_c)) { # inizializzo $x_change_c

		my $line = <>;
		return undef if !defined($line);
		chomp $line;
		($x_change_c, $y_change_c)=split /\t/, $line;
		$x_c = $x_change_c - 1; # x della prima finestra non tutta nulla
		$y_c = 0;
	} 
	elsif ($x_c >= $x_change_c){
		$y_c = $y_change_c;
		$x_c = $x_change_c;
		my $line = <>;
		return undef if !defined($line);
		chomp $line;
		($x_change_c, $y_change_c)=split /\t/, $line;
	} 
	return 1;
}



sub get_line_autocov
{

	my $line = <AUTOCOV>;
	return undef if !defined($line);
	chomp $line;

	while ( ($line =~ /^>/) or ($line =~ /0\t0/) ) {
		$line = <AUTOCOV>;
		return undef if !defined($line);
		chomp $line;
	}

	$x_a = $x_change_a; 
	$y_a = $y_change_a; 

	my @xy = split /\t/,$line; 

	return \@xy;
}
