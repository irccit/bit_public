#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;


$,="\t";
$\="\n";

my $usage = "cat wublast_alignments | $0 -r regions_file.fa
\traggruppa gli allineamenti per coppie di regioni";

my $regions_file = undef;

GetOptions (
	'reg_file|r=s' => \$regions_file,
) or die ($usage);

die $usage if !defined $regions_file;


open REGIONS,$regions_file or die "Can't open file ($regions_file)!";

my @regions = ();
my $old_chr = undef;
my $old_b = undef;
while (my $line = <REGIONS>) {
	chomp $line;
	next if !$line;
	next if !($line =~ /^>/);

	my ($r_chr,$r_b,$r_e) = ($line =~ /^>\d+:(\w+),(\d+),(\d+)$/);

	die "Empty columns in file $regions_file" if (!defined $r_chr or !defined $r_b or !defined $r_e);

	die "File $regions_file not sorted on starts" if (defined $old_chr and ($r_chr eq $old_chr) and ($r_b < $old_b) );
	$old_chr = $r_chr;
	$old_b = $r_b;

	my @tmp = ($r_chr,$r_b,$r_e);
	push @regions,\@tmp;
}
my $reg_l = scalar @regions;


my %reg2reg = ();
my $old_sx_chr = undef;
my $old_sx_b = undef;

while (<>) {
	chomp;
	my ($sx_chr,$sx_b,$sx_e,$dx_chr,$dx_b,$dx_e) = ($_ =~ /^\d+:(\w+),(\d+),(\d+)\t\d+:(\w+),(\d+),(\d+)\t*/);
	#1:10,216402,216639      617:19,58701120,58701400        5.3e-33 1       144.54 ...
	if (defined $old_sx_chr) {
		if ($sx_chr eq $old_sx_chr) {
			die "Input file not sorted on left starts" if $sx_b < $old_sx_b;
		} else {
			foreach my $key (keys %reg2reg) {
				print ">$key";
				foreach my $align (@{$reg2reg{$key}}) {
					print $align;
				}
			}
			%reg2reg = ();
		}
	}

	$old_sx_chr = $sx_chr;
	$old_sx_b = $sx_b;

	my $sx_reg = undef;
	my $dx_reg = undef;
	for (my $i=0; $i<$reg_l; $i++) {

		next if (${$regions[$i]}[0] ne $sx_chr);
		die "Regions not found for alignment: \n$_" if (${$regions[$i]}[1] >= $sx_e);
		next if !( (${$regions[$i]}[1] <= $sx_b) and (${$regions[$i]}[2] >= $sx_e) );

		$sx_reg = join '_',@{$regions[$i]};
		last;
	}

	for (my $i=0; $i<$reg_l; $i++) {

		next if (${$regions[$i]}[0] ne $dx_chr);
		next if !( (${$regions[$i]}[1] <= $dx_b) and (${$regions[$i]}[2] >= $dx_e) );

		$dx_reg = join '_',@{$regions[$i]};
		last;
	}
	die "Regions not found for alignment: \n$_" if !defined $dx_reg;

	push @{$reg2reg{"$sx_reg\t$dx_reg"}},$_;
}


foreach (keys %reg2reg) {
	print ">$_";
	foreach my $align (@{$reg2reg{$_}}) {
		print $align;
	}
}
