select 
	o.gene_stable_id,
	h.chr_name,
	h.gene_chrom_start,
	h.gene_chrom_end,
	o.homol_stable_id,
	o.chr_name,
	o.gene_chrom_start,
	o.gene_chrom_end
from hsapiens_gene_ensembl__homologs_mmusculus__dm as o 
join hsapiens_gene_ensembl__gene__main as h on (o.gene_stable_id = h.gene_stable_id)
where 
	o.homol_stable_id is not NULL 
	and o.description="UBRH"
	and h.confidence="KNOWN"
order by h.chr_name, h.gene_chrom_start, h.gene_chrom_end
