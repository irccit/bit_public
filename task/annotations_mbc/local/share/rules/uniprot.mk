CUR_RELEASE_URL =  ftp://ftp.uniprot.org/pub/databases/uniprot/current_release 

idmapping.dat.gz:
	wget -O $@ -q $(CUR_RELEASE_URL)/knowledgebase/idmapping/$@

.META: idmapping.dat.gz
	1	UniProt_AC 
	2	ID_type 
	3	ID 

