# Copyright 2008-2009 Gabriele Sales <gbrsales@gmail.com>

ANNOTATION_DIR     ?= $(BIOINFO_ROOT)/task/annotations/dataset/ensembl/mmusculus/50/
SEQUENCE_DIR       ?= $(BIOINFO_ROOT)/task/sequences/dataset/ensembl/mmusculus/50/
5UTR_STRUCTURES    ?= $(ANNOTATION_DIR)/5utr_structures.gz
3UTR_STRUCTURES    ?= $(ANNOTATION_DIR)/3utr_structures.gz
ENSEMBL_UTR_PREFIX ?=

ALL_DNA := $(shell cd $(SEQUENCE_DIR); bmake_query dna)
extern $(5UTR_STRUCTURES)
extern $(3UTR_STRUCTURES)
extern $(ANNOTATION_DIR)/exons.gz as EXONS
extern $(addsuffix .fa,$(addprefix $(SEQUENCE_DIR),$(ALL_DNA))) as SEQUENCES

$(ENSEMBL_UTR_PREFIX)5utrs.fa: $(5UTR_STRUCTURES) $(EXONS) $(SEQUENCES)
	zcat $< \
	| utr_sequences 5 <(zcat $^2) $(SEQUENCE_DIR) >$@

$(ENSEMBL_UTR_PREFIX)3utrs.fa: $(3UTR_STRUCTURES) $(EXONS) $(SEQUENCES)
	zcat $< \
	| utr_sequences 3 <(zcat $^2) $(SEQUENCE_DIR) >$@
