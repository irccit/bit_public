SEQUENCE_DIR = $(BIOINFO_ROOT)/task/sequences/dataset/ucsc/hsapiens/hg18
param dna as ALL_CHRS_M from $(SEQUENCE_DIR)
ALL_CHRS = $(subst chrM,,$(ALL_CHRS_M))

URL_PREFIX = http://mlg.ucd.ie/files/profisi/data/

ALL += $(addsuffix .bw, $(ALL_CHRS))

chr.sizes: $(BIOINFO_ROOT)/task/sequences/dataset/ucsc/hsapiens/$(VERSION)/all_chr.len
	cat $<\
	| append_each_row -B -F "" chr > $@

chr%.bw: chr.sizes
	wget -O - $(URL_PREFIX)/chr$*.$(VERSION).bed.bz2 \
	| bzcat \
	| cut -f 1,2,3,5 \
	| bawk 'BEGIN{pre_end=-1} {if($$2>=pre_end){print $$0; pre_end=$$3}}' \    *remove overlapping segments*
	| bsort -S10% -k1,1 -k2,2n > $@.tmp
	bedGraphToBigWig $@.tmp $< $@
	rm $@.tmp

