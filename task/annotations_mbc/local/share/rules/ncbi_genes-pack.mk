# Copyright 2008 Molineris Ivan <ivan.molineris@gmail.com>
# Copyright 2008-2009 Gabriele Sales <gbrsales@gmail.com>

NCBI_URL  := ftp://ftp.ncbi.nlm.nih.gov/gene/DATA/
ALL_GZ    := gene2accession.gz gene2go.gz gene2pubmed.gz \
             gene2refseq.gz gene_group.gz gene_history.gz \
             gene_info.gz gene_refseq_uniprotkb_collab.gz
ALL_PLAIN := gene2sts gene2unigene go_process.xml \
             mim2gene

ALL += $(ALL_GZ) $(addsuffix .gz,$(ALL_PLAIN))


define check_date
	@set -e; \
	date="$$(ftp_mdate '$(NCBI_URL)/$*')"; \
	if [[ $$date -gt $(VERSION) ]]; then \
		echo "Version mismatch: remote data is more recent than this dataset." >&2; \
		exit 1; \
	fi
endef


$(ALL_GZ): %:
	wget -O- '$(NCBI_URL)/$@' \
	| zcat \
	| unhead \
	| gzip >$@

$(addsuffix .gz,$(ALL_PLAIN)): %.gz:
	wget -O- '$(NCBI_URL)/$*' \
	| unhead \
	| gzip >$@

gene2summary.gz:
	 geneDocSum -q "has_summary[prop] " -o tab -t Name -t Summary | gzip > $@

.META: gene2summary.gz
	1   gene_id
	2   gene_name
	3   summary

.META: gene2accession.gz
	1   tax_id                                   9
	2   gene_id                                  1246500
	3   status                                   -
	4   rna_nucleotide_accession.version         -
	5   rna_nucleotide_gi                        -
	6   protein_accession.version                AAD12597.1
	7   protein_gi                               3282737
	8   genomic_nucleotide_accession.version     AF041837.1
	9   genomic_nucleotide_gi                    3282736
	10  start_position_on_the_genomic_accession  -
	11  end_position_on_the_genomic_accession    -
	12  orientation                              ?
	13  assembly                                 -

.META: gene2go.gz
	1  tax_id     3702
	2  gene_id    814629
	3  go_id      GO:0003676
	4  evidence   IEA
	5  qualifier  -
	6  go_term    nucleic acid binding
	7  pubmed     -
	8  category   Function

.META: gene2pubmed.gz
	1  tax_id     9
	2  gene_id    1246500
	3  pubmed_id  9873079

.META: gene2refseq.gz
	1   tax_id                                   9
	2   gene_id                                  1246500
	3   status                                   PROVISIONAL
	4   rna_nucleotide_accession.version         -
	5   rna_nucleotide_gi                        -
	6   protein_accession.version                NP_047184.1
	7   protein_gi                               10954455
	8   genomic_nucleotide_accession.version     NC_001911.1
	9   genomic_nucleotide_gi                    10954454
	10  start_position_on_the_genomic_accession  348
	11  end_position_on_the_genomic_accession    1190
	12  orientation                              -
	13  assembly                                 -

.META: gene_group.gz
	1  tax_id         7425
	2  gene_id        100116762
	3  relationship   Related functional gene
	4  other_tax_id   7425
	5  other_gene_id  100122024

.META: gene_history.gz
	1  tax_id                9
	2  gene_id               -
	3  discontinued_gene_id  1246494
	4  discontinued_symbol   repA1
	5  discontinue_date      20031113

.META: gene_info.gz
	1   tax_id                                 7
	2   gene_id                                5692769
	3   symbol                                 NEWENTRY
	4   locus_tag                              -
	5   synonyms                               -
	6   db_xrefs                               -
	7   chromosome                             -
	8   map_location                           -
	9   description                            Record to support submission of GeneRIFs [...]
	10  type_of_gene                           other
	11  symbol_from_nomenclature_authority     -
	12  full_name_from_nomenclature_authority  -
	13  nomenclature_status                    -
	14  other_designations                     -
	15  modification_date                      20071023

.META: gene_refseq_uniprotkb_collab.gz
	1  ncbi_protein_accession       AP_000064
	2  uniprotkb_protein_accession  Q65958

.META: gene2sts.gz
	1  unists   5320
	2  gene_id  65536

.META: gene2unigene
	1  gene_id          1268433
	2  unigene_cluster  Aga.201

.META: mim2gene
	1  mim_number  100300
	2  gene_id     100188340
	3  type        gene
