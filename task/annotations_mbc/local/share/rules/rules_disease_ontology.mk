ALL+= do_ensg_inclusive.gz do_ensg_inclusive2.gz do_ontology_inclusive.gz

do_rif.db:
	wget -O $@ $(SQLITE_URL)

do_entrez_inclusive.gz: do_rif.db
	java -classpath ":$(PRJ_ROOT)/local/src/:$(PRJ_ROOT)/local/src/sqlite-jdbc-3.7.2.jar" GetAssociationsDO $< | gzip > $@

ENTREZ_ENSG = $(PRJ_ROOT)/dataset/ncbi/genes/hsapiens/20130301/entrez-ensembl.map.gz
do_ensg_inclusive.gz: do_entrez_inclusive.gz $(ENTREZ_ENSG) do_ontology_inclusive.gz
	zcat $^3 | cut -f 1,2 | sort | uniq | bawk 'NF==2{print}'> $@.tmp
	zcat $^3 | cut -f 3,4 | sort | uniq | bawk 'NF==2{print}' >> $@.tmp
	zcat $< | translate -a -d -j -v -k <(zcat $^2) 1 | cut -f 2,3 \
	| translate -a -d -j -v -k <(sort $@.tmp | uniq) 2 \
	| bawk 'NF==3 && $$1 != "" {print}' | sort | uniq | gzip > $@

do_ensg_inclusive2.gz: do_entrez_inclusive.gz $(ENTREZ_ENSG)
	zcat $< | translate -a -d -j -k <(zcat $^2) 1 | cut -f 2,3,4 | sort  | uniq | gzip > $@

# data@decoder:~/Dropbox/work/mattia/disease_ontology$ javac GetAssociationsDO.java data@decoder:~/Dropbox/work/mattia/disease_ontology$ java -classpath ".:sqlite-jdbc-3.7.2.jar" GetAssociationsDO do_rif.db > do_entrez

do_ontology_inclusive.gz: do_rif.db
	java -classpath ":$(PRJ_ROOT)/local/src/:$(PRJ_ROOT)/local/src/sqlite-jdbc-3.7.2.jar" GetOntology $< | gzip > $@

do_ontology: do_rif.db
	java -classpath ":$(PRJ_ROOT)/local/src/:$(PRJ_ROOT)/local/src/sqlite-jdbc-3.7.2.jar" GetOntology_onlySon $< > $@


#data@decoder:~/Dropbox/work/mattia/disease_ontology$ time java -classpath ".:sqlite-jdbc-3.7.2.jar" GetOntology do_rif.db > pluto
#real	9m2.979s
#user	9m1.492s
#sys	0m1.116s
