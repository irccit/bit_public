SPECIES_MAP := $(TASK_ROOT)/local/share/fantom_species.map
FANTOM_SPECIES   = $(shell translate $(SPECIES_MAP) 1 <<<"$(SPECIES)")
BASE_URL = http://fantom.gsc.riken.jp/4/download/Tables
LEVEL123_CLUSTER_DIR_URL = $(BASE_URL)/$(FANTOM_SPECIES)/CAGE/promoters/level_123_clusters
TAG_CLUSTER_DIR_URL = $(BASE_URL)/$(FANTOM_SPECIES)/CAGE/promoters/tag_clusters
UCSC_VERSION ?= hg18
UCSC_DIR = $(BIOINFO_ROOT)/task/annotations/dataset/ucsc/$(SPECIES)/$(UCSC_VERSION)
CLOSER_KNOW_GENE_MAX_DIST ?= 10000
MAX_TC_GENE_DIST ?= 1000

extern $(UCSC_DIR)/transcript-LocusLink.map

.DOC: MAX_TC_GENE_DIST
	taken from http://fantom.gsc.riken.jp/download/Supplemental_Materials/Suzuki_et_al_2009/Suzuki_et_al_Supplementary_all.pdf
	We obtained the genomic mappings of all human mRNAs from the UCSC
	BLAT alignments, discarded mRNAs whose 5’ ends don’t map, and then associated
	each promoter with all mRNAs whose mapped TSS is within 1000 base pairs of the
	CAGE promoter. Using the mapping from mRNAs to Entrez genes provided by NCBI
	we associated promoters with Entrez genes and constructed the gene locus (union of all
	mRNA mappings) of each gene.


CAGE_promoters_level1.gz CAGE_promoters_level2.gz CAGE_promoters_level3.gz: CAGE_promoters_level%.gz:
	wget -O - $(LEVEL123_CLUSTER_DIR_URL)/level$*.tbl.txt.bz2\
	| bzcat \
	| unhead -n 45 -f $@.meta \
	| bawk '{$$3=$$3-1; if($$5=="F"){$$5="+1"}else{$$5="-1"} print}' \               *the fanotm coordinates are 1 based*
	| perl -lpe 's/\tchr/\t/g' \
	| gzip > $@
	
.META: CAGE_promoters_level3.gz
	1	id
	2	chr
	3	b
	4	e
	5	strand
	6	EntrezGene_id
	7	EntrezGene_symbol
	8	EntrezGene_synonyms
	9	norm_RIKEN1_PMA_0h
	10	norm_RIKEN1_PMA_1h
	11	norm_RIKEN1_PMA_4h
	12	norm_RIKEN1_PMA_12h
	13	norm_RIKEN1_PMA_24h
	14	norm_RIKEN1_PMA_96h
	15	norm_RIKEN3_PMA_0h
	16	norm_RIKEN3_PMA_1h
	17	norm_RIKEN3_PMA_4h
	18	norm_RIKEN3_PMA_12h
	19	norm_RIKEN3_PMA_24h
	20	norm_RIKEN3_PMA_96h
	21	norm_RIKEN6_PMA_0h
	22	norm_RIKEN6_PMA_1h
	23	norm_RIKEN6_PMA_4h
	24	norm_RIKEN6_PMA_12h
	25	norm_RIKEN6_PMA_24h
	26	norm_RIKEN6_PMA_96h
	27	raw_RIKEN1_PMA_0h
	28	raw_RIKEN1_PMA_1h
	29	raw_RIKEN1_PMA_4h
	30	raw_RIKEN1_PMA_12h
	31	raw_RIKEN1_PMA_24h
	32	raw_RIKEN1_PMA_96h
	33	raw_RIKEN3_PMA_0h
	34	raw_RIKEN3_PMA_1h
	35	raw_RIKEN3_PMA_4h
	36	raw_RIKEN3_PMA_12h
	37	raw_RIKEN3_PMA_24h
	38	raw_RIKEN3_PMA_96h
	39	raw_RIKEN6_PMA_0h
	40	raw_RIKEN6_PMA_1h
	41	raw_RIKEN6_PMA_4h
	42	raw_RIKEN6_PMA_12h
	43	raw_RIKEN6_PMA_24h
	44	raw_RIKEN6_PMA_96h

CAGE_promoters_tag_clust-coords.gz:
	wget -O - $(TAG_CLUSTER_DIR_URL)/tag_cluster_coordinates.tbl.txt.bz2 \
	| bzcat \
	| unhead -n 8 -f $@.meta \
	| bawk '{if($$3=="F"){$$3="+1"}else{$$3="-1"} print}' \               *the coordinates of this file seems to be 0 based since there is a b=0 in chrM*
	| perl -lpe 's/\tchr/\t/g' \
	| bsort -k 4,4 -k 5,5n -S 10% \
	| gzip > $@

.META: CAGE_promoters_tag_clust-coords.gz
	1	id	CAGE tag cluster id
	2	name	CAGE cluster name
	3	strand
	4	chr	chromosome where the cluster is located
	5	b	start position of the cluster
	6	e	end position of the cluster
	7	rep_pos	representative position in the cluster

CAGE_tc2known_gene.gz: CAGE_promoters_tag_clust-coords.gz $(UCSC_DIR)/known_genes.whole 
	distances -n -m $(CLOSER_KNOW_GENE_MAX_DIST) \
		<(bawk '{print $$id,$$chr,$$b,$$e}' $<) \
		<(bawk '$$strand==1 {print $$id,$$chr,$$b,$$b+1} $$strand==-1 {print $$id,$$chr,$$e,$$e-1}' $^2 | bsort -k2,2 -k3,3n)\
	> $@.tmp
	bawk '{print $$id}' $< | filter_1col -v 1 <(cut -f 1 $@.tmp) \
	| append_each_row "NA	NA" >>$@.tmp2
	cat $@.tmp $@.tmp2 | gzip > $@
	rm $@.tmp $@.tmp2

.META: CAGE_tc2known_gene.gz
	1	tc_id
	2	known_gene_id		may be NA
	3	distance		may be NA

CAGE_tc2entrez.gz: CAGE_tc2known_gene.gz $(UCSC_DIR)/transcript-LocusLink.map
	zcat $< | translate -v -e NA -a <(bawk '{print $$1,$$3}' $^2) 2 \
	| gzip > $@

.META: CAGE_tc2entrez.gz
	1	tc_id
	2	known_gene_id		may be NA
	3	entrez_gene_id		may be NA
	4	distance		may be NA

CAGE_tc_unknown.gz: CAGE_tc2known_gene.gz
	bawk '$$distance=="NA" || $$distance>$(MAX_TC_GENE_DIST) {print $$tc_id}' $< \
	| gzip > $@

CAGE_tc_intergenic.gz: CAGE_promoters_tag_clust-coords.gz $(UCSC_DIR)/known_genes.whole
	intersection -l $@.tmp \
		<(bawk '{print $$id,$$chr,$$b,$$e}' $<  | bsort -k2,2 -k3,3n):2:3:4 \
		<(bawk '{print $$id,$$chr,$$b,$$e}' $^2 | bsort -k2,2 -k3,3n):2:3:4 \
	> /dev/null
	distances -n -m $(CLOSER_KNOW_GENE_MAX_DIST) \
		$@.tmp \
		<(bawk '$$strand==1 {print $$id,$$chr,$$b,$$b+1} $$strand==-1 {print $$id,$$chr,$$e,$$e-1}' $^2 | bsort -k2,2 -k3,3n)\
	| bawk '$$distance=="NA" || $$distance>$(CLOSER_KNOW_GENE_MAX_DIST) {print $$tc_id,}' $< \
	| gzip > $@

CAGE_promoters_tag_clust-expression.gz:  $(TASK_ROOT)/local/share/data/FANTOM4_$(SPECIES)_tc_sample2tissue
	wget -O - $(TAG_CLUSTER_DIR_URL)/tag_cluster_expressions.tbl.txt.bz2 \
	| bzcat \
	| grep -v '^#'\
	| unhead \
	| perl -lpe 's/\tchr/\t/g' \
	| bawk '$$3 != "NA"' \          * remove samples labelled as NA (NA has non translation in the sample to tissue map) *
	| translate -a $< 3 \
	| gzip > $@

.META: CAGE_promoters_tag_clust-expression.gz
	1	id
	2	library
	3	sample
	4	tissue
	5	raw_single
	6	norm_single
	7	raw_multi
	8	norm_multi
	9	raw_rescue
	10	norm_rescue

.DOC: CAGE_promoters_tag_clust.expression.gz
	for the normalizaion procedure see
	https://wave.google.com/wave/#restored:wave:googlewave.com!w%252B0Fbgl4rvA
	#
	description of sample (column 3)
	##ColumnVariable[norm.rescue] = normalized tag count (tpm) for rescued mappers
	#sample_id	sample_name
	see $(TASK_ROOT)/local/share/data/FANTOM4_tc_sample2tissue 

CAGE_promoters_tag_clust-expression.tissue_matrix.raw_rescue.gz CAGE_promoters_tag_clust-expression.tissue_matrix.norm_resque.gz\
CAGE_promoters_tag_clust-expression.tissue_matrix.raw_multi.gz CAGE_promoters_tag_clust-expression.tissue_matrix.norm_multi.gz\
CAGE_promoters_tag_clust-expression.tissue_matrix.raw_single.gz CAGE_promoters_tag_clust-expression.tissue_matrix.norm_single.gz: CAGE_promoters_tag_clust-expression.tissue_matrix.%.gz: CAGE_promoters_tag_clust-expression.gz
	bawk '$$sample!= "NA" {print $$id, $$tissue, $$$*}' $< \
	| bawk '{print $$1 "_" $$2, $$3}' \
	| bsort -S10% -k 1,1 \
	| stat_base -g -a \			   * average value for each tissue *
	| perl -lnpe 's/_/\t/' \
	| bawk '{if($$3==0){$$3="NA"} print}' \
	| map2incidence_weight \
	| gzip > $@

CAGE_promoters_tag_clust-expression.tissue_matrix.best.raw_rescue.gz CAGE_promoters_tag_clust-expression.tissue_matrix.best.norm_rescue.gz\
CAGE_promoters_tag_clust-expression.tissue_matrix.best.raw_multi.gz CAGE_promoters_tag_clust-expression.tissue_matrix.best.norm_multi.gz\
CAGE_promoters_tag_clust-expression.tissue_matrix.best.raw_single.gz CAGE_promoters_tag_clust-expression.tissue_matrix.best.norm_single.gz : CAGE_promoters_tag_clust-expression.tissue_matrix.best.%.gz: CAGE_promoters_tag_clust-expression.gz
	bawk '$$sample!= "NA" {print $$id, $$tissue, $$$*}' $< \
	| bawk '{print $$1 "_" $$2, $$3}' \
	| bsort -S10% -k 1,1 \
	| stat_base -g -b \			   * best value for each tissue *
	| perl -lnpe 's/_/\t/' \
	| bawk '{if($$3==0){$$3="NA"} print}' \
	| map2incidence_weight \
	| gzip > $@

CAGE_promoters_tag_clust-expression.sample_matrix.gz: CAGE_promoters_tag_clust-expression.gz
	bawk '$$sample!= "NA" {print $$id, $$sample, $$norm_rescue}' $<\          * even if in the sample to tissue map there is the sample -1 Undefined, in the mouse file there are samples labelled ad NA and NA has non translation in the sample to tissue map...*
	| bawk '{print $$1 "_" $$2, $$3}' \
	| bsort -S10% -k 1,1 \
	| stat_base -g -a \			   * average value for each sample *
	| perl -lnpe 's/_/\t/' \
	| map2incidence_weight \
	| gzip > $@
