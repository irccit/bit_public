# Copyright 2008-2010 Gabriele Sales <gbrsales@gmail.com>
# Copyright 2009      Ivan Molineris <ivan.molineris@gmail.com>

include $(BIOINFO_ROOT)/local/share/makefile/global-rules.mk

SPECIES            ?= hsapiens
VERSION            ?= 50
MART_VERSION       ?= www
UPSTREAM_EXTENSION ?= 10000

SPECIES_MAP     := $(BIOINFO_ROOT)/task/annotations/local/share/species-ensembl.map
ENSEMBL_SPECIES := "$$(translate $(SPECIES_MAP) 1 <<<"$(SPECIES)")"
MYSQL_CMD       := mysql --user=anonymous --host=ensembldb.ensembl.org --port=5306 -BCAN
MYSQL_CMD_MART  := mysql --user=anonymous --host=martdb.ensembl.org -P 5316 -BCAN ensembl_mart_$(VERSION)
SEQUENCE_DIR    ?= $(BIOINFO_ROOT)/task/sequences/dataset/ensembl/$(SPECIES)/$(VERSION)
MARTSERVICE_URL ?= http://$(MART_VERSION).ensembl.org/biomart/martservice


import gene-structures
extern $(SEQUENCE_DIR)/all_chr.len as ALL_CHR_LEN


ALL   += genes.gz gene-xref.map.gz \
         transcripts.gz transcript-gene.map.gz transcript-xref.map.gz \
         exons.gz exon-transcript.map.gz \
	 exon_transctip_gene_full.gz 
CLEAN += gene-readable.map.gz \
         overlapping_exons-alternative_transcripts.info \
         introns.gz nr_introns.gz first_intron_of_transcripts.gz


define xref
	$(MYSQL_CMD) $$(cat $<) <<<" \
		SELECT stable_id, db_name, x.dbprimary_acc, x.display_label, x.description \
		FROM object_xref AS ox \
			LEFT JOIN $(1)_stable_id AS gs ON ox.ensembl_id = gs.$(1)_id \
			LEFT JOIN xref AS x ON ox.xref_id = x.xref_id \
			LEFT JOIN external_db AS ed ON x.external_db_id = ed.external_db_id \
			WHERE ensembl_object_type='$(1)' AND stable_id is not NULL"\
	| sort -S40% \
	| gzip >$@
endef

INTERMEDIATE += database.name
database.name:
	set -e; \
	pattern="$(ENSEMBL_SPECIES)_core_$(VERSION)_%"; \
	$(MYSQL_CMD) <<<"SHOW DATABASES LIKE '$$pattern'" \
	| at_least_rows -e 1 >$@

genes.gz: database.name
	$(MYSQL_CMD) $$(cat $<) <<<" \
		SELECT stable_id, name, seq_region_start-1, seq_region_end, seq_region_strand, biotype, display_label, g.description \
		FROM gene AS g \
			LEFT JOIN gene_stable_id AS gs ON g.gene_id = gs.gene_id \
			LEFT JOIN seq_region AS sr ON g.seq_region_id = sr.seq_region_id \
			LEFT JOIN xref AS xr ON g.display_xref_id = xr.xref_id" \
	| sort -S40% \
	| gzip >$@

.META: genes.gz
	1  gene_ID      ENSG00000000003
	2  chr          X
	3  start        99770450
	4  stop         99778450
	5  strand       -1
	6  biotype      protein_coding
	7  label        TSPAN6
	8  description  Tetraspanin-6 (Tspan-6)(Transmembrane 4 superfamily member 6)(T245 protein)(Tetraspanin TM4-D)(A15 homolog) [Source:UniProtKB/Swiss-Prot;Acc:O43657]

.DOC: transcript.gz genes.gz
	for detail about biotype see http://vega.sanger.ac.uk/info/about/gene_and_transcript_types.html

gene-xref.map.gz: database.name
	$(call xref,gene)

.META: gene-xref.map.gz
	1	gene_ID  ENSG00000000003
	2	database_ID    EMBL
	3	external_ID    AC100821
	4	external_NAME	PINCO
	5	external_DESC	pingo gene involved in pallino proces

transcript-xref.map.gz: database.name
	$(call xref,transcript)


.META: transcript-xref.map.gz
	1  transcript_ID  ENST00000000233
	2  database_ID    AFFY_HG_Focus
	3  external_ID    201526_at
	4	external_NAME	PINCO
	5 	external_DESC	pingo gene involved in pallino proces

translation-xref.map.gz: database.name
	$(call xref,translation)

.META: translation-xref.map.gz
	1	translation_id  ENSG00000000003
	2	database_id    EMBL
	3	external_id    AC100821
	4	external_NAME	PINCO
	5 	external_DESC	pingo gene involved in pallino proces

gene-readable.map.gz: 
	sed 's/__SPECIES__/$(SPECIES)/g' < $(TASK_ROOT)/local/share/queries/mart_readable.xml > $@_query.xml
	wget -q -O - --post-file $@_query.xml $(MARTSERVICE_URL) \
	| gzip > $@
	rm $@_query.xml

.META: gene-readable.map.gz
	1  gene_id      ENSG00000208234
	2  description  cippo lippo
	3  gene_name    AC019043.8
	4  biotype      scRNA_pseudogene
	5  transcript_gencode_basic
	6  status       NOVEL

INTERMEDIATE += gene-omim.query
gene-omim.query: $(TASK_ROOT)/local/share/queries/mart_omim.xml
	sed 's/__SPECIES__/$(SPECIES)/g' <$< >$@

gene-omim.map.gz: gene-omim.query
	wget -q -O - --post-file $< $(MARTSERVICE_URL) \
	| bawk '$$2' \    *select only genes whit a omimi disease associated *
	| gzip >$@

gene-homologous.map.gz:
	(\
	for i in $$( $(MYSQL_CMD_MART) <<< "SHOW TABLES LIKE '$(SPECIES)_gene_ensembl__homolog_%'" | unhead -n 2 ); do \
		species=` echo $$i | sed 's/$(SPECIES)_gene_ensembl__homolog_//' | sed 's/__dm$$//'`; \
		$(MYSQL_CMD_MART) <<< 	"SELECT	\
		stable_id_4016_r3, stable_id_4016, stable_id_4016_r1, stable_id_4016_r2, description_4014 stable_id_4016_r1 FROM $$i WHERE stable_id_4016_r3 is not NULL or stable_id_4016 is not NULL or stable_id_4016_r2 is not NULL ;" \
		| append_each_row $$species; \
	done; \
	) | gzip > $@

gene-homologous.one2one.to_clean: gene-homologous.map.gz
	zcat $<\
	| bawk '$$5=="ortholog_one2one" {print $$4}'\
	| symbol_count | bawk '$$2>1' > $@

gene-homologous.one2one_clean.map.gz: gene-homologous.map.gz gene-homologous.one2one.to_clean
	zcat $< \
	| bawk '$$5=="ortholog_one2one" {print $$2,$$4,$$6}'\
	| filter_1col -v 2 <(cut -f 1 $^2) \
	| gzip > $@


transcripts.gz: transcript.info
	cut -f1,3- $< \
	| gzip >$@

.META: transcripts.gz
	1  transcript_ID  ENSMUST00000000001
	2  chr            3
	3  start          107910199
	4  stop           107949007
	5  strand         -1
	6  biotype        protein_coding
	7  status         KNOWN

transcript-gene.map.gz: database.name 
	$(MYSQL_CMD) $$(cat $<) <<<" \
		SELECT ts.stable_id, gs.stable_id \
		FROM transcript AS t \
			LEFT JOIN transcript_stable_id AS ts ON t.transcript_id = ts.transcript_id \
			LEFT JOIN gene_stable_id AS gs ON t.gene_id = gs.gene_id \
		WHERE is_current = 1" \
	| sort | uniq \
	| gzip >$@

.META: transcript-gene.map.gz
	1  transcript_ID  ENSMUST00000000001
	2  gene_ID        ENSMUSG00000000001

translation-transcript.map.gz: database.name 
	$(MYSQL_CMD) $$(cat $<) <<<" \
		SELECT ps.stable_id, ts.stable_id \
		FROM translation AS p \
			LEFT JOIN translation_stable_id AS ps ON p.translation_id = ps.translation_id \
			LEFT JOIN transcript_stable_id AS ts ON p.transcript_id = ts.transcript_id \
		WHERE ps.stable_id is not NULL AND ts.stable_id is not NULL" \
	| sort | uniq \
	| gzip >$@

translation-gene.map.gz: translation-transcript.map.gz transcript-gene.map.gz
	zcat $< | translate <(zcat $^2) 2 \
	| sort | uniq \
	| gzip >$@

gene-refseq.map.gz: transcript-xref.map.gz translation-xref.map.gz transcript-gene.map.gz translation-gene.map.gz 
	(\
		bawk '$$2~/RefSeq/' $< | translate <(zcat $^3) 1;\
		bawk '$$2~/RefSeq/' $^2 | translate <(zcat $^4) 1;\
	) \
	| gzip > $@


INTERMEDIATE += transcript.info
transcript.info: database.name
	$(MYSQL_CMD) $$(cat $<) <<<" \
		SELECT ts.stable_id, gs.stable_id, sr.name, seq_region_start-1, seq_region_end, seq_region_strand, biotype, status \
		FROM transcript AS t \
			LEFT JOIN transcript_stable_id AS ts ON t.transcript_id = ts.transcript_id \
			LEFT JOIN gene_stable_id AS gs ON t.gene_id = gs.gene_id \
			LEFT JOIN seq_region AS sr ON t.seq_region_id = sr.seq_region_id \
		WHERE is_current = 1 AND \
		      sr.name NOT LIKE '%\_%'" \            * do not consider transcripts on non standard chromosomes *
	| sort -S40% -k1,1 -k2,2 >$@


gene-entrez.map.gz: translation-xref.map.gz translation-transcript.map.gz transcript-gene.map.gz
	bawk '$$database_id == "EntrezGene" {print $$translation_id, $$external_id}' $< \
	| translate -a -d -j <(zcat $^2) 1 \
	| translate -a -d -j <(zcat $^3) 2 \
	| cut -f 3- \
	| gzip > $@

exons.gz: database.name
	$(MYSQL_CMD) $$(cat $<) <<<" \
		SELECT es.stable_id, sr.name, seq_region_start-1, seq_region_end, seq_region_strand, phase, end_phase \
		FROM exon AS e \
			LEFT JOIN exon_stable_id AS es ON e.exon_id = es.exon_id \
			LEFT JOIN seq_region AS sr ON e.seq_region_id = sr.seq_region_id \
		WHERE is_current = 1 AND \
		      sr.name NOT LIKE '%\_%'" \
	| sort -S40% -k1,1 \
	| gzip >$@

.META: exons.gz
	1  exon_ID    ENSMUSE00000097910
	2  chr        10
	3  start      6794511
	4  stop       6794667
	5  strand     -1
	6  phase      2
	7  end_phase  0

introns.gz: exons.gz exon-transcript.map.gz 
	zcat $<\
	| translate -a -d <(zcat $(word 2,$^)) 1 \
	| expandsets -s ',' 2 \
	| bawk '{print $$3,$$4,$$5,$$2}' \
	| introns_coords \
	| sort -k 1,1 -k 2,2n \
	| gzip > $@ 

.META: introns.gz
	1  chr           1
	2  start         141594868
	3  stop          141598375
	4  transcrit_ID  ENSMUST00000111986

first_intron_of_transcripts.gz: introns.gz transcripts.gz
	zcat $<\
	| translate -a <(bawk '{print $$transcript_ID, $$strand}' $^2) 4 \
	| bawk '{print $$0,$$2*$$5}' \                                          * the added column is used in the following find_best -r *
	| find_best -r 4 6 \
	| cut -f -5 \                                                           * remove the added column                                *
	| gzip > $@

.META: first_intron_of_transcripts.gz
	1 chr
	2 b
	3 e
	4 transcrit_ID
	5 strand

nr_introns.gz: exons.gz transcripts.gz
	zcat exons.gz | cut -f 2,3,4 | sort -k 1,1 -k 2,2n | union \            * union fo all exons                                    *
	| bawk '{print $$0,$$1}' | introns_coords \	                        * intergenic region included                            *
	| sort -k 1,1 -k 2,2n | cut -f -3 \
	| intersection - <(zcat $(word 2,$^) | sort -k 2,2 -k 3,3n):2:3:4 \     * associate transcript id and remove intergenic regions *
	| cut -f 1-3,8 \
	| sort -k1,1 -k2,2n \
	| collapsesets 4 \				                        * collapse on transcript id                             *
	| gzip > $@

.META: nr_introns.gz
	1  chr           1
	2  start         141594868
	3  stop          141598375
	4  transcrit_ID  ENSMUST00000111986

exon-transcript.map.gz: database.name
	$(MYSQL_CMD) $$(cat $<) <<<" \
		SELECT es.stable_id, ts.stable_id \
		FROM exon_transcript AS et \
			LEFT JOIN exon_stable_id AS es ON et.exon_id = es.exon_id \
			LEFT JOIN transcript_stable_id AS ts ON et.transcript_id = ts.transcript_id" \
	| sort -S40% -k1,1 -k2,2 \
	| gzip >$@

.META: exon-transcript.map.gz
	1  exon_ID        ENSMUSE00000097910
	2  transcript_ID  ENSMUST00000019896

.PHONY: check.overlapping_exons-same_transcript
check.overlapping_exons-same_transcript: check.overlapping_exons-same_transcript.in
	if [[ $$(intersection $<:3:4:5 $<:3:4:5 \
	         | bawk '$$8<$$10 && $$9==$$11' \
	         | wc -l) != 0 ]]; then \
		echo "*** Found overlapping exons in the same transcript." >&2; \
		exit 1; \
	fi

INTERMEDIATE += overlapping_exons-same_transcript.in
check.overlapping_exons-same_transcript.in: exons.gz exon-transcript.map.gz
	zcat $< \
	| cut -f-4 \
	| translate -a -d -g ';' <(zcat $(word 2,$^)) 1 \
	| expandsets 2 \
	| sort -S40% -k3,3 -k4,4n -k5,5n >$@

INTERMEDIATE += overlapping_exons-alternative_transcripts
overlapping_exons-alternative_transcripts: overlapping_exons-alternative_transcripts.in
	intersection $<:4:5:6 $<:4:5:6 \
	| bawk '$$8<$$11 && $$9!=$$12 && $$10==$$13' >$@

INTERMEDIATE += overlapping_exons-alternative_transcripts.in
overlapping_exons-alternative_transcripts.in: exons.gz exon-transcript.map.gz transcript-gene.map.gz
	zcat $< \
	| cut -f-4 \
	| translate -a -d -g ';' <(zcat $(word 2,$^)) 1 \
	| expandsets 2 \
	| translate -a -d -g ';' <(zcat $(word 3,$^)) 2 \
	| sort -S40% -k4,4 -k5,5n -k6,6n >$@

.PHONY: check.overlapping_exons-alternative_transcripts.info
overlapping_exons-alternative_transcripts.info: overlapping_exons-alternative_transcripts
	set -e; \
	overlap_num=$$(wc -l <$<); \
	gene_num=$$(cut -f 10 $< | sort -S40% | uniq | wc -l); \
	echo "Found $$overlap_num overlapping pairs of exons from alternative transcripts, belonging to $$gene_num different genes." >$@

overlapping_genes.gz: exons.gz exon-transcript.map.gz transcript-gene.map.gz
	bawk '{print $$chr,$$start,$$stop,$$exon_ID}' $< \
	| bsort -k1,1 -k2,2n -S80% | union | cut -f 4 \
	| translate -w -d <(zcat $^2) \
	| translate -w -d <(zcat $^3) \
	| enumerate_rows | expandsets 2 | sort | uniq > $@.tmp1
	cut -f 1 $@.tmp1 | symbol_count >$@.tmp2
	translate -a $@.tmp2 1 < $@.tmp1 \
	| gzip >$@
	rm $@.tmp*

.META: overlapping_genes.gz
	1	exon_cluster_id
	2	gene_count_in_cluster
	3	ensg

.DOC: overlapping_genes.gz
	if a exon_cluster exist with count > 1 => there are more than 1 ENSG with exon in the same region, those ensg are reported in the col 3


transcript_structures.gz:
	with_ensembl_api $(VERSION) fetch-ensembl-transcript_structures $(ENSEMBL_SPECIES) \
	| gzip >$@

.META: transcript_structures.gz
	FASTA
	> transcript_ID chromosome coding_start coding_stop strand
	exon_ID exon_start exon_stop

upstreams.gz: transcripts.gz $(ALL_CHR_LEN)
	zcat $< \
	| upstreams $^2 $(UPSTREAM_EXTENSION) \
	| gzip >$@

nr_upstreams.gz: upstreams.gz genes.gz
	intersection -l $@.missing \
		<(zcat $< \
			| bawk '$$3 != 0' \					* in the MT there is this case *
			| sort -k 2,2 -k 3,3n\
		):2:3:4 \
		<(zcat $^2 | sort -k 2,2 -k 3,3n):2:3:4 \
	| cut -f -9 \
	| bawk '{if($$9==1){print $$1,$$3,$$5,$$8,$$9 }else{ print $$1,$$4,$$2,$$8,$$9}}' \
	| bawk '{print $$0,$$3-$$2}' \
	| find_best -r 4 6 \    * the shortest *
	| cut -f -5 > $@.tmp
	cat $@.missing $@.tmp \
	| bawk '{print $$4,$$1,$$2,$$3,$$5}'| gzip > $@
	rm $@.missing $@.tmp


cdnas.gz:
	with_ensembl_api $(VERSION) fetch-ensembl-cdnas $(ENSEMBL_SPECIES) \
	| sort -k1,1 -k2,2n -k3,3n -k4,4n -S40% \
	| gzip >$@

.META: cdnas.gz
	1  ID            ENSMUST00000000001
	2  length        3204
	3  coding_start  84
	4  coding_stop   1149
	5  5utr_start    0
	6  3utr_stop     3204

cdnas_ncrna.len.gz: $(SEQUENCE_DIR)/cdna.fa.gz $(SEQUENCE_DIR)/ncrna.fa.gz 
	zcat $^ | fastalen | perl -pe 's/\s.*gene:(ENS[^\t]+)/\t$$1/' \
	| gzip > $@

exons_transcript_relative_coords.gz: exons.gz exon-transcript.map.gz 
	zcat $<\
	| translate -a -d -j <(zcat $^2) 1 \
	| bawk '{print $$2,$$1,$$3,$$4,$$5, $$5-$$4, $$6*$$4}' \    *aggiungo una coordinata strand*b per ordinare correttamente*
	| bsort -k1,1 -k 7,7n -S30% | cut -f -6 \        *sorto tendo conto dello strand e poi elimino la coordinata su cui sorto*
	| tab2fasta \
	| repeat_fasta_pl '$$\="\n"; $$,="\t"; my $$c=0; print $$header; for(@table){print @{$$_},$$c,$$c+$${$$_}[4]; $$c+=$${$$_}[4]}' \
	| fasta2tab \
	| gzip > $@

.META: exons_transcript_relative_coords.gz
	1	enst	ENST00000000233
	2	ense	ENSE00000720374
	3	chr	7
	4	chr_b	127229136
	5	chr_e	127229217
	6	len	81
	7	relative_b	0
	8	relative_e	81

exon_transctip_gene_full.gz: exons.gz exon-transcript.map.gz transcript-gene.map.gz genes.gz
	zcat $< | translate -d -j -a <(\
		zcat $^2 | translate -d -j -a <(\
			zcat $^3\
		) 2 \
	) 1 \
	| translate -a <(zcat $^4) 3 \
	| cut -f 1-3,5,6,8- \
	| gzip > $@

.META: exon_transctip_gene_full.gz
	1	ENSE	ENSE00000327880
	2	ENST	ENST00000010299
	3	ENSG	ENSG00000009780
	4	ENSG_b	28052489
	5	ENSG_e	28088475
	6	biotype	protein_coding
	7	GeneName	FAM76A
	8	GeneDescr	family with sequence similarity 76, member A [Source:HGNC Symbol;Acc:28530]
	9	chr	1
	10	ENSE_b	28059113
	11	ENSE_e	28059168
	12	strand	1
	13	phase	2
	14	end_phase	0

