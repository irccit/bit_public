ENS_VERSION ?= 42
BIN_DIR ?= $(BIOINFO_ROOT)/task/annotations/local/bin



.DELETE_ON_ERROR:

ensg_ensf:
	echo "SELECT DISTINCT s.stable_id, f.stable_id FROM family as f JOIN family_member as g ON(f.family_id = g.family_id) JOIN homo_sapiens_core_$(ENS_VERSION)_36d.gene_stable_id as s ON(s.gene_id = g.member_id)" \
	| mysql -BCAN -u anonymous -h ensembldb.ensembl.org ensembl_compara_$(ENS_VERSION) \
	| sort -k1,1 > $@

ensfamily_desc:
	echo "SELECT DISTINCT stable_id,description FROM family;" \
	| mysql -BCAN -u anonymous -h ensembldb.ensembl.org ensembl_compara_$(ENS_VERSION) \
	| sort -k1,1 > $@

%.family_ensfamily: % ensg_ensf
	cat $< | sort -k1,1 \
	| $(BIN_DIR)/join_annotation.pl -a $(word 2,$^) -tmp $@.tmp $(DISPLAY_GENES) \
	> $@ 

%.family_ensfamily.desc: %.family_ensfamily ensfamily_desc
	cut -f -4 $< | sort -k2,2 \
		| join3_pl - -i -u -1 2 -2 1 -- $(word 2,$^) \
		| sort_pl 3 \
	> $@

