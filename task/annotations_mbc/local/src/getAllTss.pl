#!/usr/bin/perl -w

use strict;
use Bio::EnsEMBL::Registry;
use Bio::EnsEMBL::Utils::Exception qw(throw);
use Getopt::Long;

my $usage = "
$0
  This tool downloads compara files from ensembl and convert it in axt format. 
  [--help]                      show this menu
   --version|-v           	the ensembl release to use (eg. 57)
   --chr|-c            		(e.g. chr22)
   --query|-q                   (e.g. human) the query species from which alignments are queried
\n";


my $help = 0;
my ($version, $chr, $qy_species, $tg_species);
GetOptions('help|h' => \$help,
	   'version|v=s' => \$version,
	   'chr|c=s' => \$chr,
	   'query|q=s' => \$qy_species,
);

if ($help) {
  print STDERR $usage;
  exit 0;
}

if (not defined ($version) or not defined ($chr) or not defined($qy_species)) {
	print STDERR $usage;
	exit 1;
}


my $host   = 'ensembldb.ensembl.org';
my $user   = 'anonymous';
my $port   = 5306;

#ensembl wants just the chr number
if ($chr =~ /^chr(\S+)$/) {
	$chr = $1;
} else {
	print "Wrong chr given\n";
	exit 1;
}


my $registry = 'Bio::EnsEMBL::Registry';

$registry->load_registry_from_db(
    -host => $host, -user => $user, -port => $port, -db_version => $version);

my $slice_adaptor = $registry->get_adaptor($qy_species, 'Core', 'Slice');
my $slice = $slice_adaptor->fetch_by_region( 'chromosome', $chr);

my $genes = $slice->get_all_Genes();
while ( my $gene = shift @{$genes} ) {
    my $transcripts = $gene->get_all_Transcripts();
    while ( my $transcript = shift @{$transcripts} ) {
       my $t_name = $transcript->stable_id();    
       my $t_start = $transcript->seq_region_start();    
       my $t_strand = $transcript->seq_region_strand();   
       $t_strand = ($t_strand == 1)? '+' : '-';
#       my $t_gene = $transcript->get_Gene();
       #my $hasGene = 1;
       #if (!$t_gene) {
#		$t_gene = $transcript->get_nearest_Gene();
#		$hasGene = 0;
#       }
#       my $t_gene_name = $t_gene->stable_id();
#       print "$t_name\t$chr\t$t_start\t$t_strand\t$hasGene\t$t_gene_name\n";
       print "$t_name\t$chr\t$t_start\t$t_strand\n";
    }
}
