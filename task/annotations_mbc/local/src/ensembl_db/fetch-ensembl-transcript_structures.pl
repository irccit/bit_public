#!/usr/bin/env perl
#
# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

use warnings;
use strict;
$,="\t";
$\="\n";

my $species = shift @ARGV;

use Bio::EnsEMBL::Registry;

my $registry = 'Bio::EnsEMBL::Registry';
$registry->load_registry_from_db(
	-host => 'ensembldb.ensembl.org',
	-user => 'anonymous'
);

my $slice_adaptor = $registry->get_adaptor("$species", 'Core', 'Slice');
my $transcript_adaptor  = $registry->get_adaptor("$species", 'Core', 'Transcript');

foreach my $chromosome ( @{$slice_adaptor->fetch_all('chromosome')} ) {
	foreach my $transcript ( @{$chromosome->get_all_Transcripts(1)} ) {
		next unless $transcript->is_current;
		
		my $stable_id = $transcript->stable_id;
		my $start = $transcript->coding_region_start;
		if (defined($start)) {
			$start -= 1;
		} else {
			$start = "";
		}
		my $end = $transcript->coding_region_end;
		$end = "" unless defined($end);
		
		my @exons = @{$transcript->get_all_Exons()};
		my $strand;
		if ($exons[0]->strand == 1) {
			$strand = '+';
		} elsif ($exons[0]->strand == -1) {
			$strand = '-';
		} else {
			die "unknown strand"
		}
		
		print ">$stable_id", $chromosome->seq_region_name, $start, $end, $strand;
		
		foreach my $exon (@exons) {
			my $exon_id = $exon->stable_id;
			my $exon_start = $exon->start-1;
			my $exon_end = $exon->end;
			print $exon_id, $exon_start, $exon_end;
		}
	}
}
