#ifndef SEED_H
#define SEED_H

class Sequence;

class SeedIterator
{
public:
	SeedIterator(const Sequence& sequence, const size_t seed_size);
	
	bool next();
	void skip(size_t offset);
	const char* seed() const;
	size_t offset() const;

private:
	const Sequence& sequence_;
	const size_t seed_size_;
	size_t offset_;
	size_t search_pos_;
};

#endif //SEED_H
