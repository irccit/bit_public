#ifndef SEED_H
#define SEED_H

class Sequence;

class SeedIterator
{
public:
	SeedIterator(const Sequence& sequence, const size_t seed_size, const size_t stride);
	
	bool next();
	void seek(const size_t offset);
	
	const char* seed() const;
	size_t seed_offset() const;
	size_t seed_size() const;
	size_t stride() const;

private:
	const Sequence& sequence_;
	const size_t seed_size_;
	const size_t stride_;
	size_t offset_;
	size_t search_pos_;
};

#endif //SEED_H
