#include <iostream>
#include <vector>
#include "alignment.h"
#include "io.h"
#include "mask.h"
#include "seed.h"

using namespace std;

ExtendableAlignment* find_best_alignment(vector<ExtendableAlignment*> extensions)
{
	vector<ExtendableAlignment*>::const_iterator it = extensions.begin(), end = extensions.end();
	ExtendableAlignment* best_alignment;
	size_t best_length = 0;
	
	for (; it != end; ++it)
	{
		size_t length = (*it)->target_stop - (*it)->target_start;
		if (length > best_length)
		{
			best_length = length;
			best_alignment = *it;
		}
	}
	
	return best_alignment;
}

void update_target_mask(SequenceMask& mask, const ExtendableAlignment& alignment)
{
	Region r = { alignment.target_start, alignment.target_stop };
	mask.add(r);
}

void free_extensions(vector<ExtendableAlignment*> extensions)
{
	vector<ExtendableAlignment*>::const_iterator it = extensions.begin(), end = extensions.end();
	for (; it != end; ++it)
		delete *it;
}

int main(int argc, char* argv[])
{
	if (argc != 3)
	{
		cerr << "Unexpected argument number." << endl;
		exit(1);
	}
	
	Sequence query(argv[1]);
	SeedIterator seed_iterator(query, 100, 50);
	Sequence target(argv[2]);
	SequenceMask target_mask(target, 50);
	size_t last_stop_on_query = 0;
	
	cout << query.length() << "\t" << target.length() << endl;
	
	while (seed_iterator.next())
	{
		SeedAlignmentIterator alignment_iterator(query, seed_iterator.seed_offset(), seed_iterator.seed_size(), target, target_mask);
		vector<ExtendableAlignment*> extensions;
		
		while (alignment_iterator.next())
		{
			ExtendableAlignment* alignment = new ExtendableAlignment(alignment_iterator.alignment());
			alignment->set_query_lower_bound(last_stop_on_query);
			alignment->set_query_upper_bound(query.length());
			alignment->set_target_lower_bound(alignment_iterator.unmasked_target_region().start);
			alignment->set_target_upper_bound(alignment_iterator.unmasked_target_region().stop);
			
			alignment->extend();
			extensions.push_back(alignment);
		}
		
		ExtendableAlignment* best_alignment;
		switch (extensions.size())
		{
		case 0:
			continue;
		
		case 1:
			best_alignment = extensions[0];
			break;
		
		default:
			best_alignment = find_best_alignment(extensions);
			break;
		}
		
		cout << best_alignment->query_start << "\t" << best_alignment->query_stop << "\t" \
		     << best_alignment->target_start << "\t" << best_alignment->target_stop << endl;
		
		last_stop_on_query = best_alignment->query_stop;
		update_target_mask(target_mask, *best_alignment);
		seed_iterator.seek(last_stop_on_query + seed_iterator.stride());
		free_extensions(extensions);
	}
	
	return 0;
}
