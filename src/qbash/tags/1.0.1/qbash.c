#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <drmaa.h>

static const char* cmd;
static char cwd[4096];
static pid_t pid;
static char hostname[4096];
static int interrupted = 0;
static char* job_id = NULL;
static const char* output_log_spec = NULL;
static const char* error_log_spec = NULL;
static char diagnosis[DRMAA_ERROR_STRING_BUFFER];

static void print_help(const char* name)
{
	fprintf(stderr, "Usage: qbash -c QUOTED_COMMAND\n");
	exit(1);
}

static void parse_args(const int argc, char** argv)
{
	int i;
	for (i = 1; i < argc; ++i)
	{
		if (strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0)
			print_help(argv[0]);
	}

	if (argc != 3)
	{
		fprintf(stderr, "[qbash] Unexpected argument number.\n");
		exit(1);
	}
	else if (strcmp(argv[1], "-c"))
	{
		fprintf(stderr, "[qbash] Expected '-c' as first argument.\n");
		exit(1);
	}

	cmd = argv[2];
}

static void check_cwd(const char* cwd)
{
	const char* root = getenv("BIOINFO_ROOT");
	if (root == NULL)
	{
		fprintf(stderr, "[qbash] BIOINFO_ROOT is not defined.\n");
		exit(1);
	}

	int root_len = strlen(root);
	if (root_len == 0)
	{
		fprintf(stderr, "[qbash] BIOINFO_ROOT is empty.\n");
		exit(1);
	}

	if (root[root_len-1] != '/')
	{
		char* tmp = malloc(sizeof(char)*(root_len+2));
		if (!tmp)
		{
			fprintf(stderr, "[qbash] malloc() failed.\n");
			exit(1);
		}

		sprintf(tmp, "%s/", root);
		root = tmp;
		++root_len;
	}

	if (strncmp(root, cwd, root_len))
	{
		fprintf(stderr, "[qbash] Outside BIOINFO_ROOT.\n");
		exit(1);
	}
}

static void cleanup()
{
	if (job_id)
		drmaa_control(job_id, DRMAA_CONTROL_TERMINATE, NULL, 0);

	if (output_log_spec)
		unlink(output_log_spec+1);
	if (error_log_spec)
		unlink(error_log_spec+1);

	if (drmaa_exit(diagnosis, sizeof(diagnosis)) != DRMAA_ERRNO_SUCCESS)
	{
		fprintf(stderr, "[qbash] drmaa_exit() failed: %s\n", diagnosis);
		exit(1);
	}
}

static void signal_handler(int signal)
{
	fprintf(stderr, "[qbash] signal received, exiting...\n");
	interrupted = 1;
}

static char* create_log_spec(const char* suffix)
{
	char* spec = malloc(sizeof(char)*4096);
	if (!spec)
	{
		fprintf(stderr, "[qbash] malloc() failed.\n");
		exit(1);
	}
	else if (snprintf(spec, 4096, ":%s/%s-%ld%s", cwd, hostname, pid, suffix) >= 4096)
	{
		fprintf(stderr, "[qbash] sprintf() failed.\n");
		exit(1);
	}

	return spec;
}
	

static drmaa_job_template_t* create_job_template()
{
	drmaa_job_template_t* jt;
	if (drmaa_allocate_job_template(&jt, diagnosis, sizeof(diagnosis)) != DRMAA_ERRNO_SUCCESS)
	{
		fprintf(stderr, "[qbash] drmaa_allocate_job_template() failed: %s\n", diagnosis);
		exit(1);
	}

	drmaa_set_attribute(jt, DRMAA_WD, cwd, NULL, 0);
	drmaa_set_attribute(jt, DRMAA_REMOTE_COMMAND, "bash", NULL, 0);

	const char* argv[3];
	argv[0] = "-c";
	argv[1] = cmd;
	argv[2] = NULL;
	drmaa_set_vector_attribute(jt, DRMAA_V_ARGV, argv, NULL, 0);

	drmaa_set_attribute(jt, DRMAA_JOIN_FILES, "n", NULL, 0);
	output_log_spec = create_log_spec(".out");
	drmaa_set_attribute(jt, DRMAA_OUTPUT_PATH, output_log_spec, NULL, 0);
	error_log_spec = create_log_spec(".err");
	drmaa_set_attribute(jt, DRMAA_ERROR_PATH, error_log_spec, NULL, 0);

	const char* env[2];
	env[0] = "BASH_ENV=$HOME/.bash_profile";
	env[1] = NULL;
	drmaa_set_vector_attribute(jt, DRMAA_V_ENV, env, NULL, 0);

	return jt;
}

static int open_log(const char* filename)
{
	int log = open(filename, O_RDONLY | O_NONBLOCK);
	if (log == -1 && errno != ENOENT)
	{
		fprintf(stderr, "[qbash] open() failed.\n");
		return 1;
	}
	else
		return log;
}

static void relay(int in, int out)
{
	char buf[4096];

	while (1)
	{
		int count = read(in, buf, 4096);
		if (count == 0)
			break;
		else if (count == -1)
		{
			if (errno == EAGAIN)
				break;
			else
			{
				fprintf(stderr, "[qbash] read() failed.\n");
				exit(1);
			}
		}

		do
		{
			int written = write(out, buf, count);
			if (written == -1)
			{
				fprintf(stderr, "[qbash] write() failed.\n");
				exit(1);
			}
			else
				count -= written;
		} while (count > 0);
	}
}

int main(int argc, char** argv)
{
	parse_args(argc, argv);

	if (getcwd(cwd, 4096) == NULL)
	{
		fprintf(stderr, "[qbash] getcwd() failed.\n");
		return 1;
	}
	check_cwd(cwd);

	pid = getpid();
	if (gethostname(hostname, sizeof(hostname)))
	{
		fprintf(stderr, "[qbash] gethostname() failed.\n");
		return 1;
	}

	signal(SIGINT, signal_handler);
	signal(SIGTERM, signal_handler);
	atexit(cleanup);

	if (drmaa_init(NULL, diagnosis, sizeof(diagnosis)) != DRMAA_ERRNO_SUCCESS)
	{
		fprintf(stderr, "[qbash] drmaa_init() failed: %s\n", diagnosis);
		return 1;
	}

	drmaa_job_template_t* jt = create_job_template();

	job_id = malloc(sizeof(char)*4096);
	if (!job_id)
	{
		fprintf(stderr, "[qbash] malloc() failed.\n");
		return 1;
	}
	else if (drmaa_run_job(job_id, 4096, jt, diagnosis, sizeof(diagnosis)) != DRMAA_ERRNO_SUCCESS)
	{
		fprintf(stderr, "[qbash] drmaa_run_job() failed: %s\n", diagnosis);
		return 1;
	}
	drmaa_delete_job_template(jt, NULL, 0);

	int output_log = -1;
	int error_log = -1;
	int stat;
	while (1)
	{
		int wait_res = drmaa_wait(job_id, NULL, 0, &stat, 1, NULL, diagnosis, sizeof(diagnosis));
		if (interrupted)
			return 1;
		else if (wait_res == DRMAA_ERRNO_SUCCESS)
			job_id = NULL;

		if (output_log == -1)
			output_log = open_log(output_log_spec+1);
		if (output_log != -1)
			relay(output_log, STDOUT_FILENO);

		if (error_log == -1)
			error_log = open_log(error_log_spec+1);
		if (error_log != -1)
			relay(error_log, STDERR_FILENO);

		if (wait_res == DRMAA_ERRNO_SUCCESS)
			break;
		else if (wait_res != DRMAA_ERRNO_EXIT_TIMEOUT)
		{
			fprintf(stderr, "[qbash] drmaa_wait() failed: %s\n", diagnosis);
			return 1;
		}
	}

	int exited;
	if (drmaa_wifexited(&exited, stat, diagnosis, sizeof(diagnosis)) != DRMAA_ERRNO_SUCCESS)
	{
		fprintf(stderr, "[qbash] drmaa_wifexited() failed: %s\n", diagnosis);
		return 1;
	}
	
	if (exited)
	{
		int exit_code;
		if (drmaa_wexitstatus(&exit_code, stat, diagnosis, sizeof(diagnosis)) != DRMAA_ERRNO_SUCCESS)
		{
			fprintf(stderr, "[qbash] drmaa_wexitstatus() failed: %s\n", diagnosis);
			return 1;
		}

		return exit_code;
	}
	else
		return 1;
}

