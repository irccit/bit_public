#include "setting.h"

extern int nodes;
extern int nedges;
extern int *edges;
extern int *mark;
int *lowm;


int updatelowm(int k){
  int m,nextm;

  m = lowm[k];
  nextm = lowm[m];
  while (m != nextm ) {
    m = nextm;
    nextm = lowm[m];
  }
  return m;
}

void listallamarkcomp(){
  int nc;  /* numero delle componenti connesse  */
  int m, k, v, w, vlowm, wlowm;
  int minlowm;
 
  lowm = (int *)calloc(nodes,sizeof(int));

  nc=0;
 
  for (m=0; m<nedges; m++){
    v = edges[2*m];
    w = edges[2*m+1];
   
    // if both nodes are unmarked
    if ( (mark[v] == 0) && (mark[w] == 0) ){
      nc++;
      mark[v] = mark[w] = nc;
      lowm[mark[v]] = lowm[mark[w]] = nc;        
    } // if only one node is just marked
    else if ( (mark[v] != 0) && (mark[w] == 0) ){
      mark[w] = updatelowm(lowm[mark[v]]);
    } // if only one node is just marked
    else if ( (mark[v] == 0) && (mark[w] != 0) ){
      mark[v] = updatelowm(lowm[mark[w]]);
    } // if both nodes are just marked
    else {
      vlowm = updatelowm(mark[v]);
      wlowm = updatelowm(mark[w]);
      if (vlowm != wlowm) {
	minlowm = min(vlowm,wlowm);
	lowm[vlowm] = lowm[wlowm] = minlowm;
	lowm[mark[w]] = lowm[mark[v]]=  minlowm;
	mark[v] = mark[w] =  minlowm;
      }
    }
    
  }
  
  /* end finding connencted components */
  
  for (k=nc; k>=1; k--) {
    lowm[k] = updatelowm(k);
  }

  for (v=0; v<nodes; v++)
    mark[v] = lowm[mark[v]];
  
  free(lowm);
  return;
}

