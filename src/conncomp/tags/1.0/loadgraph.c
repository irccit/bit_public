#include "setting.h"
#include <stdlib.h>

int *edges = NULL;
int *mapext2in = NULL;
int *mapin2ext = NULL;
int *mark = NULL;
int nedges;
int nodes;

void  listallamarkcomp();

int load_num(char *repr, char* type) {
  char *endptr;
  int res;

  int invalid_num = repr == '\0';
  if (!invalid_num) {
    res = strtol(repr, &endptr, 10);
    invalid_num = *endptr != 0 || res < 1;
  }
  if (invalid_num) {
    fprintf(stderr, "Invalid %s number.\n", type);
    exit(1);
  }

  return res;
}

int main(int argc, char** argv) {
  int n1, n2, v1, v2;
  int v, nd, m;
  int maxlabel=0;

  if (argc != 3) {
    fprintf(stderr, "Unexpected argument number.\n");
    return 1;
  }

  nodes = load_num(argv[1], "node");
  nedges = load_num(argv[2], "edge");
  
  //fprintf(stderr,"Number of total edges: %d\n", nedges);
  
  edges = malloc(nedges*2*sizeof(int));
  n1=0;
  n2=1;
  m = 0;

  while (m < nedges) {
    
    if (fscanf(stdin, "%d\t%d\n", &v1, &v2) != 2) {
      fprintf(stderr, "Invalid content at line %d:\n", m+1);
      fprintf(stderr, "The latest loaded edge was the %dth.\n", m);
      return 1;
    }
    
  
    maxlabel = max(maxlabel,v1);
    maxlabel = max(maxlabel,v2);
    
    edges[n1] = v1;
    edges[n2] = v2;
    n1+=2;
    n2+=2;
    m++;
  }
  
  if (!feof(stdin)) {
    fprintf(stderr, "Unexpected content after line %d.\n", m+1);
    return 1;
  }
  else if (m != nedges) {
    fprintf(stderr, "Premature end of input at line %d.\n", m+1);
    fprintf(stderr, "%d %d\n", m, nedges);
    return 1;
  }
  
  maxlabel += 1;
  mapext2in = malloc(maxlabel*sizeof(int));
  mapin2ext = malloc(nodes*sizeof(int));
  
  for (v=0; v<maxlabel; v++){
    mapext2in[v] = -1;
  }
  nd = 0;
  for (m=0; m<nedges*2; m++){
    if (mapext2in[edges[m]] == -1){
      mapext2in[edges[m]] = nd;
      mapin2ext[nd] = edges[m];
      nd++;
    }
    edges[m] = mapext2in[edges[m]];
  }

  if (nd != nodes) {
    fprintf(stderr, "Mismatch in node count.\n");
    fprintf(stderr, "%d %d\n", nd, nodes);
    return 1;
  }

  mark = (int *)calloc(nodes,sizeof(int));
  listallamarkcomp();

  //output
  for (v=0; v<nodes; v++){
    printf("%d\t%d\n", mapin2ext[v], mark[v]);
  }

  free(mark);
  free(mapext2in);
  free(mapin2ext);
  free(edges);
  return 0;
}
